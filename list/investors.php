<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>

    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->
     
    <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section mobile_hidden">Investors</div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>            
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                </li>
                <li>
                    <a class="link_nav buyticket trigger_app_nav" data-section="buyticket">Get your Ticket</a>
                </li>
            </ul>
        </div>
    </div>

    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-investors_list active section_1">

        <div class="content_section center">
            <div class="wrapper">
                <div class="info_text_top">
                    <h1 class="top_title" style="text-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);">Investors</h1>        
                </div>
            </div>
        </div>
    </section><!-- section -->

    <section class="section section_logo section_users section_list white active section_1 section_pt">

        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text paralax_section">
                            <h1 class="main_title">Here is a list of Investors that have already confirmed for 2016.</h1>
                            <div class="descr descr_small_margin">As founders we do not beat about the bush: A startup conference without investors is pretty much worthless. But a startup conference that has started bonding with high-quality investors long before it was even call a startup conference is priceless. Attend Bits &amp; Pretzels and start building your network on the foundation we layed as founders. The earlier, the better! 
                            </div>  
                        </div>             
                        <div class="wrap_speakers paralax_section">
                            <div class="conteiner">
                                <div class="list_users clearfix">
                                    <div class="user_box">
                                        <div class="user-offset">
                                            <div class="img_user"><img src="/src/images/user.jpg" alt=""></div>
                                            <p class="name_user">Vorname Nachname</p>
                                            <p>Job Title</p>
                                            <p>Company</p>
                                        </div>
                                    </div>
                                    <div class="user_box">
                                        <div class="user-offset">
                                            <div class="img_user"><img src="/src/images/user.jpg" alt=""></div>
                                            <p class="name_user">Vorname Nachname</p>
                                            <p>Job Title</p>
                                            <p>Company</p>
                                        </div>
                                    </div>
                                    <div class="user_box">
                                        <div class="user-offset">
                                            <div class="img_user"><img src="/src/images/user.jpg" alt=""></div>
                                            <p class="name_user">Vorname Nachname</p>
                                            <p>Job Title</p>
                                            <p>Company</p>
                                        </div>
                                    </div>
                                    <div class="user_box">
                                        <div class="user-offset">
                                            <div class="img_user"><img src="/src/images/user.jpg" alt=""></div>
                                            <p class="name_user">Vorname Nachname</p>
                                            <p>Job Title</p>
                                            <p>Company</p>
                                        </div>
                                    </div>
                                    <div class="user_box">
                                        <div class="user-offset">
                                            <div class="img_user"><img src="/src/images/user.jpg" alt=""></div>
                                            <p class="name_user">Vorname Nachname</p>
                                            <p>Job Title</p>
                                            <p>Company</p>
                                        </div>
                                    </div>
                                    <div class="user_box">
                                        <div class="user-offset">
                                            <div class="img_user"><img src="/src/images/user.jpg" alt=""></div>
                                            <p class="name_user">Vorname Nachname</p>
                                            <p>Job Title</p>
                                            <p>Company</p>
                                        </div>
                                    </div>
                                    <div class="user_box">
                                        <div class="user-offset">
                                            <div class="img_user"><img src="/src/images/user.jpg" alt=""></div>
                                            <p class="name_user">Vorname Nachname</p>
                                            <p>Job Title</p>
                                            <p>Company</p>
                                        </div>
                                    </div>
                                    <div class="user_box">
                                        <div class="user-offset">
                                            <div class="img_user"><img src="/src/images/user.jpg" alt=""></div>
                                            <p class="name_user">Vorname Nachname</p>
                                            <p>Job Title</p>
                                            <p>Company</p>
                                        </div>
                                    </div>
                                    <div class="user_box">
                                        <div class="user-offset">
                                            <div class="img_user"><img src="/src/images/user.jpg" alt=""></div>
                                            <p class="name_user">Vorname Nachname</p>
                                            <p>Job Title</p>
                                            <p>Company</p>
                                        </div>
                                    </div>
                                    <div class="user_box">
                                        <div class="user-offset">
                                            <div class="img_user"><img src="/src/images/user.jpg" alt=""></div>
                                            <p class="name_user">Vorname Nachname</p>
                                            <p>Job Title</p>
                                            <p>Company</p>
                                        </div>
                                    </div>
                                    <div class="user_box">
                                        <div class="user-offset">
                                            <div class="img_user"><img src="/src/images/user.jpg" alt=""></div>
                                            <p class="name_user">Vorname Nachname</p>
                                            <p>Job Title</p>
                                            <p>Company</p>
                                        </div>
                                    </div>
                                    <div class="user_box">
                                        <div class="user-offset">
                                            <div class="img_user"><img src="/src/images/user.jpg" alt=""></div>
                                            <p class="name_user">Vorname Nachname</p>
                                            <p>Job Title</p>
                                            <p>Company</p>
                                        </div>
                                    </div>
                                    <div id="hiddden-list" class="list_users-hidden">
                                        <div class="user_box">
                                            <div class="user-offset">
                                                <div class="img_user"><img src="/src/images/user.jpg" alt=""></div>
                                                <p class="name_user">Vorname Nachname</p>
                                                <p>Job Title</p>
                                                <p>Company</p>
                                            </div>
                                        </div>
                                        <div class="user_box">
                                            <div class="user-offset">
                                                <div class="img_user"><img src="/src/images/user.jpg" alt=""></div>
                                                <p class="name_user">Vorname Nachname</p>
                                                <p>Job Title</p>
                                                <p>Company</p>
                                            </div>
                                        </div>
                                        <div class="user_box">
                                            <div class="user-offset">
                                                <div class="img_user"><img src="/src/images/user.jpg" alt=""></div>
                                                <p class="name_user">Vorname Nachname</p>
                                                <p>Job Title</p>
                                                <p>Company</p>
                                            </div>
                                        </div>
                                        <div class="user_box">
                                            <div class="user-offset">
                                                <div class="img_user"><img src="/src/images/user.jpg" alt=""></div>
                                                <p class="name_user">Vorname Nachname</p>
                                                <p>Job Title</p>
                                                <p>Company</p>
                                            </div>
                                        </div>
                                        <div class="user_box">
                                            <div class="user-offset">
                                                <div class="img_user"><img src="/src/images/user.jpg" alt=""></div>
                                                <p class="name_user">Vorname Nachname</p>
                                                <p>Job Title</p>
                                                <p>Company</p>
                                            </div>
                                        </div>
                                        <div class="user_box">
                                            <div class="user-offset">
                                                <div class="img_user"><img src="/src/images/user.jpg" alt=""></div>
                                                <p class="name_user">Vorname Nachname</p>
                                                <p>Job Title</p>
                                                <p>Company</p>
                                            </div>
                                        </div>
                                        <div class="user_box">
                                            <div class="user-offset">
                                                <div class="img_user"><img src="/src/images/user.jpg" alt=""></div>
                                                <p class="name_user">Vorname Nachname</p>
                                                <p>Job Title</p>
                                                <p>Company</p>
                                            </div>
                                        </div>
                                        <div class="user_box">
                                            <div class="user-offset">
                                                <div class="img_user"><img src="/src/images/user.jpg" alt=""></div>
                                                <p class="name_user">Vorname Nachname</p>
                                                <p>Job Title</p>
                                                <p>Company</p>
                                            </div>
                                        </div>
                                        <div class="user_box">
                                            <div class="user-offset">
                                                <div class="img_user"><img src="/src/images/user.jpg" alt=""></div>
                                                <p class="name_user">Vorname Nachname</p>
                                                <p>Job Title</p>
                                                <p>Company</p>
                                            </div>
                                        </div>
                                        <div class="user_box">
                                            <div class="user-offset">
                                                <div class="img_user"><img src="/src/images/user.jpg" alt=""></div>
                                                <p class="name_user">Vorname Nachname</p>
                                                <p>Job Title</p>
                                                <p>Company</p>
                                            </div>
                                        </div>
                                        <div class="user_box">
                                            <div class="user-offset">
                                                <div class="img_user"><img src="/src/images/user.jpg" alt=""></div>
                                                <p class="name_user">Vorname Nachname</p>
                                                <p>Job Title</p>
                                                <p>Company</p>
                                            </div>
                                        </div>
                                        <div class="user_box">
                                            <div class="user-offset">
                                                <div class="img_user"><img src="/src/images/user.jpg" alt=""></div>
                                                <p class="name_user">Vorname Nachname</p>
                                                <p>Job Title</p>
                                                <p>Company</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div id="load-more" class="button light_blue">Load More</div>
                            </div>
                        </div>        
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
</div>

<?#php include realpath('layout/signup-form.php');?>
<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>

