<?php
// local fix to match my vhost settings
$pathPrefix = '';
$host = $_SERVER['HTTP_HOST'];
if ($host == 'bap.local') {
  $pathPrefix = '../';
  require_once $pathPrefix.'data/tcs_array.php';
  require_once $pathPrefix.'data/speakers_array.php';
  require_once $pathPrefix.'lib/People.php';
}
include_once realpath($pathPrefix.'layout/top.php');
include_once realpath($pathPrefix.'layout/header.php');
?>

<div class="inner_sections">

    <?php include_once realpath($pathPrefix.'layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>

    <!--
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

    <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section mobile_hidden">Table Captains 2016</div>
            <ul class="sub_nav ul-reset">
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                    </li>            
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                    </li>
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                    </li>
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                    </li>
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                    </li>
                    <li>
                        <a class="link_nav buyticket trigger_app_nav" data-section="buyticket"><?php echo Meta::getButtonWording(); ?></a>
                    </li>
                </ul>
        </div>
    </div>

    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-captains active section_1">

        <div class="content_section center">
            <div class="wrapper">
                <div class="info_text_top">
                    <h1 class="top_title" style="text-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);">Table Captains 2016</h1>
                </div>
            </div>
        </div>
    </section><!-- section -->

    <section class="section section_logo section_users section_list white active section_1 section_pt">

        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">
                            Here is a list of all our Table Captains for 2016 and an explanation what the heck we are talking about.
                            </h1>
                            <div class="descr center" style="margin-bottom:20px">
<!--                               Basically, what you need to achieve as a founder is to make an investor, entrepreneur, expert or journalist sit down with you at a table and listen to your ideas. This is much easier at a richly set table in a traditional beer tent at the Oktoberfest!-->
                                <br>
<!--                                On most of the events we attended we found one big problem: -->
                                Every table at the grand finale of Bits &amp; Pretzels at the traditional Oktoberfest will be hosted by a handpicked luminary. 
                                And every attendee can select their most interesting Table Captain. 
                                This way we can guarantee you a unique networking experience with exciting conversations based on shared interests.
                                <br>
                                <br>
                                Starting early September you can choose your Table Captain from the list below. But keep in mind: first-come-first-serve applies for Oktoberfest tables too!
                            </div>
                            <div class="link_box center" style="margin-bottom:70px">
                                <a href="#faq" class="link">All information about the Table Captain Concept<span class="icon icon-arrow_right" style="top:2px"></span></a>
                            </div>
                        </div>
                        <div class="wrap_speakers">

                        <div class="selections center">
                            <button class="selection-button" data-type="forbes">Forbes 30 under 30</button>
                            <button class="selection-button" data-type="industry_leader">Industry Leaders</button>
                            <button class="selection-button" data-type="investor">Investors</button>
                            <button class="selection-button" data-type="entrepreneur">Entrepreneurs</button>
                            <button class="selection-button" data-type="press">Media</button>
                            <button class="selection-button active" data-type="all">View All</button>
                        </div>
                            <div class="conteiner">
                                <div class="list_users clearfix">
                                    <?php foreach(People::setType('table-captain')->setDataSource($tcs)->get(0, 'all', true, true) as $tc):  ?>

                                        <div class="user_box tc_box" data-type="<?php echo $tc['type']; ?>">
                                            <div class="user-offset">
                                                <div class="img_user">                                                             <img class="lazyload-scroll" data-src="/src/images/table-captains/default/<?php echo $tc['image']; ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />                                                             <noscript>                                                                 <img src="/src/images/table-captains/default/<?php echo $tc['image']; ?>" alt="">                                                             </noscript>                                                         </div>
                                                <p class="name_user"><?php echo $tc['name']; ?></p>
                                                <div class="title"><?php echo $tc['title']; ?></div>
                                                <div class="company"><?php echo $tc['company']; ?></div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                                    <?php if(1===2):?>
                                <div id="hiddden-list" class="list_users-hidden">
                                    <?php foreach(People::setType('table-captain')->setDataSource($tcs)->get(12, 'all') as $tc):  ?>

                                        <div class="user_box tc_box" data-type="<?php echo $tc['type']; ?>">
                                            <div class="user-offset">
                                                <div class="img_user">                                                             <img class="lazyload-scroll" data-src="/src/images/table-captains/default/<?php echo $tc['image']; ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />                                                             <noscript>                                                                 <img src="/src/images/table-captains/default/<?php echo $tc['image']; ?>" alt="">                                                             </noscript>                                                         </div>
                                                <p class="name_user"><?php echo $tc['name']; ?></p>
                                                <div class="title"><?php echo $tc['title']; ?></div>
                                                <div class="company"><?php echo $tc['company']; ?></div>
                                            </div>
                                        </div>

                                    <?php endforeach; ?>
                                </div>
                                <div id="load-more" class="button light_blue">Load More</div>
                                    <?php endif; ?>



                                 <div class="descr descr_small_margin center">Many More to Come</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
    <section id="faq" class="section silver active section_1 section_pt">
    <div class="content_section center">
      <div class="wrapper_inner">
        <div class="valign">
          <div class="middle">
            <div class="wrapp_main_text">
              <h1 class="main_title">FAQ</h1>
            </div>

            <div class="grid clearfix">
              <div class="valign">
                <div class="middle">

                  <div class="topic topic_open" id="1">
                    <h1 class="question_title">How does the Table Captain networking work?</h1>
                    <div class="arrow_pointer"></div>
                    <div class="description_info descr descr_no_margin" style="display:none">
                      <p class="question_text">
                      On 27th September 2016 more than 250 tables at the Oktoberfest will be hosted by a Table Captain (a well-known, successful internet expert, CEO, CTO, investor, editor-in chief etc.). To ensure exciting conversations in a relaxed atmosphere you will be able to select your personal Table Captain before the event according to the "first-come-first-serve“-principle. Ask questions, get advice and make worthwhile contacts that move your business idea to the next level.
                      </p>
                    </div>
                  </div>

                  <div class="topic topic_open" id="2">
                    <h1 class="question_title">Where will the Table Captain networking take place?</h1>
                    <div class="arrow_pointer"></div>
                    <div class="description_info descr descr_no_margin" style="display:none">
                    <p class="question_text">
                       The Table Captain networking will take place in the Schottenhamel tent at the Oktoberfest, which is located at Theresienwiese in Munich.
                    </p>
                    </div>
                  </div>
                   
                   <div class="topic topic_open" id="3">
                    <h1 class="question_title">At what time does the Table Captain networking start and how long does it go?</h1>
                    <div class="arrow_pointer"></div>
                    <div class="description_info descr descr_no_margin" style="display:none">
                      <p class="question_text">
                          The Table Captain networking starts at 9 am. Your seat will be reserved for you, but you have to be punctual to take your place. It’s important to stay at least for 3-4 hours.
                      </p>
                    </div>
                  </div>

                  <div class="topic topic_open" id="4">
                    <h1 class="question_title">When can I choose my Table Captain?</h1>
                    <div class="arrow_pointer"></div>
                    <div class="description_info descr descr_no_margin" style="display:none">
                      <p class="question_text">
                          Table Captain selection will start approximately 10 days before the event. We’ll keep you updated regarding the exact start of the selection process. 
                      </p>
                    </div>
                  </div>

                  <div class="topic topic_open" id="5">
                    <h1 class="question_title">How can I choose my Table Captain?</h1>
                    <div class="arrow_pointer"></div>
                    <div class="description_info descr descr_no_margin" style="display:none">
                      <p class="question_text">
                          As a ticket owner you will receive a mail with access to the selection tool “NetworkTable” before the event starts (approximately 10 days prior to the event). With this tool you can choose your seat at one of the tables. According to the "first-come-first-serve“-principle you can choose your favorite Table Captain.
                      </p>
                    </div>
                  </div>

                  <div class="topic topic_open" id="6">
                    <h1 class="question_title">Can it occur that my Table Captain cancels?</h1>
                    <div class="arrow_pointer"></div>
                    <div class="description_info descr descr_no_margin" style="display:none">
                      <p class="question_text">
                          Unfortunately, a change in Table Captains cannot be ruled out. We will inform you as soon as possible of any changes in Table Captains, but we’re not responsible for short term cancellations. We’ll ask the Table Captain to contact you directly via e-mail, so that you have the chance to ask all your questions.
                      </p>
                    </div>
                  </div>

                  <div class="topic topic_open" id="7">
                    <h1 class="question_title">Do I get catering?</h1>
                    <div class="arrow_pointer"></div>
                    <div class="description_info descr descr_no_margin" style="display:none">
                      <p class="question_text">
                          You will get two vouchers for food and beverage for the Table Captain networking. You receive those vouchers during the first two conference days in the International Congress Center.
                        </p>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section><!-- section -->
</div>

<?php
#include realpath('layout/signup-form.php');
include realpath($pathPrefix.'layout/footer.php');
include realpath($pathPrefix.'layout/bottom.php');
