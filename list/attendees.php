<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>

   <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section"><span class="mobile_hidden">Attendees</span></div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>            
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                </li>
<!--
                <li>
                    <a class="link_nav buyticket trigger_app_nav" data-section="buyticket">Get your Ticket</a>
                </li>
-->
            </ul>
        </div>
    </div>

    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-attendees active section_1">

        <div class="content_section center">
            <div class="wrapper">
                <div class="info_text_top">
                    <h1 class="top_title" style="text-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);">Attendees</h1>        
                </div>
            </div>
        </div>
    </section><!-- section -->
    
    <section class="section section_logo section_users section_list white active section_1 section_pt">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title">Here is a list of Attendees that have already confirmed for 2016.</h1>
                            <div class="descr descr_small_margin">
                                With more than 3.600 attendees Bits &amp; Pretzels was a huge success in 2015  – but this year is going to be a blast! We will grow in quantity and quality. Further strengthenig our position as the biggest startup conference of Central Europe. Join us and become a part of the exciting startup ecosystem!
                            </div>  
                        </div>             
                        <div class="wrap_speakers">
                            <div class="conteiner">
                                <div id="myList" class="list_users clearfix">
                                <?php
                                    /* START: SET UP DATABASE */
                                    $con = mysqli_connect('wp338.webpack.hosteurope.de', 'db11265772-bits', 'SmiP#7VLbZ9pOuX9', 'db11265772-official');
                                    mysqli_set_charset($con, 'utf8');
                                    if (mysqli_connect_errno()){ echo "Failed to connect to MySQL: " . mysqli_connect_error(); }; 
                                    $res = mysqli_query($con, 'SELECT * FROM ' . mysqli_real_escape_string($con, 'attendees'));
                                    $data = [];
                                    while($row = mysqli_fetch_assoc($res)) { $data[] = $row;};
                                    /* END: SET UP DATABASE */
                                    
                                    for ($i = 0; $i < 252; $i++):?>
                                        <div class="user_box">
                                            <div class="user-offset">
                                                <div class="">
                                                    <div class="img_user img_user-wrapper" style="width:100%;height:130px; overflow:hidden;">
                                                        <img style="border:0; border-radius:0" class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="<?= $data[$i]['image'] ?>" style="width:100%; height:auto; border-radius:0" alt="">
                                                        <noscript>
                                                            <img style="border:0; border-radius:0" src="<?= $data[$i]['image'] ?>" style="width:100%; height:auto; border-radius:0" alt="">
                                                        </noscript>
                                                    </div>
                                                </div>
                                                <p class="name_user"><?= $data[$i]['fname'] . ' ' . $data[$i]['lname'] ?></p>
                                                <p><?= $data[$i]['title'] ?></p>
                                                <p><?= $data[$i]['company'] ?></p>
                                            </div>
                                        </div>
                                    <?php endfor; ?>
                                    </div>
                                </div>
                                 <div id="loadMore" class="button light_blue">Load More</div>
                            </div>
                        </div>        
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
</div>

<?#php include realpath('layout/signup-form.php');?>
<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>

