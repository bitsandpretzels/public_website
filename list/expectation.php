<?php
// local fix to match my vhost settings
$pathPrefix = '';
$host = $_SERVER['HTTP_HOST'];
if ($host == 'bap.local') {
  $pathPrefix = '../';
    
  require_once $pathPrefix.'data/expectation_array.php';

  require_once $pathPrefix.'data/tcs_array.php';
  require_once $pathPrefix.'data/speakers_array.php';
  require_once $pathPrefix.'lib/People.php';
}
include_once realpath($pathPrefix.'layout/top.php');
include_once realpath($pathPrefix.'layout/header.php');
?>

<div class="inner_sections">

    <?php include_once realpath($pathPrefix.'layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>

    <!--
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

    <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section mobile_hidden">What to expect?</div>
            <ul class="sub_nav ul-reset">
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                    </li>            
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                    </li>
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                    </li>
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                    </li>
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                    </li>
                    <li>
                        <a class="link_nav buyticket trigger_app_nav" data-section="buyticket"><?php echo Meta::getButtonWording(); ?></a>
                    </li>
                </ul>
        </div>
    </div>
    
    <style>
    .giveMeEllipsis {
       overflow: hidden;
       text-overflow: ellipsis;
       display: -webkit-box;
       -webkit-box-orient: vertical;
       -webkit-line-clamp: N; /* number of lines to show */
       line-height: X;        /* fallback */
       max-height: X*N;       /* fallback */
    }
        :root{
            --sec_count:#06c;
        }
    
    </style>
    <section class="section section_logo section_users section_list white active section_1 section_pt">

        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">
                            What to expect?
                            </h1>
                        </div>
                        <div class="wrap_speakers">

                        <div class="selections center">
                            <button class="selection-button" data-type="founder">for founders</button>
                            <button class="selection-button" data-type="investor">for investors</button>
                            <button class="selection-button" data-type="corporate">for corporates</button>
                            <button class="selection-button active" data-type="all">view all</button>
                        </div>
                            <div class="conteiner" >
                                <div class="list_users clearfix" >
                                    <div class="descr descr_small_margin center" >
                                        <div class="grid grid-3 grid-3-simple clearfix">
                                        
                                            <?php foreach(People::setType('expectation')->setDataSource($expec)->get(0, 'all', true, true) as $ex):  ?>
                                                <div class="col col-1" style="width: 30%; margin-bottom:50px; min-width:320px; margin 10px; display: inline-block; float: none;">
                                                    <div class="tc_box" data-type="<?php echo $ex['type']; ?>">
                                                        <div class="grid-offset">
                                                            <div class="valign">
                                                                <div class="middle">
                                                                    <div class="img_box">
                                                                        <a href="#" data-featherlight="#fl">
                                                                            <img class="lazyload-scroll" data-src="src/images/sideevents/<?php echo $ex['image']; ?>" style="max-width: 310px !important;" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                                            <noscript>
                                                                                <img src="/src/images/sideevents/<?php echo $ex['image']; ?>" style="max-width: 310px;" alt="<?php echo $ex['title']; ?>">
                                                                            </noscript>
                                                                        </a>
                                                                    </div>
                                                                    <a href="#" data-featherlight="#fl"><h2 class="down_sub_title" style="margin-bottom:5px; margin-top:18px"><?php echo $ex['title']; ?></h2></a>
                                                                    <div class="descr descr_small_margin giveMeEllipsis" style="margin-top:8px">
                                                                       <?php echo $ex['description']; ?>
                                                                    </div>
                                                                    <div class="link_box" style="margin:0">
                                                                        <href a="https://www.bitsandpretzels.com/<?php echo $ex['link_page']; ?>" ><span style="cursor:pointer;" data-featherlight="https://www.bitsandpretzels.com/<?php echo $ex['link_page']; ?>" class="link"><?php echo $ex['link_text']; ?><span class="icon icon-arrow_right"></span></span></href>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            <?php endforeach; ?>
                                        </div>
                                        <div id="loadMore" class="button light_blue">Load More</div>
                                   </div>
                               </div>   
                                    <?php if(1===2):?>
                                <div id="hiddden-list" class="list_users-hidden">
                                    <?php foreach(People::setType('expectation')->setDataSource($expec)->get(12, 'all') as $ex):  ?>

                                         <div class="col col-1" style="margin-bottom:50px">
                                            <div class="grid-offset">
                                                <div class="valign">
                                                    <div class="middle">
                                                        <div class="img_box">
                                                            <a href="#" data-featherlight="#fl">
                                                                <img class="lazyload-scroll" data-src="/src/images/sideevents/<?php echo $ex['image']; ?>" style="max-width: 452px;" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                                <noscript>
                                                                    <img src="/src/images/sideevents/<?php echo $ex['image']; ?>" style="max-width: 452px;" alt="<?php echo $ex['title']; ?>">
                                                                </noscript>
                                                            </a>
                                                        </div>
                                                        <a href="#" data-featherlight="#fl"><h2 class="down_sub_title" style="margin-bottom:5px; margin-top:18px"><?php echo $ex['title']; ?></h2></a>
                                                        <div class="descr descr_small_margin" style="margin-top:8px">
                                                           <?php echo $ex['description']; ?>
                                                        </div>
                                                        <div class="link_box" style="margin:0">
                                                            <span style="cursor:pointer;" data-featherlight="#fl" class="link"><?php echo $ex['link_text']; ?><span class="icon icon-arrow_right"></span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    <?php endforeach; ?>
                                </div>
                                <div id="load-more" class="button light_blue">Load More</div>
                                    <?php endif; ?>



                                 <div class="descr descr_small_margin center">Many More to Come</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php
#include realpath('layout/signup-form.php');
include realpath($pathPrefix.'layout/footer.php');
include realpath($pathPrefix.'layout/bottom.php');
