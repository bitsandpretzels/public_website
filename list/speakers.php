<?php
// local fix to match my vhost settings
$pathPrefix = '';
$host = $_SERVER['HTTP_HOST'];
if ($host == 'bap.local') {
  $pathPrefix = '../';  
  require_once $pathPrefix.'data/speakers_array.php';
  require_once $pathPrefix.'lib/People.php';
}
include_once realpath($pathPrefix.'layout/top.php');
include_once realpath($pathPrefix.'layout/header.php');
?>

<div class="inner_sections">

    <?php include_once realpath($pathPrefix.'layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>

    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

   <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section mobile_hidden">Speakers 2017</div>
            <ul class="sub_nav ul-reset">
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                    </li>            
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                    </li>
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                    </li>
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                    </li>
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                    </li>
                    <li>
                        <a class="link_nav buyticket trigger_app_nav" data-section="buyticket"><?php echo Meta::getButtonWording(); ?></a>
                    </li>
                </ul>
        </div>
    </div>
    
     <section id="section_top" data-inner="section_top_info" class="section section_top section_top-speaker active section_1" style="background:url('src/images/2016/welcome_hero-blue.png'), center, center; background-size:cover;">

        <div class="content_section center">
            <div class="wrapper">
                <div class="info_text_top">
                    <h1 class="top_title" style="text-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);">Speakers 2017</h1>        
                </div>
            </div>
        </div>
    </section><!-- section -->
    <section id="content" class="section section_logo section_users section_list white active section_1 section_pt">
        <div class="content_section center">
            <div class="wrapper_inner">
<!--
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">Here is a list of all <br>our 2016 Speakers.</h1>
                            
                            <div class="descr descr_small_margin center">Big ideas deserve a big stage. Hence our mainstage has been further enlarged for 2016. Ready for inspiring keynote speeches of our exciting collection of rising founders, leading entrepreneurs, experienced investors and visionary thinkers. The Bits & Pretzels stages are set!
                            </div>  
                        </div>  
                    </div>           
                </div>
               <div class="wrapper_inner">
                    <div class="row" style="margin-top:50px">
                         <div class="three columns descr center">
                             <strong style="display:block; margin-bottom:10px">Main Stage</strong>
                             On our mainstage you will hear inspiring talks (e.g. from speakers like Richard Branson or Nathan Blecharczyk) and get valuable founder insights.
                         </div>
                         <div class="three columns descr center">
                            <strong style="display:block; margin-bottom:10px">Academy Stage</strong>
                            We love learning! That’s why startups can expect 13 well designed masterclasses for founders on our academy stage.
                         </div>
                         <div class="three columns descr center">
                            <strong style="display:block; margin-bottom:10px">Pitch Stage</strong>
                            On our startup pitch stage, hundreds of startups of our 6 Clusters compete on the first two days of the event.
                        </div>
                        <div class="three columns descr center">
                            <strong style="display:block; margin-bottom:10px">Side Stages</strong>
                            Choose your <a href="<?php echo Router::getRoute('cluster'); ?>" style="color:#005690">Cluster</a> – Future Commerce, Hot Lifestyle, Big Money etc. – and find your desired slot on our 2 side stages.
                        </div>
                    </div>
                </div>         
-->
                   <div class="valign">
                    <div class="middle">  
                        <div class="wrap_speakers">
                          
                          
                            <div class="conteiner">
                                <h1 class="main_title center" style="margin-top:80px">Speakers 2017</h1>
                                <div class="list_users clearfix">
                                <?php foreach(People::setType('speaker')->setDataSource($speakers_2017)->get(0, 200) as $person): ?>
                                    <div class="user_box cluster_speaker">
                                        <div class="user-offset">
                                            <div class="img_user">
                                                <img class="lazyload-scroll" data-src="src/images/speakers/investor_stage/<?php echo $person['image']; ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                <noscript>
                                                    <img src="src/images/speakers/investor_stage/<?php echo $person['image']; ?>" alt="">
                                                </noscript>
                                            </div>
                                            <p class="name_user"><?php echo $person['name']; ?></p>
                                            <div class="title"><?php echo $person['title']; ?></div>
                                            <div class="company"><?php echo $person['company']; ?></div>
                                        </div>
                                    </div>
                                    <?php 
                                    endforeach; ?>
                                </div>
                            </div>
                           
                           
                           
                            <div class="conteiner">
                                <h1 class="main_title center" style="margin-top:80px">Mainstage Speakers 2016</h1>
                                <div class="list_users clearfix">
<!--
                                   <a href="<?php echo Router::getRoute('spacey'); ?>" alt="Kevin Spacey">
                                        <div class="user_box cluster_speaker">
                                            <div class="user-offset">
                                                <div class="img_user">
                                                    <img class="lazyload-scroll" data-src="/src/images/speakers/default/spacey.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                    <noscript>
                                                        <img src="/src/images/speakers/default/spacey.png" alt="">
                                                    </noscript>
                                                </div>
                                                <p class="name_user">Kevin Spacey</p>
                                                <div class="title">Oscar-winning Actor,</div>
                                                <div class="company">Producer, Startup Investor</div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="<?php echo Router::getRoute('branson'); ?>" alt="Richard Branson">
                                        <div class="user_box cluster_speaker">
                                            <div class="user-offset">
                                                <div class="img_user">
                                                    <img class="lazyload-scroll" data-src="/src/images/speakers/default/branson.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                    <noscript>
                                                        <img src="/src/images/speakers/default/branson.jpg" alt="">
                                                    </noscript>
                                                </div>
                                                <p class="name_user">Richard Branson</p>
                                                <div class="title">Founder</div>
                                                <div class="company">Virgin Group</div>
                                            </div>
                                        </div>
                                    </a>
-->
                                <?php foreach(People::setType('speaker')->setDataSource($speakers_2016)->get(0, 200) as $person): 
                                    if (strpos($person['track'],"main")!==false) { ?>
                                    <div class="user_box cluster_speaker">
                                        <div class="user-offset">
                                            <div class="img_user">
                                                <img class="lazyload-scroll" data-src="/src/images/speakers/default/<?php echo $person['image']; ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                <noscript>
                                                    <img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt="">
                                                </noscript>
                                            </div>
                                            <p class="name_user"><?php echo $person['name']; ?></p>
                                            <div class="title"><?php echo $person['title']; ?></div>
                                            <div class="company"><?php echo $person['company']; ?></div>
                                        </div>
                                    </div>
                                    <?php }
                                    endforeach; ?>
                                </div>
                            </div>
                               
                           <div class="conteiner">
                                <h1 class="main_title center" style="margin-top:80px">Academy Speakers 2016</h1>
                                <div class="list_users clearfix">
                                <?php foreach(People::setType('speaker')->setDataSource($speakers_2016)->get(0, 200) as $person): 
                                    if (strpos($person['track'],"academy")!==false) { ?>
                                    <div class="user_box cluster_speaker">
                                        <div class="user-offset">
                                            <div class="img_user">
                                                <img class="lazyload-scroll" data-src="/src/images/speakers/default/<?php echo $person['image']; ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                <noscript>
                                                    <img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt="">
                                                </noscript>
                                            </div>
                                            <p class="name_user"><?php echo $person['name']; ?></p>
                                            <div class="title"><?php echo $person['title']; ?></div>
                                            <div class="company"><?php echo $person['company']; ?></div>
                                        </div>
                                    </div>
                                    <?php }
                                    endforeach; ?>
                                </div>
                            </div>
                            
                            
                            
                            <div class="conteiner">
                                <h1 class="main_title center" style="color:#f1a237; margin-top:80px">Future Commerce Speakers 2016</h1>
                                <div class="list_users clearfix">
                                <?php foreach(People::setType('speaker')->setDataSource($speakers_2016)->get(0, 200) as $person): 
                                    if (strpos($person['track'],"commerce")!==false) { ?>
                                    <div class="user_box cluster_speaker">
                                        <div class="user-offset">
                                            <div class="img_user">
                                                <img class="lazyload-scroll" data-src="/src/images/speakers/default/<?php echo $person['image']; ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                <noscript>
                                                    <img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt="">
                                                </noscript>
                                            </div>
                                            <p class="name_user"><?php echo $person['name']; ?></p>
                                            <div class="title"><?php echo $person['title']; ?></div>
                                            <div class="company"><?php echo $person['company']; ?></div>
                                        </div>
                                    </div>
                                    <?php }
                                    endforeach; ?>
                                </div>
                            </div>
                            
                            
                         
                            <div class="conteiner">
                                <h1 class="main_title center" style="color:#e65a33; margin-top:50px">Fast Mobility Speakers 2016</h1>
                                <div class="list_users clearfix">
                                    <?php foreach(People::setType('speaker')->setDataSource($speakers_2016)->get(0, 200) as $person): 
                                    if (strpos($person['track'],"mobility")!==false) { ?>
                                        <div class="user_box cluster_speaker">
                                            <div class="user-offset">
                                                <div class="img_user"><img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt=""></div>
                                                <p class="name_user"><?php echo $person['name']; ?></p>
                                                <div class="title"><?php echo $person['title']; ?></div>
                                                <div class="company"><?php echo $person['company']; ?></div>
                                            </div>
                                        </div>
                                    <?php }
                                        endforeach; ?>
                                    </div>
                                </div>

                               
                            <div class="conteiner">
                                <h1 class="main_title center" style="color:#eb0066; margin-top:50px">Hot Lifestyle Speakers 2016</h1>
                                <div class="list_users clearfix">
                                    <?php foreach(People::setType('speaker')->setDataSource($speakers_2016)->get(0, 200) as $person): 
                                    if (strpos($person['cluster'],"lifestyle")!==false) { ?>
                                        <div class="user_box cluster_speaker">
                                            <div class="user-offset">
                                                <div class="img_user">
                                                    <img class="lazyload-scroll" data-src="/src/images/speakers/default/<?php echo $person['image']; ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                    <noscript>
                                                        <img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt="">
                                                    </noscript>
                                                    </div>
                                                <p class="name_user"><?php echo $person['name']; ?></p>
                                                <div class="title"><?php echo $person['title']; ?></div>
                                                <div class="company"><?php echo $person['company']; ?></div>
                                            </div>
                                        </div>
                                    <?php }
                                        endforeach; ?>
                                    </div>
                                </div>
								
							<div class="conteiner">
                                <h1 class="main_title center" style="color:#4C3D88; margin-top:50px">Sophisticated IoT Speakers 2016</h1>
                                <div class="list_users clearfix">
                                  <?php foreach(People::setType('speaker')->setDataSource($speakers_2016)->get(0, 200) as $person): 
                                    if (strpos($person['cluster'],"iot")!==false) { ?>
                                        <div class="user_box cluster_speaker">
                                            <div class="user-offset">
                                                <div class="img_user"><img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt=""></div>
                                                <p class="name_user"><?php echo $person['name']; ?></p>
                                                <div class="title"><?php echo $person['title']; ?></div>
                                                <div class="company"><?php echo $person['company']; ?></div>
                                            </div>
                                        </div>
                                    <?php }
                                        endforeach; ?>
                                    </div>
                                </div>       
								
                            <div class="conteiner">
                                <h1 class="main_title center" style="color:#0AACAF; margin-top:50px">Smart Company Speakers 2016</h1>
                                <div class="list_users clearfix">
                                  <?php foreach(People::setType('speaker')->setDataSource($speakers_2016)->get(0, 200) as $person): 
                                    if (strpos($person['cluster'],"smart_company")!==false) { ?>
                                        <div class="user_box cluster_speaker">
                                            <div class="user-offset">
                                                <div class="img_user"><img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt=""></div>
                                                <p class="name_user"><?php echo $person['name']; ?></p>
                                                <div class="title"><?php echo $person['title']; ?></div>
                                                <div class="company"><?php echo $person['company']; ?></div>
                                            </div>
                                        </div>
                                    <?php }
                                        endforeach; ?>
                                    </div>
                                </div>
                                                                
                                <div class="conteiner">
                                    <h1 class="main_title center" style="color:#006764; margin-top:50px">Big Money Speakers 2016</h1>
                                    <div class="list_users clearfix">
                                        <?php foreach(People::setType('speaker')->setDataSource($speakers_2016)->get(0, 200) as $person): 
                                        if (strpos($person['track'],"money")!==false) { ?>
                                            <div class="user_box cluster_speaker">
                                                <div class="user-offset">
                                                    <div class="img_user">
                                                        <img class="lazyload-scroll" data-src="/src/images/speakers/default/<?php echo $person['image']; ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                        <noscript>
                                                            <img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt="">
                                                        </noscript>
                                                    </div>
                                                    <p class="name_user"><?php echo $person['name']; ?></p>
                                                    <div class="title"><?php echo $person['title']; ?></div>
                                                    <div class="company"><?php echo $person['company']; ?></div>
                                                </div>
                                            </div>
                                        <?php }
                                            endforeach; ?>
                                    </div>
                                </div>
                                
                                <div class="conteiner">
                                    <h1 class="main_title center" style="margin-top:50px">Gründerland Bayern - Bavarian Startups 2016</h1>
                                    <div class="list_users clearfix">
                                        <?php foreach(People::setType('speaker')->setDataSource($speakers_2016)->get(0, 200) as $person): 
                                        if (strpos($person['track'],"gruenderland")!==false) { ?>
                                            <div class="user_box cluster_speaker">
                                                <div class="user-offset">
                                                    <div class="img_user">
                                                        <img class="lazyload-scroll" data-src="/src/images/speakers/default/<?php echo $person['image']; ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                        <noscript>
                                                            <img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt="">
                                                        </noscript>
                                                    </div>
                                                    <p class="name_user"><?php echo $person['name']; ?></p>
                                                    <div class="title"><?php echo $person['title']; ?></div>
                                                    <div class="company"><?php echo $person['company']; ?></div>
                                                </div>
                                            </div>
                                        <?php }
                                            endforeach; ?>
                                    </div>
                                </div>
                               
                               
                               
                               
                               
                               
                               
                                <hr class="gray-section-divider" style="margin: 40px 0 0 0; width:100%;">
								<h1 class="main_title center" style="margin-top:3em">2015 speakers</h1> 
                                <div class="list_users clearfix">
                                    <?php foreach(People::setType('speaker')->setDataSource($speakers_2015)->get(0, 'all', true, false) as $speaker):  ?>

                                        <div class="user_box tc_box" data-type="<?php echo $tc['type']; ?>">
                                            <div class="user-offset">
                                                <div class="img_user">
                                                    <img class="lazyload-scroll" data-src="/src/images/2015/speakers/<?php echo $speaker['image']; ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                    <noscript>
                                                        <img src="/src/images/2015/speakers/<?php echo $speaker['image']; ?>" alt="<?= $speaker['name'] ?>">
                                                    </noscript>
                                                </div>
                                                <p class="name_user"><?php echo $speaker['name']; ?></p>
                                                <div class="title"><?php echo $speaker['title']; ?></div>
                                                <div class="company"><?php echo $speaker['company']; ?></div>
                                            </div>
                                        </div>

                                    <?php endforeach; ?>
                                 </div>
                                <div id="load-more" style="display:none" class="button light_blue">Load More</div>
                            </div>
                        </div>        
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
</div>

<?php
#include realpath($pathPrefix.'layout/signup-form.php');
include realpath($pathPrefix.'layout/footer.php');
include realpath($pathPrefix.'layout/bottom.php');