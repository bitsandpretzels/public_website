
<?php include 'layout/top.php';?>
<?php include 'layout/header.php';?>

<div class="inner_sections">
    <div id="sub_nav" class="sub_nav_wrap fixed">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li">
                    <a href="#content" class="link_nav" data-section="content">The Event</a>
                </li>
                <li class="hidden_li">
                    <a href="#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>
                <li class="hidden_li">
                    <a href="#captain" class="link_nav" data-section="captain">Table Captains</a>
                </li>
                <li class="hidden_li">
                    <a href="#gallery" class="link_nav" data-section="captain">Gallery</a>
                </li>
               <li class="hidden_li" style="margin-right:184px">
                    <a href="#story" class="link_nav" data-section="captain">Our Story</a>
                </li>
                <li>
                    <a href="#" class="link_nav buyticket choose-cat-trigger-wrapper trigger_app_nav" data-section="buyticket"><?php echo Meta::getButtonWording(); ?></a>
                </li>
            </ul>
        </div>
    </div>

<!-- START: Revolution Slider -->
<?php include_once ('layout/headers/home_revslider_1.php');?>
<!-- END: Revolution Slider -->



    <!-- START: content section -->
   <section id="content" class="section section_users white active section_1 section_pt" data-show-inner="false" data-show-footer="false">
       <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center" style="margin-bottom:30px; margin-top:20px">3 days of vision, learning and networking.</h1>
                            <div class="descr descr_small_margin">
                                <div class="descr" style="margin-bottom:0; margin-top:15px;">
                                <!-- Calendar X-->
                                     <div class="calx-sub-btn" style="height:44px;display:inline-block;" data-calendar="wq1291" data-configure="true" data-title="24.%20%E2%80%93%2026.%20September%202017" data-counter="false" data-icon="calendar" data-dimming="white" data-theme="2"></div>
                                </div>
                                <script>
                                    // Calendar X Function, activated on first scroll
                                    (function(){var e = document.createElement("script");e.type = "text/javascript";e.async = true;e.src = "https://calendarx.com/js/sub.btn.init.js";e.className = "calendarx-script";document.getElementsByTagName("body")[0].appendChild(e);})();
                                </script>
                                <div style="margin-top:35px">
                                    Bits &amp; Pretzels is a 3-day founders festival that connects 5.000 startup enthusiasts with Speakers and Table Captains like the founders of Virgin, Airbnb, Evernote or Zendesk. Bits &amp; Pretzels creates unique networking around Munich Oktoberfest and encourages meaningful relationships between founders, investors and the media.
                                </div>
                        <img class="desktop_only" src="/src/images/agenda_preview.png" alt="timeline" style="width:100%; max-width:550px; margin:0 auto; padding: 45px 0 20px 0;">
                        <img class="mobile_only" src="/src/images/agenda_preview_mobile.png" alt="timeline" style="width:100%; max-width:180px; margin:0 auto; padding: 50px 0 20px 0;">
                            </div>
                            <div style="margin-top:25px" class="link_box center">
                                <a href="https://youtu.be/Xu4b5nEeUIc" alt="play" data-lity class="link">Watch 2016 Highlight Movie<span class="icon icon-arrow_right"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
    <section id="spacey" class="section-spacey">
        <div class="container">
            <div class="row clearfix">
                <div class="twelve columns Concept-SubTitle" style="text-align:right">
                    "Combining a startup conference <br>with the Oktoberfest is brilliant."<br><span class="quote-author">Kevin Spacey</span>
                </div>
            </div>
        </div>
    </section>
    <section id="speakers" class="section section_users silver active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center" style="margin-bottom:30px">Speakers at Bits &amp; Pretzels 2017.</h1>
<!--                            <h2 class="down_sub_title center">Because Visions do come from Visionaries.</h2>-->
                            <div class="descr descr_small_margin center">
                            Some of the most successful national and international entrepreneurs, investors and CEOs shared their insights on stage and opened up new perspectives for Bits &amp; Pretzels attendees.
                            </div>
                        </div>
                        <div class="wrap_speakers">
                            <div class="conteiner">
                                <div class="list_users">
                                    <?php foreach(People::setType('speaker')->setDataSource($speakers_2017)->get(0, 12) as $person):  ?>
                                        <div class="user_box">
                                            <div class="user-offset">
                                                <div class="img_user">
                                                    <img class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="src/images/speakers/investor_stage/<?php echo $person['image']; ?>" alt="">
                                                    <noscript>
                                                        <img class="lazyload-scroll" src="src/images/speakers/investor_stage/<?php echo $person['image']; ?>" alt="">
                                                    </noscript>
                                                </div>
                                                <p class="name_user"><?php echo $person['name']; ?></p>
                                                <div class="title"><?php echo $person['title']; ?></div>
                                                <div class="company"><?php echo $person['company']; ?></div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div class="link_box center"><a href="<?php echo Router::getRoute('speakers'); ?>" class="link">View All Speakers<span class="icon icon-arrow_right"></span></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
   <section id="branson" class="section-branson">
        <div class="container">
            <div class="row clearfix">
               <div class="seven columns center Concept-SubTitle">
<!--                    "Bits &amp; Pretzels is a very professional event that is just as much about having fun as it is about bringing together a fascinating array of guest speakers." <br><span class="quote-author">Richard Branson</span>-->
                    "Highly professional and as much about having fun as about bringing together a fascinating array of speakers"<br><span class="quote-author">Richard Branson</span>
                </div>
            </div>
        </div>
    </section>
    <section id="captain" class="section section_users_right active silver section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="content_section">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="grid grid-2 grid-2-simple clearfix">
                            <div class="col col-1">
                                <div class="grid-offset">
                                    <div class="info_box">
                                        <h1 class="main_title">Oh captain. My captain. <br>Table Captain.</h1>
                                        <div class="descr descr_small_margin" style="margin-top:40px">
                                        Once a year the most influential executives gather at the Bits &amp; Pretzels 
                                        Oktoberfest tables. Every table at the grand finale is hosted by a Table 
                                        Captain and all attendees can choose one of those handpicked CEOs, editors-in-chief 
                                        or investors before the event even starts. Through that we make sure that everyone 
                                        gets the most out of Bits &amp; Pretzels and enjoys the best networking experience in a relaxed atmosphere.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-2">
                                <div class="grid-offset">
                                    <div class="conteiner">
                                        <div class="list_users">
                                            <?php foreach(People::setType('table-captain')->setDataSource($tcs_2017)->get(0, 8) as $key => $tc):  ?>
                                                <div class="user_box <?php echo ($key>7) ? 'hidden_thirteen' : ''; ?>">
                                                    <div class="user-offset">
                                                        <div class="img_user">
                                                            <img class="lazyload-scroll" class="on-cat-site" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="/src/images/table-captains/2017/<?php echo $tc['image']; ?>" alt="">
                                                            <noscript>
                                                                <img class="lazyload-scroll" class="on-cat-site" src="/src/images/table-captains/2017/<?php echo $tc['image']; ?>" alt="">
                                                            </noscript>
                                                        </div>
                                                        <p class="name_user"><?php echo $tc['name']; ?></p>
                                                        <div class="title"><?php echo $tc['title']; ?></div>
                                                        <div class="company"><?php echo $tc['company']; ?></div>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="link_box center" style="width:100%">
                                <a href="<?php echo Router::getRoute('captains'); ?>" class="link">View all Table Captains<span class="icon icon-arrow_right"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
    <section id="partner" class="section section_users active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                       <div class="wrapp_main_text">
                           <h1 class="main_title center" style="margin-bottom:30px">You're in good hands.</h1>
                           <div class="descr descr_small_margin center" style="margin-bottom:0">
                               Partnerships with the aim of forming long-lasting relationships with mutual benefits forms the basis for the success of the founders festival. Amazing companies join the event as a partner year for year.
                           </div>
                       </div>
                       <div class="wrap_speakers" style="max-width:900px">
                           <div class="conteiner">
                               <div class="list_users">
                                   <?php for($i = 0; $i <= 18; $i++):
                                   if (strpos($partners_2017[$i]['type'],"sponsor")!==false): ?>
                                      
                                   <div class="user_box partner_section" style="height:auto;">
                                       <div class="user-offset">
                                           <a href="<?= $partners_2017[$i]['link']; ?>" target="_blank">
                                               <img class="lazyload" data-src="/src/images/partners/<?= $partners_2017[$i]['logo']; ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="width:100%" alt="<?= $partners_2017[$i]['name']; ?>"/>
                                               <noscript>
                                                   <img style="width:100%" src="/src/images/partners/<?= $partners_2017[$i]['logo']; ?>" alt="<?= $partners_2017[$i]['name']; ?>">
                                               </noscript>
                                           </a>
                                       </div>
                                   </div>
                       
                                   <?php endif; endfor; ?>
                               </div>
                           </div>
                       </div>
                        <div class="link_box center" style="width:100%; margin-top:30px">
                                <a href="<?php echo Router::getRoute('partners'); ?>" class="link">View all our partners<span class="icon icon-arrow_right"></span></a>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="gallery" class="clearfix desktop_only">
        <div class="img_wrapper-zoom">
            <img class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="src/images/gallery/1.jpg?v=<?php echo $currentScriptVersion; ?>" alt="Academy" title="Startup Academy">
            <noscript>
                <img class="lazyload-scroll"src="src/images/gallery/1.jpg?v=<?php echo $currentScriptVersion; ?>" alt="Academy" title="Startup Academy">
            </noscript>
        </div>
        <div class="img_wrapper-zoom">
            <img class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="src/images/gallery/2.jpg?v=<?php echo $currentScriptVersion; ?>" alt="" >
            <noscript>
                <img class="lazyload-scroll"src="src/images/gallery/2.jpg?v=<?php echo $currentScriptVersion; ?>"  alt="Academy" title="Startup Academy">
            </noscript>
        </div>
        <div class="img_wrapper-zoom">
            <img class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="src/images/gallery/3.jpg?v=<?php echo $currentScriptVersion; ?>" alt="Kevin Spacey">
            <noscript>
                <img class="lazyload-scroll"src="src/images/gallery/3.jpg?v=<?php echo $currentScriptVersion; ?>"  alt="Academy" title="Startup Academy">
            </noscript>
        </div>
        <div class="img_wrapper-zoom">
            <img class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="src/images/gallery/4.jpg?v=<?php echo $currentScriptVersion; ?>" alt="" >
            <noscript>
                <img class="lazyload-scroll"src="src/images/gallery/4.jpg?v=<?php echo $currentScriptVersion; ?>"  alt="Academy" title="Startup Academy">
            </noscript>
        </div>
        <div class="img_wrapper-zoom">
            <img class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="src/images/gallery/5.jpg?v=<?php echo $currentScriptVersion; ?>" alt="" >
            <noscript>
                <img class="lazyload-scroll"src="src/images/gallery/5.jpg?v=<?php echo $currentScriptVersion; ?>"  alt="Academy" title="Startup Academy">
            </noscript>
        </div>
        <div class="img_wrapper-zoom">
            <img class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="src/images/gallery/6.jpg?v=<?php echo $currentScriptVersion; ?>" alt="" >
            <noscript>
                <img class="lazyload-scroll"src="src/images/gallery/6.jpg?v=<?php echo $currentScriptVersion; ?>"  alt="Academy" title="Startup Academy">
            </noscript>
        </div>
        <div class="img_wrapper-zoom">
            <img class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="src/images/gallery/7.jpg?v=<?php echo $currentScriptVersion; ?>" alt="" >
            <noscript>
                <img class="lazyload-scroll"src="src/images/gallery/7.jpg?v=<?php echo $currentScriptVersion; ?>"  alt="Academy" title="Startup Academy">
            </noscript>
        </div>
        <div class="img_wrapper-zoom">
            <img class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="src/images/gallery/8.jpg?v=<?php echo $currentScriptVersion; ?>" alt="" >
            <noscript>
                <img class="lazyload-scroll"src="src/images/gallery/8.jpg?v=<?php echo $currentScriptVersion; ?>"  alt="Academy" title="Startup Academy">
            </noscript>
        </div>
        <div class="img_wrapper-zoom">
            <img class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="src/images/gallery/9.jpg?v=<?php echo $currentScriptVersion; ?>" alt="" >
            <noscript>
                <img class="lazyload-scroll"src="src/images/gallery/9.jpg?v=<?php echo $currentScriptVersion; ?>"  alt="Academy" title="Startup Academy">
            </noscript>
        </div>
        <div class="img_wrapper-zoom">
            <img class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="src/images/gallery/10.jpg?v=<?php echo $currentScriptVersion; ?>" alt="" >
            <noscript>
                <img class="lazyload-scroll"src="src/images/gallery/10.jpg?v=<?php echo $currentScriptVersion; ?>"  alt="Academy" title="Startup Academy">
            </noscript>
        </div>
        <div class="img_wrapper-zoom">
            <img class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="src/images/gallery/11.jpg?v=<?php echo $currentScriptVersion; ?>" alt="" >
            <noscript>
                <img class="lazyload-scroll"src="src/images/gallery/11.jpg?v=<?php echo $currentScriptVersion; ?>"  alt="Academy" title="Startup Academy">
            </noscript>
        </div>
        <div class="img_wrapper-zoom">
            <img class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="src/images/gallery/12.jpg?v=<?php echo $currentScriptVersion; ?>" alt="" >
            <noscript>
                <img class="lazyload-scroll"src="src/images/gallery/12.jpg?v=<?php echo $currentScriptVersion; ?>"  alt="Academy" title="Startup Academy">
            </noscript>
        </div>
    </section>
    <section id="story" class="section_pt silver desktop_only center">
        <div class="container">
            <div class="row clearfix center">
               <div class="twelve columns center">
                    <div class="wrapp_main_text center">
                       <h1 class="main_title center" style="margin-bottom:20px">This is our story.</h1>
<!--
                       <div class="descr descr_small_margin center">
                            <strong>From founders – For Founders</strong>
                       </div>
-->
                   </div>
                </div>
            </div>
        </div>
       <img src="/src/images/story.png" alt="timeline" style="width:100%; max-width:1300px; margin:0 auto; padding: 12px 0 20px 0;">
    </section>
    <section id="tweets" class="section-tweets" style="overflow:hidden;min-height:300px">
        <div class="container" style="overflow:hidden;">
            <div class="row clearfix desktop_only" style="margin-top:130px;margin-bottom:94px; overflow:hidden;">
               <div class="four columns center">
                   <blockquote class="twitter-tweet" data-lang="de">
                   <p lang="en" dir="ltr">Huge thanks &amp; well done to the <a href="https://twitter.com/hashtag/bits16?src=hash">#bits16</a> team!! Still buzzing from all inspirations and experiences gained over last 3 days <a href="https://twitter.com/bitsandpretzels">@bitsandpretzels</a></p>&mdash; start zero (@startzero_ffm) <a href="https://twitter.com/startzero_ffm/status/781063422070951936">28. September 2016</a>
                   </blockquote>
                </div>
                <div class="four columns center">
                   <blockquote class="twitter-tweet" data-lang="de"><p lang="en" dir="ltr"><a href="https://twitter.com/hashtag/bits16?src=hash">#bits16</a> is over! It was a blast and I will be back for more <a href="https://twitter.com/hashtag/BitsandPretzels?src=hash">#BitsandPretzels</a> in 2017! <a href="https://twitter.com/hashtag/bits17?src=hash">#bits17</a> <a href="https://twitter.com/hashtag/lovedit?src=hash">#lovedit</a> <a href="https://twitter.com/hashtag/inspiring?src=hash">#inspiring</a> <a href="https://twitter.com/hashtag/startup?src=hash">#startup</a> <a href="https://twitter.com/hashtag/startuplife?src=hash">#startuplife</a></p>&mdash; Michael Sinnhuber (@msinnhuber) <a href="https://twitter.com/msinnhuber/status/781163509853937665">28. September 2016</a></blockquote>
                </div>
                  <div class="four columns center">
                   <blockquote class="twitter-tweet" data-lang="de"><p lang="en" dir="ltr">Big THANKS to <a href="https://twitter.com/BerndStorm">@BerndStorm</a> <a href="https://twitter.com/andiarbeit_">@andiarbeit_</a> &amp; <a href="https://twitter.com/felixhaas">@felixhaas</a> for <a href="https://twitter.com/bitsandpretzels">@bitsandpretzels</a>! So far 10 new business contacts, 2 pitches delivered &amp; 1 selfie!</p>&mdash; Sham Jaff (@sham_jaff) <a href="https://twitter.com/sham_jaff/status/780038797383856129">25. September 2016</a></blockquote>
                </div>
            </div>
            <div class="row clearfix" style="margin-top:100px; overflow:hidden;">
               <div class="twelve columns center">
                    <div id="buyticket" class="button_mb choose-cat-trigger-wrapper" style="margin-top:30px; margin-bottom:65px;">
                        <div class="button_box button_box_orange button_box_auto trigger_app_nav">
                            <a href="#category" style="text-transform:uppercase; font-size:20px;font-weight:700" class="button button-orange"><span><?php echo Meta::getButtonWording(); ?></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- END: content section -->
<?php

//	include 'layout/signup-form.php';
	include 'layout/footer.php';
	include 'layout/bottom.php';