<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>

    <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section mobile_hidden">Meet Kevin Spacey</div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                </li>
                <li>
                    <a href="#content" class="link_nav buyticket" data-section="buyticket">Participate</a>
                </li>
            </ul>
        </div>
    </div>

    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-spacey-2 active section_1"></section><!-- section -->
    <div class="descr" style="font-size:12px; margin-left:5px">(Image: Getty Images)</div>
    <section id="content" class="section section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="min-height: 0px; padding-top:30px">
        <div class="content_section center" style="min-height: 0px">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center" style="margin-bottom:30px">
                            Win a lunch with Kevin Spacey
                            <br>
                            plus a free Bits &amp; Pretzels VIP Package!
                            </h1>
                            <div class="descr descr_small_margin center">
                                Kevin Spacey, Oscar-winning actor, writer, producer and tech investor,
                                will join this year‘s Founders Festival in Munich to inspire 5000+ founders, startup enthusiasts,
                                investors and business incubators from all over the world. Spacey will give a 30 minute speech for founders – followed by a 15 minute Q&A session.
                                <br><br>
                                We want to make an entrepreneur's dream come true by providing one lucky winner with an unforgettable experience:
                                <strong>sitting at Kevin Spacey's table for an exclusive lunch in a secret location!</strong>
                            </div>
                            <span class="descr" style="color:#0094d4; margin-bottom:0">
                                 <span class="text_icon" style="background-image:url('src/images/icon_agenda.svg'); margin-top:15px"></span>
                                 Sun | 25.09.2016
                            </span>
                            <address class="descr" style="color:#0094d4; margin-left:20px">
                                 <span class="text_icon" style="background-image:url('src/images/icon_location.svg'); margin-top:15px"></span>
                                 Munich
                            </address>
                            <h1 class="main_title center" style="margin-bottom:20px; margin-top:80px">
                                <span style="margin-right:10px">🎉</span>
                                    The Grand Prize
                                <span style="margin-left:10px">🎉</span>
                            </h1>
                            <ul class="descr descr_small_margin descr-cluster" style="width:100%; max-width:460px">
                                <li style="list-style:square">An exclusive lunch with Kevin Spacey at his table</li>
                                <li style="list-style:square">A full 3-day conference ticket for Bits &amp; Pretzels</li>
                                <li style="list-style:square">2 nights in a superior room in the Sofitel luxury hotel (5-Star) from 24.09-26.09.2016</li>
                            </ul>

                            <h1 class="main_title center" style="margin-bottom:20px; margin-top:65px">
                            How to participate
                            </h1>
                            <ul class="descr descr_small_margin descr-cluster" style="width:100%; max-width:532px; margin-top:30px">
                                <li><strong>Step 1:</strong> Share this link on facebook with a short explanation why you desire to meet Kevin Spacey using the Hashtag <span style="color:#005690">#IWantToMeetKevin</span> (make sure that the post is <strong>public</strong>)</li>
                            </ul>
                            <div id="buyticket" class="button_mb choose-cat-trigger-wrapper" style="margin-top:35px; margin-bottom:0">
                                <div class="button_box button_box_orange button_box_auto">
                                    <a style="background:#3b5998; border:none; color:white" 
                                        href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.bitsandpretzels.com%2Fmeet-kevin&amp;src=sdkpreparse" 
                                        target="_blank" title="Share on Facebook" class="button orange">
                                    Share this on Facebook
                                    </a>
                                </div>
                            </div>
                            <ul class="descr descr_small_margin descr-cluster center" style="width:100%; max-width:532px; margin-top:56px">
                                <li><strong>Step 2:</strong> Fill out the form, claim your raffle ticket and keep your fingers crossed.</li>
                            </ul>
                        </div>
                        <script type="text/javascript" src="https://form.jotformeu.com/jsform/62554241715352"></script>
                        <div class="promotional_code" style="padding-bottom:0; padding-top:10px">
                            <div class="booking" style="margin-bottom:0;">
                                <div style="font-size:.8em;margin-bottom:0; color:#ea5c3f" class="descr descr_small_margin">The raffle will end in <span style="font-weight:600;"id="third-countdown"></span><span style="font-weight:600;"> hours</span></div>
                            </div>
                        </div>
                        <div class="descr descr_small_margin center" style="margin-top:20px">
                            We WON’T share any personal data, promise!
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
    <section id="networking" class="section section_devices silver active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <h1 class="main_title center" style="margin-bottom:50px">Tell me more...</h1>
                        <div class="wrapp_main_text">
                            <div class="descr descr_small_margin center">
                                Kevin Spacey gained popularity with  films like The Usual Suspects, Se7en, LA Confidential and American Beauty. With two Academy Awards, eleven years as Artistic Director of the legendary Old Vic theatre and countless other awards, Spacey’s turn to TV in 2013 garnered him legions of new fans. He inhabits the character of Congressman Frank Underwood on Netflix’s first original series, House of Cards. For his role Spacey has won Golden Globe and SAG awards, and is nominated again this year for an Emmy for Best Actor in a Drama Series. For his services to theatre, the arts and international culture, Spacey recently received an honorary Knighthood from Her Majesty Queen Elizabeth II.
                                <br><br>
                                But that’t not all: In addition to being recognized for his work in the arts, Kevin Spacey is an active investor in many technology companies. He is a highly sought after speaker that has mesmerized audiences at tech conferences all over the world, including the World Economic Forum at Davos, where he spoke this past year about the art of storytelling and its role in the future of technology.
                                <br><br>
                                Kevin Spacey is coming over to Germany to join Bits &amp; Pretzels. We’re more than excited to hear him live on stage!
                                <div id="buyticket" class="button_mb choose-cat-trigger-wrapper" style="margin-top:45px; margin-bottom:0">
                                    <div class="button_box button_box_orange button_box_auto">
                                        <a href="<?php echo Router::getRoute('home'); ?>"title="Share on Facebook" class="button orange">
                                        LEARN MORE ABOUT BITS &amp; PRETZELS
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!--
                        <div id="buyticket" class="button_mb choose-cat-trigger-wrapper center" style="margin-top:45px; margin-bottom:0">
                            <div class="button_box button_box_orange button_box_auto" style="width:255px; margin:0 auto">
                                <script async="async" id="button_5c7f4cba-9cdb-4403-98ef-4f563b15297f" src="//www.paywithatweet.com/embeds/5c7f4cba-9cdb-4403-98ef-4f563b15297f" charset="utf-8"></script>
                            </div>
                        </div>
-->
<!--
                       <div id="buyticket" class="button_mb choose-cat-trigger-wrapper" style="margin-top:45px; margin-bottom:0">
                            <div class="button_box button_box_orange button_box_auto">
                                <a href="http://www.paywithapost.de/post_payments/new/5c7f4cba-9cdb-4403-98ef-4f563b15297f/facebook" title="Share on Facebook" class="button orange">
                                PARTICIPATE
                                 </a>
                            </div>
                        </div>
-->
<!--

                            <div id="buyticket" class="button_mb choose-cat-trigger-wrapper" style="margin-top:45px; margin-bottom:0;">
                                <div class="button_box button_box_orange button_box_auto">
                                    <a style="background:#1da1f2; border:none; color:white" href="https://twitter.com/intent/tweet?source=&text=%5BAdd%20your%20text%20here%5D%20%23IWantToMeetKevin%20https%3A%2F%2Fwww.bitsandpretzels.com%2Fcontest:%20&via=bitsandpretzels" title="Share on Twitter" class="button orange">
                                    Share this on Twitter
                                    </a>
                                </div>
                            </div>
-->
<div id="fb-root"></div>
<?php include realpath('layout/signup-form.php');?>
<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>