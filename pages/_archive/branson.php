<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>


    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

    <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section mobile_hidden">Richard Branson</div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>            
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                </li>
                <li>
                    <a class="link_nav buyticket trigger_app_nav" data-section="buyticket">Get your Ticket</a>
                </li>
            </ul>
        </div>
    </div>

    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-branson active section_1">
        <div class="content_section center">
            <div class="wrapper">
                <div class="info_text_top">
                    <h1 class="top_title">Richard Branson <br>is coming</h1>
                </div>
            </div>
        </div>
    </section><!-- section -->



    <section id="content" class="section section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="min-height: 0px">
        <div class="content_section center" style="min-height: 0px">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">Richard Branson joins Bits &amp; Pretzels <br>as a Speaker</h1>
                            <div class="descr descr_small_margin center">Sir Richard Charles Nicolas Branson, successful British Entrepreneur and Virgin-Founder himself
is joining Bits &amp; Pretzels in September. The iconic entrepreneur and self-made billionaire will
be on stage at this years’ Founders Festival in Munich and inspire 5000+ founders, startup
enthusiasts, investors and business incubators from all over the world. 
<br>From September 25 – 27,
2016 you have the unique chance to meet Richard in person during the 3-day festival, where the
digital startup environment meets in Munich during Oktoberfest.<div class="link_box"></div>
                            </div>                             
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
    <section id="networking" class="section section_devices silver active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                            <h1 class="main_title center">Who is that guy?</h1>
                            <img class="lazyload-scroll desktop_only" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="/src/images/info/branson.png" alt="timeline" style="width:100%; max-width:800px; margin:0 auto; padding: 54px 0 54px 0;">
                            <noscript>
                                <img class="lazyload-scroll desktop_only" src="/src/images/info/branson.png" alt="timeline" style="width:100%; max-width:800px; margin:0 auto; padding: 54px 0 54px 0;">
                            </noscript>
                            <img class="lazyload-scroll mobile_only" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="/src/images/info/branson_mobile.png" alt="timeline" style="width:100%; max-width:235px; margin:0 auto; padding: 48px 0 41px 0;">
                            <noscript>
                                <img class="lazyload-scroll mobile_only" src="/src/images/info/branson_mobile.png" alt="timeline" style="width:100%; max-width:235px; margin:0 auto; padding: 48px 0 41px 0;">
                            </noscript>
                        <div class="wrapp_main_text">
                            <div class="descr descr_small_margin center">Richard Branson is best known from founding the popular Virgin brand. In 1970, he started with a
small mail-order record business and gradually founded other companies in different industrial
sectors, which he also bundled under the name Virgin. One of his most well-known companies
within the group: Virgin Atlantic, founded in 1984, is now one of the largest airlines in the world. As
of today, Virgin Group contains more than 100 companies worldwide with approximately 60,000
employees in 50 countries, making it one of the biggest business conglomerates worldwide.
                           <br><br>
                            But that’s not all: Branson has challenged himself with many record breaking adventures,
including the fastest ever Atlantic Ocean crossing, a series of hot air balloon adventures and
kitesurfing across the English Channel. He has also written six books, was ranked the world’s most
followed person on LinkedIn and has more than 11.5 million followers across his social networks.
<br><br>This celebrity super entrepreneur and famous man behind the Virgin empire is coming over to
Germany to join Bits &amp; Pretzels. We are more than excited to hear his story on stage!<div class="link_box"></div>
                            </div>                             
                        </div>  
                        <div id="buyticket" class="button_mb choose-cat-trigger-wrapper" style="margin-top:90px">
                            <div class="button_box button_box_orange button_box_auto trigger_app_nav">
                                <a href="#category" class="button orange"><span>Register for 2017 Ticket Sale</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<?php include realpath('layout/signup-form.php');?>
<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>