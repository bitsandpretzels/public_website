<?php include 'layout/top.php';?>
<?php include 'layout/header.php';?>

<div class="inner_sections">
    <div id="sub_nav" class="sub_nav_wrap fixed">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li">
                    <a href="#content" class="link_nav" data-section="content">The Event</a>
                </li>
                <li class="hidden_li">
                    <a href="#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>
                <li class="hidden_li">
                    <a href="#clusters" class="link_nav" data-section="clusters">Clusters</a>
                </li>
                <li class="hidden_li">
                    <a href="#captain" class="link_nav" data-section="captain">Table Captains</a>
                </li>
                <li class="hidden_li">
                    <a href="#academy" class="link_nav" data-section="academy">Academy</a>
                </li>
                <li class="hidden_li">
                    <a href="#networking" class="link_nav" data-section="networking">Networking App</a>
                </li>
                <li class="hidden_li" style="margin-right:81px">
                    <a href="#sponsors" class="link_nav" data-section="sponsors">Partners</a>
                </li>
                <li>
                    <a href="#category" class="link_nav buyticket trigger_app_nav choose-cat-trigger-wrapper" data-section="buyticket">Get Your Ticket</a>
                </li>
            </ul>
        </div>
    </div>


<!-- START: Revolution Slider -->
<?php include_once ('layout/headers/onepager_revslider.php');?>
<!-- END: Revolution Slider -->

<!-- START: content section -->
   <section id="content" class="section section_users white active section_1 section_pt" data-show-inner="false" data-show-footer="false">
       <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center" style="margin-bottom:30px; margin-top:20px">Uniting the startup world <br>during the Oktoberfest.</h1>
                            <div class="descr descr_small_margin">
                                <table class="hard-facts center">
                                    <tr>
                                    <td><span>5,000+</span> Attendees</td>
                                    <td><span>300+</span> Table Captains</td>
                                    <td><span>400+</span> Journalists</td>
                                    </tr>
                                </table>
                                <div style="margin-top:35px">
                                    Bits &amp; Pretzels connects more than 5.000 participants with Speakers and Table Captains like founders of Virgin (Richard Branson), Airbnb, Evernote, Kayak, Home24, Runtastic or Delivery Hero. 
                                    Bits &amp; Pretzels creates unique networking around Munich Oktoberfest and encourages meaningful relationships between founders, investors and the media. 
                                </div>
                                <div class="descr" style="margin-bottom:0; margin-top:25px">
                                     <span class="text_icon" style="background-image:url('src/images/icon_agenda_blue.svg'); margin-top:15px"></span>
                                     25 – 27. September 2016
                                </div>
                            </div>
                        </div>
                        <img class="desktop_only" src="/src/images/timeline_desktop.png" alt="timeline" style="width:100%; max-width:1060px; margin:0 auto; padding: 45px 0 20px 0;">
                        <img class="mobile_only" src="/src/images/timeline_mobile.png" alt="timeline" style="width:100%; max-width:275px; margin:0 auto; padding: 50px 0 20px 0;">
                    </div>
                </div>
                <div class="link_box center" style="margin: 50px 0; margin-bottom:0;">
                    <a href="<?php echo Router::getRoute('agenda'); ?>" class="link">View Full Agenda<span class="icon icon-arrow_right" style="top:2px"></span></a>
                </div>
            </div>
        </div>
    </section><!-- section -->
 
    <section id="speakers" class="section section_users silver active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center" style="margin-bottom:30px">Speakers at Bits &amp; Pretzels 2016.</h1>
<!--                            <h2 class="down_sub_title center">Because Visions do come from Visionaries.</h2>-->
                            <div class="descr descr_small_margin center">Our Bits &amp; Pretzels speakers will open up new perspectives for your startup in countless awesome sessions and panel discussions.
                            </div>
                        </div>
                        <div class="wrap_speakers">
                            <div class="conteiner">
                                <div class="list_users">
                                       <div class="user_box">
                                            <a href="<?php echo Router::getRoute('spacey'); ?>"><div class="user-offset">
                                                    <div class="img_user">
                                                        <img class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="/src/images/speakers/default/spacey.png" alt="">
                                                        <noscript>
                                                            <img class="lazyload-scroll" src="/src/images/speakers/default/spacey.png" alt="">
                                                        </noscript>
                                                    </div>
                                                    <p class="name_user">Kevin Spacey</p>
                                                    <div style="color:#7a7a7a" class="title">Oscar-winning Actor,</div>
                                                    <div style="color:#7a7a7a" class="company">Producer, Startup Investor</div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="user_box">
                                            <a href="<?php echo Router::getRoute('branson'); ?>"><div class="user-offset">
                                                    <div class="img_user">
                                                        <img class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="/src/images/speakers/default/branson.jpg" alt="">
                                                        <noscript>
                                                            <img class="lazyload-scroll" src="/src/images/speakers/default/branson.jpg" alt="">
                                                        </noscript>
                                                    </div>
                                                    <p class="name_user">Richard Branson</p>
                                                    <div style="color:#7a7a7a" class="title">Founder</div>
                                                    <div style="color:#7a7a7a" class="company">Virgin Group</div>
                                                </div></a>
                                        </div>
                                    <?php foreach(People::setType('speaker')->setDataSource($speakers)->get(2, 12) as $person):  ?>
                                        <div class="user_box">
                                            <div class="user-offset">
                                                <div class="img_user">
                                                    <img class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt="">
                                                    <noscript>
                                                        <img class="lazyload-scroll" src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt="">
                                                    </noscript>
                                                </div>
                                                <p class="name_user"><?php echo $person['name']; ?></p>
                                                <div class="title"><?php echo $person['title']; ?></div>
                                                <div class="company"><?php echo $person['company']; ?></div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div class="link_box center"><a href="<?php echo Router::getRoute('speakers-startup'); ?>" class="link">View All Speakers<span class="icon icon-arrow_right"></span></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
    
    <section id="clusters" class="section section_users active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center" style="margin-bottom:2px">Brand-new: Our Industry Clusters</h1>
                            <div class="descr descr_small_margin center" style="margin-top:50px">
                                <div class="link_box">
                                    Bits &amp; Pretzels 2016 will be divided into six clusters, representing the most important mega trends of the digital entrepreneurship industry. Based on those clusters, we will create rooms and ways for startup founders, investors and journalists to network and learn in the best way possible.
                                </div>
                            </div>
                        </div>
                        <div class="wrap_speakers">
                            <div class="conteiner">
                                <div class="list_users">
                                    <a href="/cluster/commerce">
                                        <div class="user_box" style="margin-bottom:0">
                                            <div class="user-offset">
                                                <img class="lazyload-scroll" style="width:100%" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="/src/images/cluster/full_icon/commerce.svg" alt="Future Commerce">
                                                <noscript>
                                                    <img class="lazyload-scroll" style="width:100%" src="/src/images/cluster/full_icon/commerce.svg" alt="Future Commerce">
                                                </noscript>
                                                <p class="name_user commerceorange" style="margin-top:10px; font-size:1.2em">Future Commerce</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="/cluster/mobility">
                                        <div class="user_box" style="margin-bottom:0">
                                            <div class="user-offset">
                                                <img class="lazyload-scroll" style="width:100%" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="/src/images/cluster/full_icon/mobility.svg" alt="Fast Mobility">
                                                <noscript>
                                                    <img class="lazyload-scroll" style="width:100%" src="/src/images/cluster/full_icon/mobility.svg" alt="Fast Mobility">
                                                </noscript>
                                                <p class="name_user mobileorange" style="margin-top:10px; font-size:1.2em">Fast Mobility</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="/cluster/lifestyle">
                                        <div class="user_box" style="margin-bottom:0">
                                            <div class="user-offset">
                                                <img class="lazyload-scroll" style="width:100%" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="/src/images/cluster/full_icon/lifestyle.svg" alt="Hot Lifestyle">
                                                <noscript>
                                                    <img class="lazyload-scroll" style="width:100%" src="/src/images/cluster/full_icon/lifestyle.svg" alt="Hot Lifestyle">
                                                </noscript>
                                                <p class="name_user pink" style="margin-top:10px; font-size:1.2em">Hot Lifestyle</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="/cluster/iot">
                                        <div class="user_box" style="margin-bottom:0">
                                            <div class="user-offset">
                                                <img class="lazyload-scroll" style="width:100%" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="/src/images/cluster/full_icon/iot.svg" alt="Sophisticated IoT">
                                                <noscript>
                                                    <img class="lazyload-scroll" style="width:100%" src="/src/images/cluster/full_icon/iot.svg" alt="Sophisticated IoT">
                                                </noscript>
                                                <p class="name_user purple" style="margin-top:10px; font-size:1.2em">Sophisticated IoT</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="/cluster/smart-company">
                                        <div class="user_box" style="margin-bottom:0">
                                            <div class="user-offset">
                                                <img class="lazyload-scroll" style="width:100%" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="/src/images/cluster/full_icon/smart_company.svg" alt="Smart Company">
                                                <noscript>
                                                    <img class="lazyload-scroll" style="width:100%" src="/src/images/cluster/full_icon/smart_company.svg" alt="Smart Company">
                                                </noscript>
                                                <p class="name_user darkgreen" style="margin-top:10px; font-size:1.2em">Smart Company</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="/cluster/money">
                                        <div class="user_box" style="margin-bottom:0">
                                            <div class="user-offset">
                                                <img class="lazyload-scroll" style="width:100%" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="/src/images/cluster/full_icon/money.svg" alt="Big Money">
                                                <noscript>
                                                    <img class="lazyload-scroll" style="width:100%" src="/src/images/cluster/full_icon/money.svg" alt="Big Money">
                                                </noscript>
                                                <p class="name_user green" style="margin-top:10px; font-size:1.2em">Big Money</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div id="buyticket" class="button_mb choose-cat-trigger-wrapper" style="margin-top:30px; margin-bottom:20px;">
                                <div class="button_box button_box_orange button_box_auto trigger_app_nav">
                                        <a href="#category" class="button orange"><span>Register for 2017 Ticket Sale</span></a>
                                </div>
                            </div>
                            <div class="link_box" style="display:block">
                                <a href="<?php echo Router::getRoute('cluster'); ?>" class="link">More info about the clusters<span class="icon icon-arrow_right"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section id="captain" class="section section_users_right silver active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="content_section">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="grid grid-2 grid-2-simple clearfix">
                            <div class="col col-1">
                                <div class="grid-offset">
                                    <div class="info_box">
                                        <h1 class="main_title">Choose an expert-hosted table<br>at the Oktoberfest.</h1>
<!--                                        <h2 class="down_sub_title">O captain! My captain! May I join your table.</h2>-->
                                        <div class="descr descr_small_margin">There’s this shy species everybody speaks about and only few catch a glimpse of: infuential executives. But once a year they gather at the Bits &amp; Pretzels Oktoberfest tables. Lured by socialbility and lots of fresh talents. Choose one of the CEOs, editors-in-chief or investors as your Table Captain and enjoy their experience!
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-2">
                                <div class="grid-offset">
                                    <div class="conteiner">
                                        <div class="list_users">
                                            <?php foreach(People::setType('table-captain')->setDataSource($tcs)->get(0, 8) as $key => $tc):  ?>
                                                <div class="user_box <?php echo ($key>7) ? 'hidden_thirteen' : ''; ?>">
                                                    <div class="user-offset">
                                                        <div class="img_user">
                                                            <img class="lazyload-scroll" class="on-cat-site" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="/src/images/table-captains/default/<?php echo $tc['image']; ?>" alt="">
                                                            <noscript>
                                                                <img class="lazyload-scroll" class="on-cat-site" src="/src/images/table-captains/default/<?php echo $tc['image']; ?>" alt="">
                                                            </noscript>
                                                        </div>
                                                        <p class="name_user"><?php echo $tc['name']; ?></p>
                                                        <div class="title"><?php echo $tc['title']; ?></div>
                                                        <div class="company"><?php echo $tc['company']; ?></div>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="link_box center" style="width:100%">
                                <a href="<?php echo Router::getRoute('captains'); ?>" class="link">View all Table Captains<span class="icon icon-arrow_right"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
    
    <section id="academy" class="section section_wbox wbox_2 active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="content_section">
            <div class="wrapper_inner center">
                <div class="valign">
                    <div class="middle">
                        <div class="wbox">
                            <h1 class="main_title"style="margin-bottom:30px">Academy: Enjoy Masterclasses,<br> specially designed for startups</h1>
<!--                            <h2 class="down_sub_title center">Start with an investment. Never stop with improvement.</h2>-->
                            <div class="descr descr_small_margin">
                                We love learning! And as founders we know that founding is a process of constant learning. That’s why startups can expect 13 masterclasses from experienced entrepreneurs like Neil Patel, Florian Gschwandtner or Carsten Maschmayer on our academy stage.
<!--                                That’s why we want to support you with the Bits &amp; Pretzels Academy. A platform to reach out, connect and learn from the best. With a growing number of selected, high-quality lectures of experienced entrepreneurs and startup experts. Pay attention!-->
                            </div>
                            <div class="link_box" style="margin-top:25px">
                                <a href="<?php echo Router::getRoute('academy'); ?>" class="link">All Masterclasses<span class="icon icon-arrow_right"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
    <section id="journalists" class="section section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="padding-bottom:30px;">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center" style="margin-bottom:30px">Take your chance <br>to meet 400+ journalists.</h1>
<!--                            <h2 class="down_sub_title center">So what’s the news? Maybe you!</h2>-->
                            <div class="descr descr_small_margin center">
                                Always dreamed of your startup being mentioned in papers like SZ or WIRED? At Bits &amp; Pretzels you can give the media and the public a chance to become aware of your ideas. 
                            In 2016 hundreds of national and international journalists will cover the conference hunting for an exciting story. So how about you start telling your story!
<!--
                            What’s the difference between new and news? Public awareness. <br>At Bits &amp; Pretzels you can give the media and the public a chance to become aware of your ideas. 
                            In 2016 hundreds of national and international journalists will cover the conference hunting for an exciting story. So how about you start telling your story!
-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="list_logos logos-grid-5 clearfix" style="margin:50px auto;">
                    <?php include 'layout/media-slider.php'; ?>
                </div>
            </div>
        </div>
    </section><!-- section -->
    <section id="networking" class="section section_devices silver active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="padding-bottom:0;">
        <div class="content_section">
            <div class="wrapper_inner">
                <div class="grid grid-2 grid-2-simple clearfix">
                    <div class="col col-1">
                        <div class="grid-offset">
                            <div class="valign">
                                <div class="middle">
                                    <h1 class="main_title" style="margin-bottom:30px">
                                    Find people who really matter to you by using our networking app.
                                    </h1>
                                    <div class="descr descr_small_margin">
                                    You’ve already scheduled meetings with 3 investors and 2 well-known jounalists – before the event even started. 
                                    Wouldn’t you consider this a success? With our newly developed networking app we make this possible for you. 
                                    Just install the app, find interesting contacts, connect with one click and directly schedule a meeting at the event!
<!--                                    <br><br>You will receive your login details for the App vie e-Mail after you purchased your ticket.-->
                                    </div>
                                    
<!--                                    <h2 class="down_sub_title">One click is all you need <br>to click with someone.</h2>-->
<!--                                    <h1 class="main_title">Find people who really matter to you by using our newly developed networking app.</h1>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-2">
                         <div class="grid-offset">
                            <div class="valign">
                                <div class="middle">
                                    <div class="img_box">
                                        <a href="//vimeo.com/174807183" class="video-play-button" alt="play" data-lity>
                                            <svg style="position:absolute; z-index:1"version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0 0 104 104" enable-background="new 0 0 104 104" xml:space="preserve">
                                            <path fill="none" stroke="#ea5c3f" stroke-width="4" stroke-miterlimit="10" d="M26,35h52L52,81L26,35z"/>
                                            <circle class="video-play-circle" fill="none" stroke="#ea5c3f" stroke-width="4" stroke-miterlimit="10" cx="52" cy="52" r="50"/>
                                            </svg>
                                            <span class="video-play-outline"></span>
                                        </a>
                                        <img class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="/src/images/devices.png" style="z-index:-4"alt="Devices">
                                        <noscript>
                                            <img src="/src/images/devices.png" class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="z-index:-4"alt="Devices">
                                        </noscript>
                                    </div>
                                </div>
                            </div>
                         </div>
                    </div>
                    <div class="link_box center" style="width:100%; margin:50px">
                        <a href="https://bitsandpretzels.pathable.com/" target="_blank" class="link">To the networking app<span class="icon icon-arrow_right" style="top:2px"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
    <section id="sponsors" class="section section_logo active section_1 section_pt white" data-show-inner="false" data-show-footer="false">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center" style="margin-bottom:40px">Bits &amp; Pretzels would not be Bits &amp; Pretzels without our sponsors.</h1>
                            <div class="descr descr_small_margin center" style="margin-bottom:50px">
                            Thank you. Thank you. Thank you. That’s basically all we need to say about this list of our sponsors. Your help is the priceless foundation for establishing Bits &amp; Pretzels as the biggest festival for founders in Europe.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="list_logos logos-grid-5 clearfix" style="margin:0 auto">
                    <?php include 'layout/sponsor-slider.php'; ?>
                </div>
                <div id="buyticket" class="button_mb choose-cat-trigger-wrapper" style="margin-top:50px; margin-bottom:20px">
                    <div class="button_box button_box_orange button_box_auto trigger_app_nav">
                            <a href="#category" class="button orange"><span>Register for 2017 Ticket Sale</span></a>
                    </div>
                </div>
                <div class="link_box" style="margin-top:0px">
                    <a href="<?php echo Router::getRoute('partners'); ?>" class="link">View all Partners<span class="icon icon-arrow_right"></span></a>
                </div>
            </div>
        </div>
    </section>
    <section id="diashow" class="section_logo active section_1 section_pt silver" data-show-inner="false" data-show-footer="false" style="padding-bottom:0 !important; height:auto !important; max-height:700px; overflow:hidden;">
        <div class="center">
                <h1 class="main_title center" style="margin-bottom:40px">Get a few more impressions…</h1>
                    <?php include 'layout/slider_diashow.php'; ?>
        </div>
    </section>
</div>
<!-- END: content section -->
<?php

	include 'layout/signup-form.php';
	include 'layout/footer.php';
	include 'layout/bottom.php';