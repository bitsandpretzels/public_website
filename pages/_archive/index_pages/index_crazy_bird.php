<?php include 'layout/top.php';?>
<?php // include 'layout/header.php';?>

<div class="inner_sections">
    <div id="sub_nav" class="sub_nav_wrap fixed">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li" style="opacity:0">
                    <a href="#content" class="link_nav" data-section="content">The Event</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="#clusters" class="link_nav" data-section="clusters">Clusters</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="#captain" class="link_nav" data-section="captain">Table Captains</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="#academy" class="link_nav" data-section="academy">Academy</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="#networking" class="link_nav" data-section="networking">Networking App</a>
                </li>
                <li class="hidden_li" style="margin-right:81px; opacity:0">
                    <a href="#sponsors" class="link_nav" data-section="sponsors">Partners</a>
                </li>
                <li>
                    <a href="#eventbrite" class="link_nav buyticket choose-cat-trigger-wrapper" data-section="buyticket">Get 2017 Tickets</a>
                </li>
            </ul>
        </div>
    </div>

<!-- START: Revolution Slider -->
<?php include_once ('layout/headers/after_event_revslider.php');?>
<!-- END: Revolution Slider -->



<section id="eventbrite" class="section section_users white active section_1 section_pt" data-show-inner="false" data-show-footer="false">
       <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
<!--
                            <div class="descr descr_small_margin">
                                <div style="margin-top:0; margin-bottom:60px">
                                    We will stay consistent with our tradition to top your experience every year and already began with the preparation for Bits &amp; Pretzels 2017. The Festival will take place from September 24th till September 26th 2017 in Munich.

                                </div>
                            </div>
                            <h1 class="main_title center" style="margin-bottom:60px; margin-top:20px">
                                Crazy Bird Ticket Sale will start at 28/09/2016 at 4.00 pm CET!
                            </h1>
-->
                        </div>
                    </div>
                </div>
                <div class="descr descr_big_margin" style="color:#ea5c37; margin-bottom:50px">
                    <strong>++++++ Sorry, Crazy Bird Tickets are sold out! ++++++</strong>
                </div> 
                <div class="newsletterbar center clearfix" style="background:none; margin-bottom:60px">
                    <div class="wrapper">
                        <form name="" method="post" action="//bitsandpretzels.us8.list-manage.com/subscribe/post?u=59db0da2c55155bbedfa0c507&amp;id=3c5efc9e7b" id="newsletterForm">
                          <div class="wrap_field">
                            <input autocomplete="off" type="email" name="EMAIL" placeholder="Be the first to get notified about Early Bird Ticket Sale" class="text_field" id="emailInput" style="border:1px solid #005690">
                          </div>
                          <input type="hidden" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>" name="IPADRESS">
                          <input type="hidden" value="NL Subscriber" name="SOURCE">
                          <div style="position: absolute; left: -5000px;">
                            <input type="text" name="b_59db0da2c55155bbedfa0c507_3c5efc9e7b" tabindex="-1" value="">
                          </div>
                          <button 
                          type="submit" 
                          id="emailSubmit" 
                          disabled="disabled" 
                          class="button disabled" 
                          data-toggle="tooltip" 
                          data-placement="bottom" 
                          title="Sign up">Sign me up!</button>
                        </form>
                    </div>
                </div>
<!--                <div style="width:100%; text-align:left;" ><iframe  src="//eventbrite.com/tickets-external?eid=27797199171&ref=etckt" frameborder="0" height="530" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="auto" allowtransparency="true"></iframe><div style="font-family:Helvetica, Arial; font-size:10px; padding:5px 0 5px; margin:2px; width:100%; text-align:left;" ><a class="powered-by-eb" style="color: #dddddd; text-decoration: none;" target="_blank" href="http://www.eventbrite.com/l/registration-online/"></a></div></div>-->
                <div class="wrapp_main_text">
                    <div class="descr descr_small_margin">
                        <div style="margin-top:0; margin-bottom:60px">
                            Bits &amp; Pretzels connects more than 5.000 participants with founders of Virgin (Richard Branson), Airbnb, Evernote, Kayak, Home24, Runtastic, Delivery Hero and many more. Bits & Pretzels creates unique networking around Munich Oktoberfest and encourages meaningful relationships between founders, investors and the media.
                        </div>
                    </div>
                    <div class="center" style="width:100%">
                        <div style="width:57px; height:57px; margin: 0 auto; background:url('/src/images/logo_bitsandpretzels.svg')"></div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
<?php

	include 'layout/signup-form.php';
	include 'layout/footer.php';
	include 'layout/bottom.php';
