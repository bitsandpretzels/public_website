<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>


<div class="inner_sections students-in-tech-page">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>

    <!--
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

    <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section mobile_hidden">Students</div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>            
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                </li>
                <li>
                    <a class="link_nav buyticket trigger_app_nav" data-section="buyticket">Get your Ticket</a>
                </li>
            </ul>
        </div>
    </div>

    <section id="section_top" class="flex-center section section_top active section_1 student" data-show-inner="false" data-show-footer="false">

      <div class="category-section-title">
        <h1> Student </h1>
        <div class="descr sub_line center">Get a free student ticket</div>
      </div>

    </section><!-- section -->


    <section class="section white active section_1 section_pt" style="padding-bottom: 30px">

        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">Attention everyone: Students wanted!</h1>
                            <div class="descr center">
                                Today’s students are the founders of the future. That’s why we’re looking for students who are interested in entrepreneurship, tech and the most recent innovations on earth. There are countless brilliant young people who want to make a difference, and we want to support them by giving them the chance to attend Bits & Pretzels to get inspired and motivated.
                                <br>
                                <br>
                                <strong>That’s why we’re giving away 150 free tickets <br>for aspiring students with gamechanging ideas.</strong>
                                <br>
                                <br>
                                <div class="link_box center">
                                	<a class="link" href="<?php echo Router::getRoute('student') ?>">
                                		Check out why you should join Bits & Pretzels
                                	</a>
                                </div>
                            </div>
                        </div>
                        <div class="grid grid-1 grid-1-simple clearfix container_info" style="padding-top: 0px; margin: 0 auto;display:flex; justify-content: center; ">
                            <div class="col quote" style="margin: 0 auto">
                                <div class="info_box">
                                    <h1 class="main_title center" style="max-width:500px">“Inspiring and motivating! I got loads of business cards, ideas and and a vision of how to go on with my startup in the next month.”</h1>
                                    <div class="list_users clearfix">
                                        <div class="user_box" style="float:none;margin: 0 auto">
                                            <div class="user-offset">
                                                <div class="img_user"><img src="/src/images/testimonials/default/nolden.png" alt=""></div>
                                                <p class="name_user">Nils Nolden</p>
                                                <p>System Engineer</p>
                                                <p>Freelancer</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->


<section id="partners" class="section section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="padding-top: 0px;padding-bottom: 30px;min-height: 0px;display:none">

        <div class="content_section center" style="min-height: 0px">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">

                        <div class="wrapp_main_text">
                            <h1 class="main_title center" style="margin:0px">Supporters</h1>
                        </div>


                        <div class="list_logos logos-grid-5 mb0 clearfix" style="margin-top:30px">


                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><img src="/src/images/logos/women/blooming_founders_logo-small.png" alt=""></div>
                                </div>
                            </div>
                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><img src="/src/images/logos/women/fempreneur_logo.png" alt=""></div>
                                </div>
                            </div>
                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><img src="/src/images/logos/women/bsg.png" alt=""></div>
                                </div>
                            </div>

                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><img src="/src/images/logos/women/femtrepreneur.png" alt=""></div>
                                </div>
                            </div>

                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><img src="/src/images/logos/women/womens_startup_lab.png" alt=""></div>
                                </div>
                            </div>

                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><img src="/src/images/logos/women/sheworks.png" alt=""></div>
                                </div>
                            </div>


                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><img src="/src/images/logos/women/ff.png" alt=""></div>
                                </div>
                            </div>


                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><img src="/src/images/logos/women/not_another_woman_magazine.png" alt=""></div>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->





    <section class="section section_wbox students-in-tech active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <a name="apply"></a>

        <div class="content_section">
            <div class="wrapper_inner center">
                <div class="valign">
                    <div class="middle">

                        <h1 class="main_title text-shadow" style="color:white">You are one of those students. <br> Then you should immediately apply here until July 3rd 2016*</h1>

                        <div class="wbox">


                           <form class="jotform-form" action="https://submit.jotformeu.com/submit/61380834720352/" method="post" enctype="multipart/form-data" name="form_60284187128357" id="60284187128357" accept-charset="utf-8" style="max-width: 550px;margin: 0 auto">

                                            <!--
                                            <div id="errors_message" class="error_message">
                                                <div class="field-col">Please, fill the required fields!</div>
                                            </div>
                                            -->
                                            <div class="field_wrap">
                                                <div class="label-col">
                                                    <label for="first_name" class="main_label">First Name:<span class="required">*</span></label>
                                                </div>
                                                <div class="field-col">
                                                    <input id="first_name" required name="q1_vollstandigerName1[first]" class="texfield_form" autocomplete="off" type="text">
                                                </div>
                                            </div>
                                            <div class="field_wrap">
                                                <div class="label-col">
                                                    <label for="last_name" class="main_label">Last Name:<span class="required">*</span></label>
                                                </div>
                                                <div class="field-col">
                                                    <input id="last_name" required name="q1_vollstandigerName1[last]" class="texfield_form" autocomplete="off" type="text">
                                                </div>
                                            </div>
                                            <div class="field_wrap">
                                                <div class="label-col">
                                                    <label for="email" class="main_label">Email Address:<span class="required">*</span></label>
                                                </div>
                                                <div class="field-col">
                                                    <input id="email" required name="q6_email6" class="texfield_form" autocomplete="off" type="email">
                                                </div>
                                            </div>

                                            <div class="field_wrap">
                                                <div class="label-col">
                                                    <label for="email" class="main_label">Phone Number:<span class="required">*</span></label>
                                                </div>
                                                <div class="field-col clearfix">
                                                    <input style="width:30%;float:left" placeholder="+049 Countrycode" id="email" required name="q4_phoneNumber[area]" class="texfield_form" autocomplete="off" type="text">
                                                    <input style="width:70%;float:left" placeholder="Phone number" id="email" required name="q4_phoneNumber[phone]" class="texfield_form" autocomplete="off" type="text">
                                                </div>
                                            </div>

                                            <div class="field_wrap">
                                                <div class="label-col">
                                                    <label for="email" class="main_label">LinkedIn/XING<span class="required">*</span></label>
                                                </div>
                                                <div class="field-col">
                                                    <input id="email" required name="q7_linkedin" class="texfield_form" autocomplete="off" type="text">
                                                </div>
                                            </div>

                                            <div class="field_wrap">
                                                <div class="label-col">
                                                    <label for="email" class="main_label">University<span class="required">*</span></label>
                                                </div>
                                                <div class="field-col">
                                                    <input id="email" required name="q9_university" class="texfield_form" autocomplete="off" type="text">
                                                </div>
                                            </div>

                                            <div class="field_wrap clearfix">
                                                <div class="label-col">
                                                    <label for="email" class="main_label">Faculty<span class="required">*</span></label>
                                                </div>
                                                <div class="field-col clearfix">
                                                    <select name="q10_faculty" style="border:1px solid #BCBCBC;width:100%;padding:13px;border-radius:3px;text-indent: 5px;line-height: 26px;">
                                                        <option value="Business & Economics">Business &amp; Economics</option>
                                                        <option value="Humanities">Humanities</option>
                                                        <option value="Engineering">Engineering</option>
                                                        <option value="Informatics">Informatics</option>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="field_wrap clearfix">
                                                <div class="label-col">
                                                    <label for="email" class="main_label">Course of Studies<span class="required">*</span></label>
                                                    <br>
                                                </div>
                                                <div class="field-col">
                                                    <input id="email" required name="q11_courseOf" class="texfield_form" autocomplete="off" type="text">
                                                </div>
                                            </div>

                                            <div class="field_wrap clearfix">
                                                <div class="label-col">
                                                    <label for="email" class="main_label">Intended Degree<span class="required">*</span></label>
                                                </div>
                                                <div class="field-col clearfix">
                                                    <select name="q13_intendedDegree" style="border:1px solid #BCBCBC;width:100%;padding:13px;border-radius:3px;text-indent: 5px;line-height: 26px;">
                                                        <option value="Bachelor">Bachelor</option>
                                                        <option value="Master">Master</option>
                                                        <option value="PhD">PhD</option>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="field_wrap">
                                                <div class="label-col">
                                                    <label for="email" class="main_label" style="line-height: normal">Expected End of Studies <span class="required">*</span></label>
                                                </div>
                                                <div class="field-col clearfix">
                                                    <div style="margin: 0 auto">
                                                        <input placeholder="Month" style="width:25%;float:left;" id="email" required name="q14_expectedEnd[month]" class="texfield_form" autocomplete="off" type="text">
                                                        <input placeholder="Day" style="width:25%;float:left;margin-left:10px" id="email" required name="q14_expectedEnd[day]" class="texfield_form" autocomplete="off" type="text">
                                                        <input placeholder="Year" style="width:25%;float:left;margin-left:10px" id="email" required name="q14_expectedEnd[year]" class="texfield_form" autocomplete="off" type="text">
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="field_wrap">
                                                <div class="label-col">
                                                    <label for="email" class="main_label" style="line-height: normal">What are you looking for?<span class="required"></span></label>
                                                </div>
                                                <div class="field-col">
                                                    <label for="input_35_0">Internship</label>
                                                    <input type="radio" id="input_35_0" name="q15_whatAre" value="Internship" style="display: inline-block;">
                                                    <br>
                                                    <br>
                                                    <label for="input_35_1">Working Student Job</label>
                                                    <input type="radio" class="form-checkbox validate[required]" id="input_35_1" name="q15_whatAre" value="Working Student Job" style="display: inline-block;">
                                                    <br>
                                                    <br>
                                                    <label for="input_35_2">Trainee Program</label>
                                                    <input type="radio" class="form-checkbox validate[required]" id="input_35_2" name="q15_whatAre" value="Trainee Program" style="display: inline-block;">
                                                    <br>
                                                    <br>
                                                    <label for="input_35_3">Permanent Employment</label>
                                                    <input type="radio" class="form-checkbox validate[required]" id="input_35_3" name="q15_whatAre" value="Permanent Employment" style="display: inline-block;">
                                                    <br>
                                                    <br>
                                                    <label for="input_35_4">Founding a Startup</label>
                                                    <input type="radio" class="form-checkbox validate[required]" id="input_35_4" name="q15_whatAre" value="Founding a Startup" style="display: inline-block;">

                                                 </div>
                                            </div>

                                            <div class="field_wrap">
                                                <div class="label-col">
                                                    <label for="input_16" class="main_label" style="line-height: normal">Upload Certificate of Enrollment<span class="required">*</span></label>
                                                </div>
                                                <div class="field-col">
                                                    <input class="form-upload validate[required]" type="file" id="input_16" name="q16_uploadCertificate16" file-accept="pdf, doc, docx, xls, xlsx, csv, txt, rtf, html, zip, mp3, wma, mpg, flv, avi, jpg, jpeg, png, gif" file-maxsize="8000" file-minsize="0" file-limit="1">
                                                </div>
                                            </div>


                                            <div class="field_wrap">
                                                <div class="field-col"><div class="line_form"></div></div>
                                            </div>
                                            <div class="field_wrap">
                                                <div class="label-col">
                                                    <label for="brthday" class="main_label label_required">* indicates required</label>
                                                </div>
                                                <div class="field-col">
                                                    <input type="text" name="b_59db0da2c55155bbedfa0c507_0a0c825bee" tabindex="-1" value="" style="display:none">
                                                    <input class="button orange submit_button" value="Apply" type="submit">
                                                </div>
                                            </div>
                                            <div lcass="field_wrap">
                                            	<a style="color:gray;font-size:12px;font-style:italic" href="<?php echo Router::getRoute('tos-students-in-tech'); ?>">*Conditions of participation</a>
                                            </div>
                                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


</div>

<?php include realpath('layout/signup-form.php');?>
<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>
