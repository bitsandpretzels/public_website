<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>

    <!--
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

    <div id="sub_nav" class="sub_nav_wrap">

        <div class="wrap_subnav_inner">
            <div class="title_section linkMenu"><span class="mobile_hidden">Choosen Category: </span>Student</div>
            <ul class="sub_nav ul-reset">

                <li class="hidden_li">
                    <a href="#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>

                <li class="hidden_li">
                    <a href="#academy" class="link_nav" data-section="speakers">Academy</a>
                </li>
                <li class="hidden_li">
                    <a href="#attendees" class="link_nav" data-section="investors">Attendees</a>
                </li>
                <li class="hidden_li">
                    <a href="#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                </li>

                <li class="hidden_li">
                    <a href="#investors" class="link_nav" data-section="pitc">Investors</a>
                </li>
                <li class="hidden_li">
                    <a href="#captain" class="link_nav" data-section="captain">Table Captains</a>
                </li>
                
                <li>
                    <a href="<?php echo Router::getRoute('buy_student'); ?>" class="link_nav buyticket" data-section="buyticket">Apply</a>
                </li>
                
            </ul>
        </div>
    </div>


    <section id="section_top" class="flex-center section section_top active section_1 student" data-show-inner="false" data-show-footer="false">

      <div class="category-section-title">
        <h1> Student </h1>
        <div class="descr sub_line center">Seven good reasons for students to join Bits & Pretzels!</div>
      </div>

    </section><!-- section -->

   <!-- section Oktoberfest -->
   <?php include_once realpath('layout/elements/oktoberfest.php');?>
    <!-- section visionaires -->
   <?php include_once realpath('layout/elements/section-visionaires.php');?>



    <section id="academy" class="section section_wbox wbox_2 active section_1 section_pt" data-show-inner="false" data-show-footer="false">

        <div class="content_section"> <!-- paralax_section class ausgeblendet-->
            <div class="wrapper_inner center">
                <div class="valign">
                    <div class="middle">
                        <div class="wbox">
                            <h1 class="main_title">Academy: Start with an investment.<br>Never stop with improvement.</h1>
                            <div class="descr descr_small_margin">As founders we know that founding is a process of constant learning. That’s why we want to support founders and future founders with the Bits &amp; Pretzels Academy. A platform to reach out, connect and learn from the best. With a growing number of selected, high-quality lectures of experienced entrepreneurs and startup experts. Pay attention!<div class="link_box"><a href="<?php echo Router::getRoute('academy', true); ?>" class="link">More info & all masterclasses<span class="icon icon-arrow_right"></span></a></div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->

    <section id="attendees" class="section section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false">

        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">How do you catch a dream job?<br>Go to their breeding ground.</h1>
                            <h2 class="down_sub_title center">Find a job at the hottest startups or the big players.</h2>
                            <div class="descr descr_small_margin center">You are tired of writing thousands of emails to HR? Stop writing and go where the jobs are born. At Bits &amp; Pretzels you will meet hundreds of rising startups and innovative corporations. And psssst! They go to Bits &amp; Pretzels to find awesome talents like you! So place your details in our data bank and show up! <div class="link_box"><!--<a href="" class="link">View all Attendees<span class="icon icon-arrow_right"></span></a>--></div>
                            </div>

                        </div>

                        <?php include_once realpath('layout/elements/section-startups.php'); ?>

                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->

    <section id="schedule" class="section section_devices silver active section_1 section_pt" data-show-inner="false" data-show-footer="false">

        <div class="content_section">
            <div class="wrapper_inner">
                <div class="grid grid-2 grid-2-simple clearfix">
                    <div class="col col-1">
                        <div class="grid-offset">
                            <div class="valign">
                                <div class="middle">
                                    <h1 class="main_title">One click is all you need <br>to click with someone.</h1>
                                    <h2 class="down_sub_title">Find people who really matter to you by using our newly developed networking tool.</h2>
                                    <div class="descr descr_small_margin">You’ve already scheduled meetings with 3 investors and 2 well-known jounalists – before the event even started. Wouldn’t you consider this a success? With our newly developed networking app we make this possible for you. Just install the app, find interesting contacts, connect with one click and directly schedule a meeting at the event!</div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-2">
                         <div class="grid-offset">
                            <div class="valign">
                                <div class="middle">
                                    <div class="img_box">
                                        <a href="//vimeo.com/174807183" class="video-play-button" alt="play" data-lity>
                                            <svg style="position:absolute; z-index:1"version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0 0 104 104" enable-background="new 0 0 104 104" xml:space="preserve">
                                            <path fill="none" stroke="#ea5c3f" stroke-width="4" stroke-miterlimit="10" d="M26,35h52L52,81L26,35z"/>
                                            <circle class="video-play-circle" fill="none" stroke="#ea5c3f" stroke-width="4" stroke-miterlimit="10" cx="52" cy="52" r="50"/>
                                            </svg>
                                            <span class="video-play-outline"></span>
                                        </a>
<!--                                        <a href="//vimeo.com/174807183" style="width:100px; position:absolute; top:calc(50% - 50px); left:calc(50% - 50px);" alt="play" data-lity><img src="/src/images/play-circle-outline.png" alt="play" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);"></a>-->
                                        <img class="lazyload-scroll" data-src="/src/images/devices.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                        <noscript>
                                            <img class="lazyload-scroll" src="/src/images/devices.png" style="z-index:-4"alt="Devices">
                                        </noscript>
                                    </div>
                                </div>
                            </div>
                         </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->


    <section id="investors" class="section section_logo active section_1 section_pt" data-show-inner="false" data-show-footer="false">

        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">How do you catch investors?<br>Go to their hunting ground.</h1>
                            <h2 class="down_sub_title center">Meet 400+ investors hunting for the hottest ideas.</h2>
                            <div class="descr descr_small_margin center">You are tired of writing thousands of emails to investors which probably don’t get read?
                                Stop writing and go where the investors are. At Bits & Pretzels you can meet hundreds of Business Angels or institutional investors.
                                Schedule meetings or join their office hours. They go to Bits & Pretzels only to find awesome startups. Don’t miss out on getting 1 on 1 with them!
                                <div class="link_box"><!--<a href="" class="link">View all Investors<span class="icon icon-arrow_right"></span></a>--></div>
                            </div>

                        </div>

                        <?php include_once realpath('layout/elements/section-vc.php'); ?>

                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->

    <section id="captain" class="section section_users_right white silver active section_1 section_pt" data-show-inner="false" data-show-footer="false">

        <div class="content_section">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="grid grid-2 grid-2-simple mb60 clearfix">
                            <div class="col col-1">
                                <div class="grid-offset">
                                    <div class="info_box">
                                        <h1 class="main_title">O captain! My captain!<br>May I join your table.</h1>
                                        <h2 class="down_sub_title">Join an expert-hosted table at the Oktoberfest.</h2>
                                        <div class="descr descr_small_margin">There’s this shy species everybody speaks about and only few catch a glimpse of: infuential executives. But once a year they gather at the Bits &amp; Pretzels Oktoberfest tables. Lured by socialbility and lots of fresh talents. Choose one of the CEOs, founders or investors as your Table Captain and enjoy the personal insights into this exiting ecosystem!<div class="link_box"><a href="<?php echo Router::getRoute('captains-student'); ?>" class="link">More info &amp; all the Table Captains<span class="icon icon-arrow_right"></span></a></div></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-2">
                                <div class="grid-offset">
                                    <div class="conteiner">
                                        <div class="list_users">
                                            <?php foreach(People::setType('table-captain')->setDataSource($tcs)->get(0, 12) as $key => $tc):  ?>

                                                 <div class="user_box <?php echo ($key>7) ? 'hidden_thirteen' : ''; ?>">
                                                    <div class="user-offset">
                                                        <div class="img_user">                                                             <img class="lazyload-scroll" data-src="/src/images/table-captains/default/<?php echo $tc['image']; ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />                                                             <noscript>                                                                 <img src="/src/images/table-captains/default/<?php echo $tc['image']; ?>" alt="">                                                             </noscript>                                                         </div>
                                                        <p class="name_user"><?php echo $tc['name']; ?></p>
                                                        <div class="title"><?php echo $tc['title']; ?></div>
                                                        <div class="company"><?php echo $tc['company']; ?></div>
                                                    </div>
                                                </div>

                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--
                        <div id="apply" class="button_mb">
                            <div class="button_box button_box_orange button_box_auto">
                                <a href="<?php echo Router::getRoute('buy_student'); ?>" class="button orange"><span>Apply</span></a>
                            </div>
                        </div>
                        -->
                    </div>
                </div>
            </div>
        </div>

        <div id="link_to_top" class="link_to_top link_to_top_abs">
            <div class="svg_arrow">

                <svg version="1.1" id="Layer_1" class="arrow_mouse" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     width="24.01px" height="27.5px" viewBox="0 27.5 24.01 27.5" enable-background="new 0 27.5 24.01 27.5" xml:space="preserve">
                <g>
                    <path fill="#353535" d="M0.289,40.715c0.386,0.385,1.01,0.385,1.395,0l10.322-10.32l10.322,10.32c0.385,0.385,1.009,0.385,1.394,0
                        c0.386-0.386,0.386-1.009,0-1.395L12.779,28.377c-0.023-0.029-0.034-0.064-0.062-0.091c-0.195-0.196-0.455-0.29-0.712-0.286
                        c-0.257-0.004-0.516,0.09-0.713,0.286c-0.027,0.026-0.038,0.063-0.063,0.093L0.289,39.32C-0.096,39.706-0.096,40.33,0.289,40.715z"
                        />
                    <circle fill="#353535" cx="12.006" cy="53" r="2.001"/>
                </g>
                </svg>
            </div>
            <div class="text">Go to top</div>
        </div>
    </section><!-- section -->

</div>

<?php include realpath('layout/signup-form.php');?>
<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>
