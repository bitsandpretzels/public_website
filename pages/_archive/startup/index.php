<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>

    <!--
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

    <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <div class="title_section linkMenu"><span class="mobile_hidden">Choosen Category: </span>Startup</div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li">
                    <a href="#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>
                <li class="hidden_li">
                    <a href="#investors" class="link_nav" data-section="investors">Investors</a>
                </li>
                <li class="hidden_li">
                    <a href="#networking" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li">
                    <a href="#pitch" class="link_nav" data-section="pitc">Pitch</a>
                </li>
                <li class="hidden_li">
                    <a href="#captain" class="link_nav" data-section="captain">Table Captains</a>
                </li>
                <li class="hidden_li">
                    <a href="#academy" class="link_nav" data-section="academy">Academy</a>
                </li>
                <li class="hidden_li">
                    <a href="#business-partner" class="link_nav" data-section="connect">Connect</a>
                </li>
                <li class="hidden_li">
                    <a href="#journalists" class="link_nav" data-section="media">Media</a>
                </li>
                <li>
                    <a href="<?php echo Router::getRoute('buy_startup'); ?>" class="link_nav buyticket" data-section="buyticket">Buy Ticket</a>
                </li>
            </ul>
        </div>
    </div>

    <section id="section_top" class="section section_top active section_1 startup flex-center" data-show-inner="false" data-show-footer="false">

        <div class="category-section-title">
          <h1> STARTUP </h1>
          <div class="descr sub_line center">Nine good reasons for startups to join Bits &amp; Pretzels!</div>

        </div>

        <!--
        <div class="content_section center">
            <div class="wrapper">
                <div class="svg_text">
                    <div class="top_title mobile_only">Startup</div>
                    <div class="inner_svg_text">
                        <div class="descr sub_line">Nine good reasons for startups to join Bits &amp; Pretzels!</div>
                    </div>
                </div>
            </div>
        </div>
      -->
    </section><!-- section -->


   <!-- section Oktoberfest -->
   <?php include_once realpath('layout/elements/oktoberfest.php');?>



    <section id="speakers" class="section section_users silver active section_1 section_pt" data-show-inner="false" data-show-footer="false">

        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">Visions do come from Visionaries.</h1>
                            <h2 class="down_sub_title center">Get inspired by the speakers for Bits &amp; Pretzels 2016.</h2>
                            <div class="descr descr_small_margin center">Our Bits &amp; Pretzels stage and panel discussions host selected speakers that are able to open up new perspectives for your startup.<div class="link_box"><!--<a href="" class="link">View All Speakers<span class="icon icon-arrow_right"></span></a>--></div>
                            </div>

                        </div>

                        <div class="wrap_speakers">
                            <div class="conteiner">
                                <div class="list_users">
                                    <?php foreach(People::setType('speaker')->setDataSource($speakers)->get(0, 12) as $person):  ?>
                                        <div class="user_box">
                                            <div class="user-offset">
                                                <div class="img_user">
                                                    <img class="lazyload-scroll" data-src="/src/images/speakers/default/<?php echo $person['image']; ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                    <noscript>
                                                        <img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt="">
                                                    </noscript>
                                                </div>
                                                <p class="name_user"><?php echo $person['name']; ?></p>
                                                <div class="title"><?php echo $person['title']; ?></div>
                                                <div class="company"><?php echo $person['company']; ?></div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div class="link_box center"><a href="<?php echo Router::getRoute('speakers-startup'); ?>" class="link">View All Speakers<span class="icon icon-arrow_right"></span></a></div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->


    <section id="investors" class="section section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false">

        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">How do you catch investors?<br>Go to their hunting ground.</h1>
                            <h2 class="down_sub_title center">Meet 400+ investors hunting for the hottest startups.</h2>
                            <div class="descr descr_small_margin center">
                            You are tired of writing thousands of emails to investors which probably don’t get read? Stop writing and go where the investors are. At Bits &amp; Pretzels you will meet hundreds of Business Angels and institutional investors. And psssst! They go to Bits &amp; Pretzels to find awesome startups like you.
                            <div class="link_box"><!--<a href="" class="link">Featured Investors<span class="icon icon-arrow_right"></span></a>--></div>
                            </div>

                        </div>

                        <?php include_once realpath('layout/elements/section-vc.php');?>

                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->

    <section id="networking" class="section section_devices silver active section_1 section_pt" data-show-inner="false" data-show-footer="false">

        <div class="content_section">
            <div class="wrapper_inner">
                <div class="grid grid-2 grid-2-simple clearfix">
                    <div class="col col-1">
                        <div class="grid-offset">
                            <div class="valign">
                                <div class="middle">
                                    <h1 class="main_title">One click is all you need <br>to click with someone.</h1>
                                    <h2 class="down_sub_title">Find people who really matter to you by using our newly developed networking tool.</h2>
                                    <div class="descr descr_small_margin">You’ve already scheduled meetings with 3 investors and 2 well-known jounalists – before the event even started. Wouldn’t you consider this a success? With our newly developed networking app we make this possible for you. Just install the app, find interesting contacts, connect with one click and directly schedule a meeting at the event!</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-2">
                         <div class="grid-offset">
                            <div class="valign">
                                <div class="middle">
                                    <div class="img_box">
                                        <a href="//vimeo.com/174807183" class="video-play-button" alt="play" data-lity>
                                            <svg style="position:absolute; z-index:1"version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0 0 104 104" enable-background="new 0 0 104 104" xml:space="preserve">
                                            <path fill="none" stroke="#ea5c3f" stroke-width="4" stroke-miterlimit="10" d="M26,35h52L52,81L26,35z"/>
                                            <circle class="video-play-circle" fill="none" stroke="#ea5c3f" stroke-width="4" stroke-miterlimit="10" cx="52" cy="52" r="50"/>
                                            </svg>
                                            <span class="video-play-outline"></span>
                                        </a>
<!--                                        <a href="//vimeo.com/174807183" style="width:100px; position:absolute; top:calc(50% - 50px); left:calc(50% - 50px);" alt="play" data-lity><img src="/src/images/play-circle-outline.png" alt="play" style="box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);"></a>-->
                                        <img class="lazyload-scroll" data-src="/src/images/devices.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                        <noscript>
                                            <img class="lazyload-scroll" src="/src/images/devices.png" style="z-index:-4"alt="Devices">
                                        </noscript>
                                    </div>
                                </div>
                            </div>
                         </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->

    <section id="pitch" class="section section_wbox wbox_1 active section_1 section_pt" data-show-inner="false" data-show-footer="false">

        <div class="content_section"> <!-- paralax_section class ausgeblendet-->
            <div class="wrapper_inner center">
                <div class="valign">
                    <div class="middle">
                        <div class="wbox">
                            <h1 class="main_title center">What is the first step to convince somebody?<br>The step on our stage.</h1>
                            <h2 class="down_sub_title center">Get your ticket and apply for our pitch competition.</h2>
                            <div class="descr descr_small_margin center">At first you’ll see an intimidatingly huge audience and feel the stage fright growing inside of you. But just an instant later you’ll feel the imitably thrill and see your chance to convince a huge crowd of investors, entrepreneurs and journalists. Just buy a ticket for Bits &amp; Pretzels, apply and win them over! (Application will start in mid July 2016) <div class="link_box"><a href="<?php echo Router::getRoute('pitch'); ?>" class="link">More Info<span class="icon icon-arrow_right"></span></a></div></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->

    <section id="captain" class="section section_users_right white active section_1 section_pt" data-show-inner="false" data-show-footer="false">

        <div class="content_section">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="grid grid-2 grid-2-simple clearfix">
                            <div class="col col-1">
                                <div class="grid-offset">
                                    <div class="info_box">
                                        <h1 class="main_title">O captain! My captain!<br>May I join your table.</h1>
                                        <h2 class="down_sub_title">Join an expert-hosted table at the Oktoberfest.</h2>
                                        <div class="descr descr_small_margin">There’s this shy species everybody speaks about and only few catch a glimpse of: infuential executives. But once a year they gather at the Bits &amp; Pretzels Oktoberfest tables. Lured by socialbility and lots of fresh talents. Choose one of the CEOs, editors-in-chief or investors as your Table Captain and enjoy their experience!<div class="link_box"><a href="<?php echo Router::getRoute('captains-startup'); ?>" class="link">More info &amp; all the Table Captains<span class="icon icon-arrow_right"></span></a></div></div>

                                    </div>
                                </div>
                            </div>
                            <div class="col col-2">
                                <div class="grid-offset">
                                    <div class="conteiner">
                                        <div class="list_users">
                                            <?php foreach(People::setType('table-captain')->setDataSource($tcs)->get(0, 12) as $key => $tc):  ?>

                                                <div class="user_box <?php echo ($key>7) ? 'hidden_thirteen' : ''; ?>">
                                                    <div class="user-offset">
                                                        <div class="img_user">
                                                            <img class="lazyload-scroll on-cat-site" data-src="/src/images/table-captains/default/<?php echo $tc['image']; ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                            <noscript>
                                                                <img class="on-cat-site" src="/src/images/table-captains/default/<?php echo $tc['image']; ?>" alt="">
                                                            </noscript>
                                                        </div>
                                                        <p class="name_user"><?php echo $tc['name']; ?></p>
                                                        <div class="title"><?php echo $tc['title']; ?></div>
                                                        <div class="company"><?php echo $tc['company']; ?></div>
                                                    </div>
                                                </div>

                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->

    <section id="academy" class="section section_wbox wbox_2 active section_1 section_pt" data-show-inner="false" data-show-footer="false">

        <div class="content_section"> <!-- paralax_section class ausgeblendet-->
            <div class="wrapper_inner center">
                <div class="valign">
                    <div class="middle">
                        <div class="wbox">
                            <h1 class="main_title">Academy: Start with an investment.<br>Never stop with improvement.</h1>
                            <h2 class="down_sub_title center">Enjoy masterclasses, specially designed for startups, with content that really matters.</h2>
                            <div class="descr descr_small_margin">As founders we know that founding is a process of constant learning. That’s why we want to support you with the Bits &amp; Pretzels Academy. A platform to reach out, connect and learn from the best. With a growing number of selected, high-quality lectures of experienced entrepreneurs and startup experts. Pay attention!<div class="link_box"><a href="<?php echo Router::getRoute('academy', true); ?>" class="link">More info & all masterclasses<span class="icon icon-arrow_right"></span></a></div></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->

<section id="business-partner" class="section section_logo silver active section_1 section_pt" data-show-inner="false" data-show-footer="false">

        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">Looking for a business partner? Look here!</h1>
                            <h2 class="down_sub_title center">Meet the big players and the rising stars of the ecosystem.</h2>
                            <div class="descr descr_small_margin center">Reliable business partners are hard to spot. That’s why we line them all up at Bits &amp; Pretzels. And give you a magnifying glass. Before the event starts you’ll get comprehensive information about all attending startups. Future key accounts, partners or competitors? Make your selection as long as they are small! <div class="link_box"><!--<a href="" class="link">View All Speakers<span class="icon icon-arrow_right"></span></a>--></div></div>
                        </div>

                        <?php include_once realpath('layout/elements/section-corporate.php');?>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->


    <section id="journalists" class="section section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false">

        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">So what’s the news? Maybe you!</h1>
                            <h2 class="down_sub_title center">Take your chance to meet 400+ journalists.</h2>
                            <div class="descr descr_small_margin center">What’s the difference between new and news? Public awareness. At Bits &amp; Pretzels you can give the media and the public a chance to become aware of your ideas. In 2016 hundreds of national and international journalists will cover the conference hunting for an exciting story. So how about you start telling your story!</div>
                        </div>

                        <?php include_once realpath('layout/elements/section-journalists.php');?>

                        <div id="buyticket" class="button_mb">
                            <div class="button_box button_box_orange button_box_auto">
                                <a href="<?php echo Router::getRoute('buy_startup'); ?>" class="button orange"><span>Register for 2017 Ticket Sale</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="link_to_top" class="link_to_top link_to_top_abs">
            <div class="svg_arrow">

                <svg version="1.1" id="Layer_1" class="arrow_mouse" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     width="24.01px" height="27.5px" viewBox="0 27.5 24.01 27.5" enable-background="new 0 27.5 24.01 27.5" xml:space="preserve">
                <g>
                    <path fill="#353535" d="M0.289,40.715c0.386,0.385,1.01,0.385,1.395,0l10.322-10.32l10.322,10.32c0.385,0.385,1.009,0.385,1.394,0
                        c0.386-0.386,0.386-1.009,0-1.395L12.779,28.377c-0.023-0.029-0.034-0.064-0.062-0.091c-0.195-0.196-0.455-0.29-0.712-0.286
                        c-0.257-0.004-0.516,0.09-0.713,0.286c-0.027,0.026-0.038,0.063-0.063,0.093L0.289,39.32C-0.096,39.706-0.096,40.33,0.289,40.715z"
                        />
                    <circle fill="#353535" cx="12.006" cy="53" r="2.001"/>
                </g>
                </svg>
            </div>
            <div class="text">Go to top</div>
        </div>

    </section><!-- section -->

</div>

<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>
