<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>

    <!--
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

  <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
        </div>
    </div>
    
    <section id="apply" class="section section_wbox accredit_section acredit active section_1 section_pt" style="position: relative;top: 30px;">

        <div class="content_section">
            <div class="wrapper_inner center">
                <div class="valign">
                    <div class="middle">
                        <div class="col col-tr">
                            <div class="start_up_apply start_up_apply-big light_blue form_h">
                                <div class="blue_box blue_box-darkest">
                                    <h2 class="st_heading">Journalist</h2>
                                </div>
                                <div class="sub_links">
                                    <ul>
                                        <li><a style="cursor:default" href="javascript:void(0)">General Program</a></li>
                                        <li><a style="cursor:default" href="javascript:void(0)">Oktoberfest</a></li>
                                        <li><a style="cursor:default" href="javascript:void(0)">Access to Workshops</a></li>
                                        <li><a style="cursor:default" href="javascript:void(0)">After party</a></li>
                                        <li><a style="cursor:default" href="javascript:void(0)">Networking Tool</a></li>
                                         <li><a style="cursor:default" href="javascript:void(0)">Speaker Matchmaking</a></li>
                                        <li><a style="cursor:default" href="javascript:void(0)">Table Captain Matchmaking</a></li>
                                        <li><a style="cursor:default" href="javascript:void(0)">Startup Matchmaking</a></li>
                                        <li><a style="cursor:default" href="javascript:void(0)">Access to Press Lounge</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sv">
                            <div class="wbox form_h">
                               <h1 class="main_title">Accredit now!</h1>
                               <div style="font-size:.8em;margin-bottom:20px;margin-top:0; color:#ea5c3f" class="descr descr_small_margin">
                                   <strong>+++ only possible until September 23, 2pm +++</strong>
                               </div>
                               
                                <form action="https://submit.jotformeu.com/submit/53363909295364/" name="apply_now" class="form">
                                    <!--
                                    <div id="error_message" class="error_message">
                                        <div class="field-col">Please, fill the required fields!</div>
                                    </div>
                                    -->
                                    <div class="field_wrap">
                                        <div class="label-col">
                                            <label for="first_name" class="main_label">First Name:<span class="required">*</span></label>
                                        </div>
                                        <div class="field-col">
                                            <input id="first_name" name="q3_name[first]" class="texfield_form" required autocomplete="off" type="text">
                                        </div>
                                    </div>
                                    <div class="field_wrap">
                                        <div class="label-col">
                                            <label for="last_name" class="main_label">Last Name:<span class="required">*</span></label>
                                        </div>
                                        <div class="field-col">
                                            <input id="last_name" name="q3_name[last]" class="texfield_form" required autocomplete="off" type="text">
                                        </div>
                                    </div>
                                    <div class="field_wrap">
                                        <div class="label-col">
                                            <label for="email" class="main_label">Email Address:<span class="required">*</span></label>
                                        </div>
                                        <div class="field-col">
                                            <input id="email" name="q36_email" class="texfield_form" required autocomplete="off" type="email">
                                        </div>
                                    </div>
                                    <div class="field_wrap">
                                        <div class="label-col">
                                            <label for="media" class="main_label">Media:<span class="required">*</span></label>
                                        </div>
                                        <div class="field-col">
                                            <input id="media" name="q22_nameOf22" class="texfield_form" required autocomplete="off" type="text">
                                        </div>
                                    </div>
                                    <div class="field_wrap">
                                        <div class="label-col">
                                            <label for="website" class="main_label">Website:<span class="required">*</span></label>
                                        </div>
                                        <div class="field-col">
                                            <input id="website" name="q23_website" class="texfield_form" required autocomplete="off" type="text">
                                        </div>
                                    </div>

                                    <div class="field_wrap">
                                        <div class="label-col">
                                            <label for="country" class="main_label">Country:<span class="required">*</span></label>
                                        </div>
                                        <div class="field-col">
                                            <input id="country" name="q24_country" class="texfield_form" required autocomplete="off" type="text">
                                        </div>
                                    </div>


                                    <div class="field_wrap">
                                        <div class="label-col">
                                            <label for="days">I am planning to attend the following days :<span class="required">*</span></label>
                                        </div>
                                        <div class="field-col">
                                             <label for="input_34_0">Sunday, Sept 25th</label>
                                            <input type="checkbox" id="input_34_0" name="q34_iAm34[]" value="Sunday, Sept 25th" style="display: inline-block;">
                                            <br>
                                            <br>
                                             <label for="input_34_1">Monday, Sept 26th</label>
                                            <input type="checkbox" class="form-checkbox validate[required]" id="input_34_1" name="q34_iAm34[]" value="Monday, Sept 26th" style="display: inline-block;">
                                            <br>
                                            <br>
                                            <label for="input_34_2">Tuesday, Sept 27th</label>
                                            <input type="checkbox" class="form-checkbox validate[required]" id="input_34_2" name="q34_iAm34[]" value="Tuesday, Sept 27th" style="display: inline-block;">
                                        </div>
                                    </div>

                                    <br>

                                    <div class="field_wrap">
                                        <div class="label-col">
                                            <label for="input_35_0">I am planning to come with a press team (photographer, cameraman, etc.):<span class="required">*</span></label>
                                        </div>
                                        <div class="field-col">
                                            <label for="input_35_0">Yes</label>
                                            <input type="radio" id="input_35_0" name="q35_iAm35[]" value="YES" style="display: inline-block;">
                                            <br>
                                            <br>
                                            <label for="input_35_0">No</label>
                                            <input type="radio" class="form-checkbox validate[required]" id="input_35_1" name="q35_iAm35[]" value="NO" style="display: inline-block;">
                                        </div>
                                    </div>

                                    <br>


                                    <div class="field_wrap">
                                        <div class="field-col"><div class="line_form"></div></div>
                                    </div>
                                    <div class="field_wrap">
                                        <div class="label-col">
                                            <label for="brthday" class="main_label label_required">* indicates required</label>
                                        </div>
                                        <div class="field-col">
                                            <input type="hidden" name="formID" value="53363909295364">
                                            <input class="button orange submit_button" value="Apply" type="submit">
                                        </div>
                                    </div>



                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="link_to_top" class="link_to_top link_to_top_abs">
            <div class="svg_arrow">

                <svg version="1.1" id="Layer_1" class="arrow_mouse" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     width="24.01px" height="27.5px" viewBox="0 27.5 24.01 27.5" enable-background="new 0 27.5 24.01 27.5" xml:space="preserve">
                <g>
                    <path fill="#353535" d="M0.289,40.715c0.386,0.385,1.01,0.385,1.395,0l10.322-10.32l10.322,10.32c0.385,0.385,1.009,0.385,1.394,0
                        c0.386-0.386,0.386-1.009,0-1.395L12.779,28.377c-0.023-0.029-0.034-0.064-0.062-0.091c-0.195-0.196-0.455-0.29-0.712-0.286
                        c-0.257-0.004-0.516,0.09-0.713,0.286c-0.027,0.026-0.038,0.063-0.063,0.093L0.289,39.32C-0.096,39.706-0.096,40.33,0.289,40.715z"
                        />
                    <circle fill="#353535" cx="12.006" cy="53" r="2.001"/>
                </g>
                </svg>
            </div>
            <div class="text">Go to top</div>
        </div>
    </section>
</div>

<?php include realpath('layout/signup-form.php');?>
<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>
