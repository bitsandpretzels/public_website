<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>


<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>

    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->
   <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section"><span class="mobile_hidden">Networking App</span></div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>            
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                </li>
                <li>
                    <a class="link_nav buyticket trigger_app_nav" data-section="buyticket">Get your Ticket</a>
                </li>
            </ul>
        </div>
    </div>


    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-networking-app active section_1">

        <div class="content_section center">
            <div class="wrapper">
                <div class="info_text_top">
                    <h1 class="top_title">Networking</h1>
                    <!--
                    <h2 class="main_title">Showcase your innovation and get inspired by others.<span class="stay_block">Be part of the international startup exhibition.</span></h2>  
                    -->        
                </div>
            </div>
        </div>
    </section><!-- section -->

    <section id="find" class="section section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">1. Find people you are interested in.</h1>
                            <p class="center">Our newly developed networking tool will help you to find attendees you should meet.</p> 
                            <div class="center">
                                <img src="/src/images/info/find.png" style="max-width: 100%"> 
                            </div>                         
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
 
    <section id="plan" class="section silver section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">2. See when they are available.</h1>
                            <p class="center">Negotiating a time and place for your meeting can be frustrating. We are giving you a smart, simple and easy-to-use tool that does the job for you.</p> 
                            <br>
                            <div class="center">
                                <img src="/src/images/info/plan.png" style="max-width: 100%"> 
                            </div>                         
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->


    <section id="agenda" class="section section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">3. Take your schedule to the event.</h1>
                            <p class="center">Your time is precious. Get the most out of Bits &amp; Pretzels by scheduling meetings on your desktop computer. Your calendar will be automatically synced with the mobile app.</p>
                            <br>
                            <div class="center">
                                <img src="/src/images/info/schedule.png" style="max-width: 100%"> 
                            </div>                         
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->

    
</div>

<?php include realpath('layout/signup-form.php');?>
<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>

