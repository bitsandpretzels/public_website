<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>

    <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section mobile_hidden">Meet Kevin Spacey</div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>            
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="#content" class="link_nav buyticket" data-section="buyticket">Participate</a>
                </li>
            </ul>
        </div>
    </div>
<!--    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-spacey-2 active section_1" style="max-height:50px"></section> section -->
    <section id="content" class="section section_logo white active section_top-oktoberfest section_1 section_pt" data-show-inner="false" data-show-footer="false" style="min-height: 0px; padding-top:30px; padding-bottom:200px">
        <div class="content_section center" style="min-height: 0px">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text" style="padding-top:60px">
                            <?php 
                                $secret = 'eac92f8bd8bf08584b94f9ec4f1f2d6c';
                                if (array_key_exists('pwat_checksum', $_GET) && array_key_exists('pwat_uid', $_GET)):
                                    $checksum = $_GET['pwat_checksum'];
                                    $uid = $_GET['pwat_uid'];

                                    // User darf auf die Seite. Sonst nicht.
                                    if(sha1($uid . $secret) == $checksum): ?>

                                    <script type="text/javascript" src="https://form.jotformeu.com/jsform/62533292543354"></script>

                                    <?php else: ?>
                                    <h1 style="margin:200px 0; color:white">Thank you for participating, good luck! :)</h1>

                                    <?php endif; else: ?>
                                    <h1 style="margin:200px 0; color:white">Thank you for participating, good luck! :)</h1>
                            
                                <?php endif; ?>
<!--
                            <ul>
                                <li>
                                    <a style="color:#005690" href="<?php echo Router::getRoute('meet-kevin/conditions'); ?>">Terms of Conditions</a>
                                </li>
                            </ul>
-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="row">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="text-center">
                  <h3></h3>
                  <p class="lead"></p>
                  <a href="" class="btn btn-primary"></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha256-Sk3nkD6mLTMOF0EOpNtsIry+s1CsaqQC1rVLTAy+0yc= sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
    <script src="/info/paywithatweet/assets/js/waypoints.min.js"></script>
    <script src="/info/paywithatweet/assets/js/scripts.js"></script>
    <script src="/info/paywithatweet/firal_sdk.js"></script>

<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>