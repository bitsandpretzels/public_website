<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>


<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>

    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->
     <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section desktop_only"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section"><span class="mobile_hidden">Women in Tech</span></div>
            <ul class="sub_nav ul-reset">
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                    </li>            
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                    </li>
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                    </li>
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                    </li>
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                    </li>
                    <li>
                        <a class="link_nav buyticket trigger_app_nav" data-section="buyticket"><?php echo Meta::getButtonWording(); ?></a>
                    </li>
                </ul>
        </div>
    </div>

    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-women-in-tech active section_1">

        <div class="content_section center">
            <div class="wrapper">
                <div class="info_text_top">
                    <h1 class="top_title" style="text-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);">Women in Tech</h1>
                    <!--
                    <h2 class="main_title">Showcase your innovation and get inspired by others.<span class="stay_block">Be part of the international startup exhibition.</span></h2>  
                    -->        
                </div>
            </div>
        </div>
    </section><!-- section -->

    <section class="section white active section_1 section_pt" style="padding-bottom: 30px">

        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle"  style="vertical-align: 0 !important;">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">Attention everyone! Mind the gender gap!</h1>
                            <div class="descr center">
                                Anyone working in the startup world knows intuitively that women are not well represented in founder roles or technical positions. There are countless brilliant female founders in the international startup ecosystem but they are still underrepresented in the public awareness. A look at the attendees’ and speakers’ lists of world’s startup conferences pictures the same lack of female entrepreneurs.
                                <br>
                                <br>
                                Our colleagues at the Web Summit Lisbon and the Pioneers Festival started an initiative called „Women in Tech“ to push female entrepreneurs and generally sensibilize the startup scene for gender diversity. We think this is exactly the right way and want to follow their lead: 
                                <br>
                                <br>
                                <strong>300 free tickets for aspiring women with gamechanging ideas.</strong>
                            </div> 
                        </div>   
                        <div class="grid grid-2 grid-2-simple clearfix container_info" style="padding-top: 0px">
                            <div class="col quote quote-1">
                                <div class="info_box">                                                         
                                    <h1 class="main_title">“Bits &amp; Pretzels definitely brings interesting people from different businesses together. It's a great opportunity for getting new insights, networking and discussing new trends and challenges.”</h1>
                                    <div class="list_users clearfix">
                                        <div class="user_box" style="float:none;margin: 0 auto">
                                            <div class="user-offset">
                                                <div class="img_user"><img src="/src/images/testimonials/default/schwetje.png" alt=""></div>
                                                <p class="name_user">Sonja Schwetje</p>
                                                <p>Editor in Chief</p>
                                                <p>n-tv</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col quote">
                                 <div class="info_box">                                                         
                                    <h1 class="main_title">“A unique conference that really blends technology, heritage and the huge Oktoberfest successfully to an event that goes far beyond the usual networking conference.”</h1>
                                    
                                    <div class="list_users clearfix">
                                        <div class="user_box" style="float:none;margin: 0 auto">
                                            <div class="user-offset">
                                                <div class="img_user"><img src="/src/images/testimonials/default/rein.png" alt=""></div>
                                                <p class="name_user">Raffaela Rein</p>
                                                <p>Founder and CEO</p>
                                                <p>CareerFoundry</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>                
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->


<section id="partners" class="section section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="padding-top: 0px;padding-bottom: 30px;min-height: 0px">

        <div class="content_section center" style="min-height: 0px">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
            
                        <div class="wrapp_main_text">
                            <h1 class="main_title center" style="margin:0px">Supporters</h1>                                                 
                        </div>  
       

                        <div class="list_logos logos-grid-5 mb0 clearfix" style="margin-top:30px">


                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><img src="/src/images/logos/women/blooming_founders_logo-small.png" alt=""></div>
                                </div>
                            </div>
                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><img src="/src/images/logos/women/fempreneur_logo.png" alt=""></div>
                                </div>
                            </div>
                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><img src="/src/images/logos/women/bsg.png" alt=""></div>
                                </div>
                            </div>
                   
                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><img src="/src/images/logos/women/femtrepreneur.png" alt=""></div>
                                </div>
                            </div>

                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><img src="/src/images/logos/women/womens_startup_lab.png" alt=""></div>
                                </div>
                            </div>

                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><img src="/src/images/logos/women/sheworks.png" alt=""></div>
                                </div>
                            </div>


                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><img src="/src/images/logos/women/ff.png" alt=""></div>
                                </div>
                            </div>


                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><img src="/src/images/logos/women/not_another_woman_magazine.png" alt=""></div>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->

    



    <section class="section section_wbox women-in-tech active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="position: relative;top:10px"> 
        <a name="apply"></a>

        <div class="content_section">
            <div class="wrapper_inner center">
                <div class="valign">
                    <div class="middle">
                        <?php $time = time(); ?>
                        <?php if($time < 1461974400): ?>
                        <h1 class="main_title text-shadow">You are one of those female founders. <br> Then you should immediately apply here.*</h1>
                        <?php endif; ?>

                        <div class="wbox" style="<?php echo (($time > 1461974400) ? 'margin-top:7.3rem' : '') ?>">     

                            <?php if($time > 1461974400): ?>
                            <div style="padding: 2em">
                            Application is closed. <br>
                            Winners will be notified soon.
                            </div>
                            <?php endif; ?>
                           
                           <?php if($time < 1461974400): ?>
                            <form action="https://submit.jotformeu.com/submit/60774196670363/" method="post" id="mc-embedded-subscribe-form womenInTechForm" name="mc-embedded-subscribe-form" class="form">
                                <!--
                                <div id="error_message" class="error_message">
                                    <div class="field-col">Please, fill the required fields!</div>
                                </div>
                                -->
                                <div class="field_wrap">
                                    <div class="label-col">
                                        <label for="first_name" class="main_label">First Name:<span class="required"></span></label>
                                    </div>
                                    <div class="field-col">
                                        <input id="first_name" name="q1_vollstandigerName1[first]" class="texfield_form" autocomplete="off" type="text" required>
                                    </div>
                                </div>
                                <div class="field_wrap">
                                    <div class="label-col">
                                        <label for="last_name" class="main_label">Last Name:<span class="required"></span></label>
                                    </div>
                                    <div class="field-col">
                                        <input id="last_name" name="q1_vollstandigerName1[last]" class="texfield_form" autocomplete="off" type="text" required>
                                    </div>
                                </div>
                                <div class="field_wrap">
                                    <div class="label-col">
                                        <label for="email" class="main_label">Email Address:<span class="required"></span></label>
                                    </div>
                                    <div class="field-col">
                                        <input id="email" name="q6_email6" class="texfield_form" autocomplete="off" type="email" required>
                                    </div>
                                </div>
                                <div class="field_wrap">
                                    <div class="label-col">
                                        <label for="email" class="main_label">Company Name:<span class="required"></span></label>
                                    </div>
                                    <div class="field-col">
                                        <input id="email" name="q9_companyName" class="texfield_form" autocomplete="off" type="text" required>
                                    </div>
                                </div>
                                <div class="field_wrap clearfix">
                                    <div class="label-col">
                                        <label for="email" class="main_label">Category<span class="required"></span></label>
                                    </div>
                                    <div class="field-col clearfix">
                                        <select name="q18_category" style="border:1px solid #BCBCBC;width:100%;padding:13px;border-radius:3px;text-indent: 5px;line-height: 26px;">    
                                            <option value="Startup">Startup</option>
                                            <option value="Investor">Investor</option>
                                            <option value="Corporate">Corporate</option>
                                            <option value="Student">Student</option>
                                            <option value="Journalist">Journalist</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="field_wrap">
                                    <div class="label-col">
                                        <label for="email" class="main_label">Position:<span class="required"></span></label>
                                    </div>
                                    <div class="field-col">
                                        <input id="email" name="q11_position" class="texfield_form" autocomplete="off" type="text" required>
                                    </div>
                                </div> 
                                <div class="field_wrap">
                                    <div class="label-col">
                                        <label for="email" class="main_label">LinkedIn / Xing Profile: <span class="required"></span></label>
                                    </div>
                                    <div class="field-col">
                                        <input id="email" name="q7_linkedin" class="texfield_form" autocomplete="off" type="text" required>
                                    </div>
                                </div>                                                                                                                                  
                                <!--
                                <div class="field_wrap">
                                    <div class="label-col">
                                        <label for="mc" class="main_label">Stage:<span class="required">*</span></label>
                                    </div>
                                    <div class="field-col clearfix">
                                        <input class="button orange default" onclick="$('#hidden-stage-value').val('early');alert($('#hidden-stage-value').val())" value="Early" type="buton" style="width:49%;float:left;margin-right:2%">
                                        <input class="button orange-negative default" onclick="$('#hidden-stage-value').val('later');alert($('#hidden-stage-value').val())" value="Later" type="buton" style="width:49%;float:left">
                                        <input type="hidden" id="hidden-stage-value" name="stage" value="early"/>
                                    </div>
                                </div>
                                -->                                
                                <div class="field_wrap">
                                    <div class="field-col"><div class="line_form"></div></div>
                                </div>
                                <div class="field_wrap">
                                    <div class="label-col">
                                        <label for="brthday" class="main_label label_required">All fields are required</label>                                        
                                    </div>
                                    <div class="field-col">
                                        <div class="error_message catchy none"> Please complete the form before you submit </div>
                                        <input type="text" name="b_59db0da2c55155bbedfa0c507_f033f7bd45" tabindex="-1" value="">
                                        <input class="button orange submit_button unity_form_submit_" data-action="https://submit.jotformeu.com/submit/60774196670363/" name="subscribe" value="Register" type="submit">
                                        <div style="display: none">
                                        <br>
                                        <input class="button orange submit_button unity_form_submit" data-action="https://submit.jotformeu.com/submit/60823146425351/" name="subscribe" value="Recommend someone" type="button">
                                        </div>
                                    </div>
                                </div>
                                <label for="brthday" class="main_label label_required" style="line-height: 14px">* A raffle on 29th of April will decide about who gets one of the 300 hundred tickets. Tickets are not transferable.</label>
                             </form>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    
</div>
<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>

