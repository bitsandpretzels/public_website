<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

    <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section mobile_hidden">Volunteers</div>
        </div>
    </div>


    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-volunteers active section_1">

        <div class="content_section center">
            <div class="wrapper">
                <div class="info_text_top">
                    <h1 class="top_title" style="text-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);">Volunteers</h1>
                    <!--
                    <h2 class="main_title">Apply to become a volunteer and join the team of <span class="stay_block">the Bits & Pretzels Founders Festival.</span></h2>  
                    -->        
                </div>
            </div>
        </div>
    </section><!-- section -->

    <section class="section  white active section_1 section_pt">

        <div class="content_section center paralax_section">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title">Become a Volunteer at the Bits &amp; Pretzels Founders Festival 2016.</h1><br>
                            <div class="descr descr_small_margin">Our volunteer program puts you right in the epicenter of the startup ecosystem. If you are at least 18 years old you can become a part of Bits &amp; Pretzels 2016 and help us to make it a huge success. In return we will help you to make exciting experiences and valuable contacts with like-minded people and industry leaders of today and tomorrow. 
<br><br>
So if you feel gravitated to startups or even think about becoming a founder yourself then please apply for your exclusive glimpse behind the scences. We are looking forward to welcoming you in our team!
                            </div>  
                            <br><br>
                            <a href="https://form.jotformeu.com/62024492673355" class="button orange"><span>Apply</span></a> 
                            <div class="descr descr_big_margin" style="color:#ea5c37; margin-bottom:50px; margin-top:30px">
                             Application Deadline: August 31 
                            </div>  
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
    <section class="section silver active section_1 section_pt">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">   
                        <div class="wrapp_main_text">
                            <h1 class="main_title">FAQ</h1>
                        </div>
                        <div class="wrap_quest"> 
                            <div class="grid clearfix">
                                <div class="valign">    
                                    <div class="middle"> 
                                        <div class="topic topic_open">                                                     
                                            <h1 class="question_title">Who is allowed to apply?</h1>
                                            <div class="arrow_pointer"></div>
                                            <div class="description_info descr descr_no_margin" style="display:none">
                                                <p class="question_text">
                                                  Applicants must be 18 years of age or older (on or before 25 September 2016) and must have a good level of English. Beyond that, we are looking for volunteers who are reliable, friendly punctual and willing to perform their task.
                                                </p>
                                            </div>
                                        </div>  

                                        <div class="topic topic_open">                                                     
                                            <h1 class="question_title">Until when do I have to apply?</h1>
                                            <div class="arrow_pointer"></div>
                                            <div class="description_info descr descr_no_margin" style="display:none">
                                                <p class="question_text">
                                                    To inform our applicants as early as possible if their application is accepted we have certain deadlines that must be complied. This ensures that every volunteer has the possibility to take care of his or her accommodation early enough. There might be a second application period depending on the amount of applications in the first run, but we strictly recommend to apply as early as possible.
                                                </p> 
                                                <div class="question_subtitle">August 31: Application Deadline</div>
                                                <p class="question_text">
                                                    The Volunteer application must be completed by 10pm. 
                                                </p>

                                                <div class="question_subtitle">Application Results Sent Ongoing</div>
                                                <p class="question_text">
                                                    We will select the participants ongoing. Apply early to know first.
                                                </p>
                                            </div>
                                        </div> 
                                           
                                        <div class="topic topic_open">                                                     
                                            <h1 class="question_title">I have specific questions, whom do I contact?</h1>
                                            <div class="arrow_pointer"></div>
                                            <div class="description_info descr descr_no_margin" style="display:none">
                                                <p class="question_text">
                                                    Please contact <a href="mailto:volunteers@bitsandpretzels.com" style="color:#005690" alt="mail to bitsandpretzels">volunteers@bitsandpretzels.com</a> for any further volunteer queries.
                                                </p> 
                                            </div>
                                        </div> 
<!--
                                        <div class="topic topic_open">                                                     
                                            <h1 class="question_title">What are my tasks at the conference?</h1>
                                            <div class="arrow_pointer"></div>
                                            <div class="description_info descr descr_no_margin" style="display:none">
                                                <div class="question_subtitle">Serve the Conference</div>
                                                <p class="question_text">
                                                  Commit 12 hours (approximately, without orientation) of volunteer service to the conference. 
                                                </p>

                                                <div class="question_subtitle">Attend Mandatory Orientation</div>
                                                <p class="question_text">
                                                    Saturday, 24 September 2016 (location and time will be announced in the End of August). Please plan your travel to arrive in Munich NO LATER THAN NOON on September 24, so that you will be able to check in before this mandatory meeting time.
                                                </p>

                                                <div class="question_subtitle">Bits &amp; Pretzels 2016</div>
                                                <p class="question_text">
                                                 All day, 25 - 27 September 2016<br> We recommend that you stay until Wednesday, September 28 so you can celebrate a successful conference with your new Bits &amp; Pretzels colleagues on Tuesday evening.
                                                </p>

                                                <div class="question_subtitle">Be Punctual</div>
                                                <p class="question_text no-space">
                                                 Arrive at the Student Volunteer Office at least 30 minutes before each of your scheduled shift times with a smile on your face and a passion to serve! Examples of Student Volunteer assignments:
                                                </p>
                                                <ul class="questions_list questions_list-dots">
                                                    <li>
                                                        Monitoring doors
                                                    </li>
                                                    <li>
                                                        Checking badges
                                                    </li>
                                                    <li>
                                                        Facilitating spoken-language translations
                                                    </li>
                                                    <li>
                                                        Assisting conference contributors at various venues
                                                    </li>
                                                    <li>
                                                        Providing general assistance for attendees in all areas of the conference center
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>                                    
-->
                                    </div>
                                </div>
                            </div>    
                        </div>        
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
</div>

<?#php include realpath('layout/signup-form.php');?>
<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>
