<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>

    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

     <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section desktop_only"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section"><span class="mobile_hidden">Exhibition - Global Initiative</span></div>
            <ul class="sub_nav ul-reset">
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                    </li>            
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                    </li>
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                    </li>
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                    </li>
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                    </li>
                    <li>
                        <a class="link_nav buyticket" href="#apply" data-section="buyticket"><?php echo Meta::getButtonWording(); ?></a>
                    </li>
                </ul>
        </div>
    </div>

    
    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-exhibition active section_1" style="background:url('/src/images/exhibition_int_big_img.jpg'), center, center; background-size:cover;">

        <div class="content_section center">
            <div class="wrapper">
                <div class="info_text_top">
                    <h1 class="top_title" style="text-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);">Startup Exhibition
Global Initiative</h1>    
                </div>
            </div>
        </div>
    </section><!-- section -->

    <section class="section white active section_1 section_pt" id="content" style="padding-bottom: 30px;">

        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle" style="vertical-align: 0 !important;">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center" style="margin-bottom:12px">Hello World: We're inviting 50 international startups to join #bits17</h1>
                            <div class="descr center">In 2017, we'd like to see even more great international startups at our festival. As money is usually short among startup founders and international travel costs are high, we'd like to invite 50 INTERNATIONAL startups to join our startup exhibition FOR FREE! The package includes three free conference tickets for startup employees and an exhibition booth for one day. Apply now using the form below, and we will select the best startups afterwards. 
                            <br>
                            <br>
                            Exhibition booths will be sorted by these clusters:
                            </div>  
                                    
                        <div class="wrap_speakers" style="margin: 0 auto; display: table;">
                            <div class="conteiner" style="margin: 0 auto;">
                                <div class="list_users center">
                                    <a href="https://www.bitsandpretzels.com/cluster">
                                        <div class="user_box" style="width:100px !important; padding: 0px 5px; !important; height:140px; text-align: center;">
                                            <div class="user-offset">
                                                <img class="lazyload" data-src="/src/images/cluster/icon_2017/commerce.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="width:80px" />
                                                <noscript>
                                                    <img style="width:80px" src="/src/images/cluster/icon_2017/commerce.png" alt="Future Commerce">
                                                </noscript>
                                                <!-- <p class="name_user commerceorange" style="margin-top:10px; font-size:1.2em">Future Commerce</p> -->
                                            </div>
                                        </div>
                                        </a>
                                    <a href="https://www.bitsandpretzels.com/cluster">
                                        <div class="user_box" style="width:100px !important; padding: 0px 5px; !important; height:140px; text-align: center;">
                                            <div class="user-offset">
                                                <img class="lazyload" data-src="/src/images/cluster/icon_2017/mobility.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="width:80px" />
                                                <noscript>
                                                    <img style="width:80px" src="/src/images/cluster/icon_2017/mobility.png" alt="Fast Mobility">
                                                </noscript>
                                                <!-- <p class="name_user mobileorange" style="margin-top:10px; font-size:1.2em">Fast Mobility</p> -->
                                            </div>
                                        </div>
                                    </a>
                                    <a href="https://www.bitsandpretzels.com/cluster">
                                        <div class="user_box" style="width:100px !important; padding: 0px 5px; !important; height:140px; text-align: center;">
                                            <div class="user-offset">
                                                <img class="lazyload" data-src="/src/images/cluster/icon_2017/lifestyle.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="width:80px" />
                                                <noscript>
                                                    <img style="width:80px" src="/src/images/cluster/icon_2017/lifestyle.png" alt="Hot Lifestyle">
                                                </noscript>
                                                <!-- <p class="name_user pink" style="margin-top:10px; font-size:1.2em">Hot Lifestyle</p> -->
                                            </div>
                                        </div>
                                    </a>
                                    <a href="https://www.bitsandpretzels.com/cluster">
                                        <div class="user_box" style="width:100px !important; padding: 0px 5px; !important; height:140px; text-align: center;">
                                            <div class="user-offset">
                                                <img class="lazyload" data-src="/src/images/cluster/icon_2017/iot.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="width:80px" />
                                                <noscript>
                                                    <img style="width:80px" src="/src/images/cluster/icon_2017/iot.png" alt="Sophisticated IoT">
                                                </noscript>
                                                <!-- <p class="name_user purple" style="margin-top:10px; font-size:1.2em">Sophisticated IoT</p> -->
                                            </div>
                                        </div>
                                    </a>
                                    <a href="https://www.bitsandpretzels.com/cluster">
                                        <div class="user_box" style="width:100px !important; padding: 0px 5px; !important; height:140px; text-align: center;">
                                            <div class="user-offset">
                                                <img class="lazyload-scroll" data-src="/src/images/cluster/icon_2017/smart_company.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="width:80px" />
                                                <noscript>
                                                    <img style="width:80px" src="/src/images/cluster/icon_2017/smart_company.png" alt="Smart Company">
                                                </noscript>
                                                <!-- <p class="name_user darkgreen" style="margin-top:10px; font-size:1.2em">Smart Company</p> -->
                                            </div>
                                        </div>
                                    </a>
                                    <a href="https://www.bitsandpretzels.com/cluster">
                                        <div class="user_box" style="width:100px !important; padding: 0px 5px; !important; height:140px; text-align: center;">
                                            <div class="user-offset">
                                                <img class="lazyload-scroll" data-src="/src/images/cluster/icon_2017/money.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="width:80px" />
                                                <noscript>
                                                    <img style="width:80px" src="/src/images/cluster/icon_2017/money.png" alt="Big Money">
                                                </noscript>
                                                <!-- <p class="name_user green" style="margin-top:10px; font-size:1.2em">Big Money</p> -->
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                     </div>
                    
                      
                    </div>
                </div>
                
            </div>
        </div>
    </section><!-- section -->
   <!-- <section class="section white active section_1 section_pt">
        <div class="content_section center">
                <div class="wrapper_inner">
                    <div class="valign">
                        <div class="middle">
                            <div class="wrapp_main_text">
                                
                                
                                <div><h1 class="main_title center">Your benefits</h1></div>
                    <div class="descr center"><p style="color:#005690; font-size:1.3em">Conference Tickets</p>Three tickets for your team are included in the package.</div>
                    <div class="descr center"><p style="color:#005690; font-size:1.3em">Exhibition booth</p>Showcase your startup to media and investors for one day.</div>
                    <div class="descr center"><p style="color:#005690; font-size:1.3em">Printed company profile</p>To help you traveling with small luggage. Your booth already comes with a branding.</div>
                    <div class="descr center"><p style="color:#005690; font-size:1.3em">1-on-1 meetings</p>Get the most out of your stay and arrange 1-on-1 meetings with investors and other attendees.</div>
                    <div class="descr center"><p style="color:#005690; font-size:1.3em">Preferred Table Captain selection</p>Select your Table Captain before everyone else does and meet the person that elevates your startup the most.</div>                    

                           
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </section>                  -->
    
    <section class="section white active section_1 section_pt">
        <div class="content_section center">
                <div class="wrapper_inner">
                    <div class="valign">
                        <div class="middle" style="display: block;">
                            <div class="wrapp_main_text" style="margin-top: -90px;">
                                
                                <div><h1 class="main_title center">Your benefits</h1></div>
                                <div class="descr center" style="margin-bottom: 20px;"><p style="color:#005690; font-size:1.3em">Conference Tickets</p>Three 3-day festival tickets for your team are included in the package</div>
                                <div class="descr center" style="margin-bottom: 20px;"><p style="color:#005690; font-size:1.3em">Exhibition booth</p>Showcase your startup on one of the conference days.</div>
                                <div class="descr center" style="margin-bottom: 20px;"><p style="color:#005690; font-size:1.3em">Printed company profile</p>To help you traveling with small luggage. Your booth already comes with a branding.</div>
                                <div class="descr center" style="margin-bottom: 20px;"><p style="color:#005690; font-size:1.3em">1-on-1 meetings</p>Get the most out of your stay and arrange 1-on-1 meetings with investors and other attendees.</div>
                                <div class="descr center" style="margin-bottom: 20px;"><p style="color:#005690; font-size:1.3em">Preferred Table Captain selection</p>Select your Table Captain before everyone else does and meet the person that elevates your startup the most.</div>  
                            </div>                  
                        <!--
                        <div class="grid grid-2 grid-2-simple clearfix container_info">
                            <div class="col quote quote-1">
                                <div class="info_box">                                                         
                                    <h1 class="main_title">“Bits &amp; Pretzels is a conference where one can actually make a business.<br> We made excellent contacts that will help us to close our A-round soon.”</h1>
                                    <div class="list_users clearfix">
                                        <div class="user_box" style="float:none;margin: 0 auto">
                                            <div class="user-offset">
                                                <div class="img_user">
                                                    <img class="lazyload-scroll" data-src="/src/images/testimonials/default/potocnik.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                    <noscript>
                                                        <img src="/src/images/testimonials/default/potocnik.png" alt="">
                                                    </noscript>
                                                </div>
                                                <p class="name_user">Tim Potocnik</p>
                                                <p>Managing Director and Co-Founder</p>
                                                <p>Eurosender</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col quote">
                                 <div class="info_box">                                                         
                                    <h1 class="main_title">“Well organized, great service for a good price and awesome evening events. I honestly can recommend taking part to other startups.”</h1>
                                    
                                    <div class="list_users clearfix">
                                        <div class="user_box" style="float:none;margin: 0 auto">
                                            <div class="user-offset">
                                                <div class="img_user">
                                                    <img class="lazyload-scroll" data-src="/src/images/testimonials/default/raisch.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                    <noscript>
                                                        <img src="/src/images/testimonials/default/raisch.png" alt="">
                                                    </noscript>
                                                </div>
                                                <p class="name_user">Matthias Raisch</p>
                                                <p>Managing Director and Founder</p>
                                                <p>pareton</p>
                                            </div>
                                        </div>
                                    </div> 

                                </div> 
                            </div> 
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </section>          
    
    
     <section class="section section_wbox apply active section_1 section_pt" data-show-inner="false" data-show-footer="false" >
        <a name="apply"></a>

        <div class="content_section" >
            <div class="wrapper_inner center">
                <div class="valign">
                    <div class="middle" style="background: rgba(255, 255, 255, 0);">
                        <div style="background:rgba(255, 255, 255, 0); margin: auto; text-align: center !important; position: relative;">
                            

    <div class="four columns offset-by-four">
    <div class="form-all" style="text-align:center !important;"> 
        <ul class="form-section page-section"; style="background: rgb(255, 255, 255, 0) !important"> 
            <li class="form-line" id="id_10"> 
                <div id="cid_10" class="form-input-wide"> 
                    <div style="text-align:center; padding-top:15px;" class="form-buttons-wrapper"> 
                       
                        <h1 class="main_title center"><p style="color:#FFFFFF">Want to be part of it?</p></h1>
                        <div class="descr center"><p style="color:#FFFFFF;">fill out the form and we will select the best startups until July 15th<br>
Good luck!</p></div>
                    </div> 
                       <div class="form-buttons-wrapper">
                        <form action="https://form.jotformeu.com/71453803807356" method="post" target="_blank">
                            <button style="font-family:raleway, sans-serif; color:#FFFFFF; cursor:pointer; border-radius: 4px; border-color:#ea5c3f; background:#ea5c3f; width:100%; height: 47px;">Apply now</button>
                        </form>
                        </div>
                </div>
            </li> 
            <li class="form-line" data-type="control_divider" id="id_4"> 
                <div id="cid_4" class="form-input-wide"> 
                    <div data-component="divider" style="border-bottom:1px solid transparent;height:1px;margin-left:0px;margin-right:0px;margin-top:5px;margin-bottom:5px;"> </div> 
                </div>
                </li> 
            <li class="form-line" data-type="control_divider" id="id_5"> 
            <div id="cid_5" class="form-input-wide"> 
                <div data-component="divider" style="border-bottom:1px solid transparent;height:1px;margin-left:0px;margin-right:0px;margin-top:5px;margin-bottom:5px;"> </div> 
            </div> 
        </ul> 
    </div> 
    </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="section silver active section_1 section_pt">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">   
                        <div class="wrapp_main_text">
                            <h1 class="main_title">FAQ</h1>
                        </div> 
                        <div class="grid clearfix">
                            <div class="valign">    
                                <div class="middle">  
                                    <div class="topic topic_open">                                                     
                                        <h1 class="question_title">What is included?</h1>
                                        <div class="arrow_pointer"></div>
                                        <div class="description_info descr descr_no_margin" style="display:none">
                                            <div class="question_subtitle">Before Bits & Pretzels</div> 
                                            <ul class="questions_list questions_list-unordered">
                                               <li>
                                                   Company logo and link on event website and exhibitor listings
                                               </li>
                                            </ul>
                                            <div class="question_subtitle">During Bits &amp; Pretzels</div> 
                                            <ul class="questions_list questions_list-unordered">
                                               <li>
                                                   Personal exhibition counter for one day (September 24 OR 25)
                                               </li>
                                                <li>
                                                   Three 3-day tickets for Bits &amp; Pretzels
                                               </li>
                                                <li>
                                                   Wi-Fi and electricity provided
                                               </li>
                                               <li>
                                                   Printed company logo with company description
                                               </li>
                                                <li>
                                                   Brand inclusion in event app and exhibitor listings
                                               </li>
                                                <li>
                                                   Preferred right to choose a Table Captain
                                               </li>
                                                <li>
                                                    Invitation to launch party
                                               </li>
                                            </ul>
                                        </div>
                                    </div> 
                                    <div class="topic topic_open">                                                     
                                        <h1 class="question_title">How do I participate?</h1>
                                        <div class="arrow_pointer"></div>
                                        <div class="description_info descr descr_no_margin" style="display:none">
                                            <ol class="questions_list">
                                               <li>
                                                  Fill out the application form above with all the important details
                                               </li>
                                                <li>
                                                  After June 30th, the lucky winners will be contacted
                                               </li>
                                                <li>
                                                  Book your free exhibition package and plan your trip to Munich
                                               </li>
                                               <li>
                                                  Exhibit at Bits &amp; Pretzels
                                               </li>
                                            </ol>
                                        </div>
                                    </div> 
                                    <div class="topic topic_open">                                                     
                                        <h1 class="question_title">When will participants be announced?</h1>
                                        <div class="arrow_pointer"></div>
                                        <div class="description_info descr descr_no_margin" style="display:none">
                                            <p class="question_text">
                                                As soon as the application deadline ends on June 30th, we will start selecting the best startups. The winners will be contacted as soon as possible.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="topic topic_open">                                                     
                                        <h1 class="question_title">I have already bought tickets, can I return them?</h1>
                                        <div class="arrow_pointer"></div>
                                        <div class="description_info descr descr_no_margin" style="display:none">
                                            <p class="question_text">
                                                Yes, in case you've already bought tickets and are amongst the lucky winners, you can transfer or return the previously purchased tickets.
                                            </p>
                                        </div>
                                    </div>   
                                    <div class="topic topic_open">                                                     
                                        <h1 class="question_title">What is the purpose of exhibiting?</h1>
                                        <div class="arrow_pointer"></div>
                                        <div class="description_info descr descr_no_margin" style="display:none">
                                            <p class="question_text">
                                                It's all about boosting awareness that will help you grow, increasing your media exposure and helping you to build profound business relationships with corporations and investors. Use your chance to get in touch with thousands of attendees, present your company, find new clients and business partners.
                                            </p>
                                        </div>
                                    </div> 
                                    <div class="topic topic_open">                                                     
                                        <h1 class="question_title">I have specific questions, whom do I contact?</h1>
                                        <div class="arrow_pointer"></div>
                                        <div class="description_info descr descr_no_margin" style="display:none">
                                            <p class="question_text">
                                                Please contact <a style="color:#27A5E3" href='&#109;a&#105;&#108;to&#58;e&#37;78h%69bit&#37;6&#57;o%6&#69;%40%6&#50;its&#37;6&#49;n&#100;%70re&#37;7&#52;&#122;&#101;&#108;s&#46;&#37;6&#51;om'>exh&#105;b&#105;t&#105;o&#110;&#64;&#98;i&#116;san&#100;pretzel&#115;&#46;c&#111;m</a> for any further exhibition queries.
                                            </p>
                                        </div>
                                    </div>                                                                                          
                                </div>
                            </div>
                        </div>            
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
</div>


<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>

