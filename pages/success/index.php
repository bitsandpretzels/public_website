<?php include 'layout/top.php';?>
<?php include 'layout/header.php';?>

<div class="inner_sections">

    <!--
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

  <div id="sub_nav" class="sub_nav_wrap">
     <div class="wrap_subnav_inner">
        <div class="title_section">Congratulations!</div>
      </div>
  </div>
    
    <!-- START: Revolution Slider -->
    <?php include_once ('layout/headers/slider_success.php');?>
    <!-- END: Revolution Slider -->
    
    <section class="section section_logo white active section_1 section_pt section_success">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text descr_big_margin paralax_section">
                            <h1 class="main_title center" id="content" style="margin-bottom:35px">We are happy to see you at Bits &amp; Pretzels. The event will be even better when your friends are there too.</h1>
                            <ul class="share-buttons">
                          <li>
                              <a href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.bitsandpretzels.com%2Fspacey&t=Kevin%20Spacey%20holds%20opening%20keynote%20at%20Bits%20%26%20Pretzels%20%2F%2F%20Meet%20him%20in%20Munich!" title="Share on Facebook" target="_blank">
                                  <img alt="Share on Facebook" src="https://www.bitsandpretzels.com/src/images/icons_share/Facebook.png">
                              </a>
                          </li>
                          <li>
                              <a href="https://twitter.com/intent/tweet?source=https%3A%2F%2Fwww.bitsandpretzels.com%2Fspacey&text=Kevin%20Spacey%20holds%20opening%20keynote%20at%20Bits%20%26%20Pretzels%20%2F%2F%20Meet%20him%20in%20Munich!:%20https%3A%2F%2Fwww.bitsandpretzels.com%2Fspacey&via=bitsandpretzels" target="_blank" title="Tweet">
                                  <img alt="Tweet" src="https://www.bitsandpretzels.com/src/images/icons_share/Twitter.png">
                              </a>
                          </li>
                          <li>
                              <a href="http://www.tumblr.com/share?v=3&u=https%3A%2F%2Fwww.bitsandpretzels.com%2Fspacey&t=Kevin%20Spacey%20holds%20opening%20keynote%20at%20Bits%20%26%20Pretzels%20%2F%2F%20Meet%20him%20in%20Munich!&s=" target="_blank" title="Post to Tumblr">
                                  <img alt="Post to Tumblr" src="https://www.bitsandpretzels.com/src/images/icons_share/Tumblr.png">
                              </a>
                          </li>
                              <li>
                          <a href="http://pinterest.com/pin/create/button/?url=https%3A%2F%2Fwww.bitsandpretzels.com%2Fspacey&media=https://www.bitsandpretzels.com/src/images/info/spacey_facebook.png&description=The%20Oscar-winning%20actor%20and%20tech%20investor%20will%20be%20on%20stage.%20Join%20Bits%20%26%20Pretzels%20and%20don%E2%80%99t%20miss%20THE%20chance%20that%20Kevin%20will%20come%20to%20Germany!" target="_blank" title="Pin it">
                                  <img alt="Pin it" src="https://www.bitsandpretzels.com/src/images/icons_share/Pinterest.png">
                              </a>
                          </li>
                          <li>
                              <a href="https://getpocket.com/save?url=https%3A%2F%2Fwww.bitsandpretzels.com%2Fspacey&title=Kevin%20Spacey%20holds%20opening%20keynote%20at%20Bits%20%26%20Pretzels%20%2F%2F%20Meet%20him%20in%20Munich!" target="_blank" title="Add to Pocket">
                                  <img alt="Add to Pocket" src="https://www.bitsandpretzels.com/src/images/icons_share/Pocket.png">
                              </a>
                          </li>
                              <li>
                          <a href="http://www.reddit.com/submit?url=https%3A%2F%2Fwww.bitsandpretzels.com%2Fspacey&title=Kevin%20Spacey%20holds%20opening%20keynote%20at%20Bits%20%26%20Pretzels%20%2F%2F%20Meet%20him%20in%20Munich!" target="_blank" title="Submit to Reddit">
                                  <img alt="Submit to Reddit" src="https://www.bitsandpretzels.com/src/images/icons_share/Reddit.png">
                              </a>
                          </li>
                          <li>
                              <a href="http://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fwww.bitsandpretzels.com%2Fspacey&title=Kevin%20Spacey%20holds%20opening%20keynote%20at%20Bits%20%26%20Pretzels%20%2F%2F%20Meet%20him%20in%20Munich!&summary=The%20Oscar-winning%20actor%20and%20tech%20investor%20will%20be%20on%20stage.%20Join%20Bits%20%26%20Pretzels%20and%20don%E2%80%99t%20miss%20THE%20chance%20that%20Kevin%20will%20come%20to%20Germany!&source=https%3A%2F%2Fwww.bitsandpretzels.com%2Fspacey" target="_blank" title="Share on LinkedIn">
                                  <img alt="Share on LinkedIn" src="https://www.bitsandpretzels.com/src/images/icons_share/LinkedIn.png">
                              </a>
                          </li>
                          <li>
                              <a href="mailto:?subject=Kevin%20Spacey%20holds%20opening%20keynote%20at%20Bits%20%26%20Pretzels%20%2F%2F%20Meet%20him%20in%20Munich!&body=The%20Oscar-winning%20actor%20and%20tech%20investor%20will%20be%20on%20stage.%20Join%20Bits%20%26%20Pretzels%20and%20don%E2%80%99t%20miss%20THE%20chance%20that%20Kevin%20will%20come%20to%20Germany!:%20https%3A%2F%2Fwww.bitsandpretzels.com%2Fspacey" target="_blank" title="Send email">
                                  <img alt="Send email" src="https://www.bitsandpretzels.com/src/images/icons_share/Email.png">
                              </a>
                          </li>
                        </ul>
                        </div> 
<!--
                        <div class="wrapp_main_text descr_big_margin paralax_section">
                             <h1 class="main_title center">Book your Hotel with Special Discounts</h1>
                            <div class="descr descr_sub_mg center">To provide you with the best possible experience here in Munich we have hand-picked several great options for your stay.</div>
                            <a href="/info/hotels" class="button orange"><span>Have a look at our Partner-Hotels</span></a>
                        </div>
-->
                        <div class="wrapp_main_text descr_big_margin paralax_section">
                             <h1 class="main_title center">Your tickets</h1>
                            <div class="descr descr_sub_mg center">Your tickets and invoice have been emailed to you.<br> For any changes of your tickets (e.g. name, email address of attendees) click the button below.</div>
                            
                             <div id="cid_2" class="form-input-wide"> 
                        <div style="" class="form-buttons-wrapper"> 
                            <button  onClick="parent.location='https://www.eventbrite.com/gettickets'" style="font-family:raleway, sans-serif;cursor:pointer; width:50%; border-radius: 4px;height: 47px;background:#ea5c3f;" name ="input_2" id="input_2" type="submit" class="buttton, form-submit-button" data-component="button"><p style="color:#FFFFFF">Go to my tickets</p></button> 
                        </div>
                    </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
</div>

<!-- Friendbuy -->
<script>
    window['friendbuy'] = window['friendbuy'] || [];
    window['friendbuy'].push(['track', 'order',
      {
          id: '', //INPUT ORDER ID
          amount: '0.00', //INPUT ORDER AMOUNT
          //new_customer: false, //OPTIONAL, true if this is the customer's first purchase
          email: '' //INPUT EMAIL
      }
    ]);
</script>
  


<!-- Google Code for Ticket Conversion BP2016 Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 928324056;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "E8eGCKiK4mgQ2LPUugM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/928324056/?label=E8eGCKiK4mgQ2LPUugM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<!-- Facebook like btn and twitter share btn -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script>window.twttr = (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
  if (d.getElementById(id)) return t;
  js = d.createElement(s);
  js.id = id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);

  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };

  return t;
}(document, "script", "twitter-wjs"));</script>

<!-- End / Facebook like btn and twitter share btn -->

<?php include 'layout/footer.php';?>
<?php include 'layout/bottom.php';?>