<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<?php include_once realpath('layout/_inner_pages_main_nav.php');?>


 <div id="sub_nav" class="sub_nav_wrap">
     <div class="wrap_subnav_inner">
         <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
     </div>
 </div>

  
<div class="inner_sections">
<section class="page-newsletter">
    <div class="container center">
        <div class="row clearfix">
            <div class="ten columns offset-by-one">
                <h1 class="center" style="font-size:2.2em; color:white; margin-bottom:25px; text-transform:uppercase; font-weight:900">
                Startup Pitch</h1>
        <!--                    <img src="/src/images/2016/bird-overview.svg?v=<?php echo $currentScriptVersion; ?>" alt="Bird Overview" style="width:100%; margin-bottom:40px;">-->
                <p class="descr_small_margin" style="color:white; margin-bottom:20px; line-height:23px; font-size:1em">
                <strong>
                The application starts June 13th - stay tuned!
                </strong>
                </p>
            </div>
        </div>
    </div>
</section>
  <!-- START: content section -->
</div>

<style>
    .page-newsletter {
        background:url('/src/images/2016/welcome_hero-blue.png') no-repeat;
        background-size:cover;
        background-position:left center;
        padding:25vh 0 10vh 0;
        min-height:100vh;
    }
    
</style>



<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>
