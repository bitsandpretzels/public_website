
<?php include 'layout/top.php';?>
<?php include 'layout/header.php';?>

<div class="inner_sections">

    <?php include 'layout/_inner_pages_main_nav.php';?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>

    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

  <div id="sub_nav" class="sub_nav_wrap">
     <div class="wrap_subnav_inner">
        <div class="title_section">Welcome</div>
      </div>
  </div>


  <!-- START: navigation section -->

<section id="section_0" class="section section_home active section_transition" style="position: relative;">
  <div class="content_section center">
    <div class="wrapper valign">
      <div class="v-middle">
        <div class="logo"><a href="" title="Bits&amp;Pretzels"><img src="/src/images/logo.png" alt="Bits&amp;Pretzels"></a></div>
        <h1 class="main_title text-shadow">THE <span class="mobile_block text-shadow">FOUNDERS</span> <span class="mobile_block text-shadow">FESTIVAL</span></h1>
        <h2 class="sub_title text-shadow" style="margin: 35px 0 35px">September 25-27, 2016 in Munich</h2>
      </div>
    </div>
  </div>
</section>

  
  <?php if(1==2): ?>
  <!-- START: video section -->
  <section data-index="success" data-page="success" id="video" class="section section_video active section_block layer-video">
    <div id="preload_wrapper" class="mobile_hidden preload_wrapper">
      <div class="video-preloader" data-video="url">
        Video is loading...<br />
        <img src="/src/images/preloaders/squares-white.svg">
      </div>
    </div>
    <div id="video_home" class="video_home_wrap"></div>
    <div class="mobile_only top_img"><img src="/src/images/poster_mob.jpg" class="poster_img" alt=""></div>
  </section>
  <!-- END: video section -->
<?php endif; ?>



  
  <div class="mobile_only top_img"><img src="/src/images/poster_mob.jpg" class="poster_img" alt=""></div>
  


<section id="section_340x" class="section section_home active section_transition" data-show-inner="false" data-show-footer="false">
    <div class="content_section center inner_video_section_wrapper">
      <div class="wrapper valign">
              <div class="middle">
                  <h1 class="main_title" style="color:white">Congratulations!</h1>
                  <h2 class="sub_title" style="color:white">We are happy to see you @ Bits &amp; Pretzels</h2>
              </div>
          </div>
    </div> 
</section><!-- section -->
    

    <section class="section section_logo white active section_1 section_pt section_success">

        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text descr_big_margin paralax_section">
                            <h1 class="main_title">Bits & Pretzels will be even better when your friends are there too.</h1>
                            <div class="descr">
                                <div class="fb-share-button" data-href="https://www.facebook.com/bitsandpretzels/" data-layout="button_count" data-width="60"></div> or 
                                <div class="tw-like">
                                    <a class="twitter-share-button" href="https://twitter.com/bitsandpretzels" data-size="small" data-url="https://twitter.com/bitsandpretzels" data-via="https://twitter.com/bitsandpretzels" 
                                data-related="bits and pretzels">Tweet</a>
                                </div>
                            </div>  
                        </div>          
                        <div class="wrapp_main_text descr_big_margin paralax_section">
                             <h1 class="main_title">Your tickets Agenda</h1>
                            <div class="descr descr_sub_mg">Your tickets and invoice have been emailed to you.<br> For any changes of your tickets (e.g. name, email address of attendees) click the button below.</div>  
                            <a href="https://www.eventbrite.com/gettickets" class="button orange"><span>Go to my tickets</span></a>
                        </div>  
                        <div class="wrapp_main_text paralax_section">
                             <h1 class="main_title">Table Captain booking & networking app</h1>
                            <div class="descr descr_sub_mg">Table Captain booking will start in September 2016.<br> Our networking app will be live mid 2016.<br>A separate invitation will be sent to your email address.</div>  
                            <div class="img_brt">
                                <img src="/src/images/brt_logo.png" alt="Logo">
                            </div>
                        </div>                     
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
</div>


<!-- Facebook like btn and twitter share btn -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script>window.twttr = (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
  if (d.getElementById(id)) return t;
  js = d.createElement(s);
  js.id = id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);
 
  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };
 
  return t;
}(document, "script", "twitter-wjs"));</script>

<!-- End / Facebook like btn and twitter share btn -->

<?php include 'layout/footer.php';?>
<?php include 'layout/bottom.php';?>
