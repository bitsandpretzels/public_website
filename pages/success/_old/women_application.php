
<?php include 'layout/top.php';?>
<?php // include 'layout/header.php';?>

<div class="inner_sections">

    <?php include 'layout/_inner_pages_main_nav.php';?>

    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

    <div id="sub_nav" class="sub_nav_wrap" style="display: none">

        <div class="wrap_subnav_inner">
            <div class="title_section">Welcome</div>
        </div>
    </div>

<section data-index="home" id="video" class="section section_video active section_block" style="display:none">
  
  <div id="video_home" class="video_home_wrap">
    <video id="video-tag" class="video_home" controlls="none" autoplay="true" loop="false" poster="/src/images/poster.jpg" >
          <source src="/src/media/success.mp4" type="video/mp4" />
      </video>
     
    </div>

    <div class="mobile_only top_img"><img src="/src/images/poster_mob.jpg" class="poster_img" alt=""></div>
  
    <div class="content_section center inner_video_section_wrapper">
      <div class="wrapper valign">
              <div class="middle">
                  <h1 class="main_title_">THE <span class="mobile_block">FOUNDERS</span> <span class="mobile_block">FESTIVAL</span></h1>
                  <h2 class="sub_title_">25TH - 27TH SEP. 2016 IN MUNICH</h2>
              </div>
          </div>
    </div>

    <h2 style="position:absolute;bottom:0px;left:50%">DOWN</h2>


    
</section><!-- section -->

    

    <section class="section section_logo white active section_1 section_pt section_success">

        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text descr_big_margin paralax_section" style="display:none">
                            <h1 class="main_title center">Bits & Pretzels will be even better when your friends are there too.</h1>
                            <div class="descr">
                                <div class="fb-share-button" data-href="https://www.facebook.com/bitsandpretzels/" data-layout="button_count" data-width="60"></div> or 
                                <div class="tw-like">
                                    <a class="twitter-share-button" href="https://twitter.com/bitsandpretzels" data-size="small" data-url="https://twitter.com/bitsandpretzels" data-via="https://twitter.com/bitsandpretzels" 
                                data-related="bits and pretzels">Tweet</a>
                                </div>
                            </div>  
                        </div>          
                        <div class="wrapp_main_text descr_big_margin paralax_section">
                             <h1 class="main_title center">Thank you for applying for a Women in Tech ticket!</h1>
                            <div class="descr descr_sub_mg center">We will raffle the 300 free tickets on 29th of April. Stay tuned.</div>  
                            <a href="<?php echo Router::getRoute('home'); ?>" class="button orange"><span>Homepage</span></a>
                        </div>                  
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
</div>


<!-- Facebook like btn and twitter share btn -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script>window.twttr = (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
  if (d.getElementById(id)) return t;
  js = d.createElement(s);
  js.id = id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);
 
  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };
 
  return t;
}(document, "script", "twitter-wjs"));</script>

<!-- End / Facebook like btn and twitter share btn -->

<?php // include 'layout/footer.php';?>
<?php include 'layout/bottom.php';?>
