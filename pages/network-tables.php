<?php include_once realpath('layout/top.php');?>
<div class="inner_sections">
    <section id="investors" class="section section_logo white active section_1 section_pt" style="padding:0" data-show-inner="false" data-show-footer="false" style="min-height: 0px">
        <div class="content_section" style="min-height: 0px">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">How Table Captain Selection will work</h1>
                            <div class="descr descr_small_margin center" style="color:#ea5c3f; font-weight:bold; margin-top:30px">
                            Table Captain Selection will start right here <br>! <span style="text-decoration:underline">approximately 10 days before the event.</span> !
                            </div>                             
                            <div class="descr descr_small_margin">
                            <br>    
                            1. Already now you should do some research. Check the Table Captain List and note down at least three of your favorites.
                            <br><br>
                            2. You will receive an email when the selection starts. There will be different selection rounds.
                            Buyers of crazy bird tickets will start, followed by early bird buyers and so on. Be quick, it‘s first
                            come first served. If your favorite‘s table is already fully booked select another one.
                            </div>                             
                            <img src="/src/images/info/network-tables.png" style="width:100%" alt="">
                            <div class="descr" style="margin-top:50px">
                            3. As soon as you have chosen your spot you will receive an email with the table number (e.g. Row B, Table 5), which you will be able to find at the floor plan. At the third day of Bits & Pretzels you can meet your Table Captain and other attendees at your table. Have fun!
                            </div>
                        </div> 
                            <div class="center" style="width:100%">
                                <img src="/src/images/info/network-tables-2.png"alt="">
                            </div>
                        <div class="wrapp_main_text">
                            <div class="descr" style="margin-top:50px">
                            If you can’t find a captain you like you can book a spot in the cluster areas (tables at the tent will be color coded). There you will find other attendees who share the same interests.
                            </div> 
                            <div class="descr descr_small_margin center" style="color:#ea5c3f; font-weight:bold; margin-top:30px">
                            Please respect other attendees and don‘t block seats that are not yours. If you ask politely you might have the chance to meet your favorite Table Captain later that day :)
                            </div>                             
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
</div>