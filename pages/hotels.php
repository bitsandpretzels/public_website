<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>


    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

    <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section"><span class="mobile_hidden">Hotels &amp; Flights</span></div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>            
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                </li>
                <li>
                    <a class="link_nav buyticket trigger_app_nav" data-section="buyticket"><?php echo Meta::getButtonWording(); ?></a>
                </li>
            </ul>
        </div>
    </div>


    <section id="investors" class="section section_logo silver active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="min-height: 0px; padding-bottom:40px">
        <div class="content_section center" style="min-height: 0px; ">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">Hotels &amp; Flights</h1>
                            <div class="descr" style="color:#0094d4; margin-bottom:0">
                                 <span class="text_icon" style="background-image:url('src/images/icon_agenda.svg'); margin-top:15px"></span>
                                 Sun | 24.09.2017 – Thu | 26.09.2017
                            </div>
                            <div class="descr descr_small_margin center">
                                <br><strong>Day 1 &amp; 2:</strong> 	ICM - International Congress Center Munich 
                                <br>Messegelände, 81823 Munich, Germany
                                <br>
                                <br><strong>Day 3:</strong> 		Oktoberfest - Theresienwiese,  Theresienhöhe 
                                <br>80339 Munich, Germany

<!--                            To provide you with the best possible experience here in Munich we have hand-picked several great options for your stay:-->
                            </div>
                        </div>  
                    </div>
                </div>
                
            </div>
        </div>
    </section><!-- section -->
    
    <section id="investors" class="section section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="min-height: 0px; padding-top:50px;padding-bottom:40px">
        <div class="content_section center" style="min-height: 0px">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">Hotels close to ICM</h1>
<!--
                            <div class="descr descr_small_margin center">To provide you with the best possible experience here in Munich we have hand-picked several great options for your stay:<div class="link_box"></div>
                            </div>
-->
                        </div>  
                    </div>
                </div>
                <!--                Hotels nahe ICM-->
                <div id="travis-widget"><iframe id="travis-widget-frame" src="https://app.travis.events/v2/#/event/123/hotels?header=off&event_info=off&recommendation_count=9&name=bitsandpretzels&" style="width:100%;height:1000px;display: block; margin: auto;border: none;box-shadow: none;"></iframe><script type="text/javascript" src="https://app.travis.events/widget/travis-widget.js"></script></div>
            </div>
        </div>
    </section><!-- section -->
       
       
       <section id="investors" class="section section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="min-height: 0px;padding-top:0; padding-bottom:40px">
        <div class="content_section center" style="min-height: 0px">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">Hotels close to Oktoberfest</h1>
<!--
                            <div class="descr descr_small_margin center">To provide you with the best possible experience here in Munich we have hand-picked several great options for your stay:<div class="link_box"></div>
                            </div>
-->
                        </div>  
                    </div>
                </div>
                <!--                Hotels nahe Oktoberfest -->
                <div id="travis-widget"><iframe id="travis-widget-frame" src="https://app.travis.events/v2/#/event/124/hotels?header=off&event_info=off&recommendation_count=9&name=bitsandpretzels&" style="width:100%;height:1000px;display: block; margin: auto;border: none;box-shadow: none;"></iframe><script type="text/javascript" src="https://app.travis.events/widget/travis-widget.js"></script></div>
            </div>
        </div>
    </section><!-- section -->
    
    <section id="investors" class="section section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="min-height: 0px; padding-top:0">
        <div class="content_section center" style="min-height: 0px">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">Recommended Flights</h1>
<!--
                            <div class="descr descr_small_margin center">To provide you with the best possible experience here in Munich we have hand-picked several great options for your stay:<div class="link_box"></div>
                            </div>
-->
                        </div>  
                    </div>
                </div>
                <div id="travis-widget"><iframe id="travis-widget-frame" src="https://app.travis.events/v2/#/event/123/flights?header=off&event_info=off&recommendation_count=9&name=bitsandpretzels&" style="width:100%;height:1000px;display: block; margin: auto;border: none;box-shadow: none;"></iframe><script type="text/javascript" src="https://app.travis.events/widget/travis-widget.js"></script></div>
            </div>
        </div>
    </section><!-- section -->
    
</div>


<?#php include realpath('layout/signup-form.php');?>
<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>