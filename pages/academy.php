<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once ('layout/_scroll_down_hint.php');?>
    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

     <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section mobile_hidden">Academy</div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>            
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#agenda" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                </li>
                <li>
                    <a class="link_nav buyticket trigger_app_nav" data-section="buyticket"><?php echo Meta::getButtonWording(); ?></a>
                </li>
            </ul>
        </div>
    </div>

    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-academy active section_1">

    	<div class="content_section center">
    		<div class="wrapper">
                <div class="info_text_top">
                    <h1 class="top_title" style="text-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);">The Bits &amp; Pretzels Academy</h1>
                    <!--
                    <h2 class="main_title">What do startups need to be successful? <span class="stay_block">Learn it at the Bits & Pretzels Academy.</span></h2> 
                    -->         
                </div>
            </div>
    	</div>
    </section><!-- section -->

    <section id="content" class="section section_logo silver white active section_1 section_pt">

        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">Learn from the best at the Bits &amp; Pretzels Academy.</h1>
                            <div class="descr descr_big_margin center">As founders we know that the growth of your startup is directly linked to your personal growth. At Bits &amp; Pretzels we want to help you improve in every neccessary and possible way. Therefore we will dedicate in 2016 a whole stage solely to enlightning and relevant Masterclasss by successful entrepreneurs, notable scientists and visionary thinkers.
                            </div>  
                        </div>          
                        <div class="wrapp_main_text">
                             <h1 class="main_title center">Agenda</h1>
<!--                            <div class="descr descr_sub_mg center">We are currently acquiring the best possible speakers for each masterclass. Stay tuned.</div>  -->
                        </div>   
                        <div class="wrap_quest">
                            <div class="grid clearfix">
                                <div class="valign">    
                                    <div class="middle"> 
                                    <?php for($i = 0; $i < count($academy_2016); $i++) { ?>  
                                        <div class="topic flex">
                                            <div class="avatar">
                                                <img src="/src/images/speakers/default/<?= $academy_2016[$i]['speaker_image'] ?>" width="100">
                                            </div>
                                            <div class="info">
                                                <div>
                                                <h1 class="topic_title"><?= $academy_2016[$i]['headline'] ?></h1>
                                                <div class="descr descr_no_margin"><?= $academy_2016[$i]['description'] ?></div> 
                                                <div class="topic_speaker descr descr_no_margin">Speaker: <strong><?= $academy_2016[$i]['speaker'] ?></strong></div>
<!--                                                <div class="arrow_pointer"></div>-->
                                                <div class="description_info descr descr_no_margin"><?= $academy_2016[$i]['description_more'] ?></div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div> 
                        </div>                   
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
</div>


<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>

