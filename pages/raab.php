<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>


    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

    <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section mobile_hidden">Stefan Raab</div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>            
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                </li>
                <li>
                    <a class="link_nav buyticket trigger_app_nav" data-section="buyticket"><?php echo Meta::getButtonWording(); ?></a>
                </li>
            </ul>
        </div>
    </div>

    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-branson active section_1" style="background:url('src/images/2016/welcome_hero-blue.png'), center, center; background-size:cover;">
        <div class="content_section center">
            <div class="wrapper">
                <div class="info_text_top">
                    <h1 class="top_title">Stefan Raab <br>is coming</h1>
                </div>
            </div>
        </div>
    </section><!-- section -->



    <section id="content" class="section section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="min-height: 0px">
        <div class="content_section center" style="min-height: 0px">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">Stefan Raab holds keynote speech at Bits &amp; Pretzels</h1>
                            <div class="descr descr_small_margin center">Stefan Raab, TV host, entrepreneur, songwriter, musician, and TV- and music producer, will be joining this year’s Founders Festival in Munich with 5,000 founders, startup enthusiasts, and investors. On the second day of the festival, you have the chance to watch Stefan Raab’s speech, which is a unique opportunity since his public appearances are very rare.<br><br> 
                            It’s hard to imagine the last 25 years of German TV without Stefan Raab. With more than 2200 Episodes of his late night show “TV total“, shows like “Schlag den Raab“ or “Wok WM“, his productions always reached the best viewing rates. He has an absolute „doer“ mentality and has never stopped inventing and pushing new ideas forward. Even if they sometimes sounded crazy at the time he always had the courage to go through with them and be successful at the end. Raab is an amazing storyteller who always focuses on details and perfection.<br><br>
                            But that’s not all: With the company Raab TV he sold several of his TV show formats internationally. Furthermore, he invented a special shower head.<br><br>We’re super excited to welcome Stefan Raab at this year’s Bits &amp; Pretzels and can’t wait to see him on stage.
<div class="link_box"></div>
                            </div>                             
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
</div>


<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>