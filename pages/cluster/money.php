<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">
    
    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>
    <?php include_once realpath('layout/_inner_pages_cluster_nav.php');?>


    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-iot active section_1" style="background:url('/src/images/cluster/hero/money.png'), center, center; background-size:cover;">
        <div class="content_section content_section-cluster center">
            <div class="wrapper">
                <div class="cluster_icon_top" style="bottom:26%">
                    <img src="/src/images/cluster/icon_only/money.svg" style="margin-right:4%" alt="big money icon">
                </div>
                <div class="info_text_top">
                    <h1 class="top_title">BIG MONEY</h1>
                </div>
            </div>
        </div>
    </section><!-- section -->
    <section id="investors" class="section section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="min-height: 0px;">
        <div class="content_section content_section-cluster center" style="min-height: 0px">
            <div class="wrapper_inner wrapper_inner-cluster">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text wrapp_main_text-cluster">
                            <h1 class="main_title center green">Big Money</h1>
<!--                            <h2 class="down_sub_title center" style="margin-bottom:10px">presented by</h2>-->
<!--                            <img src="/src/images/cluster/logos/allianz.png" style="margin-bottom: 30px; width:100px" alt="Allianz">-->
                            <div class="descr descr_small_margin center">
                                Traditional banking, insurance, investments and money management is disrupted by digital innovation. This cluster is all about the innovation within the area of financial services. FinTech, Insurtech and Bitcoin will be discussed by the most innovative startups and market leaders.
                            <div class="link_box"></div></div>  
                            <h2 class="down_sub_title center green" style="margin-bottom:10px">Relevant for market players in:</h2>
                            <div class="center">
                                <ul class="descr descr_small_margin descr-cluster" style="width:250px">
                                    <li style="list-style:square">FinTech/Banking </li>
                                    <li style="list-style:square">Insurance </li>
                                    <li style="list-style:square">Blockchain</li>
                                    <li style="list-style:square">Investment</li>
                                    <li style="list-style:square">Crowdfunding</li>
                                </ul>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        </section>
<!--        <section id="speakers" class="section section_users white active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="padding-top:0">
        <div class="content_section content_section-cluster center">
            <div class="wrapper_inner wrapper_inner-cluster">
                <div class="valign">
                    <div class="middle">
                            <h1 class="main_title center green" style="margin-top:50px">Get inspired by awesome speakers.</h1>
                            <div class="wrap_speakers wrap_speakers-cluster">
                                <div class="conteiner">
                                    <div class="list_users list_users-cluster">
                                        <?php foreach(People::setType('speaker')->setDataSource($speakers)->get(0, 200) as $person): 
                                        if (strpos($person['cluster'],"money")!==false && strpos($person['track'],"blockchain")===false ) { ?>
                                            <div class="user_box cluster_speaker">
                                                <div class="user-offset">
                                                    <div class="img_user"><img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt=""></div>
                                                    <p class="name_user"><?php echo $person['name']; ?></p>
                                                    <div class="title"><?php echo $person['title']; ?></div>
                                                    <div class="company"><?php echo $person['company']; ?></div>
                                                </div>
                                            </div>
                                        <?php }
                                        endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
    <!--
        <section id="speakers" class="section section_users light-green active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="content_section content_section-cluster center">
            <div class="wrapper_inner wrapper_inner-cluster">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text wrapp_main_text-cluster">
                            <h1 class="main_title no-sub-title green">BLOCKCHAIN TRACK</h1>
                            <h2 class="down_sub_title center" style="margin-bottom:20px">
                                “Beyond Bitcoin: How Blockchains disrupt more than just Finance.” <br>
                            </h2>
                            <br>
                            <div class="descr descr_small_margin">
                                <div>
                                    Bitcoin has evolved into a growing mainstream alternative currency and has became a contradictory topic over the last years. Some say it’s the most important invention since the internet, others are very doubtful. In fact, the technology behind Bitcoin has become a rising trend in many companies. So, how can the underlying technology of Bitcoin, Blockchain disrupt more than just the finance sector? In this track, experts will show the hidden opportunities for startups and provide insights into one of the most complex technologies.
                                </div>                                            
                            </div>
                        </div>           
                        <div class="wrap_speakers">
                            <div class="conteiner">
                                <div class="list_users list_users-cluster">
                                      <?php foreach(People::setType('speaker')->setDataSource($speakers)->get(0, 200) as $person): 
                                        if (strpos($person['track'],"blockchain")!==false) { ?>
                                            <div class="user_box cluster_speaker">
                                                <div class="user-offset">
                                                    <div class="img_user"><img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt=""></div>
                                                    <p class="name_user"><?php echo $person['name']; ?></p>
                                                    <div class="title"><?php echo $person['title']; ?></div>
                                                    <div class="company"><?php echo $person['company']; ?></div>
                                                </div>
                                            </div>
                                    <?php }
                                    endforeach; ?>
                                </div>
                            </div>
                            <h2 class="down_sub_title center green" style="margin: 10px 0 10px 0">Curated By</h2>
                            <div class="list_users clearfix list_users-cluster-curator">
                                <div class="user_box user_box-cluster-curator">
                                    <div class="user-offset">
                                        <div class="img_user"><img src="/src/images/speakers/default/hering_christoph.png" alt="Christoph Hering"></div>
                                        <p class="name_user">Christoph Hering</p>
                                        <div class="title">CEO &amp; Co-Founder</div>
                                        <div class="company">BitShares Munich</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		
		<hr class="gray-section-divider section_divider-cluster green">
        <div class="content_section content_section-cluster center">
            <div class="wrapper_inner wrapper_inner-cluster">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text wrapp_main_text-cluster">
                            <h1 class="main_title no-sub-title green">FINTECH TRACK</h1>
							<h2 class="down_sub_title center" style="margin-bottom:10px">presented by</h2>
							<img src="/src/images/cluster/logos/hypovereinsbank.png" style="margin-bottom: 16px" width="160px" alt="Hypovereinsbank">
                            <h2 class="down_sub_title center" style="margin-bottom:20px">
                            </h2>
                            <br>
                            <div class="descr descr_small_margin">
                                <div>
                                    Digital innovation is disrupting the way people are doing banking. The traditional way of financial services are being overwhelmed by the smarter, faster and consumer-centric FinTech. In this track you’ll get insights on the actual trends in the FinTech area, get to know the main players and hear about some of the experts successful and less successful business models. 
                                </div>                                            
                            </div>
                        </div>           
                        <div class="wrap_speakers">
                            <div class="conteiner">
                                <div class="list_users list_users-cluster">

                                      <?php foreach(People::setType('speaker')->setDataSource($speakers)->get(0, 200) as $person): 
                                        if (strpos($person['track'],"fintech")!==false) { ?>
                                            <div class="user_box cluster_speaker">
                                                <div class="user-offset">
                                                    <div class="img_user"><img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt=""></div>
                                                    <p class="name_user"><?php echo $person['name']; ?></p>
                                                    <div class="title"><?php echo $person['title']; ?></div>
                                                    <div class="company"><?php echo $person['company']; ?></div>
                                                </div>
                                            </div>
                                    <?php }
                                    endforeach; ?>
                                </div>
                            </div>

                            <h2 class="down_sub_title center green" style="margin: 90px 0 10px 0">Curated By</h2>
                            <div class="list_users clearfix list_users-cluster-curator">
                                <div class="user_box user_box-cluster-curator">
                                    <div class="user-offset">
                                        <div class="img_user"><img src="/src/images/speakers/default/bajorat_andre_marcel.png" alt=""></div>
                                        <p class="name_user">André M. Bajorat</p>
                                        <div class="title">CEO</div>
                                        <div class="company">Figo</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		<hr class="gray-section-divider section_divider-cluster green">
        <div class="content_section content_section-cluster center">
            <div class="wrapper_inner wrapper_inner-cluster">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text wrapp_main_text-cluster">
                            <h1 class="main_title no-sub-title green">INSURTECH TRACK</h1>
							<h2 class="down_sub_title center" style="margin-bottom:10px">supported by</h2>
							<img src="/src/images/cluster/logos/allianz.png" style="margin-bottom: 16px" width="118px" alt="VDMA">
                            <br>
                           <h2 class="down_sub_title center" style="margin-bottom:20px">
                                “InsurTechs and insurance corporations - Competition, coexistance or collaboration?” <br>
                            </h2>
                            <br>
                            <div class="descr descr_small_margin">
                                <div>
                                    The insurance business is highly based on trust, with many global corporations as big players. In the age of digitalization, startups challenge these players with their innovative approaches to insurance and services. In this track, you can follow a keynote speech and lively discussions of representatives from startups, incubators and insurance corporations. You will get insights into challenges of startups and corporations in the changing industry, as well as into possibilities how to work together with corporations, and into needed insurance products to protect your startup as best as possible.
                                </div>                                            
                            </div>
                        </div>           
                        <div class="wrap_speakers">
                            <div class="conteiner">
                                <div class="list_users list_users-cluster">
                                     <?php foreach(People::setType('speaker')->setDataSource($speakers)->get(0, 200) as $person): 
                                                if (strpos($person['track'],"insurtech")!==false) { ?>
                                                    <div class="user_box cluster_speaker">
                                                        <div class="user-offset">
                                                            <div class="img_user"><img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt=""></div>
                                                            <p class="name_user"><?php echo $person['name']; ?></p>
                                                            <div class="title"><?php echo $person['title']; ?></div>
                                                            <div class="company"><?php echo $person['company']; ?></div>
                                                        </div>
                                                    </div>
                                            <?php }
                                            endforeach; ?>
                                        </div>
                                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
		
<!--
        <hr class="gray-section-divider section_divider-cluster green">
        <div class="content_section content_section-cluster center">
            <div class="wrapper_inner wrapper_inner-cluster">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text wrapp_main_text-cluster">
                            <h1 class="main_title no-sub-title green">INSURETECH TRACK</h1>
                            <h2 class="down_sub_title center" style="margin-bottom:10px">in partnership with</h2>
                            <img src="/src/images/cluster/logos/allianz.png" style="margin-bottom: 16px" width="100px" alt="VDMA">
                            <div class="descr descr_small_margin">
                                <div>
                                    Coming soon…
                                </div>                                            
                            </div>
                            <h2 class="down_sub_title center" style="margin-bottom:20px">
                                “XXXXXXXXXXXXXXXXXXXX” <br>// September 26
                            </h2>
                            <br>
                            <div class="descr descr_small_margin">
                                <div>
                                    The driving force in the growth of wearable tech is not only led by the smartwatch market, there are other main drivers that play a crucial role. In this track you’ll get insights into new and rising markets for wearables. Experts will introduce you to the growing sex toy market and the gaming industry, which is heavily affected by the developments of new wearable technology.
                                </div>                                            
                            </div>
                        </div>           
                        <div class="wrap_speakers">
                            <div class="conteiner">
                                <div class="list_users list_users-cluster">
                                    <?php foreach(People::setType('speaker')->setDataSource($speakers)->get(0, 6) as $person):  ?>
                                        <div class="user_box user_box-cluster">
                                            <div class="user-offset">
                                                <div class="img_user"><img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt=""></div>
                                                <p class="name_user"><?php echo $person['name']; ?></p>
                                                <div class="title"><?php echo $person['title']; ?></div>
                                                <div class="company"><?php echo $person['company']; ?></div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <h2 class="down_sub_title center green" style="margin: 10px 0 10px 0">Curated By</h2>
                            <div class="list_users clearfix list_users-cluster-curator">
                                <div class="user_box user_box-cluster-curator">
                                    <div class="user-offset">
                                        <div class="img_user"><img src="/src/images/speakers/default/<?php echo $speakers[0]['image']; ?>" alt=""></div>
                                        <p class="name_user"><?php echo $speakers[0]['name']; ?></p>
                                        <div class="title"><?php echo $speakers[0]['title']; ?></div>
                                        <div class="company"><?php echo $speakers[0]['company']; ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="wrapp_main_text wrapp_main_text-cluster">
                            <div class="descr descr_small_margin center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Expectoque quid ad id, quod quaerebam, respondeas. Sed eum qui audiebant, quoad poterant, defendebant sententiam suam. Scaevola tribunus plebis ferret ad plebem vellentne de ea re quaeri. Satisne vobis videor pro meo iure in vestris auribus commentatus? Cur igitur, inquam, res tam dissimiles eodem nomine appellas? Duo Reges: constructio interrete. Peccata paria. Quod ea non occurrentia fingunt, vincunt Aristonem; Aliter homines, aliter philosophos loqui putas oportere? Quid est, quod ab ea absolvi et perfici debeat? Sed quid minus probandum quam esse aliquem beatum nec satis beatum?<div class="link_box"></div></div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section> 
-->
    
    
   <?php include realpath('layout/cluster_cta.php');?>
    
<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>