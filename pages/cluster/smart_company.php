<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">
    
    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>
    <?php include_once realpath('layout/_inner_pages_cluster_nav.php');?>


    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-iot active section_1" style="background:url('/src/images/cluster/hero/smart_company.png'), center, center; background-size:cover;">
        <div class="content_section content_section-cluster center">
            <div class="wrapper">
                <div class="cluster_icon_top" style="bottom:49%">
                    <img src="/src/images/cluster/icon_only/lifestyle.svg" style="margin-right:2%; width:25%" alt="big money icon">
                </div>
                <div class="info_text_top">
                    <h1 class="top_title">Smart <br>Company</h1>
                </div>
            </div>
        </div>
    </section><!-- section -->
    <section id="investors" class="section section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="min-height: 0px; padding-bottom:80px">
        <div class="content_section content_section-cluster center" style="min-height: 0px">
            <div class="wrapper_inner wrapper_inner-cluster">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text wrapp_main_text-cluster">
                            <h1 class="main_title center darkgreen">Smart Company</h1>
<!--                            <h2 class="down_sub_title center" style="margin-bottom:10px">presented by</h2>-->
<!--                            <img src="/src/images/cluster/logos/salesforce.png" style="margin-bottom: 30px; width:150px" alt="Allianz">-->
                            <div class="descr descr_small_margin center">
                                The constant change in technology results in everyone and everything being more and more connected. There are billions of connected products, devices, and sensors around us, creating trillions of customer interactions. And everything is becoming connected on the smartphone. The digital revolution impacts all business models, companies and processes. In these dynamic and innovative times, Startups have more opportunities than ever to shape and redefine their market and to connect with their customers in a whole new way. Smart Startups know that they have to focus, act quickly, manage to do more with less and rocket fuel their growth with innovative solutions. The successful startups in all industries rely on digital technology; a platform to automate processes, use the data they collect and optimize all areas of the company connecting sales, service, marketing, application development and data science.
								<div class="link_box"></div></div>  
                            <h2 class="down_sub_title center darkgreen">Relevant for market players in:</h2>
                            <div class="center">
                                <ul class="descr descr_small_margin descr-cluster" style="width:250px">
                                    <li style="list-style:square">SaaS</li>
                                    <li style="list-style:square">Production</li>
                                    <li style="list-style:square">HR / Recruiting</li>
                                    <li style="list-style:square">Cyber Security</li>
                                    <li style="list-style:square">Education</li>
                                    <li style="list-style:square">LegalTech</li>
                                    <li style="list-style:square">Marketing & Advertising</li>
                                    <li style="list-style:square">Communications</li>
                                    <li style="list-style:square">Artificial Intelligence</li>
                                    <li style="list-style:square">Big Data</li>
                                </ul>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        </section>
    
        
<!--
        <section id="speakers" class="section section_users light-green active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="padding-top:0; padding-bottom:47px">
            <div class="content_section content_section-cluster center">
                <div class="wrapper_inner wrapper_inner-cluster">
                    <div class="valign">
                        <div class="middle">
                                <h1 class="main_title center darkgreen" style="margin-top:60px">Smart Company Track 1</h1>
                                <h2 class="down_sub_title center" style="margin-bottom:10px">supported by</h2>
				                    <img src="/src/images/cluster/logos/salesforce.png" style="margin-bottom: 16px" width="118px" alt="Salesforce">
                            <br>
                                <div class="wrapp_main_text wrapp_main_text-cluster">
                                    <h2 class="down_sub_title center" style="margin-bottom:20px">
                                        “Fireside Chat: FinanceFox and FlixBus disrupt industries and supercharge their growth on a single platform.” <br> // September 07 
                                    </h2>
                                </div>
                                <br>
                                <div class="wrapp_main_text wrapp_main_text-cluster">
                                    <div class="descr descr_small_margin center" style="margin-top:52px">
                                       Are you ready to super-charge the growth of your company and make your mark in a consumer driven world? More and more Startups are benefiting from the incredible, growth-driving capabilities of innovative technologies focusing their funds, time and resources to connect with customers in a whole new way. Join this fireside chat to be part of an exciting dialogue between industry-disrupting startups and the leader in enterprise cloud computing. This is the session to hear the best practices from your successful peers.
                                    <div class="link_box"></div></div>                                                
                                </div>   
                                <div class="wrap_speakers wrap_speakers-cluster">
                                    <div class="conteiner">
                                        <div class="list_users list_users-cluster">
                                            <div class="user_box cluster_speaker desktop_only" style="opacity:0">
                                                    <div class="user-offset">
                                                        <div class="img_user"><img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt=""></div>
                                                        <p class="name_user"><?php echo $person['name']; ?></p>
                                                        <div class="title"><?php echo $person['title']; ?></div>
                                                        <div class="company"><?php echo $person['company']; ?></div>
                                                    </div>
                                            </div>
                                            <?php foreach(People::setType('speaker')->setDataSource($speakers)->get(0, 200) as $person): 
                                            if (strpos($person['track'],"smart_comp2")!==false) { ?>
                                                <div class="user_box cluster_speaker">
                                                    <div class="user-offset">
                                                        <div class="img_user"><img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt=""></div>
                                                        <p class="name_user"><?php echo $person['name']; ?></p>
                                                        <div class="title"><?php echo $person['title']; ?></div>
                                                        <div class="company"><?php echo $person['company']; ?></div>
                                                    </div>
                                                </div>
                                            <?php }
                                            endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
-->
<!--
    <section id="speakers" class="section section_users  active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="padding-top:0; padding-bottom:47px">
        <div class="content_section content_section-cluster center">
            <div class="wrapper_inner wrapper_inner-cluster">
                <div class="valign">
                    <div class="middle">
                            <h1 class="main_title center darkgreen" style="margin-top:60px">Smart Company Track 2</h1>
                            <div class="wrap_speakers wrap_speakers-cluster">
                                <div class="conteiner">
                                    <div class="list_users list_users-cluster">
                                        <?php foreach(People::setType('speaker')->setDataSource($speakers)->get(0, 200) as $person): 
                                        if (strpos($person['track'],"smart_company")!==false) { ?>
                                            <div class="user_box cluster_speaker">
                                                <div class="user-offset">
                                                    <div class="img_user"><img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt=""></div>
                                                    <p class="name_user"><?php echo $person['name']; ?></p>
                                                    <div class="title"><?php echo $person['title']; ?></div>
                                                    <div class="company"><?php echo $person['company']; ?></div>
                                                </div>
                                            </div>
                                        <?php }
                                        endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <h2 class="down_sub_title center green" style="margin: 90px 0 10px 0">Curated By</h2>
                <div class="list_users clearfix list_users-cluster-curator">
                    <div class="user_box user_box-cluster-curator">
                        <div class="user-offset">
                            <div class="img_user"><img src="/src/images/speakers/default/doerje_niels.png" alt=""></div>
                            <p class="name_user">Niels Doerje</p>
                            <div class="title">Digital Entrepreneur &amp;</div>
                            <div class="company">Serial Board Director</div>
                        </div>
                    </div>
                </div>
        <hr class="gray-section-divider section_divider-cluster darkgreen" style="margin:0 auto">
        </section>
-->
<!--
        <section id="speakers" class="section section_users light-darkgreen active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="content_section content_section-cluster center">
            <div class="wrapper_inner wrapper_inner-cluster">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text wrapp_main_text-cluster">
                            <h1 class="main_title no-sub-title darkgreen">LIFESTYLE TRACK</h1>
                            <div class="descr descr_small_margin">
                                <div>
                                    coming soon…
                                </div>                                            
                            </div>
                            <h2 class="down_sub_title center" style="margin-bottom:20px">
                                “Beyond Bitcoin: How Blockchains disrupt more than just Finance.” <br>// September 26
                            </h2>
                            <br>
                            <div class="descr descr_small_margin">
                                <div>
                                    Bitcoin has evolved into a growing mainstream alternative currency and has became a contradictory topic over the last years. Some say it’s the most important invention since the internet, others are very doubtful. In fact, the technology behind Bitcoin has become a rising trend in many companies. So, how can the underlying technology of Bitcoin, Blockchain disrupt more than just the finance sector? In this track, experts will show the hidden opportunities for startups and provide insights into one of the most complex technologies.
                                </div>                                            
                            </div>
                        </div>           
                        <div class="wrap_speakers">
                            <div class="conteiner">
                                <div class="list_users list_users-cluster">
                                    <?php foreach(People::setType('speaker')->setDataSource($speakers)->get(0, 6) as $person):  ?>
                                        <div class="user_box user_box-cluster">
                                            <div class="user-offset">
                                                <div class="img_user"><img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt=""></div>
                                                <p class="name_user"><?php echo $person['name']; ?></p>
                                                <div class="title"><?php echo $person['title']; ?></div>
                                                <div class="company"><?php echo $person['company']; ?></div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <h2 class="down_sub_title center darkgreen" style="margin: 10px 0 10px 0">Curated By</h2>
                            <div class="list_users clearfix list_users-cluster-curator">
                                <div class="user_box user_box-cluster-curator">
                                    <div class="user-offset">
                                        <div class="img_user"><img src="/src/images/speakers/default/<?php echo $speakers[0]['image']; ?>" alt=""></div>
                                        <p class="name_user"><?php echo $speakers[0]['name']; ?></p>
                                        <div class="title"><?php echo $speakers[0]['title']; ?></div>
                                        <div class="company"><?php echo $speakers[0]['company']; ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="wrapp_main_text wrapp_main_text-cluster">
                            <div class="descr descr_small_margin center">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quae hic rei publicae vulnera inponebat, eadem ille sanabat. Summae mihi videtur inscitiae. Non quam nostram quidem, inquit Pomponius iocans; Est enim tanti philosophi tamque nobilis audacter sua decreta defendere. Quae cum ita sint, effectum est nihil esse malum, quod turpe non sit. Sin te auctoritas commovebat, nobisne omnibus et Platoni ipsi nescio quem illum anteponebas?
                            <div class="link_box"></div></div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
<?php include realpath('layout/cluster_cta.php');?>
    
    
<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>