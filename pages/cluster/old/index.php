<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">
    
    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>
    <?php include_once realpath('layout/_inner_pages_cluster_nav.php');?>


    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-iot active section_1" style="background:url('/src/images/cluster/hero/cluster_title.png'), center, center; background-size:cover;">
        <div class="content_section content_section-cluster center">
            <div class="wrapper">
<!--                <div class="cluster_icon_top" style="bottom:26%">-->
<!--                    <img src="/src/images/cluster/icon_only/money.svg" style="margin-right:4%" alt="big money icon">-->
<!--                </div>-->
                <div class="info_text_top">
                    <h1 class="top_title" style="text-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);">Clusters</h1>
                </div>
            </div>
        </div>
    </section><!-- section -->
    <section id="content" class="section section_users active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center" style="margin-bottom:24px">Brand-new: The Bits &amp; Pretzels’ Clusters</h1>
                        </div>
                        <div class="wrap_speakers">
                            <div class="conteiner">
                                <div class="list_users">
                                    <a href="/cluster/commerce">
                                        <div class="user_box">
                                            <div class="user-offset">
                                                <img class="lazyload" data-src="/src/images/cluster/full_icon/commerce.svg" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="width:100%" />
                                                <noscript>
                                                    <img style="width:100%" src="/src/images/cluster/full_icon/commerce.svg" alt="Future Commerce">
                                                </noscript>
                                                <p class="name_user commerceorange" style="margin-top:10px; font-size:1.2em">Future Commerce</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="/cluster/mobility">
                                        <div class="user_box">
                                            <div class="user-offset">
                                                <img class="lazyload" data-src="/src/images/cluster/full_icon/mobility.svg" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="width:100%" />
                                                <noscript>
                                                    <img style="width:100%" src="/src/images/cluster/full_icon/mobility.svg" alt="Fast Mobility">
                                                </noscript>
                                                <p class="name_user mobileorange" style="margin-top:10px; font-size:1.2em">Fast Mobility</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="/cluster/lifestyle">
                                        <div class="user_box">
                                            <div class="user-offset">
                                                <img class="lazyload" data-src="/src/images/cluster/full_icon/lifestyle.svg" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="width:100%" />
                                                <noscript>
                                                    <img style="width:100%" src="/src/images/cluster/full_icon/lifestyle.svg" alt="Hot Lifestyle">
                                                </noscript>
                                                <p class="name_user pink" style="margin-top:10px; font-size:1.2em">Hot Lifestyle</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="/cluster/iot">
                                        <div class="user_box">
                                            <div class="user-offset">
                                                <img class="lazyload" data-src="/src/images/cluster/full_icon/iot.svg" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="width:100%" />
                                                <noscript>
                                                    <img style="width:100%" src="/src/images/cluster/full_icon/iot.svg" alt="Sophisticated IoT">
                                                </noscript>
                                                <p class="name_user purple" style="margin-top:10px; font-size:1.2em">Sophisticated IoT</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="/cluster/smart-company">
                                        <div class="user_box">
                                            <div class="user-offset">
                                                <img class="lazyload-scroll" data-src="/src/images/cluster/full_icon/smart_company.svg" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="width:100%" />
                                                <noscript>
                                                    <img style="width:100%" src="/src/images/cluster/full_icon/smart_company.svg" alt="Smart Company">
                                                </noscript>
                                                <p class="name_user darkgreen" style="margin-top:10px; font-size:1.2em">Smart Company</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="/cluster/money">
                                        <div class="user_box">
                                            <div class="user-offset">
                                                <img class="lazyload-scroll" data-src="/src/images/cluster/full_icon/money.svg" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="width:100%" />
                                                <noscript>
                                                    <img style="width:100%" src="/src/images/cluster/full_icon/money.svg" alt="Big Money">
                                                </noscript>
                                                <p class="name_user green" style="margin-top:10px; font-size:1.2em">Big Money</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="wrapp_main_text">
                            <div class="descr descr_small_margin center">
                                <div class="link_box">At Bits &amp; Pretzels the latest trends and innovations in the digital tech world are  presented.
    From actual developments within the area of robotics and wearables, to sports and fitness, and mobility and e-commerce, Bits &amp; Pretzels attendees can be excited to experience the future. From seeing actual developments within the robotics and wearables field, sports and fitness, through mobility and e-commerce, Bits &amp; Pretzels attendees will be truly excited to have the opportunity to experience future trends and technology within our 6 clusters.<br><br>Each of the following six Clusters will cover a specific content slot on one of our Bits &amp; Pretzels stages, where experts within that industry will discuss the latest news. Each cluster will also be represented by handpicked startups at our Startup Exhibition and will have their own two hours pitch slot on our separate pitch stage.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    
<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>