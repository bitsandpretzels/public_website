<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">
    
    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>

    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

    <?php include_once realpath('layout/_inner_pages_cluster_nav.php');?>

    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-iot active section_1" style="background:url('/src/images/cluster/hero/iot.jpg'), center, center; background-size:cover;">
        <div class="content_section content_section-cluster center">
            <div class="wrapper">
                <div class="cluster_icon_top">
                    <img src="/src/images/cluster/icon_only/iot.svg" alt="iot icon">
                </div>
                <div class="info_text_top">
                    <h1 class="top_title" style="text-transform:none">SOPHISTICATED <br>IoT</h1>
                </div>
            </div>
        </div>
    </section><!-- section -->
    <section id="investors" class="section section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="min-height: 0px; padding-bottom:80px">
        <div class="content_section content_section-cluster center" style="min-height: 0px">
            <div class="wrapper_inner wrapper_inner-cluster">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text wrapp_main_text-cluster">
                            <h1 class="main_title center purple">Sophisticated IoT</h1>
<!--                            <h2 class="down_sub_title center" style="margin-bottom:1px">presented by</h2>-->
<!--                            <img src="/src/images/cluster/logos/ibm.png" style="margin-bottom: 30px; width:72px" alt="IBM">-->
                            <div class="descr descr_small_margin center">
                                It’s all about the most cutting-edge technologies available within the area of connected hardware, sensors and Data. New disruptive manufacturing such as robotics, medical and healthcare applications as well as many consumer applications (e.g. wearables) will be presented.
                            <div class="link_box"></div></div>  
                            <h2 class="down_sub_title center purple">Relevant for market players in:</h2>
                            <div class="center">
                                <ul class="descr descr_small_margin descr-cluster" style="width:250px">
                                    <li style="list-style:square">IoT </li>
                                    <li style="list-style:square">Wearables </li>
                                    <li style="list-style:square">Robotics</li>
                                    <li style="list-style:square">Energy &amp; Cleantech</li>
                                    <li style="list-style:square">Biotech</li>
                                </ul>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        </section>
		<!--
        <section id="speakers" class="section section_users white active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="padding-top:0">
        <div class="content_section content_section-cluster center">
            <div class="wrapper_inner wrapper_inner-cluster">
                <div class="valign">
                    <div class="middle">
                            <h2 style="margin:50px 0 0 0"class="down_sub_title center purple">Sophisticated IoT Track</h2>
                            <div class="wrap_speakers wrap_speakers-cluster">
                                <div class="conteiner">
                                    <div class="list_users list_users-cluster">
                                        <?php foreach(People::setType('speaker')->setDataSource($speakers)->get(0, 200) as $person): 
                                        if (strpos($person['cluster'],"iot")!==false) { ?>
                                            <div class="user_box cluster_speaker">
                                                <div class="user-offset">
                                                    <div class="img_user"><img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt=""></div>
                                                    <p class="name_user"><?php echo $person['name']; ?></p>
                                                    <div class="title"><?php echo $person['title']; ?></div>
                                                    <div class="company"><?php echo $person['company']; ?></div>
                                                </div>
                                            </div>
                                        <?php }
                                        endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>-->
<!--
        <section id="speakers" class="section section_users light-purple active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="content_section content_section-cluster center">
            <div class="wrapper_inner wrapper_inner-cluster">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text wrapp_main_text-cluster">
                            <h1 class="main_title no-sub-title purple">ROBOTICS TRACK</h1>
                            <h2 class="down_sub_title center" style="margin-bottom:10px">in partnership with</h2>
                            <img src="/src/images/cluster/logos/vdma.png" style="margin-bottom: 16px" width="58px" alt="VDMA">
                            <img src="/src/images/cluster/logos/automatica.png" style="margin-bottom: 16px" width="42px" alt="Automatica">
                            <h2 class="down_sub_title center" style="margin-bottom:20px">“A new era of robotics”</h2>
                            <br>
                            <div class="descr descr_small_margin">
                                <div>
                                    Within this track you’ll get insights into the robotics revolution by seeing their main trends: human-robot-collaboration, mobile robotics, cloud robotics, service robotics. Get informed about mobile agricultural robot swarms that will soon have a huge effect on the agricultural landscape and sustainability and stay tuned for the showcase of a wearable robotic exoskeleton that enables individuals with spinal cord injuries to stand upright, walk, turn, and climb and descend stairs. Want to learn more? Join the Robotics track to learn more.
                                </div>                                            
                            </div>
                        </div>           
                        <div class="wrap_speakers">
                            <div class="conteiner">
                                <div class="list_users list_users-cluster">
                                      <?php foreach(People::setType('speaker')->setDataSource($speakers)->get(0, 200) as $person): 
                                        if (strpos($person['cluster'],"robotics")!==false) { ?>
                                            <div class="user_box cluster_speaker">
                                                <div class="user-offset">
                                                    <div class="img_user"><img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt=""></div>
                                                    <p class="name_user"><?php echo $person['name']; ?></p>
                                                    <div class="title"><?php echo $person['title']; ?></div>
                                                    <div class="company"><?php echo $person['company']; ?></div>
                                                </div>
                                            </div>
                                    <?php }
                                    endforeach; ?>
                                </div>
                            </div>
                            <h2 class="down_sub_title center purple" style="margin: 90px 0 10px 0">Curated By</h2>
                            <div class="list_users clearfix list_users-cluster-curator">
                                <div class="user_box user_box-cluster-curator">
                                    <div class="user-offset">
                                        <div class="img_user"><img src="/src/images/speakers/default/schwarzkopf_patrick.png" alt="Patrick Schwarzkopf"></div>
                                        <p class="name_user">Patrick Schwarzkopf</p>
                                        <div class="title">Managing Director</div>
                                        <div class="company">VDMA Robotics + Automation</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
-->
<!--
    <hr class="gray-section-divider section_divider-cluster purple">
        <div class="content_section content_section-cluster center">
            <div class="wrapper_inner wrapper_inner-cluster">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text wrapp_main_text-cluster">
                            <h1 class="main_title no-sub-title purple">WEARABLES TRACK</h1>
                            <h2 class="down_sub_title center" style="margin-bottom:10px">in partnership with</h2>
                            <img src="/src/images/cluster/logos/wearable_technologies.png" style="margin-bottom: 16px" width="118px" alt="VDMA">
                            <h2 class="down_sub_title center" style="margin-bottom:20px">“Wearables: Today and Tomorrow”</h2>
                            <br>
                            <div class="descr descr_small_margin">
                                <div>
										Due to the growing popularity of mobile networks, wearable technology has increasingly gained market traction within the last few years. Smartwatches in the forefront allow people to use a wide variety of applications that help you with a wide spectrum of tasks including: navigation, purchases, health and fitness, and many more. We dedicate this track to delve deeper into this topic and give you insights into expert opinions. You’ll hear traditional and smartwatch makers speak about form factors and market growth. The driving force in the growth of wearable tech is not only led by the smartwatch market, there are other main drivers that play a crucial role. Experts will introduce you to the growing sex toy, self-care and tattoos market, which is heavily affected by the developments of new wearable technology.
								</div>                                            
                            </div>
                        </div>           
                        <div class="wrap_speakers">

                            <div class="conteiner">
                                <div class="list_users list_users-cluster">
                                    <?php foreach(People::setType('speaker')->setDataSource($speakers)->get(0, 200) as $person): 
                                        if (strpos($person['track'],"wearables")!==false) { ?>
                                        <div class="user_box user_box-cluster">
                                            <div class="user-offset">
                                                <div class="img_user"><img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt=""></div>
                                                <p class="name_user"><?php echo $person['name']; ?></p>
                                                <div class="title"><?php echo $person['title']; ?></div>
                                                <div class="company"><?php echo $person['company']; ?></div>
                                            </div>
                                        </div>
										<?php }
									endforeach; ?>
                                </div>
                            </div>

                            <h2 class="down_sub_title center purple" style="margin: 90px 0 10px 0">Curated By</h2>
                            <div class="list_users clearfix list_users-cluster-curator">
                                <div class="user_box user_box-cluster-curator">
                                    <div class="user-offset">
                                        <div class="img_user"><img src="/src/images/speakers/default/stammel_christian.png" alt="Christian Stammel"></div>
                                        <p class="name_user">Christian Stammel</p>
                                        <div class="title">Founder &amp; CEO</div>
                                        <div class="company">Wearable Technologies Group</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
-->
		
    
    
   <?php include realpath('layout/cluster_cta.php');?>
    
<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>