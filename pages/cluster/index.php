<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">
    
    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>
    <?php include_once realpath('layout/_inner_pages_cluster_nav.php');?>
    
    <style>
        .colNew{
            
            min-width: 300px;
            padding-top: 15px;
        }
        .floatbox{
            float: left;
            width: 70%; 
            box-sizing: border-box;
            padding-right: 2%;
        }
        .floatboxIMG{
            float: left;
            width: 30%;
            box-sizing: border-box;
        }
        
        .clusterText{
            text-align: left;
            font-size: 0.9em;
            padding-top: 10px;
        }
        .clusterHeadline{
            text-align: left;
            font-size: 0.9em;
            font-weight: 500;
            margin-top: 0px;
        }
        
        @media (max-width: 1000px) {
            .frameCLUSTER{
                width:100%;
            }
            .floatbox{
            padding-right: 0px;
        }
            .colNew{
            width:100%;
        }
            .IMGCluster{
                min-width: 90px;
                max-width:90px;
            }
            .floatbox{
            padding-left: 10px;
        }
        }
        @media (min-width: 1000px) {
            .frameCLUSTER{
                width:75%;
            }
            .IMGCluster{
                min-width: 120px;
                max-width: 120px;
            }
            .floatbox{
            padding-left: 30px;
        }
        }
        
        
    
    </style>
    

    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-iot active section_1" style="background:url('/src/images/cluster/hero/cluster_title.png'), center, center; background-size:cover;">
        <div class="content_section content_section-cluster center">
            <div class="wrapper">
<!--                <div class="cluster_icon_top" style="bottom:26%">-->
<!--                    <img src="/src/images/cluster/icon_only/money.svg" style="margin-right:4%" alt="big money icon">-->
<!--                </div>-->
                <div class="info_text_top">
                    <h1 class="top_title" style="text-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);">INDUSTRY CLUSTERS</h1>
                </div>
            </div>
        </div>
    </section><!-- section -->
    <section id="content" class="section section_users active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign" style="display: inline;">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center" style="margin-bottom:24px">Introduced in 2016 - even better in 2017: our industry clusters</h1>
                        </div>
                        <div class="wrapp_main_text">
                            <div class="descr descr_small_margin center">
                                <div class="link_box">At Bits &amp; Pretzels, we’re always keen on showing the latest trends and innovations in the digital tech world. To make all topics, branches, and technologies more tangible and visible for you, we’ve introduced six different clusters which will show up in various areas and formats at the festival:<br><br>All the startups that will be part of our exhibition will be sorted by clusters. This way you see right away which one is particularly relevant for you. Furthermore each cluster will be represented by a number of handpicked startups at our pitch stage.</div>
                            </div>
                        </div>
                        
                        <div class="grid grid-2 grid-2-simple clearfix container_info frameCLUSTER">
                            <div class="col colNew">
                                <div class="info_box">                                                         
                                            <div class="floatboxIMG">
                                                <img class="lazyload IMGCluster" data-src="/src/images/cluster/icon_2017/commerce.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"/>
                                                <noscript>
                                                    <img class="IMGCluster" src="/src/images/cluster/icon_2017/commerce.png" alt="Commerce & Marketplaces">
                                                </noscript>
                                                
                                            </div>
                                                <div class="floatbox">
                                                    <p class="name_user commerceorange clusterHeadline">Commerce &amp; Marketplaces </p>
                                                    <div class="descr descr_small_margin clusterText">The future of Ecommerce, Marketplaces, Retail, Consumer Goods, Travel &amp; Tourism </div>
                                                </div>
                                    </div>
                                </div>

                            
                            <div class="col colNew">
                                 <div class="info_box">  
                                           
                                            <div class="floatboxIMG">
                                                <img class="lazyload IMGCluster" data-src="/src/images/cluster/icon_2017/mobility.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                <noscript>
                                                    <img class="IMGCluster" src="/src/images/cluster/icon_2017/mobility.png" alt="Mobilty & Transportation">
                                                </noscript>
                                                
                                            </div>
                                                <div class="floatbox">
                                                    <p class="name_user mobileorange clusterHeadline">Mobility &amp; Transportation</p>
                                                    <div class="descr descr_small_margin clusterText">The future of Automotive, Aerospace, Logistics &amp; Infrastructre</div>
                                                </div> 
                                </div>
                            </div>
                     </div>
                    <div class="grid grid-2 grid-2-simple clearfix container_info frameCLUSTER">
                            <div class="col colNew">
                                <div class="info_box">                                                         
                                            <div class="floatboxIMG">
                                                <img class="lazyload IMGCluster" data-src="/src/images/cluster/icon_2017/lifestyle.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                <noscript>
                                                    <img class="IMGCluster" src="/src/images/cluster/icon_2017/lifestyle.png" alt="Entertainment & Lifestyle">
                                                </noscript>
                                                
                                            </div>
                                                <div class="floatbox">
                                                    <p class="name_user pink clusterHeadline">Entertainment &amp; Lifestyle</p>
                                                    <div class="descr descr_small_margin clusterText">The future of Augmented Reality / Virtual Reality, Gaming, Sports, Fitness &amp; Media</div>
                                                </div>
                                    </div>
                                </div>

                            
                            <div class="col colNew">
                                 <div class="info_box">  
                                           
                                            <div class="floatboxIMG">
                                                <img class="lazyload IMGCluster" data-src="/src/images/cluster/icon_2017/iot.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                <noscript>
                                                    <img class="IMGCluster" src="/src/images/cluster/icon_2017/iot.png" alt="Hardware & IoT">
                                                </noscript>
                                                
                                            </div>
                                                <div class="floatbox">
                                                    <p class="name_user purple clusterHeadline">Hardware &amp; Iot</p>
                                                    <div class="descr descr_small_margin clusterText">The future of Industrials, Automation, Robotics, Home &amp; Wearables </div>
                                                </div> 
                                </div>
                            </div>
                            
                            
                     </div>
                    <div class="grid grid-2 grid-2-simple clearfix container_info frameCLUSTER">
                            <div class="col colNew">
                                <div class="info_box">                                                         
                                            <div class="floatboxIMG">
                                                <img class="lazyload IMGCluster" data-src="/src/images/cluster/icon_2017/smart_company.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                <noscript>
                                                    <img class="IMGCluster" src="/src/images/cluster/icon_2017/smart_company.png" alt="Services & Intelligence">
                                                </noscript>
                                                
                                            </div>
                                                <div class="floatbox">
                                                    <p class="name_user darkgreen clusterHeadline">Services &amp; Intelligence</p>
                                                    <div class="descr descr_small_margin clusterText">The future of web-based Smart Company Solutions, LegalTech, Cyber Security, Big Data, Predictive Analytics &amp; AI </div>
                                                </div>
                                    </div>
                                </div>

                            
                            <div class="col colNew">
                                 <div class="info_box">  
                                           
                                            <div class="floatboxIMG">
                                                <img class="lazyload IMGCluster" data-src="/src/images/cluster/icon_2017/money.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                <noscript>
                                                <img class="IMGCluster" src="/src/images/cluster/icon_2017/money.png" alt="Banking & Insurance">
                                                </noscript>
                                                
                                            </div>
                                                <div class="floatbox">
                                                    <p class="name_user green clusterHeadline">Banking &amp; Insurance</p>
                                                    <div class="descr descr_small_margin clusterText">The future of FinTech, Blockchain &amp; InsurTech</div>
                                                </div> 
                                </div>
                            </div>
                            
                            
                     </div>
                        
                        
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
                        
    
<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>