<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">
    
    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>
    <?php include_once realpath('layout/_inner_pages_cluster_nav.php');?>


    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-iot active section_1" style="background:url('/src/images/cluster/hero/lifestyle.png'), center, center; background-size:cover;">
        <div class="content_section content_section-cluster center">
            <div class="wrapper">
                <div class="cluster_icon_top" style="bottom:36%">
                    <img src="/src/images/cluster/icon_only/lifestyle.svg" style="margin-right:2%; width:25%" alt="big money icon">
                </div>
                <div class="info_text_top">
                    <h1 class="top_title">Hot Lifestyle</h1>
                </div>
            </div>
        </div>
    </section><!-- section -->
    <section id="investors" class="section section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="min-height: 0px; padding-bottom:0">
        <div class="content_section content_section-cluster center" style="min-height: 0px">
            <div class="wrapper_inner wrapper_inner-cluster">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text wrapp_main_text-cluster">
                            <h1 class="main_title center pink">Hot Lifestyle</h1>

<!--                            <h2 class="down_sub_title center" style="margin-bottom:10px">supported by</h2>-->
<!--                            <img src="/src/images/cluster/logos/ispo.png" style="margin-bottom: 30px; width:150px" alt="ISPO">-->

                            <div class="descr descr_small_margin center" style="margin-top:52px">
                                Within this Cluster you’ll get insights into the latest innovations in the field of AR/VR, communication and you’ll have the opportunity to experience digital trends in the entertainment sector. This cluster will help you to get a feeling of what the future will look like in the gaming industry and help to bring back all of those childhood memories. You’ll witness how digitalisation and tech are affecting the sports and fitness industry and see how the latest sensor technology enables you to track your body for both sports and health related reasons. As an added bonus, you will be able to experience the latest trends related to travel and tourism as well, helping you to develop a well rounded overview of what the future has to offer in the lifestyle sector.
                            <div class="link_box"></div></div>  
                            <h2 class="down_sub_title center pink" style="margin-bottom:10px">Relevant for market players in:</h2>
                            <div class="center">
                                <ul class="descr descr_small_margin descr-cluster" style="width:250px">
                                    <li style="list-style:square">Entertainment (Music/Lifestyle/Leisure)</li>
                                    <li style="list-style:square">Gaming</li>
                                    <li style="list-style:square">Sports &amp; Fitness</li>
                                    <li style="list-style:square">Food</li>
                                    <li style="list-style:square">Social Networks</li>
                                    <li style="list-style:square">Travel &amp; Tourism</li>
                                    <li style="list-style:square">Media</li>
                                    <li style="list-style:square">Healthcare</li>
                                    <li style="list-style:square">Augmented Reality/VR</li>
                                    <li style="list-style:square">B2C Communications</li>
                                </ul>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        </section>
<!--
        <section id="speakers" class="section section_users white active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="padding-top:0; padding-bottom:47px">
        <div class="content_section content_section-cluster center">
            <div class="wrapper_inner wrapper_inner-cluster">
                <div class="valign">
                    <div class="middle">
                            <h1 class="main_title center pink" style="margin-top:60px">Hot Lifestyle Track</h1>
                            <div class="wrap_speakers wrap_speakers-cluster">
                                <div class="conteiner">
                                    <div class="list_users list_users-cluster">
                                        <?php foreach(People::setType('speaker')->setDataSource($speakers)->get(0, 200) as $person): 
                                        if (strpos($person['track'],"lifestyle")!==false) { ?>
                                            <div class="user_box cluster_speaker">
                                                <div class="user-offset">
                                                    <div class="img_user"><img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt=""></div>
                                                    <p class="name_user"><?php echo $person['name']; ?></p>
                                                    <div class="title"><?php echo $person['title']; ?></div>
                                                    <div class="company"><?php echo $person['company']; ?></div>
                                                </div>
                                            </div>
                                        <?php }
                                        endforeach; ?>
                                    </div>
                                </div>
                             <h2 class="down_sub_title center pink" style="margin: 10px 0 10px 0">Curated By</h2>
                            <div class="list_users clearfix list_users-cluster-curator">
                                <div class="user_box user_box-cluster-curator">
                                    <div class="user-offset">
                                        <div class="img_user"><img src="/src/images/speakers/default/groeber_tobias.png" alt=""></div>
                                        <p class="name_user">Tobias Gröber</p>
                                        <div class="title">Head of ISPO Munich</div>
                                        <div class="company">ISPO Group</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
-->
    
<!--
        <section id="speakers" class="section section_users light-pink active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="content_section content_section-cluster center">
            <div class="wrapper_inner wrapper_inner-cluster">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text wrapp_main_text-cluster">
                            <h1 class="main_title no-sub-title pink">LIFESTYLE TRACK</h1>
                            <div class="descr descr_small_margin">
                                <div>
                                    coming soon…
                                </div>                                            
                            </div>
                            <h2 class="down_sub_title center" style="margin-bottom:20px">
                                “Beyond Bitcoin: How Blockchains disrupt more than just Finance.” <br>// September 26
                            </h2>
                            <br>
                            <div class="descr descr_small_margin">
                                <div>
                                    Bitcoin has evolved into a growing mainstream alternative currency and has became a contradictory topic over the last years. Some say it’s the most important invention since the internet, others are very doubtful. In fact, the technology behind Bitcoin has become a rising trend in many companies. So, how can the underlying technology of Bitcoin, Blockchain disrupt more than just the finance sector? In this track, experts will show the hidden opportunities for startups and provide insights into one of the most complex technologies.
                                </div>                                            
                            </div>
                        </div>           
                        <div class="wrap_speakers">
                            <div class="conteiner">
                                <div class="list_users list_users-cluster">
                                    <?php foreach(People::setType('speaker')->setDataSource($speakers)->get(0, 6) as $person):  ?>
                                        <div class="user_box user_box-cluster">
                                            <div class="user-offset">
                                                <div class="img_user"><img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt=""></div>
                                                <p class="name_user"><?php echo $person['name']; ?></p>
                                                <div class="title"><?php echo $person['title']; ?></div>
                                                <div class="company"><?php echo $person['company']; ?></div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <h2 class="down_sub_title center pink" style="margin: 10px 0 10px 0">Curated By</h2>
                            <div class="list_users clearfix list_users-cluster-curator">
                                <div class="user_box user_box-cluster-curator">
                                    <div class="user-offset">
                                        <div class="img_user"><img src="/src/images/speakers/default/<?php echo $speakers[0]['image']; ?>" alt=""></div>
                                        <p class="name_user"><?php echo $speakers[0]['name']; ?></p>
                                        <div class="title"><?php echo $speakers[0]['title']; ?></div>
                                        <div class="company"><?php echo $speakers[0]['company']; ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="wrapp_main_text wrapp_main_text-cluster">
                            <div class="descr descr_small_margin center">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quae hic rei publicae vulnera inponebat, eadem ille sanabat. Summae mihi videtur inscitiae. Non quam nostram quidem, inquit Pomponius iocans; Est enim tanti philosophi tamque nobilis audacter sua decreta defendere. Quae cum ita sint, effectum est nihil esse malum, quod turpe non sit. Sin te auctoritas commovebat, nobisne omnibus et Platoni ipsi nescio quem illum anteponebas?
                            <div class="link_box"></div></div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
    
    
 <?php include realpath('layout/cluster_cta.php');?>
    
<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>