<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">
    
    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>
    <?php include_once realpath('layout/_inner_pages_cluster_nav.php');?>


    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-iot active section_1" style="background:url('/src/images/cluster/hero/commerce.png'), center, center; background-size:cover;">
        <div class="content_section content_section-cluster center">
            <div class="wrapper">
                <div class="cluster_icon_top" style="bottom:40%">
                    <img src="/src/images/cluster/icon_only/commerce.svg" style="margin-right:4%" alt="big money icon">
                </div>
                <div class="info_text_top">
                    <h1 class="top_title">FUTURE <br>COMMERCE</h1>
                </div>
            </div>
        </div>
    </section><!-- section -->
    <section id="investors" class="section section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="min-height: 0px;">
        <div class="content_section content_section-cluster center" style="min-height: 0px">
            <div class="wrapper_inner wrapper_inner-cluster">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text wrapp_main_text-cluster">
                            <h1 class="main_title center commerceorange">Future Commerce</h1>
<!--                            <h2 class="down_sub_title center" style="margin-bottom:10px">presented by</h2>-->
<!--                            <img src="/src/images/cluster/logos/amazonde.png" style="margin-bottom: 30px; width:120px" alt="Allianz">-->
                            <div class="descr descr_small_margin center" style="margin-top:12px">
                                It’s all about the current trends and development in the area of E-commerce and marketplaces. New and emerging technologies are changing the e-commerce landscape and the way people buy online. Same-day deliveries are rising the expectations, experts are already talking about drone shipping and retailers are coming up with new things everyday. This cluster will give you deeper insights into the challenges of actual e-commerce solutions, show you the latest marketplace developments and introduce you to future trends.
                            <div class="link_box"></div></div>  
                            <h2 class="down_sub_title center commerceorange" style="margin-bottom:10px">Relevant for market players in:</h2>
                            <div class="center">
                                <ul class="descr descr_small_margin descr-cluster" style="width:250px">
                                    <li style="list-style:square">E-Commerce </li>
                                    <li style="list-style:square">Marketplaces</li>
                                </ul>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        </section>
<!--
        <section id="speakers" class="section section_users white active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="padding-top:0; padding-bottom:47px">
        <div class="content_section content_section-cluster center">
            <div class="wrapper_inner wrapper_inner-cluster">
                <div class="valign">
                    <div class="middle">
                            <h1 class="main_title center commerceorange" style="margin-top:80px">Future Commerce Track</h1>
                            <div class="wrap_speakers wrap_speakers-cluster">
                                <div class="conteiner">
                                    <div class="list_users list_users-cluster">
                                        <?php foreach(People::setType('speaker')->setDataSource($speakers)->get(0, 200) as $person): 
                                        if (strpos($person['cluster'],"ecommerce")!==false) { ?>
                                            <div class="user_box cluster_speaker">
                                                <div class="user-offset">
                                                    <div class="img_user"><img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt=""></div>
                                                    <p class="name_user"><?php echo $person['name']; ?></p>
                                                    <div class="title"><?php echo $person['title']; ?></div>
                                                    <div class="company"><?php echo $person['company']; ?></div>
                                                </div>
                                            </div>
                                        <?php }
                                        endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
-->
<!--
        <section id="speakers" class="section section_users light-commerceorange active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="content_section content_section-cluster center">
            <div class="wrapper_inner wrapper_inner-cluster">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text wrapp_main_text-cluster">
                            <h1 class="main_title no-sub-title commerceorange">FUTURE COMMERCE TRACK</h1>
                            <div class="descr descr_small_margin">
                                <div>
                                    coming soon…
                                </div>                                            
                            </div>
                            <h2 class="down_sub_title center" style="margin-bottom:20px">
                                “Beyond Bitcoin: How Blockchains disrupt more than just Finance.” <br>// September 26
                            </h2>
                            <br>
                            <div class="descr descr_small_margin">
                                <div>
                                    Bitcoin has evolved into a growing mainstream alternative currency and has became a contradictory topic over the last years. Some say it’s the most important invention since the internet, others are very doubtful. In fact, the technology behind Bitcoin has become a rising trend in many companies. So, how can the underlying technology of Bitcoin, Blockchain disrupt more than just the finance sector? In this track, experts will show the hidden opportunities for startups and provide insights into one of the most complex technologies.
                                </div>                                            
                            </div>
                        </div>           
                        <div class="wrap_speakers">
                            <div class="conteiner">
                                <div class="list_users list_users-cluster">
                                    <?php foreach(People::setType('speaker')->setDataSource($speakers)->get(0, 6) as $person):  ?>
                                        <div class="user_box user_box-cluster">
                                            <div class="user-offset">
                                                <div class="img_user"><img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt=""></div>
                                                <p class="name_user"><?php echo $person['name']; ?></p>
                                                <div class="title"><?php echo $person['title']; ?></div>
                                                <div class="company"><?php echo $person['company']; ?></div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <h2 class="down_sub_title center commerceorange" style="margin: 10px 0 10px 0">Curated By</h2>
                            <div class="list_users clearfix list_users-cluster-curator">
                                <div class="user_box user_box-cluster-curator">
                                    <div class="user-offset">
                                        <div class="img_user"><img src="/src/images/speakers/default/<?php echo $speakers[0]['image']; ?>" alt=""></div>
                                        <p class="name_user"><?php echo $speakers[0]['name']; ?></p>
                                        <div class="title"><?php echo $speakers[0]['title']; ?></div>
                                        <div class="company"><?php echo $speakers[0]['company']; ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="wrapp_main_text wrapp_main_text-cluster">
                            <div class="descr descr_small_margin center">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quae hic rei publicae vulnera inponebat, eadem ille sanabat. Summae mihi videtur inscitiae. Non quam nostram quidem, inquit Pomponius iocans; Est enim tanti philosophi tamque nobilis audacter sua decreta defendere. Quae cum ita sint, effectum est nihil esse malum, quod turpe non sit. Sin te auctoritas commovebat, nobisne omnibus et Platoni ipsi nescio quem illum anteponebas?
                            <div class="link_box"></div></div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
    
<?php include realpath('layout/cluster_cta.php');?>
    
    <!-- section -->
    
<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>