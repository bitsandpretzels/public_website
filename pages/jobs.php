<?php
// local fix to match my vhost settings
$pathPrefix = '';
$host = $_SERVER['HTTP_HOST'];
if ($host == 'bap.local') {
  $pathPrefix = '../';
}
include_once realpath($pathPrefix.'layout/top.php');
include_once realpath($pathPrefix.'layout/header.php');
?>

<div class="inner_sections">

  <?php include_once realpath($pathPrefix.'layout/_inner_pages_main_nav.php');?>

  <!--
    \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    !! The sub menus have a different arrangement of pages !!
    \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
   -->

 <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section"><span class="mobile_hidden">Jobs</span></div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>            
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                </li>
                <li>
                    <a class="link_nav buyticket trigger_app_nav" data-section="buyticket"><?php echo Meta::getButtonWording(); ?></a>
                </li>
            </ul>
        </div>
    </div>

  <section class="section silver active section_1 section_pt">
    <div class="content_section center">
      <div class="wrapper_inner">
        <div class="valign">
          <div class="middle">
            <div class="wrapp_main_text">
              <h1 class="main_title" style="margin:50px 0;">Available Positions:</h1>
            </div>
            <div class="grid clearfix">
              <div class="valign">
                <div class="middle">
                  
                   <div class="topic topic_open">
                    <h1 class="question_title"><a href="#">Praktikant Event Management (m/w)</a></h1>
                    <div class="arrow_pointer"></div>
                    <div class="description_info descr descr_no_margin" style="display:none">
                          <p class="question_text">Du suchst eine spannende Herausforderung im Bereich Event und hast Lust eine erfolgreiche und innovative Großveranstaltung mitzugestalten? Dann ist das zwei bis dreimonatige Praktikum als Event Manager (m/w) bei der Startup Events UG genau das
Richtige für Dich. Wir suchen einen Praktikanten ab 15. Mai und mehrere Praktikanten idealerweise von Anfang Juli bis Ende September mit jeweils unterschiedlichen Themenschwerpunkten:<br>
                             <ul class="list-circle">
                                <li>Teilnehmermanagement</li>
                                <li>PR und Partnerschaften</li>
                                <li>Speaker Management</li>
                                <li>HR / Volunteerkoordination</li>
                                <li>Grafikdesign</li>
                                <li>Video Produktion </li>
                            </ul>
                           <br>
                            <strong>Wer sind wir?</strong>
                            <br>
                           Das “Bits &amp; Pretzels Founders Festival” hat sich seit seiner Gründung 2014 sehr schnell vom lokalen Weißwurst- Gründerfrühstück zu
einem internationalen, dreitägigen Festival für Gründer, Gründungsinteressierte und Investoren entwickelt. Unter dem Motto: “Von Gründer für Gründer” findet das Event einmal im Jahr während des Oktoberfests in München statt. Die bekanntesten Entrepreneure aus aller Welt sowie junge, aufstrebende Unternehmer treffen sich, um sich über Erfahrungen und Trends auszutauschen, zu Lernen und zu netzwerken. Das Ziel von “Bits &amp; Pretzels” ist es, eine Leitveranstaltung für Startups in Deutschland, sowie eine internationale Leuchtturmveranstaltung zu sein. 
                            <br><br>
                            <strong>Was solltest Du mitbringen?</strong>
                            <ul class="list-circle">
                                <li>Du bist immatrikulierter Student im Sommersemester 2017 (bis einschließlich September)</li>
                                <li>erste Erfahrungen im Bereich Event Management von Vorteil</li>
                                <li>erste Erfahrungen in Deinem favorisierten Themenschwerpunkt</li>
                                <li>Organisationstalent und systematische Arbeitsweise</li>
                                <li>hohes Maß an Selbstständigkeit und Lernbereitschaft</li>
                                <li>sehr gute Englisch-Kenntnisse</li>
                                <li>Interesse an der Startup Szene ist ein Plus</li>
                            </ul>
                            <br>
                            <strong>Deine Aufgaben je nach Tätigkeitsschwerpunkt:</strong>
                            <ul class="list-circle">
                                <li>Unterstützung bei der Vorbereitung, Durchführung und Koordination des Events „Bits &amp; Pretzels“ </li>
                                <li>Mitarbeit im Teilnehmerhandling und –support </li>
                                <li>Unterstützung im Speaker Management</li>
                                <li>Unterstützung im Bereich PR und Mitarbeit am Partnerprogramm</li>
                                <li>Koordination des Volunteerprogramms</li>
                                <li>Mitarbeit bei der Erstellung von Grafikmaterialen</li>
                                <li>Produktion von Werbevideos für Social Media und Content Produktion von Bühneninhalten</li>
                                <li>Recherchetätigkeit und Datenanalyse</li>
                            </ul>
                            <br>
                            <strong>Was erwartet Dich?</strong>
                            <ul class="list-circle">
                               <li>Einzigartiger Team-Drive und hochmotivierte Event-Organisatoren</li>
                                <li>Hervorragende Einblicke in die Organisation einer Großveranstaltung</li>
                                <li>Herausragende Möglichkeiten Kontakte in der Startup Szene zu knüpfen</li>
                                <li>Moderner Arbeitsplatz mitten in der Münchner Innenstadt</li>
                            </ul>
                            <br>
                            <strong>Wie werde ich Teil des Teams?</strong>
                            <br>
                            Du konntest bei jedem Punkt nicken und möchtest am liebsten morgen anfangen?
Dann sende uns Deine aussagekräftigen Bewerbungsunterlagen inklusive möglichem Eintrittstermin, Dauer und präferiertem Themenschwerpunkt an <a href="mailto:jobs@bitsandpretzels.com" style="color:#005690">jobs@bitsandpretzels.com</a>
                    </div>
                  </div>
                  <div class="topic topic_open">
                    <h1 class="question_title"><a href="#">Working Student Web Development (m/f)</a></h1>
                    <div class="arrow_pointer"></div>
                    <div class="description_info descr descr_no_margin" style="display:none">
                          <p class="question_text">Are you open to an exciting challenge alongside your studies and motivated to shape a successful and
innovative event? You can contribute up to 20h per week during the lecture period and are ready to
support us in the semester breaks up to 40h a week? Then this is definitely the right opportunity for you.
Our cheerful and ambitious team at Startup Events UG is looking for a working student Web Development
(starting immediately).<br><br>
                            <strong>Who we are:</strong>
                            <br>
                           Beginning as a small breakfast meeting for founders of the Munich startup ecosystem, the “Bits &amp; Pretzels Founders Festival” quickly became one of the most important international startup events in Europe. Founded in 2014, Bits &amp; Pretzels now takes place every year in Munich as a 3-day event with valuable content for startups and investors and ends with a special networking event at the famous Oktoberfest. Under the theme “from founders for founders”, the most famous entrepreneurs from all around the world, as well as young ambitious entrepreneurs meet to share and discuss experiences and trends. The aim of Bits &amp; Pretzels is not only to be the leading startup event in Germany, but also an international flagship event.
                            <br><br>
                            <strong>Key requirements:</strong>
                            <ul class="list-circle">
                                <li>Studies related to computer science, media design, media informatics, web design (enrolled in the winter term 2016/17 up to and including September 2017)</li>
                                <li>Sound knowledge of PHP, JavaScript, HTML5 and CSS development</li>
                                <li>Basic database knowledge in a query language (SQL)</li>
                                <li>Optionally, first experience with technologies like Git, Less, Grunt or the Google App</li>
                                <li>Very good English skills</li>
                                <li>A feeling for aesthetics and design is an advantage</li>
                            </ul>
                            <br>
                            <strong>Your responsibilities:</strong>
                            <ul class="list-circle">
                                <li>Huge self-responsibility in the further development and optimization of the event website</li>
                                <li>Various programming tasks</li>
                                <li>Linking different tools via API’s</li>
                                <li>Support in handling large amounts of data</li>
                                <li>Research and data analysis</li>
                                <li>Support with the preparation, implementation and coordination of the event Bits &amp; Pretzels</li>
                            </ul>
                            <br>
                            <strong>Our offer to you:</strong>
                            <ul class="list-circle">
                               <li>Unique team drive and highly motivated event organizers</li>
                                <li>Deep insights and involvement in an event with over 5,000 participants and speakers like Kevin Spacey or Richard Branson</li>
                                <li>Outstanding ways to make contacts in the startup scene</li>
                                <li>Modern workplace in the center of Munich</li>
                            </ul>
                            <br>
                            <strong>WHow to join the team:</strong>
                            <br>
                           You think you’re the person we are looking for? Simply send us your compelling application stating a possible starting date. Applications can be sent to <a href="mailto:jobs@bitsandpretzels.com" style="color:#005690">jobs@bitsandpretzels.com</a>
                    </div>
                  </div>
                  

                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                  <div style="height:250px;"></div>
                  


                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  

</div>
<style>
    .list-circle li {
        list-style: circle;
    }
</style>
<?php
    #include realpath('layout/signup-form.php');
    include realpath($pathPrefix.'layout/footer.php');
    include realpath($pathPrefix.'layout/bottom.php');
