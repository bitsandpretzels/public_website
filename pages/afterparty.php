<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>


    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

    <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section mobile_hidden">Afterparty</div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>            
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                </li>
                <li>
                    <a href="#networking"class="link_nav buyticket" data-section="buyticket">Get Afterparty Tickets</a>
                </li>
            </ul>
        </div>
    </div>

    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-sidevents active section_1">
        <div class="content_section center">
            <div class="wrapper">
                <div class="info_text_top">
                    <h1 class="top_title" style="text-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);">THE OFFICIAL <br>BITS &amp; PRETZELS AFTERPARTY</h1>
                </div>
            </div>
        </div>
    </section><!-- section -->

    <section id="content" class="section section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="min-height: 0px">
        <div class="content_section center" style="min-height: 0px">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
<!--                            <h1 class="main_title center" style="margin-bottom:30px">The official Bits &amp; Pretzels Afterparty</h1>-->
                            <div class="descr" style="color:#0094d4; margin-bottom:0">
                                 <span class="text_icon" style="background-image:url('src/images/icon_agenda.svg'); margin-top:15px"></span>
                                 Sun | 25.09.2016 | 21:00 PM – 3:00 AM
                            </div>
                            <address class="descr" style="color:#0094d4; margin-bottom:0">
                                 <span class="text_icon" style="background-image:url('src/images/icon_location.svg'); margin-top:15px"></span>
                                 Praterinsel (Praterinsel 3-4, 80538 Munich) <br>
                            </address>  
                            <div class="descr" style="color:#0094d4;">
                                         <span class="text_icon" style="background-image:url('src/images/icon_dresscode.svg'); margin-top:15px"></span>
                                         Dresscode: 
                                         Tracht or sexy
                                    </div>                          
                            <div class="descr descr_small_margin center">
                                Bits &amp; Pretzels stands for unique networking, awesome speakers, startup spirit, Oktoberfest AND - not to forget - <strong>an amazing AFTERPARTY!</strong> Be part on Sunday, Sept 25 for an epic night bash where all the attendees enjoy music, drinks and dance on Munich’s famous Isar island.
                            </div> 
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
    <section id="networking" class="section section_devices silver active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                            <h1 class="main_title center" style="margin-bottom:50px">
                                <span style="margin-right:10px">🎉</span>
                                What to expect?
                                <span style="margin-left:10px">🎉</span>
                            </h1>
                        <div class="wrapp_main_text">
                            <div class="descr descr_small_margin">
                                <ul style="text-align:left">
                                    <li style="list-style:disc">
                                        Free entrance for Bits &amp; Pretzels attendees
                                    </li>
                                    <li style="list-style:disc">
                                        Biergarten-Area with amazing Bavarian food by local startups
                                    </li>
                                    <li style="list-style:disc">
                                        Hyper-Hyper Floor mit Jens Witzig und Simon Rose
                                    </li>
                                    <li style="list-style:disc">
                                        House/Elektro Floor mit Jonny Leoni (best known from Heart &amp; Call me Drella)
                                    </li>
                                    <li style="list-style:disc">
                                        H.B.C (Utopia Festival)
                                    </li>
                                    <li style="list-style:disc">
                                        Inspiring people, magical moments, unique party!
                                    </li>
                                </ul>
                            </div>                             
                        </div>
<!--
                        <div id="buyticket" class="button_mb choose-cat-trigger-wrapper" style="margin-top:90px">
                            <div class="button_box button_box_orange button_box_auto trigger_app_nav">
                                <a href="#category" class="button orange"><span>Register for 2017 Ticket Sale</span></a>
                            </div>
                        </div>
-->
                       <?php if(!empty($_GET['invitation'])): ?>
                       <script type="text/javascript" src="https://form.jotformeu.com/jsform/62453490526356"></script>
                        <?php endif; ?>
                        <?php include realpath('layout/elements/social-buttons.php');?>
                    </div>
                </div>
                    <blockquote class="twitter-tweet" data-lang="de"><p lang="en" dir="ltr">Afterparty <a href="https://twitter.com/bitsandpretzels">@bitsandpretzels</a>. Lovely location, nice music and lots of, lots of, lots of opportunities to network (&amp;drink) <a href="https://twitter.com/hashtag/bits16?src=hash">#bits16</a> <a href="https://twitter.com/hashtag/startups?src=hash">#startups</a> <a href="https://t.co/6to2Vm5FlA">pic.twitter.com/6to2Vm5FlA</a></p>&mdash; BallouPR_DE (@BallouPR_DE) <a href="https://twitter.com/BallouPR_DE/status/780161731754618880">25. September 2016</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
            </div>
        </div>
    </section>
</div>


<?#php include realpath('layout/signup-form.php');?>
<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>