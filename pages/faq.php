<?php
// local fix to match my vhost settings
$pathPrefix = '';
$host = $_SERVER['HTTP_HOST'];
if ($host == 'bap.local') {
  $pathPrefix = '../';
}
include_once realpath($pathPrefix.'layout/top.php');
include_once realpath($pathPrefix.'layout/header.php');
?>

<div class="inner_sections">

  <?php include_once realpath($pathPrefix.'layout/_inner_pages_main_nav.php');?>

  <!--
    \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    !! The sub menus have a different arrangement of pages !!
    \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
   -->

 <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section"><span class="mobile_hidden">FAQ</span></div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>            
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                </li>
                <li>
                    <a class="link_nav buyticket trigger_app_nav" data-section="buyticket"><?php echo Meta::getButtonWording(); ?></a>
                </li>
            </ul>
        </div>
    </div>

  <section class="section silver active section_1 section_pt">
    <div class="content_section center">
      <div class="wrapper_inner">
        <div class="valign">
          <div class="middle">
            <div class="wrapp_main_text">
              <h1 class="main_title">FAQ</h1>
            </div>

            <div class="grid clearfix">
              <div class="valign">
                <div class="middle">

                  <div class="topic topic_open" id="1">
                    <h1 class="question_title"><a href="#1">What is Bits &amp; Pretzels?</a></h1>
                    <div class="arrow_pointer"></div>
                    <div class="description_info descr descr_no_margin" style="display:none">
                      <p class="question_text">Bits &amp; Pretzels is a 3-day conference
                      for founders and people from the startup ecosystem with over 5.000
                      attendees. We bring together the world&rsquo;s leading entrepreneurs
                      and investors to share their ideas. Join us on September 24-26, 2017
                      and do not miss the sensational finale at the Oktoberfest.</p>
                    </div>
                  </div>

                  <div class="topic topic_open" id="2">
                    <h1 class="question_title"><a href="#2">How do I get my ticket?</a></h1>
                    <div class="arrow_pointer"></div>
                    <div class="description_info descr descr_no_margin" style="display:none">
                    <p class="question_text"><strong>Option 1:</strong>
                        <br>The easiest way to get your ticket(s) is to find your e-mail order confirmation within your e-mail program. Tickets are attached to the order confirmation you received per e-mail directly after ordering your ticket as a PDF file.
                        <br><br><strong>Option 2:</strong>
                        <br>If you can't find the e-mail confirmation, just go to <a href="eventbrite.com/gettickets" target="_blank" style="color:#005690">eventbrite.com/gettickets</a>, enter the e-mail you used to register for the event and access your ticket.
                    </p>
                    </div>
                  </div>
                   
                   <div class="topic topic_open" id="3">
                    <h1 class="question_title"><a href="#3">Is the Bits &amp; Pretzels ticket refundable?</a></h1>
                    <div class="arrow_pointer"></div>
                    <div class="description_info descr descr_no_margin" style="display:none">
                      <p class="question_text">Unfortunately, we cannot issue a refund for
                      your ticket. However, if you have purchased a ticket and cannot make
                      it to the event, you still have the possibility to sell your ticket
                      to someone who is eligible for the ticket category you have
                      purchased. For important reasons, you can change the name of the
                      participants by logging into your Eventbrite account. Name changes
                      are possible until 7 days prior to the commencement of the event.</p>
                    </div>
                  </div>

                  <div class="topic topic_open" id="4">
                    <h1 class="question_title"><a href="#4">Do I have to pay tax on the ticket if I do not live in Germany?</a></h1>
                    <div class="arrow_pointer"></div>
                    <div class="description_info descr descr_no_margin" style="display:none">
                      <p class="question_text">According to German law, even foreign
                      companies and non-citizens have to pay tax on their tickets.</p>
                    </div>
                  </div>

                  <div class="topic topic_open" id="5">
                    <h1 class="question_title"><a href="#5">Where does Bits &amp; Pretzels take place?</a></h1>
                    <div class="arrow_pointer"></div>
                    <div class="description_info descr descr_no_margin" style="display:none">
                      <p class="question_text">The first two days of Bits &amp; Pretzels take place at the ICM in Munich. The last day takes place in the Schottenhamel Festzelt at the Oktoberfest.</p>
                    </div>
                  </div>

                  <div class="topic topic_open" id="6">
                    <h1 class="question_title"><a href="#6">What is included in my Bits &amp; Pretzels ticket?</a></h1>
                    <div class="arrow_pointer"></div>
                    <div class="description_info descr descr_no_margin" style="display:none">
                      <p class="question_text">Your 3-day-ticket entitles you to enter the
                      event Bits &amp; Pretzels 2017 from September 24 &ndash; 26 and
                      grants you access to selected Bits &amp; Pretzels partner events,
                      parties, and the Bits &amp; Pretzels area at the Schottenhamel tent
                      at the Oktoberfest on September 26th.</p>
                    </div>
                  </div>

                  <div class="topic topic_open" id="7">
                    <h1 class="question_title"><a href="#7">Is there an agenda available?</a></h1>
                    <div class="arrow_pointer"></div>
                    <div class="description_info descr descr_no_margin" style="display:none">
                      <p class="question_text">Yes, you can find the agenda <a href="<?php echo Router::getRoute('schedule'); ?>" style="color:#005690">here</a></p>
                    </div>
                  </div>

                  <div class="topic topic_open" id="8">
                    <h1 class="question_title"><a href="#8">Do you have any hotel suggestions?</a></h1>
                    <div class="arrow_pointer"></div>
                    <div class="description_info descr descr_no_margin" style="display:none">
                      <p class="question_text">We have prepared severeal hotel suggestions for you <a href="<?php echo Router::getRoute('hotels'); ?>" style="color:#005690">here</a>.</p>
                    </div>
                  </div>

                  <div class="topic topic_open" id="9">
                    <h1 class="question_title"><a href="#9">Where do I find further information about the Startup Exhibition?</a></h1>
                    <div class="arrow_pointer"></div>
                    <div class="description_info descr descr_no_margin" style="display:none">
                      <p class="question_text">You find all information and an exhibition FAQ <a href="<?php echo Router::getRoute('exhibition'); ?>" style="color:#005690">here</a>.</p>
                    </div>
                  </div>

                  <div class="topic topic_open" id="10">
                    <h1 class="question_title"><a href="#10">I am interested in partnering with the event, whom do I contact?</a></h1>
                    <div class="arrow_pointer"></div>
                    <div class="description_info descr descr_no_margin" style="display:none">
                      <p class="question_text">For partnership queries please contact
                      <a style="color:#2795D7" href=
                      'mailto:k%69ra%40%62its%61ndpr%65t%7Ael%73%2Ec%6Fm'>kira@bitsandpretzels.com</a></p>
                    </div>
                  </div>

                  <div class="topic topic_open" id="11">
                    <h1 class="question_title"><a href="#11">I am interested in speaking at the event, whom do I contact?</a></h1>
                    <div class="arrow_pointer"></div>
                    <div class="description_info descr descr_no_margin" style="display:none">
                      <p class="question_text">If you would like to speak at Bits &amp;
                      Pretzels this September, please contact <a style="color:#2795D7"
                      href='mailto:%73peakers@bi%74sand%70r%65%74z%65ls.com'>speakers@bitsandpretzels.com</a></p>
                    </div>
                  </div>

                  <div class="topic topic_open" id="12">
                    <h1 class="question_title"><a href="#12">I am interested in volunteering at the event, whom do I contact?</a></h1>
                    <div class="arrow_pointer"></div>
                    <div class="description_info descr descr_no_margin" style="display:none">
                      <p class="question_text">You can fill out this <a href="https://form.jotformeu.com/70512350082343" style="color:#005690">form</a> and get more details as soon as the application process starts.</p>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section><!-- section -->

</div> <!-- .inner-sections

<?php
    #include realpath('layout/signup-form.php');
    include realpath($pathPrefix.'layout/footer.php');
    include realpath($pathPrefix.'layout/bottom.php');
