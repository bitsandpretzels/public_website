<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>


    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

    <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section mobile_hidden">Side Events</div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>            
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                </li>
                <li>
                    <a class="link_nav buyticket trigger_app_nav" data-section="buyticket"><?php echo Meta::getButtonWording(); ?></a>
                </li>
            </ul>
        </div>
    </div>
	

    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-sidevents active section_1">
        <div class="content_section center">
            <div class="wrapper">
                <div class="info_text_top">
                    <h1 class="top_title">Side Events</h1>
                </div>
            </div>
        </div>
    </section><!-- section -->

	<section id="investors" class="section section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="min-height: 0px">
        <div class="content_section center" style="min-height: 0px">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">Choose your Monday evening program here.</h1>
                            <div class="descr" style="color:#0094d4; margin-bottom:25px">
                                 <span class="text_icon" style="background-image:url('src/images/icon_agenda.svg'); margin-top:15px"></span>
                                 Mon | 26.09.2016
                            </div>
                            <div class="descr descr_small_margin center">Thanks to our amazing partners we have an incredible list of independently organized but official Side Events for Monday night. Register now for beer & pitching, a crowd investment workshop, an online marketing party, a coder session or just enjoy drinks, food and surprises. <div class="link_box"></div>
                            <div class="descr descr_small_margin center">Please note that all side events can only accept a limited group of people. Entry is not guaranteed, please check out registration conditions<div class="link_box"></div>
                            </div>                             
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
	
    <?php include_once realpath('layout/_scroll_down_hint.php');?>
    <section id="networking" class="section section_devices silver active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="paddin-bottom:0">
        <div class="content_section">
            <div class="wrapper_inner">
                <h1 class="main_title center" ><span style="margin-right:10px" style="margin-bottom:10px">🎉</span>Premium Side Events<span style="margin-left:10px">🎉</span></h1>
                <div class="descr descr_small_margin center" style="margin-top:40px; margin-bottom:60px">                   
                    <span style="margin-right:10px"></span>We are very proud that some of our trusted partners organize independently awesome side events.
                </div>
                <div class="descr descr_small_margin center">
                <div class="grid grid-2 grid-2-simple clearfix">
                  
                  
                   <?php for($i = 0; $i < count($sideevents_2016); $i++) { 
                    if($sideevents_2016[$i]['premium'] === 'yes'):  ?> 
                   
                   <div class="col col-1" style="margin-bottom:50px">
                        <div class="grid-offset">
                            <div class="valign">
                                <div class="middle">
                                    <div class="img_box">
                                        <a href="#" data-featherlight="#fl<?= $i ?>">
                                            <img class="lazyload-scroll" data-src="/src/images/sideevents/<?= $sideevents_2016[$i]['main_image'] ?>" style="max-width: 452px;" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                            <noscript>
                                                <img src="/src/images/sideevents/<?= $sideevents_2016[$i]['main_image'] ?>" style="max-width: 452px;" alt="<?= $sideevents_2016[$i]['headline'] ?>">
                                            </noscript>
                                        </a>
                                    </div>
                                    <a href="#" data-featherlight="#fl<?= $i ?>"><h2 class="down_sub_title" style="margin-bottom:5px; margin-top:18px"><?= $sideevents_2016[$i]['headline'] ?></h2></a>
                                    <div class="descr descr_small_margin" style="margin-top:8px">
                                       <?= $sideevents_2016[$i]['info'] ?>
                                    </div>
                                    <div class="link_box" style="margin:0">
                                        <span style="cursor:pointer;" data-featherlight="#fl<?= $i ?>" class="link">More Info<span class="icon icon-arrow_right"></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Lightbox Content: -->
                    
                    <div class="lightbox" id="fl<?= $i ?>">
                        <div class="middle" style="text-align:center">
                            <div class="img_box" style="margin-bottom:30px">
                                <img class="lazyload-scroll" data-src="/src/images/sideevents/<?= $sideevents_2016[$i]['main_image'] ?>" style="max-width: 452px;" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                    <noscript>
                                        <img src="/src/images/sideevents/<?= $sideevents_2016[$i]['main_image'] ?>" style="max-width: 452px;" alt="<?= $sideevents_2016[$i]['hotel'] ?>">
                                    </noscript>
                            </div> 
                            <h2 class="down_sub_title" style="margin-bottom:19px; margin-top:28px"><?= $sideevents_2016[$i]['headline'] ?></h2>
                            <div class="descr descr_small_margin" style="margin-bottom:20px; color: #7a7a7a"><?= $sideevents_2016[$i]['adress'] ?></div>
                            <div class="descr descr_small_margin" style="margin-bottom:20px; color: #7a7a7a"><?= $sideevents_2016[$i]['location'] ?></div>
                            <div class="descr descr_small_margin" style="margin-bottom:20px; color: #7a7a7a"><?= $sideevents_2016[$i]['info'] ?></div>
                            <a href="<?= $sideevents_2016[$i]['booking_link'] ?>" class="button orange" style="margin-bottom:10px"><span><?= $sideevents_2016[$i]['button'] ?></span></a>
                        </div>
                    </div>
                    
                    <?php endif; }?>
                    
                </div>
            </div>
        </div>
    </section>
	
	 <?php include_once realpath('layout/_scroll_down_hint.php');?>
    <section id="networking" class="section section_devices white active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="container">
            <div class="row">
               <div class="twelve-columns center">
                   <h1 class="main_title" style="margin-bottom:70px">Further Side Events</h1>
               </div>
            </div>
           <div class="row clearfix">
                <?php for($i = 0; $i < 9; $i++) { 
                 if($sideevents_2016[$i]['premium'] === 'no'):  ?> 
                
                <div class="three columns">
                    <div class="img_box">
                        <a href="#" data-featherlight="#fl<?= $i ?>">
                            <img class="lazyload-scroll" data-src="/src/images/sideevents/<?= $sideevents_2016[$i]['main_image'] ?>" style="max-width: 452px;" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                            <noscript>
                                <img src="/src/images/sideevents/<?= $sideevents_2016[$i]['main_image'] ?>" style="max-width: 452px;" alt="<?= $sideevents_2016[$i]['headline'] ?>">
                            </noscript>
                        </a>
                    </div>
                    <a href="#" data-featherlight="#fl<?= $i ?>"><h2 class="down_sub_title" style="margin-bottom:5px; margin-top:18px"><?= $sideevents_2016[$i]['headline'] ?></h2></a>
                    <div class="descr descr_small_margin" style="margin-top:8px">
                       <?= $sideevents_2016[$i]['info'] ?>
                    </div>
                    <div class="link_box" style="margin:0">
                        <span style="cursor:pointer;" data-featherlight="#fl<?= $i ?>" class="link">More Info<span class="icon icon-arrow_right"></span></span>
                    </div>
                </div>
                 
                 <!-- Lightbox Content: -->
                 
                 <div class="lightbox" id="fl<?= $i ?>">
                     <div class="middle" style="text-align:center">
                         <div class="img_box" style="margin-bottom:30px">
                             <img class="lazyload-scroll" data-src="/src/images/sideevents/<?= $sideevents_2016[$i]['main_image'] ?>" style="max-width: 452px;" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                             <noscript>
                                 <img src="/src/images/sideevents/<?= $sideevents_2016[$i]['main_image'] ?>" style="max-width: 452px;" alt="<?= $sideevents_2016[$i]['hotel'] ?>">
                             </noscript>
                         </div> 
                         <h2 class="down_sub_title" style="margin-bottom:19px; margin-top:28px"><?= $sideevents_2016[$i]['headline'] ?></h2>
                         <div class="descr descr_small_margin" style="margin-bottom:20px; color: #7a7a7a"><?= $sideevents_2016[$i]['adress'] ?></div>
                         <div class="descr descr_small_margin" style="margin-bottom:20px; color: #7a7a7a"><?= $sideevents_2016[$i]['location'] ?></div>
                         <div class="descr descr_small_margin" style="margin-bottom:20px; color: #7a7a7a"><?= $sideevents_2016[$i]['info'] ?></div>
                         <a href="<?= $sideevents_2016[$i]['booking_link'] ?>" class="button orange" style="margin-bottom:10px"><span>REGISTER</span></a>
                     </div>
                 </div>
                 <?php endif; }?>
                   
            </div>
            <div class="row clearfix" style="margin-top:40px">
                <?php for($i = 9; $i < count($sideevents_2016); $i++) { 
                 if($sideevents_2016[$i]['premium'] === 'no'):  ?> 
                
                <div class="three columns">
                    <div class="img_box">
                        <a href="#" data-featherlight="#fl<?= $i ?>">
                            <img class="lazyload-scroll" data-src="/src/images/sideevents/<?= $sideevents_2016[$i]['main_image'] ?>" style="max-width: 452px;" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                            <noscript>
                                <img src="/src/images/sideevents/<?= $sideevents_2016[$i]['main_image'] ?>" style="max-width: 452px;" alt="<?= $sideevents_2016[$i]['headline'] ?>">
                            </noscript>
                        </a>
                    </div>
                    <a href="#" data-featherlight="#fl<?= $i ?>"><h2 class="down_sub_title" style="margin-bottom:5px; margin-top:18px"><?= $sideevents_2016[$i]['headline'] ?></h2></a>
                    <div class="descr descr_small_margin" style="margin-top:8px">
                       <?= $sideevents_2016[$i]['info'] ?>
                    </div>
                    <div class="link_box" style="margin:0">
                        <span style="cursor:pointer;" data-featherlight="#fl<?= $i ?>" class="link">More Info<span class="icon icon-arrow_right"></span></span>
                    </div>
                </div>
                 
                 <!-- Lightbox Content: -->
                 
                 <div class="lightbox" id="fl<?= $i ?>">
                     <div class="middle" style="text-align:center">
                         <div class="img_box" style="margin-bottom:30px">
                             <img class="lazyload-scroll" data-src="/src/images/sideevents/<?= $sideevents_2016[$i]['main_image'] ?>" style="max-width: 452px;" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                             <noscript>
                                 <img src="/src/images/sideevents/<?= $sideevents_2016[$i]['main_image'] ?>" style="max-width: 452px;" alt="<?= $sideevents_2016[$i]['hotel'] ?>">
                             </noscript>
                         </div> 
                         <h2 class="down_sub_title" style="margin-bottom:19px; margin-top:28px"><?= $sideevents_2016[$i]['headline'] ?></h2>
                         <div class="descr descr_small_margin" style="margin-bottom:20px; color: #7a7a7a"><?= $sideevents_2016[$i]['adress'] ?></div>
                         <div class="descr descr_small_margin" style="margin-bottom:20px; color: #7a7a7a"><?= $sideevents_2016[$i]['location'] ?></div>
                         <div class="descr descr_small_margin" style="margin-bottom:20px; color: #7a7a7a"><?= $sideevents_2016[$i]['info'] ?></div>
                         <a href="<?= $sideevents_2016[$i]['booking_link'] ?>" class="button orange" style="margin-bottom:10px"><span>REGISTER</span></a>
                     </div>
                 </div>
                 <?php endif; }?>
                   
            </div>
        </div>
    </section>
</div>

<?php include realpath('layout/signup-form.php');?>
<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>