<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>

    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

     <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section desktop_only"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section"><span class="mobile_hidden">Exhibition</span></div>
            <ul class="sub_nav ul-reset">
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                    </li>            
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                    </li>
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                    </li>
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                    </li>
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                    </li>
                    <li>
                        <a class="link_nav buyticket" href="#apply" data-section="buyticket"><?php echo Meta::getButtonWording(); ?></a>
                    </li>
                </ul>
        </div>
    </div>

    
    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-exhibition active section_1" style="background:url('/src/images/exhibition_big_img.png'), center, center; background-size:cover;">

        <div class="content_section center">
            <div class="wrapper">
                <div class="info_text_top">
                    <h1 class="top_title" style="text-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);">Startup Exhibition</h1>    
                </div>
            </div>
        </div>
    </section><!-- section -->

    <section class="section white active section_1 section_pt" id="content">

        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle" style="vertical-align: 0 !important;">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center" style="margin-bottom:12px">Take your startup to the next level!</h1>
                            <div class="descr center">Don’t miss the opportunity to showcase your innovative and exciting new product to media, corporations, and investors. Boost awareness, build up business relationships, expand your network, and get inspired by others. It’s a must-attend for startups. Just bring your team and bring it on!
                            <br>
                            <br>
                            Exhibition booths will be sorted by these clusters:
                            </div>  
                                    
                        <div class="wrap_speakers" style="margin: 0 auto; display: table;">
                            <div class="conteiner" style="margin: 0 auto;">
                                <div class="list_users center">
                                    <a href="https://www.bitsandpretzels.com/cluster">
                                        <div class="user_box" style="width:100px !important; padding: 0px 5px; !important; height:140px; text-align: center;">
                                            <div class="user-offset">
                                                <img class="lazyload" data-src="/src/images/cluster/icon_2017/commerce.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="width:80px" />
                                                <noscript>
                                                    <img style="width:80px" src="/src/images/cluster/icon_2017/commerce.png" alt="Future Commerce">
                                                </noscript>
                                                <!-- <p class="name_user commerceorange" style="margin-top:10px; font-size:1.2em">Future Commerce</p> -->
                                            </div>
                                        </div>
                                        </a>
                                    <a href="https://www.bitsandpretzels.com/cluster">
                                        <div class="user_box" style="width:100px !important; padding: 0px 5px; !important; height:140px; text-align: center;">
                                            <div class="user-offset">
                                                <img class="lazyload" data-src="/src/images/cluster/icon_2017/mobility.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="width:80px" />
                                                <noscript>
                                                    <img style="width:80px" src="/src/images/cluster/icon_2017/mobility.png" alt="Fast Mobility">
                                                </noscript>
                                                <!-- <p class="name_user mobileorange" style="margin-top:10px; font-size:1.2em">Fast Mobility</p> -->
                                            </div>
                                        </div>
                                    </a>
                                    <a href="https://www.bitsandpretzels.com/cluster">
                                        <div class="user_box" style="width:100px !important; padding: 0px 5px; !important; height:140px; text-align: center;">
                                            <div class="user-offset">
                                                <img class="lazyload" data-src="/src/images/cluster/icon_2017/lifestyle.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="width:80px" />
                                                <noscript>
                                                    <img style="width:80px" src="/src/images/cluster/icon_2017/lifestyle.png" alt="Hot Lifestyle">
                                                </noscript>
                                                <!-- <p class="name_user pink" style="margin-top:10px; font-size:1.2em">Hot Lifestyle</p> -->
                                            </div>
                                        </div>
                                    </a>
                                    <a href="https://www.bitsandpretzels.com/cluster">
                                        <div class="user_box" style="width:100px !important; padding: 0px 5px; !important; height:140px; text-align: center;">
                                            <div class="user-offset">
                                                <img class="lazyload" data-src="/src/images/cluster/icon_2017/iot.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="width:80px" />
                                                <noscript>
                                                    <img style="width:80px" src="/src/images/cluster/icon_2017/iot.png" alt="Sophisticated IoT">
                                                </noscript>
                                                <!-- <p class="name_user purple" style="margin-top:10px; font-size:1.2em">Sophisticated IoT</p> -->
                                            </div>
                                        </div>
                                    </a>
                                    <a href="https://www.bitsandpretzels.com/cluster">
                                        <div class="user_box" style="width:100px !important; padding: 0px 5px; !important; height:140px; text-align: center;">
                                            <div class="user-offset">
                                                <img class="lazyload-scroll" data-src="/src/images/cluster/icon_2017/smart_company.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="width:80px" />
                                                <noscript>
                                                    <img style="width:80px" src="/src/images/cluster/icon_2017/smart_company.png" alt="Smart Company">
                                                </noscript>
                                                <!-- <p class="name_user darkgreen" style="margin-top:10px; font-size:1.2em">Smart Company</p> -->
                                            </div>
                                        </div>
                                    </a>
                                    <a href="https://www.bitsandpretzels.com/cluster">
                                        <div class="user_box" style="width:100px !important; padding: 0px 5px; !important; height:140px; text-align: center;">
                                            <div class="user-offset">
                                                <img class="lazyload-scroll" data-src="/src/images/cluster/icon_2017/money.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="width:80px" />
                                                <noscript>
                                                    <img style="width:80px" src="/src/images/cluster/icon_2017/money.png" alt="Big Money">
                                                </noscript>
                                                <!-- <p class="name_user green" style="margin-top:10px; font-size:1.2em">Big Money</p> -->
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                     </div>
                    
                      <div class="wrapp_main_text" style="margin-top: 40px">
                            <h1 class="main_title center" style="margin-bottom:12px">Exhibition Packages</h1>
                            <div class="descr center">Presenting your idea to the world costs quite an effort but shouldn’t cost the world. That’s why we are offering a special package deal that saves you a lot of money compared to regular booth prices.<br>
                            Our attractive offer for your startup: The exhibition package corresponds to the price of 3 startup tickets. Thus, you’ll get the exhibition booth “for free”!
                            </div>  
                             <div class="descr center" ><a href="<?php echo Router::getRoute('exhibition_int'); ?>" style="color:#005690">You are an international startup? Check out our global initiative for free tickets!</a></div>
                        </div>
                                                              
                        <div class="grid grid-2 grid-2-simple clearfix">
                            <div class="col col-1">
                                <div class="start_up_apply white">
                                    <div class="blue_box" style="background-color:#3c78d8;">
                                        <h2 class="st_heading" style="margin-bottom: 0px;">Early Stage Startup</h2>
                                    </div>
                                    <div class="silver">
                                    <!--<div class="sub_links sub_links_buy sub_links-dark">
                                        <ul class="feature-list color-white">
                                            <li>3 Tickets included</li>
                                            <li>All Startup Ticket features</li>
                                            <li>3 three-day Festival Tickets</li>
                                            <li>2-day exhibition booth</li>
                                            <li>Submit business plan to 400+ Investors</li>
                                            <li>Printed company logo</li>
                                            <li>Company profile in event app</li>
                                            <li>Prefered Table Captain selection</li>
                                            <li>Targeted email announcement</li>
                                            <li>Access to exhibitor attendee list</li>
                                        </ul>
                                    </div>   -->                                 
                                    <div class="white main_price">
                                        <h3 class="h3">First Batch</h3>
                                        <span>1.197€</span>
                                        <div class="small_text alert_limited" style="color: rgba(242, 53, 44, 1); position:static; text-shadow:none">guaranteed until July 31st</div>
                                    </div>
                                    <div class="other_prices">
                                        <p class="other_prices-wrap">
                                           Second Batch
                                           <!--Until July 31--><span>1.599€</span> 
                                        </p>
                                        <p class="other_prices-wrap">
                                           Third Batch
                                           <!--Until August 31--><span>2.129€</span> 
                                        </p>
                                    </div>
                                    </div>
                                    <div class="blue_box" style="background-color:#3c78d8;">
                                        <p style="font-size: 0.9em; font-weight: 400; margin-bottom: 15px;">Limited to startups younger than 3 years AND less than 10 employees</p>
                                       
                                        <a href="#apply" class="button orange" style="margin-left: 0px !important; font-family:raleway, sans-serif; background-color:#ea5c37; color: #ffffff; font-size:1.2 em; width:100%; border-radius:4px; text-align:center;"><span>Sign up*</span></a>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col col-2">
                                <div class="start_up_apply white">
                                    <div class="blue_box blue_box-dark" style="background-color:#0b5394;">
                                        <h2 class="st_heading" style="margin-bottom: 0px;">Later Stage Startup</h2>
                                    </div>
                                     <div class="silver">
                                    <!--<div class="sub_links sub_links_buy sub_links-darker">
                                        <ul class="feature-list color-white">
                                            <li>3 Tickets included</li>
                                            <li>All Startup Ticket features</li>
                                            <li>3 three-day Festival Tickets</li>
                                            <li>2-day exhibition booth</li>
                                            <li>Submit business plan to 400+ Investors</li>
                                            <li>Printed company logo</li>
                                            <li>Company profile in event app</li>
                                            <li>Prefered Table Captain selection</li>
                                            <li>Targeted email announcement</li>
                                            <li>Access to exhibitor attendee list</li>
                                        </ul>
                                    </div>      -->                              
                                    <div class="white main_price">
                                         <h3 class="h3">First Batch</h3>
                                         <span>1.497€</span>
                                        <div class="small_text alert_limited" style="color: rgba(242, 53, 44, 1); position:static; text-shadow:none">guaranteed until July 31st</div>
                                    </div>
                                    <div class="other_prices">
                                        <p class="other_prices-wrap">
                                           Second Batch
                                           <!--Until July 31--><span>1.999€</span> 
                                        </p>
                                        <p class="other_prices-wrap">
                                           Third Batch
                                           <!--Until August 31--><span>2.499€</span> 
                                        </p>
                                    </div>
                                    </div>
                                    <div class="blue_box" style="background-color:#0b5394;">
                                        <p style="font-size: 0.9em; font-weight: 400; margin-bottom: 15px;">Limited to startups younger than 10 years AND less than 300 employees</p>
                                        <a href="#apply" class="button orange" style="margin-left: 0px !important; font-family:raleway, sans-serif; background-color:#ea5c37; color: #ffffff; font-size:1.2 em; width:100%; border-radius:4px; text-align:center;"><span>Sign up*</span></a>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        
                        
                        <div class="vat vat_mb0"><p>all prices excl. VAT<br>*we are looking for top exhibitors - that’s why we handpick all exhibitors through an application process</p></div>                                       
                                                        
                    </div>
                </div>
                
            </div>
        </div>
    </section><!-- section -->
   <!-- <section class="section white active section_1 section_pt">
        <div class="content_section center">
                <div class="wrapper_inner">
                    <div class="valign">
                        <div class="middle">
                            <div class="wrapp_main_text">
                                
                                
                                <div><h1 class="main_title center">Your benefits</h1></div>
                    <div class="descr center"><p style="color:#005690; font-size:1.3em">Conference Tickets</p>Three tickets for your team are included in the package.</div>
                    <div class="descr center"><p style="color:#005690; font-size:1.3em">Exhibition booth</p>Showcase your startup to media and investors for one day.</div>
                    <div class="descr center"><p style="color:#005690; font-size:1.3em">Printed company profile</p>To help you traveling with small luggage. Your booth already comes with a branding.</div>
                    <div class="descr center"><p style="color:#005690; font-size:1.3em">1-on-1 meetings</p>Get the most out of your stay and arrange 1-on-1 meetings with investors and other attendees.</div>
                    <div class="descr center"><p style="color:#005690; font-size:1.3em">Preferred Table Captain selection</p>Select your Table Captain before everyone else does and meet the person that elevates your startup the most.</div>                    

                           
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </section>                  -->
    
    <section class="section white active section_1 section_pt">
        <div class="content_section center">
                <div class="wrapper_inner">
                    <div class="valign">
                        <div class="middle" style="display: block;">
                            <div class="wrapp_main_text" style="margin-top: -90px;">
                                
                                <div><h1 class="main_title center">Your benefits</h1></div>
                                <div class="descr center" style="margin-bottom: 20px;"><p style="color:#005690; font-size:1.3em">Conference Tickets</p>Three 3-day festival tickets for your team are included in the package</div>
                                <div class="descr center" style="margin-bottom: 20px;"><p style="color:#005690; font-size:1.3em">Exhibition booth</p>Showcase your startup on one of the conference days at a much reduced price.</div>
                                <div class="descr center" style="margin-bottom: 20px;"><p style="color:#005690; font-size:1.3em">Printed company profile</p>To help you traveling with small luggage. Your booth already comes with a branding.</div>
                                <div class="descr center" style="margin-bottom: 20px;"><p style="color:#005690; font-size:1.3em">1-on-1 meetings</p>Get the most out of your stay and arrange 1-on-1 meetings with investors and other attendees.</div>
                                <div class="descr center" style="margin-bottom: 20px;"><p style="color:#005690; font-size:1.3em">Preferred Table Captain selection</p>Select your Table Captain before everyone else does and meet the person that elevates your startup the most.</div>  
                            </div>                  

                        <div class="grid grid-2 grid-2-simple clearfix container_info">
                            <div class="col quote quote-1">
                                <div class="info_box">                                                         
                                    <h1 class="main_title">“Bits &amp; Pretzels is a conference where one can actually make a business.<br> We made excellent contacts that will help us to close our A-round soon.”</h1>
                                    <div class="list_users clearfix">
                                        <div class="user_box" style="float:none;margin: 0 auto">
                                            <div class="user-offset">
                                                <div class="img_user">
                                                    <img class="lazyload-scroll" data-src="/src/images/testimonials/default/potocnik.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                    <noscript>
                                                        <img src="/src/images/testimonials/default/potocnik.png" alt="">
                                                    </noscript>
                                                </div>
                                                <p class="name_user">Tim Potocnik</p>
                                                <p>Managing Director and Co-Founder</p>
                                                <p>Eurosender</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col quote">
                                 <div class="info_box">                                                         
                                    <h1 class="main_title">“Well organized, great service for a good price and awesome evening events. I honestly can recommend taking part to other startups.”</h1>
                                    
                                    <div class="list_users clearfix">
                                        <div class="user_box" style="float:none;margin: 0 auto">
                                            <div class="user-offset">
                                                <div class="img_user">
                                                    <img class="lazyload-scroll" data-src="/src/images/testimonials/default/raisch.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                    <noscript>
                                                        <img src="/src/images/testimonials/default/raisch.png" alt="">
                                                    </noscript>
                                                </div>
                                                <p class="name_user">Matthias Raisch</p>
                                                <p>Managing Director and Founder</p>
                                                <p>pareton</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </section>          
    
    
     <section class="section section_wbox apply active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <a name="apply"></a>

        <div class="content_section">
            <div class="wrapper_inner center">
                <div class="valign">
                    <div class="middle" style="background: rgba(255, 255, 255, 0);">
                        <div style="background:rgba(255, 255, 255, 0); margin: auto; text-align: center !important; position: relative;">
                            <h1 class="main_title" style="padding-top: 10%; margin: auto; text-align:center; position: relative;"><p style="color:#FFFFFF">Want to be part of it?</p></h1><p style="color:#FFFFFF; text-align: center">Just enter your email address and you will receive the application form</p>
                       <!--    <script type="text/javascript" src="https://form.jotformeu.com/jsform/70783252306353"></script>
                            <script src="https://cdn.jotfor.ms/static/prototype.forms.js" type="text/javascript"></script>
<script src="https://cdn.jotfor.ms/static/jotform.forms.js?3.3.17970" type="text/javascript"></script>
<script type="text/javascript"> JotForm.init(function(){ setTimeout(function() { $('input_3').hint('Email Address '); }, 20); JotForm.alterTexts({"alphabetic":"This field can only contain letters","alphanumeric":"This field can only contain letters and numbers.","ccDonationMinLimitError":"Minimum amount is {minAmount} {currency}","ccInvalidCVC":"CVC number is invalid.","ccInvalidExpireDate":"Expire date is invalid.","ccInvalidNumber":"Credit Card Number is invalid.","ccMissingDetails":"Please fill up the Credit Card details.","ccMissingDonation":"Please enter numeric values for donation amount.","ccMissingProduct":"Please select at least one product.","characterLimitError":"Too many Characters. The limit is","characterMinLimitError":"Too few characters. The minimum is","confirmClearForm":"Are you sure you want to clear the form?","confirmEmail":"E-mail does not match","currency":"This field can only contain currency values.","cyrillic":"This field can only contain cyrillic characters","dateInvalid":"This date is not valid. The date format is {format}","dateInvalidSeparate":"This date is not valid. Enter a valid {element}.","dateLimited":"This date is unavailable.","disallowDecimals":"Please enter a whole number.","email":"Enter a valid e-mail address","fillMask":"Field value must fill mask.","freeEmailError":"Free email accounts are not allowed","generalError":"There are errors on the form. Please fix them before continuing.","generalPageError":"There are errors on this page. Please fix them before continuing.","gradingScoreError":"Score total should only be less than or equal to","incompleteFields":"There are incomplete required fields. Please complete them.","inputCarretErrorA":"Input should not be less than the minimum value:","inputCarretErrorB":"Input should not be greater than the maximum value:","lessThan":"Your score should be less than or equal to","maxDigitsError":"The maximum digits allowed is","maxSelectionsError":"The maximum number of selections allowed is","minSelectionsError":"The minimum required number of selections is","multipleFileUploads_emptyError":"{file} is empty, please select files again without it.","multipleFileUploads_fileLimitError":"Only {fileLimit} file uploads allowed.","multipleFileUploads_minSizeError":"{file} is too small, minimum file size is {minSizeLimit}.","multipleFileUploads_onLeave":"The files are being uploaded, if you leave now the upload will be cancelled.","multipleFileUploads_sizeError":"{file} is too large, maximum file size is {sizeLimit}.","multipleFileUploads_typeError":"{file} has invalid extension. Only {extensions} are allowed.","numeric":"This field can only contain numeric values","pastDatesDisallowed":"Date must not be in the past.","pleaseWait":"Please wait...","required":"This field is required.","requireEveryCell":"Every cell is required.","requireEveryRow":"Every row is required.","requireOne":"At least one field required.","submissionLimit":"Sorry! Only one entry is allowed. Multiple submissions are disabled for this form.","uploadExtensions":"You can only upload following files:","uploadFilesize":"File size cannot be bigger than:","uploadFilesizemin":"File size cannot be smaller than:","url":"This field can only contain a valid URL","wordLimitError":"Too many words. The limit is","wordMinLimitError":"Too few words. The minimum is"}); JotForm.clearFieldOnHide="disable"; });
</script> -->
<form class="jotform-form" action="https://submit.jotformeu.com/submit/70783252306353/" method="post" name="form_70783252306353" id="70783252306353" accept-charset="utf-8"> <input type="hidden" name="formID" value="70783252306353" /> 
    <div class="four columns offset-by-four">
    <div class="form-all" style="text-align:center !important;"> 
        <ul class="form-section page-section"; style="background: rgb(255, 255, 255, 0) !important"> 
            <li class="form-line jf-required" data-type="control_email" id="id_3">
                <div id="cid_3" class="form-input-wide jf-required"> 
                     <div style="padding-top:15px;" class="form-buttons-wrapper"> 
                    <input style="margin-left: 0px !important; font-family:raleway, sans-serif; border:1px solid white !important; color:#005690 !important;font-size:17px; width:100%; border-radius:4px; height:45px; text-align:center;" type="email" id="input_3" name="q3_input3" class="form-textbox validate[required, Email]" size="40" value="" placeholder="Email Address " data-component="email" /> 
                    </div>
                </div> 
            </li> 
            <li class="form-line" data-type="control_button" id="id_10"> 
                <div id="cid_10" class="form-input-wide"> 
                    <div style="text-align:center; padding-top:15px;" class="form-buttons-wrapper"> 
                        <button style="font-family:raleway, sans-serif;color:#FFFFFF; cursor:pointer; width:100%; border-radius: 4px;height: 47px;background:#ea5c3f;" id="input_10" type="submit" class="form-submit-button form-submit-button-img form-submit-button-simple_red; " data-component="button" >Submit</button> 
                    </div> 
                </div>
            </li> 
            <li class="form-line" data-type="control_divider" id="id_4"> 
                <div id="cid_4" class="form-input-wide"> 
                    <div data-component="divider" style="border-bottom:1px solid transparent;height:1px;margin-left:0px;margin-right:0px;margin-top:5px;margin-bottom:5px;"> </div> 
                </div>
                </li> 
            <li class="form-line" data-type="control_divider" id="id_5"> 
            <div id="cid_5" class="form-input-wide"> 
                <div data-component="divider" style="border-bottom:1px solid transparent;height:1px;margin-left:0px;margin-right:0px;margin-top:5px;margin-bottom:5px;"> </div> 
            </div> 
            </li>
            <li style="display:none"> Should be Empty: <input type="text" name="website" value="" />
            </li> 
        </ul> 
    </div> 
    </div>
    <input type="hidden" id="simple_spc" name="simple_spc" value="70783252306353" /> <script type="text/javascript"> document.getElementById("si" + "mple" + "_spc").value = "70783252306353-70783252306353"; </script>
</form>
<script type="text/javascript">JotForm.ownerView=true;</script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    
    <section class="section silver active section_1 section_pt">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">   
                        <div class="wrapp_main_text">
                            <h1 class="main_title">FAQ</h1>
                        </div> 
                        <div class="grid clearfix">
                            <div class="valign">    
                                <div class="middle">  
                                    <div class="topic topic_open">                                                     
                                        <h1 class="question_title">What is included?</h1>
                                        <div class="arrow_pointer"></div>
                                        <div class="description_info descr descr_no_margin" style="display:none">
                                            <div class="question_subtitle">Before Bits & Pretzels</div> 
                                            <ul class="questions_list questions_list-unordered">
                                               <li>
                                                   Company logo and link on event website and exhibitor listings
                                               </li>
                                            </ul>
                                            <div class="question_subtitle">During Bits &amp; Pretzels</div> 
                                            <ul class="questions_list questions_list-unordered">
                                               <li>
                                                   Personal exhibition counter for one day (September 24 OR 25)
                                               </li>
                                                <li>
                                                   Three 3-day tickets for Bits &amp; Pretzels
                                               </li>
                                                <li>
                                                   Wi-Fi and electricity provided
                                               </li>
                                               <li>
                                                   Printed company logo with company description
                                               </li>
                                                <li>
                                                   Brand inclusion in event app and exhibitor listings
                                               </li>
                                                <li>
                                                   Preferred right to choose a Table Captain
                                               </li>
                                                <li>
                                                    Invitation to launch party
                                               </li>
                                            </ul>
                                        </div>
                                    </div> 
                                    <div class="topic topic_open">                                                     
                                        <h1 class="question_title">How do I participate?</h1>
                                        <div class="arrow_pointer"></div>
                                        <div class="description_info descr descr_no_margin" style="display:none">
                                            <ol class="questions_list">
                                               <li>
                                                  Enter your email address in the field above (you will immediately receive an email with the link to the official application form)
                                               </li>
                                                <li>
                                                  Apply by giving us detailed information about your startup
                                               </li>
                                                <li>
                                                   Get nominated
                                               </li>
                                               <li>
                                                   Buy the exhibition package including 3 tickets for your team.
                                               </li>
                                               <li>
                                                   Exhibit at Bits &amp; Pretzels
                                               </li>
                                            </ol>
                                        </div>
                                    </div> 
                                    <div class="topic topic_open">                                                     
                                        <h1 class="question_title">When will participants be announced?</h1>
                                        <div class="arrow_pointer"></div>
                                        <div class="description_info descr descr_no_margin" style="display:none">
                                            <p class="question_text">
                                                The earlier you apply the more likely you get chosen. In order to make planning your trip easy, we choose the start-ups in three batches. If applications in the earlier batches exceed the limit of available spots, no more start-ups will be chosen to participate. So make sure you apply as early as possible.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="topic topic_open">                                                     
                                        <h1 class="question_title">I have already bought tickets, can I return them?</h1>
                                        <div class="arrow_pointer"></div>
                                        <div class="description_info descr descr_no_margin" style="display:none">
                                            <p class="question_text">
                                                Yes, you can. As soon as you have purchased your new exhibition ticket, we will refund your previous one. Please note that in case you own a discounted ticket (e.g. from Crazy or Early Bird ticket sale), the discount cannot be applied to the new exhibition ticket.
                                            </p>
                                        </div>
                                    </div>   
                                    <div class="topic topic_open">                                                     
                                        <h1 class="question_title">What is the purpose of exhibiting?</h1>
                                        <div class="arrow_pointer"></div>
                                        <div class="description_info descr descr_no_margin" style="display:none">
                                            <p class="question_text">
                                                It's all about boosting awareness that will help you grow, increasing your media exposure and helping you to build profound business relationships with corporations and investors. Use your chance to get in touch with thousands of attendees, present your company, find new clients and business partners.
                                            </p>
                                        </div>
                                    </div> 
                                    <div class="topic topic_open">                                                     
                                        <h1 class="question_title">I have specific questions, whom do I contact?</h1>
                                        <div class="arrow_pointer"></div>
                                        <div class="description_info descr descr_no_margin" style="display:none">
                                            <p class="question_text">
                                                Please contact <a style="color:#27A5E3" href='&#109;a&#105;&#108;to&#58;e&#37;78h%69bit&#37;6&#57;o%6&#69;%40%6&#50;its&#37;6&#49;n&#100;%70re&#37;7&#52;&#122;&#101;&#108;s&#46;&#37;6&#51;om'>exh&#105;b&#105;t&#105;o&#110;&#64;&#98;i&#116;san&#100;pretzel&#115;&#46;c&#111;m</a> for any further exhibition queries.
                                            </p>
                                        </div>
                                    </div>                                                                                          
                                </div>
                            </div>
                        </div>            
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
</div>


<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>

