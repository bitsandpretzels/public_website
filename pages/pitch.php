<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

    <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section mobile_hidden">Startup Pitch</div>
            <ul class="sub_nav ul-reset">
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                    </li>            
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                    </li>
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                    </li>
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                    </li>
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                    </li>
                    <li>
                        <a class="link_nav buyticket" href="#apply" data-section="buyticket"><?php echo Meta::getButtonWording(); ?></a>
                    </li>
                </ul>
        </div>
    </div>

    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-pitch active section_1" style="background:url('/src/images/pitch_big_img.png'), center, center; background-size:cover;">
        <div class="content_section content_section-cluster center">
            <div class="wrapper">
                <div class="info_text_top">
                    <h1 class="top_title" style="text-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);">STARTUP PITCH</h1>
                    <!--
                    <h2 class="main_title">Bits & Pretzels would not be possible <span class="stay_block">without our partners.</span></h2>   
                    -->       
                </div>
            </div>
        </div>
    </section><!-- section -->

   <!-- <section class="section section_logo silver active section_1 section_pt">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <div class="descr descr_big_margin" style="color:#ea5c37; margin-bottom:50px">
                            ++++++ Application closed ++++++
                            </div>  
                            <h1 class="main_title" style="margin-bottom:18px">An epic battle</h1>
                            <div class="descr descr_big_margin">
                            Over two days, all chosen startups are fighting in six different industry clusters to reach the grand finale. A group of experts and investors from each cluster will challenge the participants in the first round (September 25) until only the best six startups &ndash; our finalists &ndash; are left to present their companies on the main stage to thousands of attendees (September 26).
                            </div>  
                        </div>    
                        <div class="wrapp_main_text">
                            <h1 class="main_title" style="margin-bottom:18px">Prizes</h1>
                            <div class="descr" style="text-align:left; margin-bottom:20px">
                                <strong>The winner</strong> of the grand final gets even two „money can’t buy prizes“:
                                <br> <strong>1.</strong> Joining the finals of the <a href="http://www.extremetechchallenge.com/" target="_blank" style="color:#005690">Extreme Tech Challenge</a> on Necker Island with Richard Branson on the jury
                                <br> <strong>2.</strong> Finalist spot with 500 Startups in Silicon Valley
                                
                                <br><br>
                                <strong>All six finalists</strong> receive a cash prize (5.000€ each), 1-hour mentoring by a jury member, a private pitch session with the jury member / investor of your choice and will battle on the main conference stage for the grand prizes.
                                <div style="width:100%" class="center">
                                    <img src="src/images/pitch.png" alt="Neckar Island" style="margin-top:35px; width:100%; max-width:640px">
                                </div>
                            </div>  
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </section>--><!-- section -->

    <section id="content" class="section section_logo section_users section_list white active section_1 section_pt">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">

                       <div class="wrapp_main_text">
                            <h1 class="main_title center" style="margin-bottom:24px">Startup? Pitch and get funding.</h1>
                        </div>
                        <div class="wrapp_main_text">
                            <div class="descr descr_small_margin center">
                                <div class="link_box">100 chosen startups are fighting in six different industry clusters to reach the grand finale. A group of prime VCs and investors will challenge the participants in the first round until only the best six startups – our finalists – are left. They'll get the chance to present their companies on our Center Stage and pitch to 5,000 attendees.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
     <section id="gallery" class="clearfix desktop_only">
                            <div class="img_wrapper-zoom">
                                <img class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="src/images/pitch/pitch1.png?v=<?php echo $currentScriptVersion; ?>" alt="Academy" title="Startup Academy">
                                <noscript>
                                    <img class="lazyload-scroll"src="src/images/pitch/pitch1.png?v=<?php echo $currentScriptVersion; ?>" alt="Academy" title="Startup Academy">
                                </noscript>
                            </div>
                            <div class="img_wrapper-zoom">
                                <img class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="src/images/pitch/pitch2.png?v=<?php echo $currentScriptVersion; ?>" alt="" >
                                <noscript>
                                    <img class="lazyload-scroll" src="src/images/pitch/pitch2.png2.jpg?v=<?php echo $currentScriptVersion; ?>"  alt="Academy" title="Startup Academy">
                                </noscript>
                            </div>
                            <div class="img_wrapper-zoom">
                                <img class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="src/images/pitch/pitch3.png?v=<?php echo $currentScriptVersion; ?>" alt="Kevin Spacey">
                                <noscript>
                                    <img class="lazyload-scroll"src="src/images/pitch/pitch3.png?v=<?php echo $currentScriptVersion; ?>"  alt="Academy" title="Startup Academy">
                                </noscript>
                            </div>
                        </section>
                        
                        
                        
     <section id="content" class="section section_logo section_users section_list white active section_1 section_pt">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        
                        <div class="wrapp_main_text">
                            <h1 class="main_title center" style="margin-bottom:24px">A priceless experience</h1>
                        </div>
                        <div class="wrapp_main_text">
                            <div class="descr descr_small_margin center">
                                <div class="link_box">A trip to Sir Richard Branson's own Necker Island? Being part of the Extreme Tech Challenge? Meeting awesome people from all over the world? #Truestory! This is a once in a lifetime experience and a prize money can't buy; and YOU have the chance to get it!<br><br>The winner will be part of the</div>
                                <div style="padding: 15px 15px;"><a href="http://www.extremetechchallenge.com/" target="_blank"><img width="150px" height="150px" class="lazyload-scroll" src="/src/images/logos/xtc_logo.png" alt="Extreme Tech Challenge 2018"></a></div>
                                <div class="link_box">at Sir Richard Branson's very own Necker Island!</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
                      
     <section id="gallery" class="clearfix desktop_only">
                            <div class="img_wrapper-zoom">
                                <img class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="src/images/pitch/XTC1.png?v=<?php echo $currentScriptVersion; ?>" alt="Academy" title="Startup Academy">
                                <noscript>
                                    <img class="lazyload-scroll"src="src/images/pitch/XTC1.png?v=<?php echo $currentScriptVersion; ?>" alt="Academy" title="Startup Academy">
                                </noscript>
                            </div>
                            <div class="img_wrapper-zoom">
                                <img class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="src/images/pitch/XTC2.png?v=<?php echo $currentScriptVersion; ?>" alt="" >
                                <noscript>
                                    <img class="lazyload-scroll"src="src/images/pitch/XTC2.png?v=<?php echo $currentScriptVersion; ?>"  alt="Academy" title="Startup Academy">
                                </noscript>
                            </div>
                            <div class="img_wrapper-zoom">
                                <img class="lazyload-scroll" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="src/images/pitch/XTC3.png?v=<?php echo $currentScriptVersion; ?>" alt="Kevin Spacey">
                                <noscript>
                                    <img class="lazyload-scroll"src="src/images/pitch/XTC3.png?v=<?php echo $currentScriptVersion; ?>"  alt="Academy" title="Startup Academy">
                                </noscript>
                            </div>
                        </section>
                       
                        
 <section id="content" class="section section_logo section_users section_list white active section_1 section_pt">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                     <div class="wrapp_main_text">
                      
                            <div class="quote quote-1">
                                <div class="info_box">                                                         
                                    <h1 class="main_title">“An experience for which I will be grateful for the rest of my life!”</h1>
                                    <div class="list_users clearfix">
                                        <div class="user_box" style="float:none;margin: 0 auto">
                                            <div class="user-offset">
                                                <div class="img_user">
                                                    <img class="lazyload-scroll" data-src="/src/images/pitch/freya_oehle.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                    <noscript>
                                                        <img src="/src/images/pitch/freya_oehle.png" alt="">
                                                    </noscript>
                                                </div>
                                                <p class="name_user" style="weight: bold;">Freya Oehle</p>
                                                <p>Winner of Startup Pitch 2016</p>
                                            </div>
                                            <div class="descr center" ><a href="https://www.linkedin.com/pulse/one-day-paradies-die-x-treme-tech-challenge-auf-necker-freya-oehle" target="_blank" style="color:#005690">read more</a></div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        </div>
                      
                      
                      
                       
                        <div class="wrapp_main_text">
                            <h1 class="main_title">Jury</h1>
                            <div class="descr" style="margin-bottom:20px">
                                A high-class jury consisting of top-tier VCs will ensure that only the best startups of each cluster will make it to the finale.
                            </div> 
                        </div>             
                        <div class="wrap_speakers">
                            <div class="conteiner">
                                <div class="list_users clearfix">
                                <?php foreach(People::setType('speaker')->setDataSource($speakers)->get(0, 200) as $person): 
                                    if (strpos($person['track'],"jury")!==false) { ?>
                                    <div class="user_box cluster_speaker">
                                        <div class="user-offset">
                                            <div class="img_user">
                                                <img class="lazyload-scroll" data-src="/src/images/speakers/default/<?php echo $person['image']; ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                <noscript>
                                                    <img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt="">
                                                </noscript>
                                            </div>
                                            <p class="name_user"><?php echo $person['name']; ?></p>
                                            <div class="title"><?php echo $person['title']; ?></div>
                                            <div class="company"><?php echo $person['company']; ?></div>
                                        </div>
                                    </div>
                                    <?php }
                                    endforeach; ?>
                                </div>
                            </div>
                            
                            <!--
							<h1 class="main_title" style="margin-bottom:30px margin-top:50px">Moderator Pitch Stage</h1>
                            <div class="list_users clearfix list_users-cluster-curator">
                                <div class="user_box user_box-cluster-curator">
                                    <div class="user-offset">
                                        <div class="img_user"><img src="/src/images/speakers/default/klose_hannah.png" alt=""></div>
                                        <p class="name_user">Hannah Klose</p>
                                        <div class="title">Communication Manager</div>
                                        <div class="company">Mantro</div>
                                    </div>
                                </div>
                            </div>    -->  
                            </div>
                        </div>        
                    </div>
                </div>
            </div>
    </section>
    
<!--     <section class="section section_wbox apply active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <a name="apply"></a>

        <div class="content_section">
            <div class="wrapper_inner center">
                <div class="valign">
                    <div class="middle" style="background: rgba(255, 255, 255, 0);">
                        <div style="background:rgba(255, 255, 255, 0); margin: auto; text-align: center !important; position: relative;">
                            <h1 class="main_title" style="padding-top: 10%; margin: auto; text-align:center; position: relative;"><p style="color:#FFFFFF">Want to pitch?
</p></h1><p style="color:#FFFFFF; text-align: center">Just enter your email address and you will receive the pitch form.<br>(Note: application has to be submitted before August 15th, 2017)</p>
                    
<form class="jotform-form" action="https://submit.jotformeu.com/submit/70783252306353/" method="post" name="form_70783252306353" id="70783252306353" accept-charset="utf-8"> <input type="hidden" name="formID" value="70783252306353" /> 
    <div class="four columns offset-by-four">
    <div class="form-all" style="text-align:center !important;"> 
        <ul class="form-section page-section"; style="background: rgb(255, 255, 255, 0) !important"> 
            <li class="form-line jf-required" data-type="control_email" id="id_3">
                <div id="cid_3" class="form-input-wide jf-required"> 
                     <div style="padding-top:15px;" class="form-buttons-wrapper"> 
                    <input style="margin-left: 0px !important; font-family:raleway, sans-serif; border:1px solid white !important; color:#005690 !important;font-size:17px; width:100%; border-radius:4px; height:45px; text-align:center;" type="email" id="input_3" name="q3_input3" class="form-textbox validate[required, Email]" size="40" value="" placeholder="Email Address " data-component="email" /> 
                    </div>
                </div> 
            </li> 
            <li class="form-line" data-type="control_button" id="id_10"> 
                <div id="cid_10" class="form-input-wide"> 
                    <div style="text-align:center; padding-top:15px;" class="form-buttons-wrapper"> 
                        <button style="font-family:raleway, sans-serif;color:#FFFFFF; cursor:pointer; width:100%; border-radius: 4px;height: 47px;background:#ea5c3f;" id="input_10" type="submit" class="form-submit-button form-submit-button-img form-submit-button-simple_red; " data-component="button" >Submit</button> 
                    </div> 
                </div>
            </li> 
            <li class="form-line" data-type="control_divider" id="id_4"> 
                <div id="cid_4" class="form-input-wide"> 
                    <div data-component="divider" style="border-bottom:1px solid transparent;height:1px;margin-left:0px;margin-right:0px;margin-top:5px;margin-bottom:5px;"> </div> 
                </div>
                </li> 
            <li class="form-line" data-type="control_divider" id="id_5"> 
            <div id="cid_5" class="form-input-wide"> 
                <div data-component="divider" style="border-bottom:1px solid transparent;height:1px;margin-left:0px;margin-right:0px;margin-top:5px;margin-bottom:5px;"> </div> 
            </div> 
            </li>
            <li style="display:none"> Should be Empty: <input type="text" name="website" value="" />
            </li> 
        </ul> 
    </div> 
    </div>
    <input type="hidden" id="simple_spc" name="simple_spc" value="70783252306353" /> <script type="text/javascript"> document.getElementById("si" + "mple" + "_spc").value = "70783252306353-70783252306353"; </script>
</form>
 
<script type="text/javascript">JotForm.ownerView=true;</script>
                      
                        </div>
                    </div>
                </div>
            </div>
            <div class = "middle valign" style="color: white; text-align: center">We are looking for startups who at least have a MVP and less than 3mio. funding.</div>
        </div>
    </section>-->
       
        
         
          
     <section class="section section_wbox apply active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <a name="apply"></a>

        <div class="content_section">
            <div class="wrapper_inner center">
                <div class="valign">
                    <div class="middle" style="background: rgba(255, 255, 255, 0);">
                        <div style="background:rgba(255, 255, 255, 0); margin: auto; text-align: center !important; position: relative;">
                            <h1 class="main_title" style="padding-top: 10%; margin: auto; text-align:center; position: relative;"><p style="color:#FFFFFF">Want to pitch?
</p></h1><p style="color:#FFFFFF; text-align: center">Just enter your email address and you will receive the pitch form.<br>(Note: application has to be submitted before August 15th, 2017)</p>
                          <form class="jotform-form" action="https://submit.jotformeu.com/submit/71632311539351/" method="post" name="form_71632311539351" id="71632311539351" accept-charset="utf-8">
                              <div class="four columns offset-by-four">
                               <input type="hidden" name="formID" value="71632311539351" /> 
                               <div class="form-all"> 
                                   <ul class="form-section page-section"> 
                                   <li class="form-line jf-required" data-type="control_email" id="id_3"> 
                                   <label class="form-label form-label-top" id="label_3" for="input_3"> 
                                  </label> <div id="cid_3" class="form-input-wide jf-required">
                                    <div style="" class="form-buttons-wrapper">  
                                   <input style="margin-left: 0px !important; font-family:raleway, sans-serif; border:1px solid white !important; color:#005690 !important;font-size:17px; width:100%; border-radius:4px; height:45px; text-align:center;" type="email" id="input_3" name="q3_input3" class="form-textbox validate[required, Email]" size="40" value="" placeholder="Email Address " data-component="email" required="" /> 
                               </div>
                                       </div></li> <li class="form-line" data-type="control_button" id="id_10"> 
                                <div id="cid_10" class="form-input-wide"> 
                                    <div style="text-align:center;" class="form-buttons-wrapper"> 
                                       <div style="text-align:center; padding-top:15px;" class="form-buttons-wrapper"> 
                                        <button  style="font-family:raleway, sans-serif;color:#FFFFFF; cursor:pointer; width:100%; border-radius: 4px;height: 47px;background:#ea5c3f;" id="input_10" type="submit" class="form-submit-button form-submit-button-img" data-component="button">Submit</button> 
                                        </div>
                                    </div> </div> </li> <li class="form-line" data-type="control_divider" id="id_4"> <div id="cid_4" class="form-input-wide"> </div> </li> <li style="display:none"> Should be Empty: <input type="text" name="website" value="" /> </li> </ul> </div> <script> JotForm.showJotFormPowered = "0"; </script> <input type="hidden" id="simple_spc" name="simple_spc" value="71632311539351" /> <script type="text/javascript"> document.getElementById("si" + "mple" + "_spc").value = "71632311539351-71632311539351"; </script></div>
</form>
<script type="text/javascript">JotForm.ownerView=true;</script>
                       
                      
                        </div>
                    </div>
                </div>
            </div>
            <div class = "middle valign" style="color: white; text-align: center">We are looking for startups who at least have a MVP and less than 3mio. funding.</div>
        </div>
    </section>
   
    
    <section class="section silver active section_1 section_pt">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">   
                        <div class="wrapp_main_text">
                            <h1 class="main_title">FAQ</h1>
                        </div> 
                        <div class="wrap_quest">
                            <div class="grid clearfix">
                                <div class="valign">    
                                    <div class="middle"> 
                                        <div class="topic topic_open">                                                     
                                            <h1 class="question_title">Who is allowed to apply?</h1>
                                            <div class="arrow_pointer"></div>
                                            <div class="description_info descr descr_no_margin" style="display:none">
                                                <p class="question_text">
                                                    If you are a startup from Europe and beyond with a MVP and less than 3 Mio € funding, then you are eligible to apply. A jury will decide on your nomination.
                                                </p>
                                            </div>
                                        </div>  

                                        <div class="topic topic_open">                                                     
                                            <h1 class="question_title">How is the process of participation?</h1>
                                            <div class="arrow_pointer"></div>
                                            <div class="description_info descr descr_no_margin" style="display:none">
                                                <p class="question_text">
                                                    Simply apply <a href="#apply" style="color:#005690">here</a> and you will be redirected to our pitch partner's website.
                                                </p> 
                                                <ol class="questions_list">
                                                    <li>
                                                       Apply until August 15th, 2017.
                                                   </li>
                                                   <li>
                                                       Get nominated until August 31st
                                                   </li>
                                                   <li>
                                                       Pitch on our Pitch Stage on September 24 and hopefully advance to the next round.
                                                   </li>
                                                   <li>
                                                      The best 6 startups will pitch on Center Stage (September 25, 2017).
                                                   </li>
                                                </ol>
                                            </div>
                                        </div> 
                                        <div class="topic topic_open">                                                     
                                            <h1 class="question_title">When will participants be announced?</h1>
                                            <div class="arrow_pointer"></div>
                                            <div class="description_info descr descr_no_margin" style="display:none">
                                                <p class="question_text">
                                                    The international jury will select the participants. Nominated startups will be announced by the end of August.
                                                </p>
                                            </div>
                                        </div> 
                                         <div class="topic topic_open">                                                     
                                            <h1 class="question_title">What are the costs?</h1>
                                            <div class="arrow_pointer"></div>
                                            <div class="description_info descr descr_no_margin" style="display:none">
                                                <p class="question_text">
                                                    Every team accepted for the pitch competition is granted 1 free ticket. Every additional team member needs to acquire a startup ticket.
                                                </p>
                                            </div>
                                        </div>    
                                        <div class="topic topic_open">                                                     
                                            <h1 class="question_title">What can I win?</h1>
                                            <div class="arrow_pointer"></div>
                                            <div class="description_info descr descr_no_margin" style="display:none">
                                                <p class="question_text">
                                                <strong>The winner</strong> of the grand finale is invited to join the <a href="http://www.extremetechchallenge.com/" target="_blank" style="color:#005690">Extreme Tech Challenge</a> on Sir Richard Branson's Necker Island. One ticket for the challenge and travel costs for 1 person (limited to 1.000€) will be covered.
                                                </p>
                                            </div>
                                        </div>  
                                        <div class="topic topic_open">                                                     
                                            <h1 class="question_title">I have specific questions, whom do I contact?</h1>
                                            <div class="arrow_pointer"></div>
                                            <div class="description_info descr descr_no_margin" style="display:none">
                                                <p class="question_text">
                                                    Please contact <a href="mailto:pitch@bitsandpretzels.com" style="color:#005690">pitch@bitsandpretzels.com</a> for any other pitch queries.
                                                </p>
                                            </div>
                                        </div>                                                           
                                    </div>
                                </div>
                            </div>  
                        </div>        
                    </div>
                </div>
            </div>
        </div>
        <div id="buyticket" class="button_mb" style="margin-bottom:0; margin-top:80px">
<!--
        <div class="button_box button_box_orange button_box_auto">
            <a href="https://dealmatrix.com/bitsandpretzels/" target="_blank" class="link_nav buyticket button orange" style="cursor:pointer;"><span>Apply Now</span></a>
        </div>
-->
    </div>    
    </section><!-- section -->
</div>

<?#php include realpath('layout/signup-form.php');?>
<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>

