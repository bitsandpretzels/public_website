<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>


    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

    <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section mobile_hidden">Kevin Spacey</div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>            
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                </li>
<!--
                <li>
                    <a class="link_nav buyticket trigger_app_nav" data-section="buyticket">Get your Ticket</a>
                </li>
-->
            </ul>
        </div>
    </div>

    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-spacey active section_1">
        <div class="content_section center">
            <div class="wrapper">
                <div class="info_text_top">
                    <h1 class="top_title">Kevin Spacey </h1>
                </div>
            </div>
        </div>
    </section><!-- section -->

    <section id="content" class="section section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="min-height: 0px">
        <div class="content_section center" style="min-height: 0px">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center" style="margin-bottom:30px">Kevin Spacey holds the opening keynote <br> at Bits &amp; Pretzels</h1>
                            <div class="descr descr_small_margin center">
                            Kevin Spacey, Oscar-winning actor, writer, producer and tech investor, will join this years’ Founders Festival in Munich to inspire 5000+ founders, startup enthusiasts, investors and business incubators from all over the world. Kevin Spacey will give a 30 minute speech for founders followed by a 15 minute Q&amp;A session.
                            <br><br>
                            You have the unique chance to meet Kevin in person during the 3-day festival, where the digital startup environment meets in Munich during Oktoberfest.
                            <div class="link_box"></div>
                            </div>                             
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
    <section id="networking" class="section section_devices silver active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                            <h1 class="main_title center" style="margin-bottom:50px">Tell me more...</h1>
                        <div class="wrapp_main_text">
                            <div class="descr descr_small_margin center">
                            Kevin Spacey gained popularity with  films like The Usual Suspects, Se7en, LA Confidential and American Beauty. With two Academy Awards, eleven years as Artistic Director of the legendary Old Vic theatre and countless other awards, Spacey’s turn to TV in 2013 garnered him legions of new fans. He inhabits the character of Congressman Frank Underwood on Netflix’s first original series, House of Cards. For his role Spacey has won Golden Globe and SAG awards, and is nominated again this year for an Emmy for Best Actor in a Drama Series. For his services to theatre, the arts and international culture, Spacey recently received an honorary Knighthood from Her Majesty Queen Elizabeth.
                           <br><br>
                           But that’t not all: In addition to being recognized for his work in the arts, Kevin Spacey is an active investor in many technology companies. He is a highly sought after speaker that has mesmerized audiences at tech conferences all over the world, including the World Economic Forum at Davos, where he spoke this past year about the art of storytelling and its role in the future of technology.
                           <br><br>
                            Kevin Spacey is coming over to Germany to join Bits &amp; Pretzels. We’re more than excited to hear him live on stage!
                            <div class="link_box"></div>
                            </div>                             
                        </div>  
                        <div id="buyticket" class="button_mb choose-cat-trigger-wrapper" style="margin-top:90px">
                            <div class="button_box button_box_orange button_box_auto trigger_app_nav">
                                <a href="#category" class="button orange"><span>Register for 2017 Ticket Sale</span></a>
                            </div>
                        </div>
                        <ul class="share-buttons">
                          <li>
                              <a href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.bitsandpretzels.com%2Fspacey&t=Kevin%20Spacey%20holds%20opening%20keynote%20at%20Bits%20%26%20Pretzels%20%2F%2F%20Meet%20him%20in%20Munich!" title="Share on Facebook" target="_blank">
                                  <img alt="Share on Facebook" src="src/images/icons_share/Facebook.png">
                              </a>
                          </li>
                          <li>
                              <a href="https://twitter.com/intent/tweet?source=https%3A%2F%2Fwww.bitsandpretzels.com%2Fspacey&text=Kevin%20Spacey%20holds%20opening%20keynote%20at%20Bits%20%26%20Pretzels%20%2F%2F%20Meet%20him%20in%20Munich!:%20https%3A%2F%2Fwww.bitsandpretzels.com%2Fspacey&via=bitsandpretzels" target="_blank" title="Tweet">
                                  <img alt="Tweet" src="src/images/icons_share/Twitter.png">
                              </a>
                          </li>
                          <li>
                              <a href="http://www.tumblr.com/share?v=3&u=https%3A%2F%2Fwww.bitsandpretzels.com%2Fspacey&t=Kevin%20Spacey%20holds%20opening%20keynote%20at%20Bits%20%26%20Pretzels%20%2F%2F%20Meet%20him%20in%20Munich!&s=" target="_blank" title="Post to Tumblr">
                                  <img alt="Post to Tumblr" src="src/images/icons_share/Tumblr.png">
                              </a>
                          </li>
                              <li>
                          <a href="http://pinterest.com/pin/create/button/?url=https%3A%2F%2Fwww.bitsandpretzels.com%2Fspacey&media=https://www.bitsandpretzels.com/src/images/info/spacey_facebook.png&description=The%20Oscar-winning%20actor%20and%20tech%20investor%20will%20be%20on%20stage.%20Join%20Bits%20%26%20Pretzels%20and%20don%E2%80%99t%20miss%20THE%20chance%20that%20Kevin%20will%20come%20to%20Germany!" target="_blank" title="Pin it">
                                  <img alt="Pin it" src="src/images/icons_share/Pinterest.png">
                              </a>
                          </li>
                          <li>
                              <a href="https://getpocket.com/save?url=https%3A%2F%2Fwww.bitsandpretzels.com%2Fspacey&title=Kevin%20Spacey%20holds%20opening%20keynote%20at%20Bits%20%26%20Pretzels%20%2F%2F%20Meet%20him%20in%20Munich!" target="_blank" title="Add to Pocket">
                                  <img alt="Add to Pocket" src="src/images/icons_share/Pocket.png">
                              </a>
                          </li>
                              <li>
                          <a href="http://www.reddit.com/submit?url=https%3A%2F%2Fwww.bitsandpretzels.com%2Fspacey&title=Kevin%20Spacey%20holds%20opening%20keynote%20at%20Bits%20%26%20Pretzels%20%2F%2F%20Meet%20him%20in%20Munich!" target="_blank" title="Submit to Reddit">
                                  <img alt="Submit to Reddit" src="src/images/icons_share/Reddit.png">
                              </a>
                          </li>
                          <li>
                              <a href="http://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fwww.bitsandpretzels.com%2Fspacey&title=Kevin%20Spacey%20holds%20opening%20keynote%20at%20Bits%20%26%20Pretzels%20%2F%2F%20Meet%20him%20in%20Munich!&summary=The%20Oscar-winning%20actor%20and%20tech%20investor%20will%20be%20on%20stage.%20Join%20Bits%20%26%20Pretzels%20and%20don%E2%80%99t%20miss%20THE%20chance%20that%20Kevin%20will%20come%20to%20Germany!&source=https%3A%2F%2Fwww.bitsandpretzels.com%2Fspacey" target="_blank" title="Share on LinkedIn">
                                  <img alt="Share on LinkedIn" src="src/images/icons_share/LinkedIn.png">
                              </a>
                          </li>
                          <li>
                              <a href="mailto:?subject=Kevin%20Spacey%20holds%20opening%20keynote%20at%20Bits%20%26%20Pretzels%20%2F%2F%20Meet%20him%20in%20Munich!&body=The%20Oscar-winning%20actor%20and%20tech%20investor%20will%20be%20on%20stage.%20Join%20Bits%20%26%20Pretzels%20and%20don%E2%80%99t%20miss%20THE%20chance%20that%20Kevin%20will%20come%20to%20Germany!:%20https%3A%2F%2Fwww.bitsandpretzels.com%2Fspacey" target="_blank" title="Send email">
                                  <img alt="Send email" src="src/images/icons_share/Email.png">
                              </a>
                          </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<?php include realpath('layout/signup-form.php');?>
<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>