<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<?php include_once realpath('layout/_inner_pages_main_nav.php');?>


 <div id="sub_nav" class="sub_nav_wrap">
     <div class="wrap_subnav_inner">
         <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
     </div>
 </div>

  
<div class="inner_sections">
<section class="page-newsletter">
    <div class="container">
        <div class="row clearfix">
            <div class="six columns offset-by-three" style="background:rgba(255, 255, 255, 0.85);  padding: 35px 25px 25px 25px; border-radius:10px; ">
            <p class="descr_small_margin" style="line-height:23px; font-size:1em;text-align:center; color:#005690">
                Sign up for our newsletter and be amongst the first ones to get notified before the official start of the next ticket round!
            </p>
            <div class="eight columns offset-by-two">
             <!-- Begin MailChimp Signup Form -->
                 <div id="mc_embed_signup">
                     <form action="//bitsandpretzels.us8.list-manage.com/subscribe/post?u=59db0da2c55155bbedfa0c507&amp;id=445dc804cf" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                         <div id="mc_embed_signup_scroll">
                             <div class="mc-field-group" style="margin-top:20px; width:100%">
                                 <input class="u-full-width" style="border:none; border-bottom:1px solid #ea5c3f !important; color:#005690 !important;font-size:1em; width:100%; font-family:Raleway, sans-serif; text-align:center; background:none" type="email" value="" placeholder="Email Address"name="EMAIL" class="required email" id="mce-EMAIL">
                             </div>
                             <input type="hidden" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>" name="IPADRESS">
                             <input type="hidden" value="Buy Ticket Button" name="SOURCE">
                             <div id="mce-responses" class="clear">
                                 <div class="response" id="mce-error-response" style="display:none"></div>
                                 <div class="response" id="mce-success-response" style="display:none"></div>
                             </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                             <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_59db0da2c55155bbedfa0c507_3c5efc9e7b" tabindex="-1" value=""></div>

                             <div class="clear" style="margin-top:20px; width:100%">
                                 <input style="cursor:pointer; width:100%; border-radius: 2.5em;height: 47px;background:#ea5c3f" type="submit" value="SIGN ME UP!" name="subscribe" id="mc-embedded-subscribe" class="button">
                             </div>
                         </div>
                     </form>
                 </div>
             <!--End mc_embed_signup-->
             
            </div>
                    
                    
            </div>
        </div>
    </div>
</section>
  <!-- START: content section -->
   <section id="content" class="section section_users white active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="padding-top:40px">
       <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center" style="margin-bottom:30px; margin-top:20px">Uniting the startup world 
during the Oktoberfest.</h1>
                            <div class="descr descr_small_margin">
                                <div class="descr" style="margin-bottom:0; margin-top:15px;">
                                <!-- Calendar X-->
                                     <div class="calx-sub-btn" style="height:44px;display:inline-block;" data-calendar="wq1291" data-configure="true" data-title="24.%20%E2%80%93%2026.%20September%202017" data-counter="false" data-icon="calendar" data-dimming="white" data-theme="2"></div>
                                </div>
                                <script>
                                    // Calendar X Function, activated on first scroll
                                    (function(){var e = document.createElement("script");e.type = "text/javascript";e.async = true;e.src = "https://calendarx.com/js/sub.btn.init.js";e.className = "calendarx-script";document.getElementsByTagName("body")[0].appendChild(e);})();
                                </script>
                                <div class="descr descr_small_margin" style="margin-top:40px">
                                        Once a year the most influential executives gather at the Bits &amp; Pretzels 
                                        Oktoberfest tables. Every table at the grand finale is hosted by a Table 
                                        Captain and all attendees can choose one of those handpicked CEOs, editors-in-chief 
                                        or investors before the event even starts. Through that we make sure that everyone 
                                        gets the most out of Bits &amp; Pretzels and enjoys the best networking experience in a relaxed atmosphere.
                                    </div>
                        <img class="desktop_only" src="/src/images/2016/agenda_preview.png" alt="timeline" style="width:100%; max-width:518px; margin:0 auto; padding: 45px 0 20px 0;">
                        <img class="mobile_only" src="/src/images/2016/agenda_preview.png" alt="timeline" style="width:100%; max-width:518px; margin:0 auto; padding: 50px 0 20px 0;">
                            </div>
                            <div style="margin-top:25px" class="link_box center">
                                <a href="https://youtu.be/Xu4b5nEeUIc" alt="play" data-lity class="link">Watch 2016 Highlight Movie<span class="icon icon-arrow_right"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
</div>

<style>
    .page-newsletter {
        background:url('/src/images/2016/welcome_hero-blue.png') no-repeat;
        background-size:cover;
        background-position:left center;
        padding:15vh 0 10vh 0;
        min-height:60vh;
    }
    
</style>



<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>
