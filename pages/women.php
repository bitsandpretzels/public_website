<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>


<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>

    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->
    <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section desktop_only"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section"><span class="mobile_hidden">Women in Tech</span></div>
            <ul class="sub_nav ul-reset">
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                    </li>            
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                    </li>
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                    </li>
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                    </li>
                    <li class="hidden_li" style="opacity:0">
                        <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                    </li>
                    <li>
                        <a class="link_nav buyticket" data-section="buyticket" href="https://form.jotformeu.com/70953039011348"><?php echo Meta::getButtonWording(); ?></a>
                    </li>
                </ul>
        </div>
    </div>

    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-women-in-tech active section_1">

        <div class="content_section center">
            <div class="wrapper">
                <div class="info_text_top">
                    <h1 class="top_title" style="text-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);">Women in Tech</h1>
                    <!--
                    <h2 class="main_title">Showcase your innovation and get inspired by others.<span class="stay_block">Be part of the international startup exhibition.</span></h2>  
                    -->        
                </div>
            </div>
        </div>
    </section><!-- section -->

    <section class="section white active section_1 section_pt" style="padding-bottom: 30px" id="content">

        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle" style="vertical-align: 0 !important;">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">Attention everyone! Mind the gender gap!</h1>
                            <div class="descr center">
                                Anyone working in the startup world knows intuitively that women are not well represented in founder roles or technical positions. There are countless brilliant female founders in the international startup ecosystem but they are still underrepresented in the public awareness.
                                <br>
                                <br>
                               This is something we'd like to change! Despite our decision not to grow bigger and to once again limit the total number of tickets to 5,000, we didn't want to reduce the number of tickets for our Women in Tech Special. It is extremely important to us to push awareness even further and are therefore happy to announce that...
                                <br>
                            </div>
                              <div class="descr center"><p style="color:#005690; font-size:1em">We're giving away 300 free tickets for aspiring women with game-changing ideas to join Bits &amp; Pretzels 2017!</p></div>
                              <div class="descr center"><p style="color:red; font-size:1.5em">Sorry, the raffle has ended. Congratulations to the lucky winners, who will be contacted within the next weeks.</p></div>
                        </div>                  
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
    
    <!--
   <section class="section section_wbox active section_1 section_pt" data-show-inner="false" data-show-footer="false" >
        <a name="apply"></a>

        <div class="content_section" >
            <div class="wrapper_inner center" style="background-image: url(/src/images/women_in_tech_signup_2.png); background-position: center center;">
                <div class="valign">
                    <div class="middle" style="background: rgba(255, 255, 255, 0);">
                        <div style="background:rgba(255, 255, 255, 0); margin: auto; text-align: center !important; position: relative;">
                            

    <div class="four columns offset-by-four">
    <div class="form-all" style="text-align:center !important;"> 
        <ul class="form-section page-section"; style="background: rgb(255, 255, 255, 0) !important"> 
            <li class="form-line" id="id_10"> 
                <div id="cid_10" class="form-input-wide"> 
                    <div style="text-align:center; padding-top:15px;" class="form-buttons-wrapper"> 
                       
                        <h1 class="main_title center"><p style="color:#FFFFFF">Want to be part of it?</p></h1>
                        <div class="descr center"><p style="color:#FFFFFF;">You've already bought a ticket? Don't worry, you can still take part and transfer your ticket to a friend or colleague.</p></div>
                    </div> 
                       <div class="form-buttons-wrapper">
                        <form action="https://form.jotformeu.com/70953039011348" method="post" target="_blank">
                            <button style="font-family:raleway, sans-serif; color:#FFFFFF; cursor:pointer; border-radius: 4px; border-color:#ea5c3f; background:#ea5c3f; width:100%; height: 47px;">Apply now</button>
                        </form>
                        </div>
                </div>
            </li> 
            <li class="form-line" data-type="control_divider" id="id_4"> 
                <div id="cid_4" class="form-input-wide"> 
                    <div data-component="divider" style="border-bottom:1px solid transparent;height:1px;margin-left:0px;margin-right:0px;margin-top:5px;margin-bottom:5px;"> </div> 
                </div>
                </li> 
            <li class="form-line" data-type="control_divider" id="id_5"> 
            <div id="cid_5" class="form-input-wide"> 
                <div data-component="divider" style="border-bottom:1px solid transparent;height:1px;margin-left:0px;margin-right:0px;margin-top:5px;margin-bottom:5px;"> </div> 
            </div> 
        </ul> 
    </div> 
    </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->


<!--  -->
<section class="section white active section_1 section_pt" style="padding-bottom: 30px" id="content">

        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle" style="vertical-align: 0 !important;">
   
                            
                            <div class="grid grid-2 grid-2-simple clearfix container_info" style="padding-top: 0px">
                                <div class="col quote quote-1">
                                    <div class="info_box">                                                         
                                        <h1 class="main_title">“Bits &amp; Pretzels definitely brings interesting people from different businesses together. It's a great opportunity for getting new insights, networking and discussing new trends and challenges.”</h1>
                                        <div class="list_users clearfix">
                                            <div class="user_box" style="float:none;margin: 0 auto">
                                                <div class="user-offset">
                                                    <div class="img_user"><img src="/src/images/testimonials/default/schwetje.png" alt=""></div>
                                                    <p class="name_user">Sonja Schwetje</p>
                                                    <p>Editor in Chief</p>
                                                    <p>n-tv</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col quote">
                                     <div class="info_box">                                                         
                                        <h1 class="main_title">“A unique conference that really blends technology, heritage and the huge Oktoberfest successfully to an event that goes far beyond the usual networking conference.”</h1>

                                        <div class="list_users clearfix">
                                            <div class="user_box" style="float:none;margin: 0 auto">
                                                <div class="user-offset">
                                                    <div class="img_user"><img src="/src/images/testimonials/default/rein.png" alt=""></div>
                                                    <p class="name_user">Raffaela Rein</p>
                                                    <p>Founder and CEO</p>
                                                    <p>CareerFoundry</p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div> 
                            
                        </div>                  
                    </div>
                </div>
            </div>
    </section><!-- section -->

<section id="partners" class="section section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="padding-top: 0px;padding-bottom: 30px;min-height: 0px">

        <div class="content_section center" style="min-height: 0px">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
            
                        <div class="wrapp_main_text">
                            <h1 class="main_title center" style="margin:0px">Supporters</h1>                                                 
                        </div>  
       

                        <div class="list_logos logos-grid-5 mb0 clearfix" style="margin-top:30px">


                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><a href="http://bloomingfounders.com/" target="_blank"><img src="/src/images/logos/women/blooming_founders_logo-small.png" alt=""></a></div>
                                </div>
                            </div>
                            <!--
                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><a href="http://www.fempreneur.de/" target="_blank"><img src="/src/images/logos/women/fempreneur_logo.png" alt=""></a></div>
                                </div>
                            </div> -->
                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><a href="http://www.berlinstartupgirl.com/" target="_blank"><img src="/src/images/logos/women/bsg.png" alt=""></a></div>
                                </div>
                            </div> 
                   
                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><a href="https://www.femtrepreneur.co/blog/" target="_blank"><img src="/src/images/logos/women/femtrepreneur.png" alt=""></a></div>
                                </div>
                            </div>
                            <!--
                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><a href="http://www.womenstartuplab.com/" target="_blank"><img src="/src/images/logos/women/womens_startup_lab.png" alt=""></a></div>
                                </div>
                            </div>-->

                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><a href="https://www.she-works.de/" target="_blank"><img src="/src/images/logos/women/sheworks.png" alt=""></a></div>
                                </div>
                            </div>

<!--
                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><img src="/src/images/logos/women/ff.png" alt=""></div>
                                </div>
                            </div>        -->


                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><a href="http://notanotherwomanmag.com/" target="_blank"><img src="/src/images/logos/women/not_another_woman_magazine.png" alt=""></a></div>
                                </div>
                            </div>

                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><a href="https://www.globalinvesther.com/" target="_blank"><img src="/src/images/logos/women/global_invest_her.png" alt=""></a></div>
                                </div>
                            </div>
                            
                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><a href="https://www.femalefounders.at/" target="_blank"><img src="/src/images/logos/women/female_founders.png" alt=""></a></div>
                                </div>
                            </div>
                            
                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><a href="http://www.women-speaker-foundation.de/" target="_blank"><img src="/src/images/logos/women/women_speaker_foundation.png" alt=""></a></div>
                                </div>
                            </div>
                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><a href="http://thenextwomen.com/summit" target="_blank"><img src="/src/images/logos/women/next_women.png" alt=""></a></div>
                                </div>
                            </div>
                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><a href="http://wefound.org/" target="_blank"><img src="/src/images/logos/women/wefound.png" alt=""></a></div>
                                </div>
                            </div>
                            <div class="logo_box quarter">
                                <div class="valign">
                                    <div class="middle"><a href="https://editionf.com/" target="_blank"><img src="/src/images/logos/women/edition_f.png" alt=""></a></div>
                                </div>
                            </div>
                       
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
    
</div>

<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>

