<?php
// local fix to match my vhost settings
$pathPrefix = '';
$host = $_SERVER['HTTP_HOST'];
if ($host == 'bap.local') {
  $pathPrefix = '../';
}
include_once realpath($pathPrefix.'layout/top.php');
include_once realpath($pathPrefix.'layout/header.php');
?>

<div class="inner_sections">

    <?php include_once realpath($pathPrefix.'layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>

    <!--
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

    <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section mobile_hidden">Team</div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>            
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#agenda" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                </li>
                <li>
                    <a class="link_nav buyticket trigger_app_nav" data-section="buyticket"><?php echo Meta::getButtonWording(); ?></a>
                </li>
            </ul>
        </div>
    </div>

    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-team active section_1">

        <div class="content_section center">
            <div class="wrapper">
                <div class="info_text_top">
                    <h1 class="top_title" style="text-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);">The Bits &amp; Pretzels Team</h1>
                    <!--
                    <h2 class="main_title">We are the pretzels.</h2>
                    -->
                </div>
            </div>
        </div>
    </section><!-- section -->

    <section class="section white active section_1 section_pt section_pt_team">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">We are the Pretzels and this is our story.</h1>
                            <div class="descr descr_small_margin center">A good breakfast is the perfect way to start the day. Apparently the same applies to startups. But it wasn’t until 2014 that this correlation became known. At first Andreas and Bernd just invited fellow founders to a traditional breakfast with Bavarian Weisswurst, a cosy get-together of Bavarian startups from founders for founders. Felix joined as the third host and together our dream-team - our three hosts Andi, Bernd and Felix - started looking for a new setting in order to take the unique concept to the next level. Big enough to meet the growing demand. Traditional enough to keep the treasured flair. And they found: The Oktoberfest.
                            <br>
                            <br>
                            Their endurance paid off: Bits &amp; Pretzels 2015 was a smashing success and for the first time brought over 3600 startup founders and investors to Munich &amp; Oktoberfest for three days of networking, inspiration and fun.
                            <br>
                            <br>
                            Since then our team and many valued volunteers worked hard to make Bits &amp; Pretzels the biggest founder’s festival in Central Europe. Welcoming thousands of founders every year with the familiar atmosphere and new improvements like the convenient networking app. Always combining tradition and innovation. Resulting in a unique experience for founders from founders.
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->

    <section class="section silver section_team white section_1 section_pt">
        <div class="container list_users clearfix" style="max-width:594px">
            <div class="user_box">
                <div class="user-offset">
                    <div class="img_user"><img src="/src/images/team/Andi.png" alt=""></div>
                    <p class="name_user">Andreas <br> Bruckschlögl</p>
                    <div class="title">Co-Founder &amp; Host <br>(Event)</div>
                    <ul class="share-buttons share-buttons-small">
                        <li>
                            <a href="https://www.linkedin.com/in/bruckschloegl" target="_blank" title="LinkedIn Profile">
                                <img alt="LinkedIn Profile" src="src/images/icons_share/LinkedIn.png">
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/andreas.bruckschloegl" title="Facebook profile" target="_blank">
                                <img alt="Facebook Profile" src="src/images/icons_share/Facebook.png">
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/andiarbeit_" target="_blank" title="Twitter profile">
                                <img alt="Twitter Profile" src="src/images/icons_share/Twitter.png">
                            </a>
                        </li>
                        <li>
                            <a href="mailto:andi@bitsandpretzels.com" title="Send email">
                                <img alt="Send email" src="src/images/icons_share/Email.png">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="user_box">
                <div class="user-offset">
                    <div class="img_user"><img src="/src/images/team/Bernd.png" alt=""></div>
                    <p class="name_user">Dr. Bernd Storm <br> vans Gravesande</p>
                    <div class="title">Co-Founder &amp; Host <br>(Partners)</div>
                    <ul class="share-buttons share-buttons-small">
                        <li>
                            <a href="https://www.linkedin.com/in/berndstorm/" target="_blank" title="LinkedIn Profile">
                                <img alt="LinkedIn Profile" src="src/images/icons_share/LinkedIn.png">
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/bernd.storm" title="Facebook profile" target="_blank">
                                <img alt="Facebook Profile" src="src/images/icons_share/Facebook.png">
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/BerndStorm" target="_blank" title="Twitter profile">
                                <img alt="Twitter Profile" src="src/images/icons_share/Twitter.png">
                            </a>
                        </li>
                        <li>
                            <a href="mailto:bernd@bitsandpretzels.com" title="Send email">
                                <img alt="Send email" src="src/images/icons_share/Email.png">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="user_box">
                <div class="user-offset">
                    <div class="img_user"><img src="/src/images/team/Felix.png" alt=""></div>
                    <p class="name_user">Felix <br>Haas</p>
                    <div class="title">Chairman &amp; Host <br>(Speakers)</div>
                    <ul class="share-buttons share-buttons-small">
                        <li>
                        <a href="https://www.linkedin.com/in/felixhaas/" target="_blank" title="LinkedIn Profile">
                        <img alt="LinkedIn Profile" src="src/images/icons_share/LinkedIn.png">
                        </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/felixhaas" title="Facebook profile" target="_blank">
                                <img alt="Facebook Profile" src="src/images/icons_share/Facebook.png">
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/felixhaas" target="_blank" title="Twitter profile">
                                <img alt="Twitter Profile" src="src/images/icons_share/Twitter.png">
                            </a>
                        </li>
                        <li>
                            <a href="mailto:felix@bitsandpretzels.com" title="Send email">
                                <img alt="Send email" src="src/images/icons_share/Email.png">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>   
            <div class="user_box">
                <div class="user-offset">
                    <div class="img_user"><img src="/src/images/team/Kira.png" alt=""></div>
                    <p class="name_user">Kira <br> Schilling</p>
                    <div class="title">Eventmanager</div>
                    <ul class="share-buttons share-buttons-small">
                        <li>
                        <a href="https://www.linkedin.com/in/kiraschilling/en" target="_blank" title="LinkedIn Profile">
                        <img alt="LinkedIn Profile" src="src/images/icons_share/LinkedIn.png">
                        </a>
                        </li>
                        <li>
                        <a href="mailto:kira@bitsandpretzels.com" title="Send email">
                        <img alt="Send email" src="src/images/icons_share/Email.png">
                        </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="user_box">
                <div class="user-offset">
                <div class="img_user"><img src="/src/images/team/Christian.png" alt=""></div>
                    <p class="name_user">Christian <br> Lohmeier</p>
                    <div class="title">Eventmanager</div>
                    <ul class="share-buttons share-buttons-small">
                        <li>
                        <a href="https://www.linkedin.com/in/christianlohmeier/" target="_blank" title="LinkedIn Profile">
                        <img alt="LinkedIn Profile" src="src/images/icons_share/LinkedIn.png">
                        </a>
                        </li>
                        <li>
                        <a href="https://www.facebook.com/christian.lohmeier.777" title="Facebook profile" target="_blank">
                        <img alt="Facebook Profile" src="src/images/icons_share/Facebook.png">
                        </a>
                        </li>
                        <li>
                        <a href="mailto:christian@bitsandpretzels.com" title="Send email">
                        <img alt="Send email" src="src/images/icons_share/Email.png">
                        </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="user_box">
                <div class="user-offset">
                    <div class="img_user"><img src="/src/images/team/Flo.png" alt=""></div>
                    <p class="name_user">Florian <br> Melzer</p>
                    <div class="title">Eventmanager</div>
                    <ul class="share-buttons share-buttons-small">
                        <li>
                        <a href="https://www.xing.com/profile/Florian_Melzer4" target="_blank" title="Xing Profile">
                        <img alt="Xing Profile" src="src/images/icons_share/xing.png">
                        </a>
                        </li>
                        <li>
                        <a href="mailto:florian.ext@bitsandpretzels.com" title="Send email">
                        <img alt="Send email" src="src/images/icons_share/Email.png">
                        </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="user_box">
                <div class="user-offset">
                    <div class="img_user"><img src="/src/images/team/Magdalena.png" alt=""></div>
                    <p class="name_user">Magdalena <br> Rogl</p>
                    <div class="title">Social Media</div>
                    <ul class="share-buttons share-buttons-small">
                        <li>
                            <a href="https://www.linkedin.com/in/magdalenarogl" target="_blank" title="LinkedIn Profile">
                                <img alt="LinkedIn Profile" src="src/images/icons_share/LinkedIn.png">
                            </a>
                        </li>
                        <li>
                        <a href="https://www.facebook.com/lenarogl" title="Facebook profile" target="_blank">
                        <img alt="Facebook Profile" src="src/images/icons_share/Facebook.png">
                        </a>
                        </li>
                        <li>
                        <a href="https://twitter.com/LenaRogl" target="_blank" title="Twitter profile">
                        <img alt="Twitter Profile" src="src/images/icons_share/Twitter.png">
                        </a>
                        </li>
                        <li>
                        <a href="mailto:magdalenaantoniarogl@gmail.com" title="Send email">
                        <img alt="Send email" src="src/images/icons_share/Email.png">
                        </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

</div>
<style>
    .section_team .list_users .user_box {
        width:33%;
    }
</style>

<?php
 #include realpath('layout/signup-form.php');
  include realpath($pathPrefix.'layout/footer.php');
  include realpath($pathPrefix.'layout/bottom.php');
