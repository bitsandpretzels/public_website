<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div data-index="buy" id="buy_section" class="inner_sections">
   
   <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
   
   
    <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
        </div>
    </div>
    <section class="section section_buy section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="promotional_code">
                    <div class="booking" style="margin-bottom:80px">
                        <div class="main_title">TICKET SALE WILL END IN</div>
                        <table style="width:60%; margin:0 auto">
                            <tr id="countdown"></tr>
                            <tr>
                                <td class="data" style="font-size:.9em">DAYS</td>
                                <td class="data" style="font-size:.9em">HOURS</td>
                                <td class="data" style="font-size:.9em">MINUTES</td> 
                                <td class="data" style="font-size:.9em">SECONDS</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="wrap_buy">
                    <div class="wrap_buy-1">
                        <div class="grid grid-1 clearfix">    
                            <div class="grid-offset">
                                <div class="start_up_apply white" style="width:100%; max-width:417px;">
                                    <div class="blue_box">
                                        <h2 class="st_heading">Academia</h2>
                                        <p class="sub_heading">University employees and employees of public organizations.</p>
                                    </div>
                                    <div class="sub_links sub_links_buy sub_links-dark">
                                        <ul class="feature-list">
                                            <li>3-Day Festival Pass</li>
                                            <li>Oktoberfest</li>
                                            <li>Access to Workshops</li>
                                            <li>After party</li>
                                            <li>Networking Tool</li>
                                            <li>Journalists Matchmaking</li>
                                            <li>Young Talents Matchmaking</li>
                                            <li>Corporate Matchmaking</li>
                                            <li>Investor Matchmaking</li>
                                        </ul>
                                    </div>
                                    <div class="silver main_price">
                                        <h3 class="h3">Lazy bird</h3>
                                        <span><span class="euro">€</span>599</span>
                                        <?php include realpath('layout/elements/limited_tickets_available.php');?>
                                    </div>
<!--
                                    <div class="other_prices">
                                        <p class="other_prices-wrap">
                                           Lazy bird<span><span class="euro">€</span>599</span> 
                                        </p>
                                        <p class="other_prices-wrap">
                                           Super lazy bird<span><span class="euro">€</span>799</span> 
                                        </p>
                                    </div>
-->
                                    <a href="<?php echo Router::getEventbriteRoute('academia'); ?>" class="button orange mx-width"><span>Order Now</span></a>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="vat"><div class="link_box"><a id="promotional_link" href="" class="link link_no_icon bold">Enter Promotional Code</a></div></div>         
                    <div id="promotional_code" class="white promotional_code dsp-none">
                        <form action="" onsubmit="window.location.href='https://www.eventbrite.com/e/bits-pretzels-2016-tickets-19797638269?discount=' + $('#code').val();return false;">
                            <input type="text" id="code" placeholder="Enter promotional code"> <button type="submit" class="button blue">Apply</button>
                        </form>
                        
                    </div>
                    <div class="vat">
                        All prices excl. VAT - One ticket per person
                    </div>

<!--
                    <div class="grey promotional_code">
                        <p class="booking">Last booking was made <span class="data" id="lastEventbriteBooking">loading... </span><span class="data">h</span> ago</p>
                    </div>                    
-->
<!--
                <div class="grey promotional_code">
                    <div class="booking">
                        <div class="main_title">WE CAN GUARANTEE SUNNY BIRD TICKETS ONLY FOR</div>
                        <table style="width:60%; margin:0 auto">
                            <tr id="second-countdown"></tr>
                            <tr>
                                <td class="data" style="font-size:.9em">HOURS</td>
                                <td class="data" style="font-size:.9em">MINUTES</td> 
                                <td class="data" style="font-size:.9em">SECONDS</td>
                            </tr>
                        </table>
                    </div>
                </div> 
-->
                    
                </div>

            </div>
        </div>
        <div class="popups_container">
            <div class="popup popup_desktop" style="visibility: hidden">
                <span class="close_popup"></span>
                <p>
                    <!-- pull data from Google Analytics API -->
                    <span class="data"><?php echo Faker::views('startup'); ?></span> people currently viewing this page
                </p>
            </div>
            <div class="popup popup_desktop" style="visibility: hidden;">
                <span class="close_popup"></span>
                <p>
                    Last booking was from <br><span class="data"><?php echo CountryNamer::getCountryNameFromIP();  ?></span>
                </p>
            </div>            
        </div>
    </section><!-- section -->

</div>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>
