<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div data-index="buy" id="buy_section" class="inner_sections">
   
   <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
   
   
    <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
        </div>
    </div>
    

    
    <section class="section section_buy section_1 section_pt" data-show-inner="false" data-show-footer="false">

        <div class="content_section center">
            <div class="wrapper_inner">
<!--
                <div class="promotional_code">
                    <div class="booking" style="margin-bottom:80px">
                        <div class="main_title">TICKET SALE WILL END IN</div>
                        <table style="width:60%; margin:0 auto">
                            <tr id="countdown"></tr>
                            <tr>
                                <td class="data" style="font-size:.9em">DAYS</td>
                                <td class="data" style="font-size:.9em">HOURS</td>
                                <td class="data" style="font-size:.9em">MINUTES</td> 
                                <td class="data" style="font-size:.9em">SECONDS</td>
                            </tr>
                        </table>
                    </div>
                </div> 
-->
<!--
                <div class="wrapp_main_text">
                    <h1 class="main_title">"One of the most important start-up events in Europe"</h1>
                </div>           

                <div class="list_users clearfix">
                    <div class="user_box">
                        <div class="user-offset">
                            <div class="img_user"><img src="/src/images/testimonials/default/schneider.png" alt=""></div>
                            <p class="name_user">Simon Schneider</p>
                            <p>Investment Director</p>
                            <p>High-Tech Gründerfonds</p>
                        </div>
                    </div>
                </div>
-->

                <div class="wrap_buy wrap_buy-3">
<!--                <div class="fb-like" data-href="https://www.facebook.com/bitsandpretzels" data-layout="standard" data-action="like" data-show-faces="true" data-share="false" style="margin-bottom:20px; text-align:left"></div>-->
                    <div class="grid grid-2 grid-2-simple clearfix">
                        <div class="col col-1">
                            <div class="grid-offset">
                                <div class="start_up_apply white" style="width:100%; max-width:417px;">
                                    <div class="blue_box">
                                        <h2 class="st_heading">Private Investor</h2>
                                        <br>
                                        <p class="sub_heading">Business Angel</p>
                                    </div>
                                    <div class="sub_links sub_links_buy sub_links-dark">
                                        <ul class="feature-list">
                                            <li>3-Day Festival Pass</li>
                                            <li>Oktoberfest</li>
                                            <li>Access to Workshops</li>
                                            <li>After party</li>
                                            <li>Networking Tool</li>
                                            <li>Startup Matchmaking</li>
                                            <li>Startup Data</li>
                                            <li>Host Office Hours</li>
                                        </ul>
                                    </div>
                                    <div class="silver main_price">
                                        <h3 class="h3">Lazy bird</h3>
                                        <span><span class="euro">€</span>999</span>
                                        <?php include realpath('layout/elements/limited_tickets_available.php');?>
                                    </div>
                                    <div class="other_prices">
                                        <p class="other_prices-wrap">
                                           Lazy bird<span><span class="euro">€</span>999</span> 
                                        </p>
<!--
                                        <p class="other_prices-wrap">
                                           Super lazy bird<span><span class="euro">€</span>1.299</span> 
                                        </p>
-->
                                    </div>
                                    <a href="<?php echo Router::getEventbriteRoute('investor_private'); ?>" class="button orange mx-width"><span>Order Now</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="col col-2">
                            <div class="grid-offset">
                                <div class="start_up_apply white" style="width:100%; max-width:417px;">
                                    <div class="blue_box blue_box-dark">
                                        <h2 class="st_heading">Institutional Investor</h2>
                                        <br>
                                        <p class="sub_heading">Venture Capital <span class="block">Private Equity</span> <span class="block">Accelerator</span></p>
                                    </div>
                                    <div class="sub_links sub_links_buy sub_links-darker">
                                        <ul class="feature-list">
                                            <li>3-Day Festival Pass</li>
                                            <li>Oktoberfest</li>
                                            <li>Access to Workshops</li>
                                            <li>After party</li>
                                            <li>Networking Tool</li>
                                            <li>Startup Matchmaking</li>
                                            <li>Startup Data</li>
                                            <li>Host Office Hours</li>
                                        </ul>
                                    </div>
                                    <div class="silver main_price">
                                        <h3 class="h3">Lazy bird</h3>
                                        <span><span class="euro">€</span>1199</span>
                                        <?php include realpath('layout/elements/limited_tickets_available.php');?>
                                    </div>
                                    <div class="other_prices">
                                        <p class="other_prices-wrap">
                                           Lazy bird<span><span class="euro">€</span>1199</span> 
                                        </p>
<!--
                                        <p class="other_prices-wrap">
                                           Super lazy bird<span><span class="euro">€</span>1.399</span> 
                                        </p>
-->
                                    </div>
                                    <a href="<?php echo Router::getEventbriteRoute('investor_institutional'); ?>" class="button orange mx-width"><span>Order Now</span></a>
                                </div>
                            </div>
                        </div>
<!--
                        <div class="col col-3">
                            <div class="grid-offset">
                                <div class="start_up_apply white">
                                    <div class="blue_box blue_box-third">
                                        <h2 class="st_heading">Investor Premium</h2>
                                        <br>
                                        <p class="sub_heading invert fw-normal">All Investors</p>
                                    </div>
                                    <div class="sub_links sub_links_buy sub_links-darkest">
                                        <ul class="feature-list">
                                            <li>3-Day Festival Pass</li>
                                            <li>Oktoberfest</li>
                                            <li>Access to Workshops</li>
                                            <li>After party</li>
                                            <li>Networking Tool</li>
                                            <li>Startup Matchmaking</li>
                                            <li>Startup Data</li>
                                            <li>Host Office Hours</li>
                                            <li>Airport Shuttle</li>
                                            <li>Name Badge sent by Mail</li>
                                            <li>Bits &amp; Pretzels Fan Package</li>
                                        </ul>                                    
                                    </div>
                                    <div class="silver main_price">
                                        <h3 class="h3">Sunny bird</h3>
                                        <span>1.299€</span>
                                        <div style="height:28px"> </div>
                                    </div>
                                    <div class="other_prices">
                                        <p class="other_prices-wrap">
                                           Lazy bird<span><span class="euro">€</span>1.399</span> 
                                        </p>
                                        <p class="other_prices-wrap">
                                           Super lazy bird<span><span class="euro">€</span>1.599</span> 
                                        </p>
                                    </div>
                                    <a href="<?php echo Router::getEventbriteRoute('investor'); ?>" class="button orange mx-width"><span>Order Now</span></a>
                                </div>
                            </div>
                        </div>
-->
                    </div> 
                    <div class="vat"><div class="link_box"><a id="promotional_link" href="" class="link link_no_icon bold">Enter Promotional Code</a></div></div>         
                    <div id="promotional_code" class="white promotional_code dsp-none">
                        <form action="" onsubmit="window.location.href='https://www.eventbrite.com/e/bits-pretzels-2016-tickets-19797638269?discount=' + $('#code').val();return false;">
                            <input type="text" id="code" placeholder="Enter promotional code"> <button type="submit" class="button blue">Apply</button>
                        </form>
                        
                    </div>
<!--
                    <div class="grey promotional_code">
                    <div class="booking">
                        <div class="main_title">WE CAN GUARANTEE SUNNY BIRD TICKETS ONLY FOR</div>
                        <table style="width:60%; margin:0 auto">
                            <tr id="second-countdown"></tr>
                            <tr>
                                <td class="data" style="font-size:.9em">HOURS</td>
                                <td class="data" style="font-size:.9em">MINUTES</td> 
                                <td class="data" style="font-size:.9em">SECONDS</td>
                            </tr>
                        </table>
                    </div>
                    </div> 
-->
                    <div class="vat">
                        All prices excl. VAT - One ticket per person
                    </div>

<!--
                    <div class="grey promotional_code">
                        <p class="booking">Last booking was made <span class="data" id="lastEventbriteBooking">loading... </span><span class="data">h</span> ago</p>
                    </div>
-->
                </div>

            </div>
        </div>
        <div class="popups_container">
            <div class="popup popup_desktop" style="visibility: hidden">
                <!--<span class="close_popup"></span>-->
                <p>
                    <!-- pull data from Google Analytics API -->
                    <span class="data"><?php echo Faker::views('startup'); ?></span> people currently viewing this page
                </p>
            </div>
            <div class="popup popup_desktop" style="visibility: hidden">
                <!--<span class="close_popup"></span>-->
                <p>
                    Last booking was from <br><span class="data"><?php echo CountryNamer::getCountryNameFromIP();  ?></span>
                </p>
            </div>            
        </div>
    </section><!-- section -->

</div>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>

