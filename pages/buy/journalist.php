<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once realpath('layout/_scroll_down_hint.php');?>

    <!--
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

  <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg'), center, center; background-size:cover;"></div></a>
        </div>
    </div>
    
    <section id="apply" class="section section_wbox accredit_section acredit active section_1 section_pt" style="position: relative;top: 30px; background:url('/src/images/press.jpg');">

        <div class="content_section">
            <div class="wrapper_inner center">
                <div class="valign">
                    <div class="middle">
                    
                        
                        <style>
                            .p_headline{
                              font-size: 32px;
                                padding-bottom: 10px;
                                font-weight: 400;
                            }
                        
                        </style>
                        
                        <div class="col" align="center">
                            
                           <div class="start_up_apply start_up_apply-big">
                            <div class="blue_box blue_box-darkest">
                                <h1 class="p_headline">Apply for a free press ticket</h1>
                                    <li><a style="text-decoration:underline;" target="_blank" href="https://form.jotformeu.com/70304099921354">click here</a></li>
                            </div>
                            </div>
                               
                               <div class="start_up_apply start_up_apply-big">
                                <div class="blue_box blue_box-darkest">
                                    <h1 class="p_headline">Press kit</h1>
                                    <li>For pictures, texts and logo <br> <a style="text-decoration:underline;" target="_blank" href="https://company-33706.frontify.com/d/hNUjQcstq3kR/bits-pretzels-2016-style-guide#/design-section/logo">click here</a>.</li>
                                </div>
                            </div>
                               
                               <div class="start_up_apply start_up_apply-big">
                                <div class="blue_box blue_box-darkest">
                                    <h1 class="p_headline">Interview Requests</h1>
                                    <li>If you have a request regarding an interview with our founders or speakers of #bits17, please send an email to <a style="text-decoration:underline;" target="_blank" href="mailto:press@bitsandpretzels.com">press@bitsandpretzels.com</a>.</li>
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </div>

    </section>
</div>


<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>
