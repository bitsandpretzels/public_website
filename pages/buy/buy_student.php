<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div data-index="buy" id="buy_section" class="inner_sections">
   
   <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
   
   
    <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
        </div>
    </div>
    

    
    <section class="section section_buy section_1 section_pt" data-show-inner="false" data-show-footer="false">

        <div class="content_section center" style="margin-top:23px">
            <div class="wrapper_inner">
                <div class="promotional_code">
                    <div class="booking" style="margin-bottom:80px">
                        <div class="main_title">TICKET SALE WILL END IN</div>
                        <table style="width:60%; margin:0 auto">
                            <tr id="countdown"></tr>
                            <tr>
                                <td class="data" style="font-size:.9em">DAYS</td>
                                <td class="data" style="font-size:.9em">HOURS</td>
                                <td class="data" style="font-size:.9em">MINUTES</td> 
                                <td class="data" style="font-size:.9em">SECONDS</td>
                            </tr>
                        </table>
                    </div>
                </div> 
<!--
                <div class="wrapp_main_text">
                    <h1 class="main_title">"Inspiring and motivating! I got loads of business cards, ideas and and a vision of how to go on with my startup in the next month."</h1>
                </div>           

                <div class="list_users clearfix">
                    <div class="user_box">
                        <div class="user-offset">
                            <div class="img_user"><img src="/src/images/testimonials/default/nolden.png" alt=""></div>
                            <p class="name_user">Nils Nolden</p>
                            <p>System Engineer</p>
                            <p>Freelancer</p>
                        </div>
                    </div>
                </div>
-->

                <div class="wrap_buy">
                    <div class="wrap_buy-1">
<!--                        <div class="fb-like" data-href="https://www.facebook.com/bitsandpretzels" data-layout="standard" data-action="like" data-show-faces="true" data-share="false" style="margin-bottom:20px; text-align:left"></div>-->
                        <div class="grid grid-1 clearfix">                         
                            <div class="start_up_apply white">
                                <div class="blue_box">
                                    <h2 class="st_heading">Student</h2>
                                    <p class="sub_heading">Students with a valid certificate of enrollment for September 2016</p>
                                </div>
                                <div class="sub_links sub_links_buy sub_links-dark">
                                    <ul class="feature-list">
                                        <li>3-Day Festival Pass</li>
                                        <li>Oktoberfest</li>
                                        <li>Access to Workshops</li>
                                        <li>After party</li>
                                        <li>Networking Tool</li>
                                        <li>Employer Matchmaking</li>
                                    </ul>
                                </div>
                                <div class="silver main_price">
                                    <h3 class="h3">Second Batch</h3>
                                    <span><span class="euro">€</span>149</span>
<!--                                    <?php include realpath('layout/elements/limited_tickets_available.php');?>-->
<!--                                    <div class="small_text alert_limited" style="position:static; text-shadow:none">Tickets will only be activated after <br>valid certificate of enrollment was sent to us!</div>-->
                                </div>
<!--
                                <div class="other_prices">
                                    <p class="other_prices-wrap">
                                       Second Batch<span><span class="euro">€</span>249</span> 
                                    </p>
                                    <p class="other_prices-wrap">
                                       Third Batch<span><span class="euro">€</span>349</span> 
                                    </p>
                                </div>
-->
<!--                                <a href="<?php echo Router::getEventbriteRoute('startup'); ?>" class="button orange mx-width"><span>Order Now</span></a>-->
                            </div>
                        </div> 
                    </div>
                     <div class="vat">
                        All prices incl. VAT - One ticket per person
                    </div>
                    <div class="white content_section">
                        <div class="wrapper_inner center">
                            <div class="valign">
                                <div class="middle">
                                    <div class="wbox mb60 mt60">
                                        <h1 class="main_title">Apply now!</h1>

                                            <form class="jotform-form" action="https://submit.jotformeu.com/submit/60284187128357/" method="post" enctype="multipart/form-data" name="form_60284187128357" id="60284187128357" accept-charset="utf-8" style="max-width: 550px;margin: 0 auto">

                                            <div class="field_wrap">
                                                <div class="label-col">
                                                    <label for="first_name" class="main_label">First Name:<span class="required">*</span></label>
                                                </div>
                                                <div class="field-col">
                                                    <input id="first_name" required name="q1_vollstandigerName1[first]" class="texfield_form" autocomplete="off" type="text">
                                                </div>
                                            </div>
                                            <div class="field_wrap">
                                                <div class="label-col">
                                                    <label for="last_name" class="main_label">Last Name:<span class="required">*</span></label>
                                                </div>
                                                <div class="field-col">
                                                    <input id="last_name" required name="q1_vollstandigerName1[last]" class="texfield_form" autocomplete="off" type="text">
                                                </div>
                                            </div>
                                            <div class="field_wrap">
                                                <div class="label-col">
                                                    <label for="email" class="main_label">Email Address:<span class="required">*</span></label>
                                                </div>
                                                <div class="field-col">
                                                    <input id="email" required name="q6_email6" class="texfield_form" autocomplete="off" type="email">
                                                </div>
                                            </div>

                                            <div class="field_wrap">
                                                <div class="label-col">
                                                    <label for="email" class="main_label">Phone Number:<span class="required">*</span></label>
                                                </div>
                                                <div class="field-col clearfix">
                                                    <input style="width:30%;float:left" placeholder="+049 Countrycode" id="email" required name="q4_phoneNumber[area]" class="texfield_form" autocomplete="off" type="text">
                                                    <input style="width:70%;float:left" placeholder="Phone number" id="email" required name="q4_phoneNumber[phone]" class="texfield_form" autocomplete="off" type="text">
                                                </div>
                                            </div>

                                            <div class="field_wrap">
                                                <div class="label-col">
                                                    <label for="email" class="main_label">LinkedIn/XING<span class="required">*</span></label>
                                                </div>
                                                <div class="field-col">
                                                    <input id="email" required name="q7_linkedin" class="texfield_form" autocomplete="off" type="text">
                                                </div>
                                            </div> 

                                            <div class="field_wrap">
                                                <div class="label-col">
                                                    <label for="email" class="main_label">University<span class="required">*</span></label>
                                                </div>
                                                <div class="field-col">
                                                    <input id="email" required name="q9_university" class="texfield_form" autocomplete="off" type="text">
                                                </div>
                                            </div>

                                            <div class="field_wrap clearfix">
                                                <div class="label-col">
                                                    <label for="email" class="main_label">Faculty<span class="required">*</span></label>
                                                </div>
                                                <div class="field-col clearfix">
                                                    <select name="q10_faculty" style="border:1px solid #BCBCBC;width:100%;padding:13px;border-radius:3px;text-indent: 5px;line-height: 26px;">    
                                                        <option value="Business & Economics">Business &amp; Economics</option>
                                                        <option value="Humanities">Humanities</option>
                                                        <option value="Engineering">Engineering</option>
                                                        <option value="Informatics">Informatics</option>
                                                    </select>
                                                </div>
                                            </div>      


                                            <div class="field_wrap clearfix">
                                                <div class="label-col">
                                                    <label for="email" class="main_label">Course of Studies<span class="required">*</span></label>
                                                    <br>
                                                </div>
                                                <div class="field-col">
                                                    <input id="email" required name="q11_courseOf" class="texfield_form" autocomplete="off" type="text">
                                                </div>
                                            </div>

                                            <div class="field_wrap clearfix">
                                                <div class="label-col">
                                                    <label for="email" class="main_label">Intended Degree<span class="required">*</span></label>
                                                </div>
                                                <div class="field-col clearfix">
                                                    <select name="q13_intendedDegree" style="border:1px solid #BCBCBC;width:100%;padding:13px;border-radius:3px;text-indent: 5px;line-height: 26px;">    
                                                        <option value="Bachelor">Bachelor</option>
                                                        <option value="Master">Master</option>
                                                        <option value="PhD">PhD</option>
                                                    </select>
                                                </div>
                                            </div>                                            


                                            <div class="field_wrap">
                                                <div class="label-col">
                                                    <label for="email" class="main_label" style="line-height: normal">Expected End of Studies <span class="required">*</span></label>
                                                </div>
                                                <div class="field-col clearfix">
                                                    <div style="margin: 0 auto">
                                                        <input placeholder="Month" style="width:25%;float:left;" id="email" required name="q14_expectedEnd[month]" class="texfield_form" autocomplete="off" type="text">
                                                        <input placeholder="Day" style="width:25%;float:left;margin-left:10px" id="email" required name="q14_expectedEnd[day]" class="texfield_form" autocomplete="off" type="text">
                                                        <input placeholder="Year" style="width:25%;float:left;margin-left:10px" id="email" required name="q14_expectedEnd[year]" class="texfield_form" autocomplete="off" type="text">
                                                    </div>
                                                </div>
                                            </div> 


                                            <div class="field_wrap">
                                                <div class="label-col">
                                                    <label for="email" class="main_label" style="line-height: normal">What are you looking for?<span class="required"></span></label>
                                                </div>
                                                <div class="field-col" style="text-align:left;">
                                                    <input type="radio" id="input_35_0" name="q15_whatAre" value="Internship" style="display: inline-block;">
                                                    <label for="input_35_0">Internship</label>
                                                    <br>
                                                    <br>
                                                    <input type="radio" class="form-checkbox validate[required]" id="input_35_1" name="q15_whatAre" value="Working Student Job" style="display: inline-block;">
                                                    <label for="input_35_1">Working Student Job</label>
                                                    <br>
                                                    <br>
                                                    <input type="radio" class="form-checkbox validate[required]" id="input_35_2" name="q15_whatAre" value="Trainee Program" style="display: inline-block;"> 
                                                    <label for="input_35_2">Trainee Program</label>
                                                    <br>
                                                    <br>
                                                    <input type="radio" class="form-checkbox validate[required]" id="input_35_3" name="q15_whatAre" value="Permanent Employment" style="display: inline-block;">
                                                    <label for="input_35_3">Permanent Employment</label>
                                                    <br>
                                                    <br>
                                                    <input type="radio" class="form-checkbox validate[required]" id="input_35_4" name="q15_whatAre" value="Founding a Startup" style="display: inline-block;">
                                                    <label for="input_35_4">Founding a Startup</label>
                                                 </div>
                                            </div>

                                            <div class="field_wrap">
                                                <div class="label-col">
                                                    <label for="input_16" class="main_label" style="line-height: normal">Upload Certificate of Enrollment<span class="required">*</span></label>
                                                </div>
                                                <div class="field-col" style="text-align:left">
                                                    <input class="form-upload validate[required]" type="file" id="input_16" name="q16_uploadCertificate16" file-accept="pdf, doc, docx, xls, xlsx, csv, txt, rtf, html, zip, mp3, wma, mpg, flv, avi, jpg, jpeg, png, gif" file-maxsize="8000" file-minsize="0" file-limit="1">
                                                </div>
                                            </div>


                                            <div class="field_wrap">
                                                <div class="field-col"><div class="line_form"></div></div>
                                            </div>
                                            <div class="field_wrap">
                                                <div class="label-col">
                                                    <label for="brthday" class="main_label label_required">* indicates required</label>
                                                </div>
                                                <div class="field-col">
                                                    <input type="text" name="b_59db0da2c55155bbedfa0c507_0a0c825bee" tabindex="-1" value="" style="display:none">
                                                    <input class="button orange submit_button" value="Apply" type="submit">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="popups_container">
            <div class="popup popup_desktop" style="visibility: hidden">
                <!--<span class="close_popup"></span>-->
                <p>
                    <!-- pull data from Google Analytics API -->
                    <span class="data"><?php echo Faker::views('startup'); ?></span> people currently viewing this page
                </p>
            </div>
            <div class="popup popup_desktop" style="visibility: hidden;display: none">
                <!--<span class="close_popup"></span>-->
                <p>
                    Last booking was from <br><span class="data"><?php echo CountryNamer::getCountryNameFromIP();  ?></span>
                </p>
            </div>            
        </div>
    </section><!-- section -->

</div>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>
