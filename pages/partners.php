<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once ('layout/_scroll_down_hint.php');?>
    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

    <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section mobile_hidden">2017 Partners</div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>            
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                </li>
                <li>
                    <a class="link_nav buyticket trigger_app_nav" data-section="buyticket"><?php echo Meta::getButtonWording(); ?></a>
                </li>
            </ul>
        </div>
    </div>

    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-partners active section_1" style="background:url('/src/images/partners_17_mob.png'), center, center; background-size:cover;">
        <div class="content_section center">
            <div class="wrapper">
                <div class="info_text_top">
                    <h1 class="top_title" style="text-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);">The Bits &amp; Pretzels 2017 Partners</h1>
                    <!--
                    <h2 class="main_title">Bits & Pretzels would not be possible <span class="stay_block">without our partners.</span></h2>   
                    -->       
                </div>
            </div>
        </div>
    </section><!-- section -->



<section id="investors" class="section section_logo white active section_1 section_pt" data-show-inner="false" data-show-footer="false" style="min-height: 0px">

        <div class="content_section center" style="min-height: 0px">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">Bits &amp; Pretzels would not be Bits &amp; Pretzels without these partners.</h1>
                            <div class="descr descr_small_margin center">Thank you. Thank you. Thank you. That’s basically all we need to say about this list of our loyal partners. Your help – in whatever form – is the priceless foundation for establishing Bits &amp; Pretzels as the biggest festival for founders in Europe.<div class="link_box"><!--<a target="_blank" href="" class="link">View all Investors<span class="icon icon-arrow_right"></span></a>--></div>
                            </div>                             
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->

   
    <section class="section section_logo white active section_1 section_pt" >
        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">Patronage</h1>
                        </div>
                        <div class="patronage_logo" style="margin-top:80px">
                            <a target="_blank" href="http://www.stmwi.bayern.de/">
                                <img class="lazyload-scroll" data-src="/src/images/partners/patronages.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                <noscript>
                                    <img src="/src/images/partners/patronages.png" alt="Bayerisches Staatsministerium">
                                </noscript>
                            </a>
                        </div>           
                        <div class="wrapp_main_text">
                            <h1 class="main_title main_title-mg center">Sponsors</h1>
                        </div>
                        <div class="list_logos list_logos-mt clearfix">
                            
                        <?php for($i = 0; $i < count($partners_2017); $i++):
                        if (strpos($partners_2017[$i]['type'],"sponsor")!==false): ?>
                            
                            <div class="logo_box logo_box-ft">
                                <div class="valign">
                                    <div class="middle">
	                                    <a target="_blank" href="<?= $partners_2017[$i]['link']; ?>">
                                            <img class="lazyload-scroll" data-src="/src/images/partners/<?= $partners_2017[$i]['logo']; ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                            <noscript>
                                                <img src="/src/images/partners/<?= $partners_2017[$i]['logo']; ?>" alt="<?= $partners_2017[$i]['name']; ?>">
                                            </noscript>
	                                    </a>
                                    </div>
                                </div> 
                            </div>    
                                               
                        <?php endif; endfor; ?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
	
	<hr class="gray-section-divider">

	
    <!-- <section class="section white section_logo white active section_1 section_pt">

        <div class="content_section center" style="min-height:auto!important">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">Premium Media Partners</h1>
                        </div>       
							<div class="list_logos list_logos-mt clearfix center"> 
							  <div class="patronage_logo" style="margin-top:0">
								<a target="_blank" href="http://www.sueddeutsche.de/">
									<img class="lazyload-scroll" data-src="/src/images/partners/sueddeutsche_zeitung.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
									<noscript>
										<img src="/src/images/partners/sueddeutsche_zeitung.png" alt="Sueddeutsche Zeitung">
									</noscript>
								</a>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </section> -->
	
    <section class="section white section_logo white active section_1 section_pt">

        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">Media Partners</h1>
                        </div>       
                        <div class="list_logos list_logos-mt clearfix">
                            
                            <?php for($i = 0; $i < count($partners_2017); $i++):
                        if (strpos($partners_2017[$i]['type'],"media partner")!==false): ?>
                            
                            <div class="logo_box logo_box-ft">
                                <div class="valign">
                                    <div class="middle">
	                                    <a target="_blank" href="<?= $partners_2017[$i]['link']; ?>">
                                            <img class="lazyload-scroll" data-src="/src/images/partners/<?= $partners_2017[$i]['logo']; ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                            <noscript>
                                                <img src="/src/images/partners/<?= $partners_2017[$i]['logo']; ?>" alt="<?= $partners_2017[$i]['name']; ?>">
                                            </noscript>
	                                    </a>
                                    </div>
                                </div> 
                            </div>    
                                               
                        <?php endif; endfor; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </section><!-- section -->


    <hr class="gray-section-divider">


   <section class="section section_logo white active section_1 section_pt" >

        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">Product Partners</h1>
                        </div>       
                        <div class="list_logos list_logos-mt clearfix">
                            
                        <?php for($i = 0; $i < count($partners_2017); $i++):
                        if (strpos($partners_2017[$i]['type'],"product partner")!==false): ?>
                            
                            <div class="logo_box logo_box-ft">
                                <div class="valign">
                                    <div class="middle">
	                                    <a target="_blank" href="<?= $partners_2017[$i]['link']; ?>">
                                            <img class="lazyload-scroll" data-src="/src/images/partners/<?= $partners_2017[$i]['logo']; ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                            <noscript>
                                                <img src="/src/images/partners/<?= $partners_2017[$i]['logo']; ?>" alt="<?= $partners_2017[$i]['name']; ?>">
                                            </noscript>
	                                    </a>
                                    </div>
                                </div> 
                            </div>    
                                               
                        <?php endif; endfor; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->


	<hr class="gray-section-divider">

    <section class="section section_logo white active section_1 section_pt" >

        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">Network Partners</h1>
                        </div>       
                        <div class="list_logos list_logos-mt clearfix">
                            <?php for($i = 0; $i < count($partners_2017); $i++):
                        if (strpos($partners_2017[$i]['type'],"network partner")!==false): ?>
                            
                            <div class="logo_box logo_box-ft">
                                <div class="valign">
                                    <div class="middle">
	                                    <a href="<?= $partners_2017[$i]['link']; ?>" target="_blank">
                                            <img class="lazyload-scroll" data-src="/src/images/partners/<?= $partners_2017[$i]['logo']; ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                            <noscript>
                                                <img src="/src/images/partners/<?= $partners_2017[$i]['logo']; ?>" alt="<?= $partners_2017[$i]['name']; ?>">
                                            </noscript>
	                                    </a>
                                    </div>
                                </div> 
                            </div>    
                                               
                        <?php endif; endfor; ?>      
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
</div>

<?#php include realpath('layout/signup-form.php');?>
<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>