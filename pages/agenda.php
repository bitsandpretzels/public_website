<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>


    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

    <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section mobile_hidden">Agenda</div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>            
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#agenda" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                </li>
                <li>
                    <a class="link_nav buyticket trigger_app_nav" data-section="buyticket"><?php echo Meta::getButtonWording(); ?></a>
                </li>
            </ul>
        </div>
    </div>

    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-agenda active section_1">
        <div class="content_section center">
            <div class="wrapper">
                <div class="info_text_top">
                    <h1 class="top_title">Agenda</h1>
                </div>
            </div>
        </div>
    </section><!-- section -->



    <?php include_once realpath('layout/_scroll_down_hint.php');?>
    <section id="networking" class="section section_devices white active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="content_section">
            <div class="wrapper_inner center">
                <div class="selections center">
                    <button class="selection-button active" onclick="toggle_day_1()">Day 1</button>
                    <button class="selection-button" onclick="toggle_day_2()">Day 2</button>
                    <button class="selection-button" onclick="toggle_day_3()">Day 3</button>
                </div>
                <div id="day_1">
                   <div class="wrapper_inner" style="margin: 30px 0 50px 0">
                        <div class="valign">
                            <div class="middle">
                                <div class="wrapp_main_text">
                                    <div class="descr" style="color:#0094d4; margin-bottom:0">
                                         <span class="text_icon" style="background-image:url('src/images/icon_agenda.svg'); margin-top:15px"></span>
                                         Sun | 25.09.2016
                                    </div>
                                    <address class="descr" style="color:#0094d4; margin-bottom:0">
                                         <span class="text_icon" style="background-image:url('src/images/icon_location.svg'); margin-top:15px"></span>
                                         ICM - International Congress Center Munich
                                                <br>Messegelände, 81823 Munich, Germany<br>
                                    </address>
                                    <div class="descr" style="color:#0094d4;">
                                         <span class="text_icon" style="background-image:url('src/images/icon_dresscode.svg'); margin-top:15px"></span>
                                         Recommended Dresscode: 
                                         Dirndl or Lederhosn
                                    </div>
                                    <div class="descr descr_small_margin center">
                                        The Bits &amp; Pretzels founders festival will start off with two days of inspiring speakers and presentations from some of the most influential founders of the digital world on 4 different kinds of stages:
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
<!--
                      <div class="wrapper_inner">
                            <div class="row" style="margin-top:50px">
                                 <div class="three columns descr center">
                                     <strong style="display:block; margin-bottom:10px">Main Stage</strong>
                                     On our mainstage you will hear inspiring talks (e.g. from speakers like Richard Branson or Nathan Blecharczyk) and get valuable founder insights.
                                 </div>
                                 <div class="three columns descr center">
                                    <strong style="display:block; margin-bottom:10px">Academy Stage</strong>
                                    We love learning! That’s why startups can expect 13 well designed masterclasses for founders on our academy stage.
                                 </div>
                                 <div class="three columns descr center">
                                    <strong style="display:block; margin-bottom:10px">Pitch Stage</strong>
                                    On our startup pitch stage, hundreds of startups of our 6 Clusters compete on the first two days of the event.
                                </div>
                                <div class="three columns descr center">
                                    <strong style="display:block; margin-bottom:10px">Side Stages</strong>
                                    Choose your <a href="<?php echo Router::getRoute('cluster'); ?>" style="color:#005690">Cluster</a> – Future Commerce, Hot Lifestyle, Big Money etc. – and find your desired slot on our 2 side stages.
                                </div>
                            </div>
                    </div>
-->

                    <img style="max-width:1300px" class="lazyload-scroll" data-src="/src/images/agenda_1.png?v=<?php echo $currentScriptVersion; ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                    <noscript>
                        <img src="/src/images/agenda_1.png?v=<?php echo $currentScriptVersion; ?>" alt="Agenda Tag 1" style="margin: 65px 0 80px 0">
                    </noscript>
                    <div id="buyticket" class="button_mb choose-cat-trigger-wrapper" style="margin-top:60px; margin-bottom:10px">
                    <div class="button_box button_box_orange button_box_auto">
                        <a href="<?php echo Router::getRoute('afterparty'); ?>" class="button orange">
                            <span style="margin-right:10px">🎉</span>
                            <span>Check out After Party</span>
                            <span style="margin-left:10px">🎉</span>
                        </a>
                    </div>
                    </div>
                </div>

                <div id="day_2" style="display:none;">
                   <div class="wrapper_inner" style="margin: 30px 0 50px 0">
                        <div class="valign">
                            <div class="middle">
                                <div class="wrapp_main_text">
                                    <div class="descr" style="color:#0094d4; margin-bottom:0">
                                         <span class="text_icon" style="background-image:url('src/images/icon_agenda.svg'); margin-top:15px"></span>
                                         Mon | 26.09.2016
                                    </div>
                                    <address class="descr" style="color:#0094d4; margin-bottom:0">
                                         <span class="text_icon" style="background-image:url('src/images/icon_location.svg'); margin-top:15px"></span>
                                         ICM - International Congress Center Munich
                                                <br>Messegelände, 81823 Munich, Germany<br>
                                    </address>
                                    <div class="descr" style="color:#0094d4;">
                                         <span class="text_icon" style="background-image:url('src/images/icon_dresscode.svg'); margin-top:15px"></span>
                                         Recommended Dresscode: 
                                         Dirndl or Lederhosn
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <img class="lazyload-scroll" data-src="/src/images/agenda_2.png?v=<?php echo $currentScriptVersion; ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                    <noscript>
                        <img style="margin: 65px 0 80px 0" src="/src/images/agenda_2.png?v=<?php echo $currentScriptVersion; ?>" alt="Agenda Tag 2">
                    </noscript>
                    <div id="buyticket" class="button_mb choose-cat-trigger-wrapper" style="margin-top:60px; margin-bottom:10px">
                    <div class="button_box button_box_orange button_box_auto">
                        <a href="<?php echo Router::getRoute('side-events'); ?>" class="button orange">
                            <span style="margin-right:10px">🎉</span>
                            <span>Check out Side Events</span>
                            <span style="margin-left:10px">🎉</span>
                        </a>
                    </div>
                </div>
                </div>

                <div id="day_3" style="display:none;">
                   <div class="wrapper_inner" style="margin: 30px 0 50px 0">
                       <div class="valign">
                            <div class="middle">
                                <div class="wrapp_main_text">
                                    <div class="descr" style="color:#0094d4; margin-bottom:0">
                                         <span class="text_icon" style="background-image:url('src/images/icon_agenda.svg'); margin-top:15px"></span>
                                         Tue | 27.09.2016
                                    </div>
                                    <address class="descr" style="color:#0094d4; margin-bottom:0">
                                         <span class="text_icon" style="background-image:url('src/images/icon_location.svg'); margin-top:15px"></span>
                                         Theresienwiese, Theresienhöhe
                                         <br>80339 Munich, Germany
                                    </address>
                                    <div class="descr" style="color:#0094d4;">
                                         <span class="text_icon" style="background-image:url('src/images/icon_dresscode.svg'); margin-top:15px"></span>
                                         Recommended Dresscode: 
                                         Dirndl or Lederhosn
                                    </div>
                                    <div class="descr center" style="margin-bottom:0">
                                        <br> And last but definitely not least, the grand finale on the 3rd day of Bits &amp; Pretzels is at the original and famous Oktoberfest in the Schottenhamel Festzelt!
                                        <br>
                                    </div>
                                </div>  
                           </div>
                        </div>
                    </div>
                    <img class="lazyload-scroll" data-src="/src/images/agenda_3.png?v=<?php echo $currentScriptVersion; ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                    <noscript>
                        <img style="margin: 65px 0 80px 0" src="/src/images/agenda_3.png?v=<?php echo $currentScriptVersion; ?>" alt="Agenda Tag 3">
                    </noscript>
                </div>
                <div class="selections center" style="margin-top:30px">
                    <button class="selection-button active" onclick="toggle_day_1()">Day 1</button>
                    <button class="selection-button" onclick="toggle_day_2()">Day 2</button>
                    <button class="selection-button" onclick="toggle_day_3()">Day 3</button>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    
    /** Some Easy Script for toggling between days **/
    
    function toggle_day_1() {
        var day_1 = document.getElementById("day_1"),
            day_2 = document.getElementById("day_2"),
            day_3 = document.getElementById("day_3");
        day_1.style.display = "block";
        day_2.style.display = "none";
        day_3.style.display = "none";
    }
    function toggle_day_2() {
        var day_1 = document.getElementById("day_1"),
            day_2 = document.getElementById("day_2"),
            day_3 = document.getElementById("day_3");
        day_1.style.display = "none";
        day_2.style.display = "block";
        day_3.style.display = "none";
    }
    function toggle_day_3() {
        var day_1 = document.getElementById("day_1"),
            day_2 = document.getElementById("day_2"),
            day_3 = document.getElementById("day_3");
        day_1.style.display = "none";
        day_2.style.display = "none";
        day_3.style.display = "block";
    }

</script>

<?#php include realpath('layout/signup-form.php');?>
<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>