<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<?php include_once realpath('layout/_inner_pages_main_nav.php');?>


 <div id="sub_nav" class="sub_nav_wrap">
     <div class="wrap_subnav_inner">
         <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
     </div>
 </div>

  
<div class="inner_sections">
<section class="page-newsletter">
    <div class="container center">
        <div class="row clearfix">
            <div class="ten columns offset-by-one">
                <h1 class="center" style="font-size:2.2em; color:white; margin-bottom:25px; text-transform:uppercase; font-weight:900">
                Select Tickets</h1>
                
                
                <!-- Corporate Element -->                                                
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">

                    <tbody>
                        <tr>
                            <td valign="top">

                                <!--[if gte mso 9]>
                                <td align="center" valign="top" ">
                                <![endif]-->
                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;">
                                    <tbody><tr>

                                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">
                                            
                                            <form class="js-order-registration responsive-form" name="ticketsForm" action="https://www.eventbrite.de/orderstart" method="post" target="_top" style="clear:both;" id="BitsPretzels">
                                            

                                            <table border="0" cellpadding="18" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #FFFFFF;border-left: 10px solid #ea5c3f;">
                                                <tbody>
                                                    <input type="hidden" name="eid" value="27797199171">
                                                    <input type="hidden" name="has_javascript" value="1">
                                                    <input type="hidden" name="payment_type" value="eventbrite">
                                                    <input type="hidden" name="legacy_event_page" value="1">
                                                    <input type="hidden" name="invite" value="">
                                                    <input type="hidden" name="affiliate" value="">
                                                    <input type="hidden" name="referrer" value="">
                                                    <input type="hidden" name="referral_code" value="">
                                                    <input type="hidden" name="w" value="">
                                                    <input type="hidden" name="waitlist_code" value="">
                                                    <input type="hidden" name="discount" value="BITSPIHB">
                                                    <!--input type="hidden" name="order_start_uuid" value="1480522955/8d4aebc8-9f4d-4dbf-8322-7447e9180aa1">
                                                    <input type="hidden" name="order_start_signature" value="ADiACkys3_dm8WXAs28AQ8SeF_uU1a21pQ"-->

                                                     <tr> <td valign="top" class="mcnTextContent" style="color: #656565;font-family: raleway, Helvetica;font-size: 14px;font-weight: normal">
                                                        <div style="text-align: left;"><span style="font-size:18px"><span style="color:#656565"><b>Private Investor (Happy Bird)</b></span></span><br>
                <span style="color:#656565"><span style="font-size:15px">€699.00</span></span>&nbsp;<span style="color:#656565"><span style="font-size:12px">(€132.81 MwSt./VAT.)</span></span></div>
                                                         </td>
                                                        <td style="text-align:right">
                                                            <div class="ticketframe">    
                                                                <div class="form-group">
                                                                    <select class="form-control" name="quant_61276600_None" style="font-size:1.1em; font-family:  Helvetica; Opensans">
                                                                        <option value="1">1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        </tr>
                                                        <tr><td colspan="2">
                                                            <div style="text-align: left;">
                                                                <span style="color: #656565;font-family: raleway, Helvetica;font-size: 12px;font-weight: normal">Business Angels&nbsp;</span>
                                                            </div>
                                                        </td>
                                                        </tr>
                                            </tbody></table>
                                            
                                            <tr><td colspan="2">
                                                        <div style="margin-top:15px">
                                                            <img max-width: 70px src='/src/images/Ticket%20Checkout/Visa.svg'>
                                                            <img max-width: 70px src='/src/images/Ticket%20Checkout/master.svg'>
                                                            <img max-width: 70px src='src/images/Ticket%20Checkout/Amex.svg'>
                                                            <img max-width: 70px src='/src/images/Ticket%20Checkout/sepa.svg'>
                                                            <img max-width: 70px src='/src/images/Ticket%20Checkout/sofort.svg'>
                                                            <p class="descr_small_margin" style="color:white; margin-bottom:20px; line-height:23px; font-size:12px">Payment per invoice also available</p>
                                                        </div>
                                                        </td></tr>
                                                        
                                                        <tr><td colspan="2">
                                                             <div class="ticket" style="">
                                                                    <button class="" type="submit" style="font-family:raleway, sans-serif;color:#FFFFFF; cursor:pointer; border-radius: 4px;background:#ea5c3f;padding: 10px 20px">Buy</button>
                                                                </div>
                                                            </td></tr>
                                            </form>
                                        </td>
                                    </tr>
                                </tbody></table>
                            </td>
                        </tr>
                    </tbody>
                </table>

                
                
                
                
            </div>
        </div>
    </div>
</section>
</div>

<style>
    .page-newsletter {
        background:url('/src/images/2016/welcome_hero-blue.png') no-repeat;
        background-size:cover;
        background-position:left center;
        padding:25vh 0 10vh 0;
        min-height:100vh;
    }
    
</style>



<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>
