<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<?php include_once realpath('layout/_inner_pages_main_nav.php');?>


 <div id="sub_nav" class="sub_nav_wrap">
     <div class="wrap_subnav_inner">
         <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
     </div>
 </div>

  
<div class="inner_sections">
<section class="page-newsletter">
    <div class="container center">
        <div class="row clearfix">
            <div class="ten columns offset-by-one">
                <h1 class="center" style="font-size:2.2em; color:white; margin-bottom:25px; text-transform:uppercase; font-weight:900">
                Select Tickets</h1>
                
               
               <!-- Erstes Ticket Element -->                                                
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
    
	<tbody>
        <tr>
            <td valign="top">
                
				<!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;">
                    <tbody><tr>
                        
                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">
                        
                            <table border="0" cellpadding="18" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #FFFFFF;border-left: 10px solid #ea5c3f; table-layout:fixed;">
                                <tbody><tr>
                                    <td valign="top" class="mcnTextContent" style="color: #656565;font-family: raleway, Helvetica;font-size: 14px;font-weight: normal">
                                        <div style="text-align: left; padding-left: 5px;"><span style="font-size:18px"><span style="color:#656565"><b>Corporate (Happy Bird)</b></span></span><br>
<span style="color:#656565"><span style="font-size:15px">€1.699.00</span></span>&nbsp;<span style="color:#656565"><span style="font-size:12px">(€322.81 MwSt./VAT.)</span></span></div>
                                         </td>
                                        <td style="text-align:right">
                                            <a href="https://www.bitsandpretzels.com/ticket_corporate"><img align="right" alt="Buy Button" src="https://www.bitsandpretzels.com/src/images/icons/buy_button.png" width="70" style="max-width:75px; padding-bottom: 0; display: inline !important; verstical-align: bottom;" class="mcnImage"></a>
                                        </td>
                                </tr>
                                <tr><td colspan="2">
                                    <div style="text-align: left;padding-left: 5px;">
                                        <span style="color: #656565;font-family: raleway, Helvetica;font-size: 12px;font-weight: normal">Founders and employees of companies older than 10 years <u>or</u> with more than 300 employees. Founders and employees of consulting companies and agencies.&nbsp;</span>
                                    </div></td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody>
</table>

                                                <!-- 2. Ticket Element Institutional Investor -->                                                
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
    
	<tbody>
        <tr>
            <td valign="top">
                
				<!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;">
                    <tbody><tr>
                        
                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">
                        
                            <table border="0" cellpadding="18" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #FFFFFF;border-left: 10px solid #ea5c3f; table-layout:fixed;">
                                <tbody><tr>
                                    <td valign="top" class="mcnTextContent" style="color: #656565;font-family: raleway, Helvetica;font-size: 14px;font-weight: normal">
                                        <div style="text-align: left; padding-left: 5px;"><span style="font-size:18px"><span style="color:#656565"><b>Institutional Investor (Happy Bird)</b></span></span><br>
<span style="color:#656565"><span style="font-size:15px">€799.00</span></span>&nbsp;<span style="color:#656565"><span style="font-size:12px">(€151.81 MwSt./VAT.)</span></span></div>
                                         </td>
                                        <td style="text-align:right">
                                            <a href="https://www.bitsandpretzels.com/ticket_institutional_investor"><img align="right" alt="Buy Button" src="https://www.bitsandpretzels.com/src/images/icons/buy_button.png" width="70" style="max-width:75px; padding-bottom: 0; display: inline !important; verstical-align: bottom;" class="mcnImage"></a>
                                        </td>
                                </tr>
                                <tr><td colspan="2">
                                    <div style="text-align: left; padding-left: 5px;">
                                        <span style="color: #656565;font-family: raleway, Helvetica;font-size: 12px;font-weight: normal">Founders and Employees of venture capital &amp; private equity companies, accelerators &amp; incubators.&nbsp;</span>
                                    </div></td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody>
</table>
                                                
                                                
                                                <!-- 3. Ticket Private Investor -->                                                
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
    
	<tbody>
        <tr>
            <td valign="top">
                
				<!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;">
                    <tbody><tr>
                        
                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">
                        
                            <table border="0" cellpadding="18" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #FFFFFF;border-left: 10px solid #ea5c3f; table-layout:fixed;">
                                <tbody><tr>
                                    <td valign="top" class="mcnTextContent" style="color: #656565;font-family: raleway, Helvetica;font-size: 14px;font-weight: normal">
                                        <div style="text-align: left; padding-left: 5px;"><span style="font-size:18px"><span style="color:#656565"><b>Private Investor (Happy Bird)</b></span></span><br>
<span style="color:#656565"><span style="font-size:15px">€699.00</span></span>&nbsp;<span style="color:#656565"><span style="font-size:12px">(€132.81 MwSt./VAT.)</span></span></div>
                                         </td>
                                        <td style="text-align:right">
                                            <a href="https://www.bitsandpretzels.com/ticket_private_investor"><img align="right" alt="Buy Button" src="https://www.bitsandpretzels.com/src/images/icons/buy_button.png" width="70" style="max-width:75px; padding-bottom: 0; display: inline !important; verstical-align: bottom;" class="mcnImage"></a>
                                        </td>
                                </tr>
                                <tr><td colspan="2">
                                    <div style="text-align: left; padding-left: 5px;">
                                        <span style="color: #656565;font-family: raleway, Helvetica;font-size: 12px;font-weight: normal">Business Angels&nbsp;</span>
                                    </div></td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody>
</table>
                                                
<!-- Later Stage Startup -->                                                
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
    
	<tbody>
        <tr>
            <td valign="top">
                
				<!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;">
                    <tbody><tr>
                        
                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">
                        
                            <table border="0" cellpadding="18" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #FFFFFF;border-left: 10px solid #ea5c3f; table-layout:fixed;">
                                <tbody><tr>
                                    <td valign="top" class="mcnTextContent" style="color: #656565;font-family: raleway, Helvetica;font-size: 14px;font-weight: normal">
                                        <div style="text-align: left; padding-left: 5px;"><span style="font-size:18px"><span style="color:#656565"><b>Later Stage Startups (Happy Bird)</b></span></span><br>
<span style="color:#656565"><span style="font-size:15px">€499.00</span></span>&nbsp;<span style="color:#656565"><span style="font-size:12px">(€94.81 MwSt./VAT.)</span></span></div>
                                         </td>
                                        <td style="text-align:right">
                                            <a href="https://www.bitsandpretzels.com/ticket_later_startup"><img align="right" alt="Buy Button" src="https://www.bitsandpretzels.com/src/images/icons/buy_button.png" width="70" style="max-width:75px; padding-bottom: 0; display: inline !important; verstical-align: bottom;" class="mcnImage"></a>
                                        </td>
                                </tr>
                                <tr><td colspan="2">
                                    <div style="text-align: left; padding-left: 5px;">
                                        <span style="color: #656565;font-family: raleway, Helvetica;font-size: 12px;font-weight: normal">Founders and employees of later stage startups. A later stage startup is a company younger than 10 years <u>and</u> with less than 300 employees. We consider startups to be companies who have the goal to realize an innovative business idea (NOT: consulting &amp; agencies).&nbsp;</span>
                                    </div></td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody>
</table>

<!-- Early Stage Startup -->                                                
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
    
	<tbody>
        <tr>
            <td valign="top">
                
				<!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;">
                    <tbody><tr>
                        
                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">
                        
                            <table border="0" cellpadding="18" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #FFFFFF;border-left: 10px solid #ea5c3f; table-layout:fixed;">
                                <tbody><tr>
                                    <td valign="top" class="mcnTextContent" style="color: #656565;font-family: raleway, Helvetica;font-size: 14px;font-weight: normal">
                                        <div style="text-align: left; padding-left: 5px;"><span style="font-size:18px"><span style="color:#656565"><b>Early Stage Startups (Happy Bird)</b></span></span><br>
<span style="color:#656565"><span style="font-size:15px">€399.00</span></span>&nbsp;<span style="color:#656565"><span style="font-size:12px">(€75.81 MwSt./VAT.)</span></span></div>
                                         </td>
                                        <td style="text-align:right">
                                            <a href="https://www.bitsandpretzels.com/ticket_early_startup"><img align="right" alt="Buy Button" src="https://www.bitsandpretzels.com/src/images/icons/buy_button.png" width="70" style="max-width:75px; padding-bottom: 0; display: inline !important; verstical-align: bottom;" class="mcnImage"></a>
                                        </td>
                                </tr>
                                <tr><td colspan="2">
                                    <div style="text-align: left; padding-left: 5px;">
                                        <span style="color: #656565;font-family: raleway, Helvetica;font-size: 12px;font-weight: normal">Founders and employees of early stage startups. An early stage startup is a company younger than 3 years <u>and</u> with less than 10 employees. We consider startups to be companies who have the goal to realize an innovative business idea (NOT: consulting &amp; agencies).&nbsp;</span>
                                    </div></td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody>
</table>

<!-- Student Ticket -->                                                
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
    
	<tbody>
        <tr>
            <td valign="top">
                
				<!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;">
                    <tbody><tr>
                        
                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">
                        
                            <table border="0" cellpadding="18" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #FFFFFF;border-left: 10px solid #0094d4; table-layout:fixed;">
                                <tbody><tr>
                                    <td valign="top" class="mcnTextContent" style="color: #656565;font-family: raleway, Helvetica;font-size: 14px;font-weight: normal">
                                        <div style="text-align: left; padding-left: 5px;"><span style="font-size:18px"><span style="color:#656565"><b>Student Application</b></span></span><br>
<span style="color:#656565"><span style="font-size:15px">€199.00</span></span>&nbsp;<span style="color:#656565"><span style="font-size:12px">(excl. VAT)</span></span></div>
                                         </td>
                                        <td style="text-align:right">
                                                <a href="https://form.jotformeu.com/70304969526361"><img align="right" alt="Apply Button" src="https://www.bitsandpretzels.com/src/images/icons/apply_button.png" width="70" style="max-width:75px; padding-bottom: 0; display: inline !important; verstical-align: bottom;" class="mcnImage"></a>
                                        </td>
                                </tr>
                                <tr><td colspan="2">
                                    <div style="text-align: left; padding-left: 5px;">
                                        <span style="color: #656565;font-family: raleway, Helvetica;font-size: 12px;font-weight: normal">If you're enrolled as a full-time student until at least September ’17 please apply here for a student ticket.&nbsp;</span>
                                    </div></td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody>
</table>
                                                

                
                
                
                
            </div>
        </div>
    </div>
</section>
</div>

<style>
    .page-newsletter {
        background:url('/src/images/2016/welcome_hero-blue.png') no-repeat;
        background-size:cover;
        background-position:left center;
        padding:25vh 0 10vh 0;
        min-height:100vh;
    }
    
</style>


<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>
