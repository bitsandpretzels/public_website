<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<?php include_once realpath('layout/_inner_pages_main_nav.php');?>


 <div id="sub_nav" class="sub_nav_wrap">
     <div class="wrap_subnav_inner">
         <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
     </div>
 </div>

  
<div class="inner_sections">
<section class="page-newsletter">
    <div class="container center">
        <div class="row clearfix">
            <div class="ten columns offset-by-one">
                <h1 class="center" style="font-size:2em; color:white; margin-bottom:25px; text-transform:uppercase; font-weight:900">
                limited to <br>
                5,000 awesome attendees <br>
                - become one of them -</h1>
        <!--                    <img src="/src/images/2016/bird-overview.svg?v=<?php echo $currentScriptVersion; ?>" alt="Bird Overview" style="width:100%; margin-bottom:40px;">-->
                <p class="descr_small_margin" style="color:white; margin-bottom:20px; line-height:23px; font-size:1em">
                <strong>Enter your email address below to receive your personal invitation and price information.</strong>
                <br>Be fast and join us for Europe‘s coolest startup event before it sells out again.
                </p>
            </div>
        </div>
           <div class="row clearfix">
               <div class="four columns offset-by-four">
                <!-- Begin MailChimp Signup Form -->
                <!--<div id="mc_embed_signup">
                <form action="//bitsandpretzels.us8.list-manage.com/subscribe/post?u=59db0da2c55155bbedfa0c507&amp;id=0af6c68cca" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">
                        <div class="mc-field-group" style="margin-top:20px; width:100%">
                            <input class="u-full-width" style="font-family:raleway, sans-serif; border:1px solid white !important; color:#005690 !important;font-size:17px; width:100%; border-radius:4px; height:45px; text-align:center" type="email" value="" placeholder="Email Address"name="EMAIL" class="required email" id="mce-EMAIL">
                        </div>
                        <input type="hidden" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>" name="IPADRESS">
                        <input type="hidden" value="Buy Ticket Button" name="SOURCE">
                        <div id="mce-responses" class="clear">
                            <div class="response" id="mce-error-response" style="display:none"></div>
                            <div class="response" id="mce-success-response" style="display:none"></div>
                        </div>
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_59db0da2c55155bbedfa0c507_3c5efc9e7b" tabindex="-1" value=""></div>
                        
                        <div class="clear" style="margin-top:20px; width:100%">
                            <input style="font-family:raleway, sans-serif;cursor:pointer; width:100%; border-radius: 4px;height: 47px;background:#ea5c3f" type="submit" value="Send me my invitation code" name="subscribe" id="mc-embedded-subscribe" class="button">
                        </div>
                    </div>
                </form>
                </div>
                <p class="descr_small_margin" style="color:white; margin-bottom:20px; line-height:23px; font-size:.6em; margin-top:10px">
                We will never share your personal information. Promised!
                </p>-->
                <!--End mc_embed_signup-->
                   
                <!-- Jot Form Signup -->
                <script src="https://cdn.jotfor.ms/static/prototype.forms.js" type="text/javascript"></script>
<script src="https://cdn.jotfor.ms/static/jotform.forms.js?3.3.17871" type="text/javascript"></script>
<script type="text/javascript"> JotForm.init(function(){ JotForm.alterTexts({"alphabetic":"This field can only contain letters","alphanumeric":"This field can only contain letters and numbers.","ccDonationMinLimitError":"Minimum amount is {minAmount} {currency}","ccInvalidCVC":"CVC number is invalid.","ccInvalidExpireDate":"Expire date is invalid.","ccInvalidNumber":"Credit Card Number is invalid.","ccMissingDetails":"Please fill up the Credit Card details.","ccMissingDonation":"Please enter numeric values for donation amount.","ccMissingProduct":"Please select at least one product.","characterLimitError":"Too many Characters. The limit is","characterMinLimitError":"Too few characters. The minimum is","confirmClearForm":"Are you sure you want to clear the form?","confirmEmail":"E-mail does not match","currency":"This field can only contain currency values.","cyrillic":"This field can only contain cyrillic characters","dateInvalid":"This date is not valid. The date format is {format}","dateInvalidSeparate":"This date is not valid. Enter a valid {element}.","dateLimited":"This date is unavailable.","disallowDecimals":"Please enter a whole number.","email":"Enter a valid e-mail address","fillMask":"Field value must fill mask.","freeEmailError":"Free email accounts are not allowed","generalError":"There are errors on the form. Please fix them before continuing.","generalPageError":"There are errors on this page. Please fix them before continuing.","gradingScoreError":"Score total should only be less than or equal to","incompleteFields":"There are incomplete required fields. Please complete them.","inputCarretErrorA":"Input should not be less than the minimum value:","inputCarretErrorB":"Input should not be greater than the maximum value:","lessThan":"Your score should be less than or equal to","maxDigitsError":"The maximum digits allowed is","maxSelectionsError":"The maximum number of selections allowed is","minSelectionsError":"The minimum required number of selections is","multipleFileUploads_emptyError":"{file} is empty, please select files again without it.","multipleFileUploads_fileLimitError":"Only {fileLimit} file uploads allowed.","multipleFileUploads_minSizeError":"{file} is too small, minimum file size is {minSizeLimit}.","multipleFileUploads_onLeave":"The files are being uploaded, if you leave now the upload will be cancelled.","multipleFileUploads_sizeError":"{file} is too large, maximum file size is {sizeLimit}.","multipleFileUploads_typeError":"{file} has invalid extension. Only {extensions} are allowed.","numeric":"This field can only contain numeric values","pastDatesDisallowed":"Date must not be in the past.","pleaseWait":"Please wait...","required":"This field is required.","requireEveryCell":"Every cell is required.","requireEveryRow":"Every row is required.","requireOne":"At least one field required.","submissionLimit":"Sorry! Only one entry is allowed. Multiple submissions are disabled for this form.","uploadExtensions":"You can only upload following files:","uploadFilesize":"File size cannot be bigger than:","uploadFilesizemin":"File size cannot be smaller than:","url":"This field can only contain a valid URL","wordLimitError":"Too many words. The limit is","wordMinLimitError":"Too few words. The minimum is"}); JotForm.clearFieldOnHide="disable"; });
</script>
       
<form class="jotform-form" action="https://submit.jotformeu.com/submit/70752736613357/" method="post" name="form_70752736613357" id="70752736613357" accept-charset="utf-8"> 
    <input type="hidden" name="formID" value="70752736613357" /> 
        <div class="form-all"> 
                <li class="form-line" data-type="control_email" id="id_3"> 
                        <div id="cid_3" class="form-input-wide jf-required"> 
                            <input style="margin-left: 0px !important; font-family:raleway, sans-serif; border:1px solid white !important; color:#005690 !important;font-size:17px; width:100%; border-radius:4px; height:45px; text-align:center" type="email" value="" placeholder="Email Address" id="input_3, mce-EMAIL" name="q3_email" class="required email" size="30" value="" data-component="email" />
                    </div>
                </li> 
                <p class="descr_small_margin" style="color:white; margin-bottom:20px; line-height:23px; font-size:.6em; margin-top:10px"></p>
                <li class="form-line" data-type="control_button" id="id_2"> 
                    <div id="cid_2" class="form-input-wide"> 
                        <div style="" class="form-buttons-wrapper"> 
                            <button style="font-family:raleway, sans-serif;cursor:pointer; width:100%; border-radius: 4px;height: 47px;background:#ea5c3f;" name ="input_2" id="input_2" type="submit" class="form-submit-button" data-component="button"><p style="color:#FFFFFF">Send me my invitation code</p></button> 
                        </div>
                    </div> 
                </li> 
                <li style="display:none"> Should be Empty: 
                    <input type="text" name="website" value="" /> 
                </li>
                <p class="descr_small_margin" style="color:white; margin-bottom:20px; line-height:23px; font-size:.6em; margin-top:10px">
                We will never share your personal information. Promised!
                </p>
        </div> 
    <input type="hidden" id="simple_spc" name="simple_spc" value="70752736613357" /> 
    <script type="text/javascript"> document.getElementById("si" + "mple" + "_spc").value = "70752736613357-70752736613357"; </script>
</form>
<script type="text/javascript">JotForm.ownerView=true;</script>
                   
                   <!-- End Signup -->
            </div>
        </div>
    </div>
</section>
  <!-- START: content section -->
</div>

<style>    
    .page-newsletter {
        background:url('/src/images/2016/welcome_hero-blue.png') no-repeat;
        background-size:cover;
        background-position:left center;
        padding:20vh 0 10vh 0;
        min-height:100vh;
    }
    
</style>


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>
