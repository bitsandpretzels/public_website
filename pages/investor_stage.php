<?php include_once realpath('layout/top.php');?>
<?php include_once realpath('layout/header.php');?>

<div class="inner_sections">

    <?php include_once realpath('layout/_inner_pages_main_nav.php');?>
    <?php include_once ('layout/_scroll_down_hint.php');?>
    <!-- 
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

     <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <div class="title_section mobile_hidden">Investors Stage</div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>            
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#agenda" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li" style="opacity:0">
                    <a href="<?php echo Router::getRoute('student'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                </li>
                <li>
                    <a class="link_nav buyticket trigger_app_nav" data-section="buyticket"><?php echo Meta::getButtonWording(); ?></a>
                </li>
            </ul>
        </div>
    </div>

    <section id="section_top" data-inner="section_top_info" class="section section_top section_top-academy active section_1" style="background:url('/src/images/investor_img.png'), center, center; background-size:cover;">

    	<div class="content_section center">
    		<div class="wrapper">
                <div class="info_text_top">
                    <h1 class="top_title" style="text-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);">INVESTORS DEALMAKING STAGE</h1>
                    <!--
                    <h2 class="main_title">What do startups need to be successful? <span class="stay_block">Learn it at the Bits & Pretzels Academy.</span></h2> 
                    -->         
                </div>
            </div>
    	</div>
    </section><!-- section -->

    <section id="content" class="section section_logo silver white active section_1 section_pt">

        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">Brand new at #bits17: The Investors Dealmaking Stage</h1>
                            <div class="descr descr_big_margin center">Exclusive insights, an intimate and cozy setting, a champagne bar, networking and dealmaking spaces - we'd like to create an inspiring environment for investors to meet the most promising digital companies. Deals will be made, insights shared, and business relations fostered - our brand new "Investors Dealmaking Stage" is the place-to-be.
                            </div>  
                        </div>          

                            <div class="wrapp_main_text" style="">
                                
                            <!--    <div><h1 class="main_title center">Your benefits</h1></div> -->
                                <div class="descr center" style="margin-bottom: 20px;"><p style="color:#005690; font-size:1.3em">Content Definition</p>Company Presentations of the most promising online- and digital companies</div>
                                <div class="descr center" style="margin-bottom: 20px;"><p style="color:#005690; font-size:1.3em">Target Group</p>Exclusively for investors and corporates</div>
                                <div class="descr center" style="margin-bottom: 20px;"><p style="color:#005690; font-size:1.3em">Format</p>10-minute company presentation</div>
                                <div class="descr center" style="margin-bottom: 20px;"><p style="color:#005690; font-size:1.3em">Expectations</p>Exclusive insights, cozy atmosphere, drinks, networking, dealmaking</div>
                            </div>        
                            
                        
                        <div class="wrapp_main_text" style="padding-top: 60px;">
                             <h1 class="main_title center"  style="padding-bottom: 20px;">Confirmed company presentations</h1>
<!--                            <div class="descr descr_sub_mg center">We are currently acquiring the best possible speakers for each masterclass. Stay tuned.</div>  -->
                        </div>   
                        <div class="wrap_quest">
                            <div class="grid clearfix">
                                <div class="valign">    
                                    <div class="middle"> 
                                    <?php for($i = 0; $i < count($investor_stage_2017); $i++) { ?>  
                                        <div class="topic flex">
                                            <div class="avatar">
                                                <img src="/src/images/speakers/investor_stage/<?= $investor_stage_2017[$i]['speaker_image'] ?>" width="100">
                                            </div>
                                            <div class="info">
                                                <div>
                                                <h1 class="topic_title"><?= $investor_stage_2017[$i]['headline'] ?></h1>
                                                <div class="descr descr_no_margin"><?= $investor_stage_2017[$i]['description'] ?></div> 
                                                <div class="topic_speaker descr descr_no_margin"><?= $investor_stage_2017[$i]['speaker'] ?></div>
<!--                                                <div class="arrow_pointer"></div>-->
<!--                                                 <div class="description_info descr descr_no_margin"><?= $investor_stage_2017[$i]['description_more'] ?></div>-->
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div> 
                        </div>                   
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->
</div>


<?php include realpath('layout/footer.php');?>
<?php include realpath('layout/bottom.php');?>

