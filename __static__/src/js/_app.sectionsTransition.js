App.SectionsTransition = {
	transition : null,
	is_header_open : true,
	is_footer_open : true,
	is_chrome : navigator.userAgent.toLowerCase().indexOf('chrome') > -1,

	init : function (){
		var sec = $('#home');
		$(document).ready(function() {
					TweenMax.fromTo(sec.find('.logo'),1,{y:-100,autoAlpha:0},{y:0,autoAlpha:1});
					TweenMax.fromTo(sec.find('.main_title'),1,{y:-100,autoAlpha:0},{y:0,delay:.3,autoAlpha:1});
					TweenMax.fromTo(sec.find('.sub_title'),1,{autoAlpha:0,y:-100},{autoAlpha:1,y:0});
					TweenMax.fromTo($('#main_nav'),1,{y:200,autoAlpha:0},{y:0,autoAlpha:1});
					TweenMax.fromTo(sec.find('.scroll_arrow'),1,{y:-50,autoAlpha:0},{y:0,autoAlpha:1, delay:.6});
		});

	},

	bind : function (){

	},

	afterChange : function ( obj ){
		var sec = $('#'+obj.currentId);
		//setTimeout ( App.SectionsTransition.showInterface, lock_time*.5*1000, sec );

		App.SectionsTransition.showInterface(sec, obj);
		App.SectionsTransition.selectLeftNav( obj.index, obj.currentId );
	},

	selectLeftNav : function ( value, section )
	{
		var left_nav = $('.left_nav');
		left_nav.find('.link').removeClass('active').eq(value).addClass('active');
		TweenMax.to( left_nav, .6,{ x: section == 'home'? 200 : 0 } )
	},



	initTransitions : function () {
		var transition = App.SectionsTransition.transition;
		transition.addTransition( new Transition (
			{
				section: '#home',
				incoming: function (obj){
					var sec = obj.incomingSection;
					TweenMax.fromTo(sec.find('.logo'),1,{y:-100,autoAlpha:0},{y:0,autoAlpha:1});
					TweenMax.fromTo(sec.find('.main_title'),1,{y:-100,autoAlpha:0},{y:0,delay:.3,autoAlpha:1});
					TweenMax.fromTo(sec.find('.sub_title'),1,{autoAlpha:0,y:-100},{autoAlpha:1,y:0});
					TweenMax.fromTo($('#main_nav'),1,{y:200,autoAlpha:0},{y:0,autoAlpha:1});
					TweenMax.fromTo(sec.find('.scroll_arrow'),1,{y:-50,autoAlpha:0},{y:0,autoAlpha:1, delay:.6});

					return 1;
				},
				outcoming: function (obj){
					var sec = obj.outcomingSection;
					//TweenMax.to(sec.find('.logo'),1,{y:-100,autoAlpha:0});
					TweenMax.to(sec.find('.main_title'),1,{y:-100,autoAlpha:0});
					TweenMax.to(sec.find('.sub_title'),1,{autoAlpha:0,y:-100,delay:.3});
					TweenMax.to($('#main_nav'),1,{y:100});
					TweenMax.to(sec.find('.scroll_arrow'),1,{y:100,autoAlpha:0});

					return 1;
				}
			}
		));

		var svg_tl;

		transition.addTransition( new Transition (
			{
				section:'#section_2',
				incoming:function (obj){
					var sec = obj.incomingSection;
					//TweenMax.fromTo(sec.find('.content_section').eq(0),1,{y:App.h*obj.destination},{y:0});

					if( App.SectionsTransition.is_chrome ){
						svg_tl = new TimelineLite({paused:true,delay:.6});
				        var rect = document.getElementById('white-rect');

				        TweenMax.set('#white-rect',{opacity:0});
				        svg_tl.fromTo("#rect-1",3,{yPercent:100,transfromOrigin:"50% 50%"},{yPercent:0,ease:Power4.easeOut},0)
				        svg_tl.fromTo("#mask-gevreche",2,{transfromOrigin:"50% 50%",yPercent:-20,ease:Power4.easeOut},{yPercent:0,ease:Power4.easeOut},0)

				        svg_tl.fromTo(rect,2,{x:window.innerWidth},{x:0,ease:Power4.easeOut,ease:Power4.easeOut,onStart:function (){
				        	TweenMax.set('#white-rect',{opacity:1});
				        },onUpdate:function() {
				        	var xval = rect._gsTransform.x;
				        	TweenLite.set("#mask-gevreche2", {x:-(xval)});
				        }},0.5)

				        svg_tl.timeScale(1.5).play();
			        }else{
			        	TweenMax.fromTo(sec.find('.content_section').eq(0),1,{y:App.h*obj.destination},{y:0});
			        }

			        TweenMax.set('#svg',{autoAlpha:1});
					TweenMax.fromTo(sec.find('.sub_title'),1,{x:-100,autoAlpha:0},{x:0,autoAlpha:1,delay:.6});
					TweenMax.fromTo(sec.find('.descr'),1,{x:-100,autoAlpha:0},{x:0,autoAlpha:1,delay:.6});
					TweenMax.fromTo(sec.find('.link_box'),1,{x:-100,autoAlpha:0},{x:0, autoAlpha:1,delay:.6});

					App.SectionsTransition.showFooterArrow(true);
					return 1;
				},
				outcoming:function (obj){
					var sec = obj.outcomingSection;

					if( App.SectionsTransition.is_chrome ){
						TweenMax.to('#svg',1,{autoAlpha:0});
					}else{
						TweenMax.to(sec.find('.content_section').eq(0),1,{y:App.h*obj.destination});
					}

					//svg_tl.reverse(0);



					TweenMax.to(sec.find('.sub_title'),.6,{x:100,autoAlpha:0});
					TweenMax.to(sec.find('.descr'),.6,{x:100,autoAlpha:0});
					TweenMax.to(sec.find('.link_box'),.6,{autoAlpha:0});

					return 1;
				}
			}
		));

		transition.addTransition( new Transition (
			{
				section:'#section_3',
				incoming:function (obj){
					var sec = obj.incomingSection;

					TweenMax.fromTo(sec, 1, {y:App.h*obj.destination}, {y:0});

					if(obj.prevId == 'section_4'){
						TweenMax.staggerFromTo(sec.find('.col'),1,{y:100,autoAlpha:0}, {y:0,autoAlpha:1}, .05);
					}else
						TweenMax.staggerFromTo(sec.find('.col'),1,{y:100,autoAlpha:0}, {y:0,autoAlpha:1,delay:.3}, .05);

					TweenMax.fromTo(sec.find('.sub_title'),1,{x:-100,autoAlpha:0},{x:0,autoAlpha:1,delay:.4});
					TweenMax.fromTo(sec.find('.descr'),1,{x:-100,autoAlpha:0},{x:0,autoAlpha:1,delay:.4});
					TweenMax.fromTo(sec.find('.link_box'),1,{x:-100,autoAlpha:0},{x:0, autoAlpha:1,delay:.6});

					return 1;
				},
				outcoming:function (obj){
					var sec = obj.outcomingSection;

					TweenMax.to(sec, 1, {y:-App.h*obj.destination})
					if(obj.currentId == 'section_4'){
						TweenMax.staggerTo(sec.find('.col'),1,{y:-300}, .05);
					}else
						TweenMax.staggerTo(sec.find('.col'),1,{z:-10,y:0,transformPerspective:200}, .2);

					TweenMax.to(sec.find('.sub_title'),.6,{x:100,autoAlpha:0});
					TweenMax.to(sec.find('.descr'),.6,{x:100,autoAlpha:0});
					TweenMax.to(sec.find('.link_box'),.6,{autoAlpha:0});

					return 1;

				}
			}
		));


		//setTimeout (App.SectionsTransition.transition.show,200,'home')
		App.SectionsTransition.transition.show('home');
	},


	showInterface : function ( sec, obj, force_open_footer ){
		var open = sec.attr('data-show-inner') == 'true';
		var footer_open = force_open_footer ? 'true' : sec.attr('data-show-footer') ==  'true';
		var footer = $('#footer').css({overflow:'hidden'});

		if ( open )$('body').addClass('inner');
		else $('body').removeClass('inner');


		if( !App.SectionsTransition.is_footer_open && footer_open){
			App.SectionsTransition.showFooter(true, 0 );
			App.SectionsTransition.is_footer_open = true;
		}else if( App.SectionsTransition.is_footer_open && !footer_open ){
			App.SectionsTransition.showFooter(false);
			App.SectionsTransition.is_footer_open = !App.SectionsTransition.is_footer_open;
		}

		if( !App.SectionsTransition.is_header_open && open){
			App.SectionsTransition.is_header_open = true;
		}else if( App.SectionsTransition.is_header_open && !open ){
			App.SectionsTransition.is_header_open = !App.SectionsTransition.is_header_open;
		}
	},

	lock : function (value){
		App.SectionsTransition.transition.lock( value );
	},

	showFooter : function ( value, del, time ){
		var footer = $('#footer');
		if( value ){
			TweenMax.fromTo(footer, time || 1, {y:footer.innerHeight()},{y:0,delay: del == undefined? 1 : 0});
			TweenMax.fromTo(footer.find('.top_footer'), time || 1, {y:-200},{y:0,delay: del == undefined? 1 : 0});
		}else{
			TweenMax.to(footer,time || 1, {y:footer.innerHeight()});
		}
	},

	showFooterArrow : function (value){
		var footer = $('#footer');
		value? footer.find('.top_footer').show() : footer.find('.top_footer').hide();
	}
}
