/*
	VERSION 1.0.0;
	
	init
		modal : ''  
		showModal : function 
		hideModal : function,
		popups :[]
		showPopup: function,
		hidePopup: function

*/
PopupUtil =  {
	settings : null,	
	current : '',

	init : function ( props ){
		if(!props)props = {};

		if(props.modal && typeof props.modal == 'string') props.modal = $(props.modal); 
		if(!props.popups && props.modal)
		{
			props.popups = [];
			props.modal.children().each( function (){
				props.popups.push( $(this) );
			})
			
		}

		
		PopupUtil.settings = props;
		if(props.modal)
		{	
			if(!props.items){
				props.items = [];
				props.modal.children().each(function (){
					props.items.push( 
						{
							selector: $(this)
						}
					);
				})
			}

			if(!props.showModal)PopupUtil.settings.showModal = function (  ){ props.modal.show(); }
			if(!props.hideModal)PopupUtil.settings.hideModal = function (  ){ props.modal.hide(); }	
		}

		if(!PopupUtil.settings.show) PopupUtil.settings.show = function ( popup ){ popup.show(); }
		if(!PopupUtil.settings.hide) PopupUtil.settings.hide = function ( popup ){ popup.hide(); }


	},

	default : function (){

	},

	show : function ( popup ){
		if( PopupUtil.settings.popups ){
			for (var i = 0,l = PopupUtil.settings.popups.length; i < l; i++) {
				var p = PopupUtil.settings.popups[i];
				if(p === PopupUtil.current)continue;
				PopupUtil.hide( $(p) ) 
			};
		}

		PopupUtil.current = popup;
		PopupUtil.settings.show( $(popup) );

		if(PopupUtil.settings.showModal)
			PopupUtil.settings.showModal();
	},

	hide : function ( popup ){
		PopupUtil.settings.hide( $(popup) );
	},

	hideAll : function (){
		PopupUtil.hideModal();
		for (var i = 0,l = PopupUtil.settings.popups.length; i < l; i++) {
			PopupUtil.hide( $(PopupUtil.settings.popups[i]) );
		};
	},

	hideModal : function (){
		if(PopupUtil.settings.hideModal)
			PopupUtil.settings.hideModal();
	},

	enableKeyboard : function ( value ){
		if ( value == false){
			$(document).off('keydown', keydown);
		}else{
			$(document).on('keydown', keydown);
		}

		function keydown (e){
			if (e.keyCode == 27) {
		        PopupUtil.hideAll();
		    }
		}
	}
}