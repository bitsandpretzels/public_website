function getViewportSize() {

    var viewportwidth;
    var viewportheight;

    // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight

    if (typeof window.innerWidth != 'undefined') {
        viewportwidth = window.innerWidth,
        viewportheight = window.innerHeight
    }

    // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
    else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0) {
        viewportwidth = document.documentElement.clientWidth,
        viewportheight = document.documentElement.clientHeight
    }   // older versions of IE
    else {
        viewportwidth = document.getElementsByTagName('body')[0].clientWidth,
        viewportheight = document.getElementsByTagName('body')[0].clientHeight
    }

    return {
        width: viewportwidth,
        height: viewportheight,
        isMobile : (navigator.userAgent.match(/i(ad|od|phone)|android/) ? true : false) || (viewportwidth <= 1024),
        fit : {width:viewportwidth+'px', height:viewportheight + 'px'}
    }
}

String.prototype.eval = function(data) {

    var res = '',
        self = this;

    while (res = self.match(/(\{\$([^}]*)\})/)) {

        if (data) {
            self = self.replace(res[0], (function() {
                return this[res[2]]; /* return eval.call( this[res[2]] ) */
            }).call(data))
        } else {
            self = self.replace(res[0], eval(res[2]))
        }

    }

    return self;
}

function prevent(e, propagation) {
    if (e && typeof e.preventDefault == 'function') {
        e.preventDefault();
    } else {
        if (window.event && window.event.returnValue) {
            window.event.returValue = false;
            e = window.event;
        }
    }
    if(propagation && e && typeof e.stopPropagation == 'function' ) e.stopPropagation();

    return false;
}

function getScrollTop() {
    if (typeof pageYOffset != 'undefined') {
        //most browsers
        return pageYOffset;
    } else {
        var B = document.body; //IE 'quirks'
        var D = document.documentElement; //IE with doctype
        D = (D.clientHeight) ? D : B;
        return D.scrollTop;
    }
}

function getDocHeight() {
    var D = document;
    return Math.max(
        D.body.scrollHeight, D.documentElement.scrollHeight,
        D.body.offsetHeight, D.documentElement.offsetHeight,
        D.body.clientHeight, D.documentElement.clientHeight
    );
}

function getSupportedTransform() {
    var prefixes = 'transform WebkitTransform MozTransform OTransform msTransform'.split(' ');
    var div = document.createElement('div');
    for(var i = 0; i < prefixes.length; i++) {
        if(div && div.style[prefixes[i]] !== undefined) {
            return prefixes[i];
        }
    }
    return false;
}


function backgroundResize($bgImg,props,right_element) {

    if (!$bgImg.length) return;

    var $bgImgParent = $bgImg.parent()

    if (typeof props.viewport == 'undefined') {

        props.viewport = getViewportSize()
    }

    if (props.viewport == 'parent') {

        props.viewport = {
            width: $bgImgParent.width(),
            height: $bgImgParent.height()
        }
        if(props.viewport.width == 0) {}
    }

    var viewport = props.viewport;

    var img_width = 0; //$bgImg.width()     //typeof props.image == 'undefined' ? $bgImg.width() : props.image.width
    var img_height = 0; //$bgImg.height()
    var left =  typeof props.left != 'undefined' ? props.left : "marginLeft",
        top = typeof props.top != 'undefined' ? props.left : "marginTop";

    if (typeof props.image != 'undefined') {
        img_width = props.image.width
        img_height = props.image.height
    } else {
        img_width = $bgImg.width()
        img_height = $bgImg.height()
    }

    if (img_width * img_height <= 0) return;

    var w_ratio = viewport.width / img_width,
        h_ratio = viewport.height / img_height;
    var _css = {};

    var setPx = typeof props.setPx == 'function' ? props.setPx :
            function(val) {
                return Math.floor(val) + 'px'
        }
    var setProps = typeof props.modifier == 'function' ? props.modifier :
        function($_img, props) {
            $_img.css(props)
        }

    var master_ratio = 1;

    if (w_ratio * img_height < viewport.height) {

        _css = {
            width: setPx(img_width * h_ratio),
            height: setPx(viewport.height)
        }
        _css[top] = 0;
        _css[left] = setPx((viewport.width - img_width * h_ratio) * .5);

        master_ratio = h_ratio;

    } else {

        _css = {
            width: setPx(viewport.width),
            height: setPx(img_height * w_ratio)
        }

        _css[left] = 0;
        _css[top] = setPx((viewport.height - img_height * w_ratio) * .5)

         master_ratio = w_ratio;
    }


    if (right_element == true) { 

        _css = {
            transform: 'translate('+Math.round(-(viewport.width - img_width)*.5)+'px, '+Math.round((viewport.height - img_height)*.5)+'px) scale(' + master_ratio + ')'
        }

    } else {
        _css = {
            transform: 'translate('+Math.round((viewport.width - img_width)*.5)+'px, '+Math.round((viewport.height - img_height)*.5)+'px) scale(' + master_ratio + ')'
        }
    }
       
  

    setProps($bgImg, _css, {
        scale : master_ratio,
        x : Math.round((viewport.width - img_width)*.5),
        y : Math.round((viewport.height - img_height)*.5)
    })
}


function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toGMTString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
    }
    return "";
}

function preloadimages(obj, cb) {
  var loaded = 0;
  var toload = 0;
  var images = obj instanceof Array ? [] : {};

  for (var i in obj) {
    toload++;
    images[i] = new Image();
    images[i].src = obj[i];
    images[i].onload = load;
    images[i].onerror = load;
    images[i].onabort = load;
  }

  function load() {
    if (++loaded >= toload) cb(images);
  }
}

function addCss(obj,style) {
   var el = document.getElementById(obj),
       css = style 
     for(i in css){
        el.style[i] = css[i]; 
     }
}

function  removeAttr(obj,atrb) {
   var el = document.getElementsByClassName(obj);
       el.removeAttribute(atrb); 
}
