
/*    
    var form = $('#form_id').form(
        {
            captcha : '#captcha_img',
            error : 'error',
            correct : function (){},             
            success : success,
            ajax : {
            }, 
        }
    )    
*/

jQuery.fn.form = function( props ) {
    var version = '2.0.6';
    var has_send = false,
        lock = false,
        form,captcha,captcha_src,items,submit,file,data,on_complete,error,correct,error_class;

    if(!props) throw 'form  need props';

    form        = this;
    submit      = props.submit ?  typeof props.submit == 'string'? $(props.submit) : props.submit : form.find(':submit');
    file        = form.find(':file');
    items       = form.find('input, textarea, select').not('[type=submit]');
    data        = props.ajax || {};


    if(props.error){
       if(typeof props.error == 'string'){
            error_class = props.error;
            error = function ( target, msg ) { target.addClass( error_class ); }
       }
       else if(typeof props.error == 'function') error = props.error;
    } 

    if(props.correct){
       if(typeof props.correct == 'string' && error_class) correct = function ( target, msg ) { target.removeClass( error_class ); }
       if(typeof props.correct == 'function') correct = props.correct;
    }else if (!props.correct)
        correct = function ( target ) {  }

    
    data.url = props.url ||  form.attr('action');
    if(!data.method) data.method = form.attr('method') || 'POST';
    if(!data.dataType) data.dataType = 'JSON';

   
    data.success = function (d){
        reloadCaptcha();   
        correctAll();

        if( props.success )
            props.success ( d, obj );

    }
    data.fail = function () {reloadCaptcha(); console.log('fail'); }


    if(props.captcha)
        addCaptcha ( props.captcha );


    submit.on('click', send);

    function send (e)
    {        
        if(e)e.preventDefault();
        if(lock)return;

        if(file.length > 0)
        {
            var fd = new FormData( form[0] ) 
            
            data.contentType    = false;
            data.processData    = false;
            data.mimeType       = 'multipart/form-data';

            if( file.length == 1 )
            {
                fd.append( 'file' , file[0].files[0] );
            }else{            
                for (var i = 0, l=file.length; i < l; i++)  fd.append( 'file_'+(i+1) , file.eq(i)[0].files[0] );                
            }

            data.data = fd;    
        }else
            data.data = form.serialize();
        
        $.ajax ( data );
    }

    function addCaptcha ( c )
    {
        captcha = typeof c == 'string'? $(c) : c;
        captcha_src = captcha.attr('src');
        captcha.css({cursor: 'pointer'}).on('click', reloadCaptcha);
    }

    function reloadCaptcha ( e )
    {
        if(e)e.preventDefault();
        if(captcha && captcha_src)
            captcha.attr('src', captcha_src + '?r=' + new Date().getTime());
    }


    function errorById( names )
    {
        if(!error)return;
        if( typeof names == 'string')
        {
            error ( form.find('#'+names) );
        }else if(  Object.prototype.toString.call( names ) == '[object Array]'  ){
             for (var i = 0, l=names.length; i < l; i++) error ( form.find('#'+names[i]) );
        }else {
            for( var key in names)
            {

                error ( form.find('#'+key), names[key]);
            }
        }
    }

    function errorByName ( names )
    {
        if(!error)return;
        if( typeof names == 'string')
        {
            error ( form.find('[name='+names+']') );
        }else if(  Object.prototype.toString.call( names ) == '[object Array]'  ){
             for (var i = 0, l=names.length; i < l; i++) error ( form.find('[name='+names[i]+']') );
        }else {
            for( var key in names)
            {
                error ( form.find('[name='+key+']'), names[key]);
            }
        }
    }

    function correctByName ( names )
    {
        if(!correct)return;
        if( typeof names == 'string')
        {
            correct ( form.find('[name='+names+']') );
        }else if(  Object.prototype.toString.call( names ) == '[object Array]'  ){
             for (var i = 0, l=names.length; i < l; i++) success ( form.find('[name='+names[i]+']') );
        }else {
            for( var key in names)
            {

                correct ( form.find('[name='+key+']'), names[key]);
            }
        }
    }

    function correctById ( names )
    {
        if(!correct)return;
        if( typeof names == 'string')
        {
            correct ( form.find('[#'+names+']') );
        }else if(  Object.prototype.toString.call( names ) == '[object Array]'  ){
             for (var i = 0, l=names.length; i < l; i++) correct ( form.find('[#'+names[i]+']') );
        }else {
            for( var key in names)
            {
                correct ( form.find('[#'+key+']'), names[key]);
            }
        }
    }

    function correctAll ()
    {
        for (var i = 0, l=items.length; i < l; i++) correct ( items.eq(i));
    }

    function clear (){
        for (var i = 0, l=items.length; i < l; i++) items.eq(i).val('');
    }

    function enable (value)
    {
        lock = !value;
    }

    var obj = 
     {
        addCaptcha     : addCaptcha,
        reloadCaptcha  : reloadCaptcha,
        errorByName    : errorByName,
        errorById      : errorById,
        correctByName  : correctByName,
        correctById    : correctById,
        correctAll     : correctAll,
        enable         : enable,
        clear          : clear,
        send           : send,
        getForm        : function (){ return form; }
    }


    return obj;

}