/*
	{
			items: items,
			next: '#next_button',
			prev: '#prev_button',
			change: onChange,
			enableDraging : 100,
			enableKeyboard : true,
			controler : ['#slides_dot ul', '<li><div class="dot"></div></li>','dot','active'],
			time : 7
		}
*/

var SliderUtil = function ( prop ){
	var version = '1.0.4';
	var select_image,
		prev_image,
		list,
		max,
		current,
		btn_next,
		btn_prev,
		change,
		animIn,
		animOut,
		lock,dots,
		time,u_id,last_destination,next_key,prev_key;
	

	last_destination = 1;	
	current		= 0;



	if(!prop || !prop.items)
	{
		return;	
	}

	list 		= typeof prop.items == 'string'? $(prop.items) : prop.items; 
	max = list.length;
	
	prev_image  = list.eq(current); 
	time		= prop.time || 0;


	if( prop.next ){
		btn_next 	= typeof prop.next == 'string'? $(prop.next) : prop.next;
		btn_next.on('click', next);
	}
	if( prop.prev)
	{
		btn_prev 	= typeof prop.prev == 'string'? $(prop.prev) : prop.prev;
		btn_prev.on('click', prev);
	}

	if( prop.enableKeyboard == true)
	{
		enableKeyboard ( 39, 37 );
	}
	
	if(prop.enableDraging)
	{
		enableDraging ( prop.enableDraging )
	}

	if(prop.controler)
	{
		addControler(prop.controler[0],prop.controler[1],prop.controler[2],prop.controler[3]);
	}

	hideOthers(current);
	autoSlide ();

	function next (event)
	{
		if(lock)return;
		if(event){
			clearAutoSlide();
			event.preventDefault();
		}
		var next = current + 1;
		if(next >= max) next = 0;
		last_destination = 1;
		show(next,false);
	}


	function prev (event)
	{
		if(lock)return;
		if(event){
			clearAutoSlide();
			event.preventDefault();
		}
		var prev = current - 1;
		if(prev < 0) prev = max-1;
		last_destination = -1;
		show(prev,false);
	}

	function show  (id, update)
	{

		if(lock)return;
		if(update == undefined)
			last_destination = id > current ? 1 : -1;

		

		if(current == id)return;
		current = parseInt(id);
		select_image = list.eq(current);

		if(animIn){
			var in_to = animIn.to;
			if(animIn.onStart){
				in_to.onStart = animIn.onStart;
				in_to.onStartParams = [select_image];
			}
			if(animIn.onComplete){
				in_to.onComplete = animIn.onComplete;
				in_to.onCompleteParams = [select_image];
			}

			TweenMax.fromTo(select_image, animIn.time, animIn.from, in_to);
		}
		if(animOut && prev_image){
			var out_to = animOut.to;
			if(animOut.onStart){
				out_to.onStart = animOut.onStart;
				out_to.onStartParams = [prev_image];
			}
			if(animOut.onComplete){
				out_to.onComplete = animOut.onComplete;
				out_to.onCompleteParams = [prev_image];
			}
			TweenMax.fromTo(prev_image, animOut.time, animOut.from, animOut.to);

		}	
		if(prop.change)
			prop.change (select_image,prev_image, current, last_destination);	


		prev_image = select_image;	
		updateControler ();
		autoSlide();
	}

	function autoSlide (){
		if(!time)return;

		if(u_id)clearTimeout(u_id);
		u_id = setTimeout(last_destination == -1?prev : next,time*1000);	
	}

	function clearAutoSlide (){
		if(u_id)clearTimeout(u_id)	;
		time = undefined;
	}

	function hideOthers (id)
	{
		list.each(function (index){
			//$(this).css({display: index == id? 'block': 'none'})
		})
	}

	function animationIn (time, from, to, onStart, onComplete)
	{
		animIn	= {time: time, from : from, to : to, onStart : onStart, onComplete : onComplete};
		return thisObj;
	}

	function animationOut (time, from, to, onStart, onComplete)
	{
		animOut	= {time: time, from : from, to : to, onStart : onStart, onComplete : onComplete};
		return thisObj;
	}

	function enableKeyboard (next, prev)
	{
		if(next == false)
		{
			$(window).off('keydown',keyDown);
		}else{
			$(window).on("keydown",keyDown)	
			next_key = next;
			prev_key = prev;	
		}
	}

	var drag_coef
	function enableDraging (value)
	{
		var par = list.parent();

		if(value == false)
		{
			par.off('mousedown', startDrag)
			par.off('mousemove', processDrag);
			$(window).off('mouseup', endDrag);
		}else{
			drag_coef = value == true? 100 : parseInt(value);
			par.on('mousedown', startDrag);
			par.on('mousemove', processDrag);
			$(window).on('mouseup', endDrag);
		}
	}

	var drag_x;
	var drag_progress = false;

	function startDrag (e){
		drag_x = e.pageX;
		drag_progress = true;
	}

	function processDrag (e){
		if(!drag_progress)return;
		var dx = drag_x - e.pageX;

		if( dx > drag_coef )
		{
			next(e);
			drag_progress = false;
		}else if ( dx < -drag_coef ){
			prev(e);
			drag_progress = false;
		}
	}

	function endDrag (e)
	{
		drag_progress = false;
	}


	function keyDown (event){
		if(event.keyCode == next_key){next(event);}
		if(event.keyCode == prev_key){prev(event);}
	}

	function getCurrent ()
	{
		return list.eq(current);
	}

	function getPrev ()
	{
		return list.eq(current-1 < 0 ? max-1 : current-1);
	}

	function getNext ()
	{
		return list.eq(current+1 >= max? 0 : current+1);
	}

	function resetTime ()
	{
		autoSlide();
	}

	function update ()
	{
		hideOthers(current)
	}

	function enable (value)
	{
		lock = !value;
	}

	function addControler (target_to_append, append_string, class_to_find, class_to_add)
	{
		var target = $(target_to_append);
		for(var i =0; i < max;i ++)
		{
			target.append(append_string);
		}

		dots = target.find('.'+class_to_find);
		dots_class = class_to_add;

		dots.each( function (index) {$(this).data('id', index);} ).on('click', function (e){ e.preventDefault();

		 show ( $(this).data('id')  )})

		updateControler();
	}



	function updateControler ()
	{
		if(!dots || !dots_class)return;
		dots.removeClass(dots_class);
		dots.eq( current ).addClass( dots_class );
	}

	var thisObj = {
		next : next,
		prev : prev,
		show : show,
		change : change,
		animationIn : animationIn,
		animationOut : animationOut,
		enableKeyboard : enableKeyboard,
		enableDraging : enableDraging,
		getCurrent : getCurrent,
		getPrev : getPrev,
		getNext : getNext,
		resetTime : resetTime,
		update : update,
		enable : enable

	}


	return thisObj;
}