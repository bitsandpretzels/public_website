var KeyboardUtil = {
	map : [],
	keyboard_event : false,
	mousemove_event : false,
	mouse : {x:0, y:0 },

	add : function (props ){
		var setting = props || {};
		if(!setting.keys)return;
		var k = Object.prototype.toString.call( setting.keys ) === '[object Array]'? setting.keys : [setting.keys];

		k = k.map(function(value){
			return typeof value == 'number'? value : value.charCodeAt(0)
		})

		var obj = {requestOver:true, lock:false};
		obj.keys = k;

		
		if( setting.down )
			obj.down = setting.down;
		if( setting.up )
			obj.up = setting.up;
		if( setting.target ){
			obj.target = typeof setting.target == 'string'? $(setting.target) : setting.target;
		}
		if( setting.condition ) obj.condition = setting.condition;
		if( setting.lock != undefined)obj.lock = setting.lock;
		if( setting.requestOver != undefined )obj.requestOver = setting.requestOver;

		KeyboardUtil.map.push( obj );

		if( !KeyboardUtil.keyboard_event )
		{
			$(window)
				.on('keydown', KeyboardUtil.down)
				.on('keyup', KeyboardUtil.up);

			KeyboardUtil.keyboard_event = true;
		}
		if( !KeyboardUtil.mousemove_event && obj.target)
			$(window).on('mousemove', KeyboardUtil.mousemove);
	},

	down : function ( e ){
		var key = 'which' in e ? e.which : e.keyCode;
		for (var i = 0; i < KeyboardUtil.map.length; i++) {
			var o = KeyboardUtil.map[i];
			if(!o.down || o.lock)continue;
			for (var k = 0; k < o.keys.length; k++) {
				if( key === o.keys[k] ){
					/*if( o.condition ){
						if( condition(o.target) ){
							o.down( KeyboardUtil.keyCodeToString( key )  )
							break;
						}
					}	*/				
						
					if( o.target && o.requestOver)
					{
						if( (KeyboardUtil.mouse.x >= o.target.offset().left && KeyboardUtil.mouse.x <= o.target.offset().left + o.target.width() && KeyboardUtil.mouse.y >= o.target.offset().top && KeyboardUtil.mouse.y <= o.target.offset().top + o.target.height()) )
						{
							if( o.condition ){
								if( o.condition(o.target) ){
									o.down( KeyboardUtil.keyCodeToString( key )  )
									break;
								}else
									continue;
							}else	
								o.down( KeyboardUtil.keyCodeToString( key )  )
							break;
						}
					}else if( o.condition ){
						if( o.condition(o.target) ){
							o.down( KeyboardUtil.keyCodeToString( key )  )
							break;
						}
					}else{
						o.down( KeyboardUtil.keyCodeToString( key )  )
						break;
					}
					
				}
			}
		}
	},

	up : function ( e ){
		var key = 'which' in e ? e.which : e.keyCode;

		for (var i = 0; i < KeyboardUtil.map.length; i++) {
			var o = KeyboardUtil.map[i];
			if(!o.up)continue;
			for (var k = 0; k < o.keys.length; k++) {
				if( key === o.keys[k] ){
					o.up( KeyboardUtil.keyCodeToString( key )  )
					break;
				}

			}
		}
	},

	remove : function ( target ){
		var t = typeof target == 'string'? $(target) : target
		for (var i = KeyboardUtil.map.length - 1; i > - 1; i--) {
			var o = KeyboardUtil.map[i];
			if( o.target && t.is(o.target) )
			{
				KeyboardUtil.map.splice( i,1 );
				break;
			}
		};

		if( KeyboardUtil.map.length == 0 )
			KeyboardUtil.map.destroy();
	},

	destroy : function ( ){
		KeyboardUtil.mousemove_event = false;
		KeyboardUtil.keyboard_event = false;
		KeyboardUtil.map = [];
		$(window)
			.off('keydown', KeyboardUtil.down)
			.off('keyup', KeyboardUtil.up)
			.off('mousemove', KeyboardUtil.mousemove);
	},

	lock : function ( target )
	{
		var t = typeof target == 'string'? $(target) : target
		for (var i = KeyboardUtil.map.length - 1; i > - 1; i--) {
			var o = KeyboardUtil.map[i];
			if( o.target && t.is(o.target) )
			{
				KeyboardUtil.map[i].lock = true;
				break;
			}
		};
	},

	unlock : function ( target )
	{
		var t = typeof target == 'string'? $(target) : target
		for (var i = KeyboardUtil.map.length - 1; i > - 1; i--) {
			var o = KeyboardUtil.map[i];
			if( o.target && t.is(o.target) )
			{
				KeyboardUtil.map[i].lock = false;
				break;
			}
		};
	},

	mousemove : function (e) {
		KeyboardUtil.mouse.x = e.pageX;
		KeyboardUtil.mouse.y = e.pageY;
	},

	keyCodeToString : function ( key ){
		for( k in KeyCode ){
			if ( KeyCode[k] === key )
				return k;
		}
		return String.fromCharCode(key);
	}
}

var KeyCode = {
	BACKSPACE:  8,
	TAB: 9,
	ENTER: 13,
	SHIFT: 16,
	CONTROL: 17,
	CAPSLOCK:20,
	ESC:27,
	SPACEBAR: 32,
	PAGEUP:   33,
	PAGEDOWN:  34,
	END:  35,
	HOME: 36,
	LEFTARROW:37,
	UPARROW:  38,
	RIGHTARROW:  39,
	DOWNARROW:   40,
	INSERT:  45,
	DELETE:  46,
	NUMLOCK:144,
	SCRLK:  145,
	PAUSE_BREAK: 19,
	NUMPAD_0:  96,
	NUMPAD_1:  97,
	NUMPAD_2:  98,
	NUMPAD_3:  99,
	NUMPAD_4:  100,
	NUMPAD_5:  101,
	NUMPAD_6:  102,
	NUMPAD_7:  103,
	NUMPAD_8:  104,
	NUMPAD_9:  105,
	NUMPAD_MULTIPLY: 106,
	NUMPAD_ADD:  107,
	NUMPAD_ENTER:  13,
	NUMPAD_SUBTRACT: 109,
	NUMPAD_DECIMAL: 110,
	NUMPAD_DIVIDE: 111,
	F1: 112,
	F2: 113,
	F3: 114,
	F4: 115,
	F5: 116,
	F6:  117,
	F7: 118,
	F8: 119,
	F9: 120,
	F11:  122,
	F12:  123,
	F13:  124,
	F14:  125,
	F15:  126,
}
