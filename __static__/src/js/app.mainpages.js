App.MainPages = {

	getvp_width : 0,
    mob : false,

    init : function () {

    	App.MainPages.bind();
        App.MainPages.initParallax();
    },

    bind : function () {

    	$(window).resize(App.MainPages.resize);
    	App.MainPages.resize();
        $(window).on('scroll', App.MainPages.fixedMenu);
        $('.sub_nav .link_nav').on('click',App.MainPages.setMenuSection);
    },

    setMenuSection : function() {

        var self = $(this);

        $('.sub_nav').find('.link_nav').each( function(){

            var s = $(this);
            if(self.is(s))return;
            s.removeClass("active");
        })

        self.addClass('active'); 
    },

    resize : function (){

    	App.MainPages.getvp_width = getViewportSize().width;
         	
    	App.MainPages.setHeightTopSection(App.MainPages.getvp_width >= 1023);

        App.MainPages.mob = $(window).width() <= 1024;


        if(App.MainPages.mob){

            $('html').addClass('mobile');
                       
        }else{
            $('html').removeClass('mobile');            
        }
    },

    setHeightTopSection : function (resize) {

        var inner = $('#section_top').data('inner');

    	if(resize){

    		var minheight = 670;
		    var window_height = $(window).height();
		    var top_section = Math.max(minheight,window_height);
		    
		  	$('#section_top').css('height',top_section);

	  	}else{
          
  	        $('#section_top').css('height',670);  
                    
            if((inner) && (App.MainPages.getvp_width <=640 ))
                $('#section_top').css('height',500);

	  	}
	},

    fixedMenu : function (){

        var top = $(window).scrollTop();
        var top_menu_height = $('#main_nav_inner').innerHeight();
        var home_section = $('#section_1').length;
        


        if(top > (top_menu_height) && top_menu_height !== null)
        {
            $('#sub_nav').addClass('fixed');
            $('#linkMenu').addClass('link_menu_fixed');
            $('#main_nav_inner').css('display', 'none');
        }
        else if(top_menu_height !== null){
            $('#sub_nav').removeClass('fixed');
            $('#linkMenu').removeClass('link_menu_fixed');
            $('#main_nav_inner').css('display', 'block');
        }

        if(home_section !== 0 && top > $('#section_1').offset().top && top_menu_height === null){
            $('#linkMenu').addClass('link_menu_dark');
        }else if(home_section !== 0 && top < $('#section_1').offset().top && top_menu_height === null){
            $('#linkMenu').removeClass('link_menu_dark');
        }

    },

    initParallax : function(){

        if(!($('html').hasClass('mobile')))
        {
        
            ParallaxUtil.fromTo( '.paralax_section', {top: "top+1", bottom:'bottom+1'}, {y:50, autoAlpha:0}, {y:0,autoAlpha:1}, {time:1.2, line:0.9} );

        }
    }

}
