<div id="linkMenu" class="link_menu link_menu_inner">
    <span class="line1"></span>
    <span class="line2"></span>
    <span class="line3"></span>
</div>

<div id="header_nav" class="header_nav">
	<ul class="ul-reset">
		<li>
			<a href="" class="link active">Program</a>
		</li>
		<li>
			<a href="<?php echo Router::getRoute('speakers'); ?>" class="link">Speakers</a>
		</li>
		<li>
			<a href="{url controller=List action=captains}" class="link">Table Captains</a>
		</li>
		<li class="sub-nav-link">
			<a href="" class="link">Who's going</a>
			<span class="icon-arrow_right"></span>
			<ul class="sub-nav ui-rest">
				<li>
					<a href="{url controller=List action=attendees}" class="link inner">Attendees</a>
				</li>
				<li>
					<a href="{url controller=List action=investors}" class="link inner">Investors</a>
				</li>
			</ul>
		</li>
		<li>
			<a href="{url controller=Info action=pitch}" class="link">Pitch</a>
		</li>
		<li>
			<a href="{url controller=Info action=exhibition}" class="link">Exhibition</a>
		</li>
		<li>
			<a href="" class="link">Program</a>
		</li>
		<li>
			<a href="{url controller=Info action=volunteers}" class="link">Volunteers</a>
		</li>
		<li>
			<a href="" class="link">Hotel</a>
		</li>
		<li>
			<a href="" class="link">Directions</a>
		</li>
		<li>
			<a href="{url controller=Info action=partners}" class="link">Partners</a>
		</li>
	</ul>
</div>

<div class="left_nav">
	<div class="link active">
		<div class="line"></div>
	</div>
	<div class="link">
		<div class="line"></div>
	</div>
	<div class="link">
		<div class="line"></div>
	</div>
	<div class="link">
		<div class="line"></div>
	</div>
</div>

