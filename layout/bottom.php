    </div> <!-- #main  layout/top.php -->
    <script src="/src/js/vendor.js?v=<?php echo $currentScriptVersion; ?>"></script>
    <!-- RS5.0 Core JS Files -->
    <script src="/revolution/js/jquery.themepunch.tools.min.js?rev=5.0"></script>
    <script src="/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0"></script>
    <script async src="/src/js/main.js?v=<?php echo $currentScriptVersion; ?>"></script>
    <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<?php
$host = $_SERVER['HTTP_HOST'];
if ($host !== 'localhost:8080') { ?>

<!--    <script src="/src/js/dist.min.js?v=<?php echo $currentScriptVersion; ?>"></script>-->
    <!-- RS5.0 Core JS Files -->
    <script>
        jQuery(document).ready(function() { 
           jQuery("#slider1").revolution({
              sliderType:"standard",
              sliderLayout:"auto",
              delay:9000,
              navigation: {
                  arrows:{enable:true} 
              }, 
              gridwidth:1230,
              gridheight:720 
            }); 
        }); 
    </script>

    <!-- Google tracking stuff -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-53161248-1', 'auto');
      ga('create', 'UA-67417047-3', {'name':'b'});
      ga('set', 'anonymizeIp', true);
      ga('send', 'pageview');
      ga('b.send', 'pageview');
    </script>

    <script type="text/javascript">
      /* <![CDATA[ */
      var google_conversion_id = 928324056;
      var google_custom_params = window.google_tag_params;
      var google_remarketing_only = true;
      /* ]]> */
    </script>

    <div style="display: none;">
      <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
    </div>
    <noscript>
      <div style="display:inline;display: none">
        <img height="1" width="1" style="border-style:none;display: none" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/928324056/?value=0&amp;guid=ON&amp;script=0"/>
      </div>
    </noscript>

      <!-- Twitter universal website tag code -->
    <script>
    !function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
    },s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
    a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
    // Insert Twitter Pixel ID and Standard Event data below
    twq('init','nwajx');
    twq('track','PageView');
    </script>
    <!-- End Twitter universal website tag code -->
   
    <!-- Hotjar Tracking Code for www.bitsandpretzels.com -->
    <script>
      !function(t,h,e,j,n,s){t.hj=t.hj||function(){(t.hj.q=t.hj.q||[]).push(arguments)},t._hjSettings={hjid:38470,hjsv:5},n=h.getElementsByTagName("head")[0],s=h.createElement("script"),s.async=1,s.src=e+t._hjSettings.hjid+j+t._hjSettings.hjsv,n.appendChild(s)}(window,document,"//static.hotjar.com/c/hotjar-",".js?sv=");
    </script>

    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
      n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
      document,'script','//connect.facebook.net/en_US/fbevents.js');
      fbq('init', '1496886453927897');
      fbq('track', "PageView");
      <?php if(Router::getCurrentRoute() === '/success'): ?>
      	fbq('track', 'Purchase', {value: '0.00', currency: 'EUR'});
      <?php elseif (strpos(Router::getCurrentRoute(), 'success') !== false): ?>
      	fbq('track', 'CompleteRegistration');
      <?php elseif (strpos(Router::getCurrentRoute(), 'ticket') !== false): ?>
      	fbq('track', 'Lead');
        <?php elseif (strpos(Router::getCurrentRoute(), '') !== false): ?>
        fbq('track', 'InitiateCheckout');
      <?php endif; ?>
    </script>
    
    <!-- This site is converting visitors into subscribers and customers with OptinMonster - http://optinmonster.com -->
    <script>var om56c072a3b929b,om56c072a3b929b_poll=function(){var r=0;return function(n,l){clearInterval(r),r=setInterval(n,l)}}();!function(e,t,n){if(e.getElementById(n)){om56c072a3b929b_poll(function(){if(window['om_loaded']){if(!om56c072a3b929b){om56c072a3b929b=new OptinMonsterApp();return om56c072a3b929b.init({"s":"15618.56c072a3b929b","staging":0,"dev":0,"beta":0});}}},25);return;}var d=false,o=e.createElement(t);o.id=n,o.src="//a.optnmnstr.com/app/js/api.min.js",o.onload=o.onreadystatechange=function(){if(!d){if(!this.readyState||this.readyState==="loaded"||this.readyState==="complete"){try{d=om_loaded=true;om56c072a3b929b=new OptinMonsterApp();om56c072a3b929b.init({"s":"15618.56c072a3b929b","staging":0,"dev":0,"beta":0});o.onload=o.onreadystatechange=null;}catch(t){}}}};(document.getElementsByTagName("head")[0]||document.documentElement).appendChild(o)}(document,"script","omapi-script");</script>
    <!-- / OptinMonster -->

    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1496886453927897&ev=PageView&noscript=1" /></noscript>

    <?php if(Router::getCurrentRoute() !== '/'): ?>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/573599b38ac4d39f7555654f/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
    <?php endif; ?>

<?php
}
?>
  </body>
</html>