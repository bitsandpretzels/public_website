<div class="sectionContent">
        <div class="swiperSection">
            <div class="swiper-container swiper-container-diashow">
                <div class="swiper-wrapper">
                    <?php for($i = 1; $i <= 14; $i++): ?>
                        <div class="swiper-slide cdSlide">
                            <img class="lazyload-scroll" data-src="/src/images/diashow/<?= $i ?>.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" alt="Diashow"/>
<!--
                            <noscript>
                                <img src="/src/images/diashow/<?= $i ?>.jpg" alt="Diashow">
                            </noscript>
-->
                        </div>

                    <?php endfor; ?>
                </div>
                <div class="swiper-pagination"></div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
    </div>
<div class="clearfix point-none"></div>    