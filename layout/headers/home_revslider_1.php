<!-- START REVOLUTION SLIDER 5.0 -->
<section class="header-slider" style="min-height:100vh";>
    <div id="rev_slider_202_1_wrapper" class="rev_slider_wrapper fullscreen-container"
       data-alias="concept1" style="background-color:#fff;padding:0px;">
        <!-- START REVOLUTION SLIDER 5.1.1RC fullscreen mode -->
        <div id="rev_slider_202_1" class="rev_slider fullscreenbanner" style="display:none;"
           data-version="5.1.1RC">
            <ul>
                <!-- START: FIRST SLIDE WITH LOGO -->
                <li
                    data-index="rs-672"
                    data-delay="7000"
                    data-transition="fade"
                    data-slotamount="default"
                    data-easein="default"
                    data-easeout="default"
                    data-masterspeed="default"
                    data-thumb="src/images/oktoberfest_hero.jpg?v=<?php echo $currentScriptVersion; ?>"
                    data-rotate="0"
                    data-saveperformance="off"
                    data-title="Bits & Pretzels"
                    data-description="">

                    <!-- MAIN IMAGE -->
                    <img src="src/images/oktoberfest_hero.jpg?v=<?php echo $currentScriptVersion; ?>" alt=""
                        data-bgposition="center center"
                        data-bgfit="cover"
                        data-bgrepeat="no-repeat"
                        data-bgparallax="5" class="rev-slidebg"
                        data-no-retina
                        >

                    <!-- LAYERS -->

                    <!-- LAYER B&P Logo -->
                    <div class="tp-caption Agency-Title tp-resizeme"
                           data-fontsize="['70','70','50','50']"
                           data-height="none"
                           data-hoffset="['0','0','0','0']"

                           data-voffset="['-65','-65','-65','-145']"
                           data-lineheight="['70','70','50','50']"
                           data-mask_in="x:0px;y:0px;"
                           data-responsive_offset="on"
                           data-splitin="none"
                           data-splitout="none"
                           data-start="1050"
                           data-transform_idle="o:1;"
                           data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power3.easeInOut;"
                           data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                           data-whitespace="nowrap"
                           data-width="none"
                           data-x="['center','center','center','center']"
                           data-y="['middle','middle','middle','middle']" id="slide-898-layer-2" style="z-index: 8; white-space: nowrap;">

                            <img style="text-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);"src="/src/images/bp_hero_logo.svg" alt=""
                        width="210" height="300"
                        data-ww="['210px','210px','210px','150px']" data-hh="300">
                        </div>

                     <!-- LAYER TAGLINE -->
                    <div class="tp-caption Concept-SubTitle tp-resizeme rs-parallaxlevel-2" id="slide-673-layer-4"
                        data-x="['center','center','center','center']"
                        data-hoffset="['0','0','0','0']"
                        data-y="['middle','middle','middle','middle']"

                        data-voffset="['25','25','25','25']"
                        data-fontsize="['72','60','47','67']"
                        data-lineheight="['68','60','47','67']"
                        data-width="none"
                        data-height="none"
                        data-whitespace="['nowrap','nowrap','nowrap','normal']"
                        data-transform_idle="o:1;"
                        data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeInOut;"
                        data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                        data-start="2400"
                        data-splitin="none"
                        data-splitout="none"
                        data-responsive_offset="on"
                        style="text-align:center; z-index: 60; white-space: nowrap; font-weight:900;letter-spacing:.15em; text-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);">
                        THE FOUNDERS FESTIVAL
                    </div>
                    
                    <!-- LAYER SUBHEADLINE -->
                    <div class="tp-caption Concept-SubTitle tp-resizeme rs-parallaxlevel-2" id="slide-673-layer-4"
                        data-x="['center','center','center','center']"
                        data-hoffset="['0','0','0','0']"
                        data-y="['middle','middle','middle','middle']"

                        data-voffset="['95','95','95','180']"
                        data-fontsize="['28','26','27','20']"
                        data-lineheight="['25','25','20','20']"
                        data-width="none"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-transform_idle="o:1;"
                        data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeInOut;"
                        data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                        data-start="2400"
                        data-splitin="none"
                        data-splitout="none"
                        data-responsive_offset="on"
                        style="z-index: 60; white-space: nowrap; font-weight:500;letter-spacing:.15em; text-shadow: 0 3px 3px rgba(0,0,0,0.5);">
                        SEP 24 - 26, 2017 IN MUNICH
                    </div>

                <div class="tp-caption" id="slide-1921-layer-6"
                    data-x="['center','center','center','center']"
                    data-hoffset="['0','0','0','0']"
                    data-y="['middle','middle','middle','middle']"

                    data-voffset="['132','112','112','198']"
                    data-width="['350','350','350','300']"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;"
                    data-transform_in="y:bottom;s:1000;e:Power4.easeOut;"
                    data-transform_out="opacity:0;s:300;"
                    data-start="1900"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="off"
                    data-responsive="off"
                    style="z-index: 10; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);">
                        <a style="width:47%;float:left;" href="https://youtu.be/Xu4b5nEeUIc" alt="play" data-lity href="#category" class="button button-blue button-small"><span>Watch Video</span></a>
                        <a style="width:47%;float:right;" href="#category" class="button button-orange button-small trigger_app_nav"><span><?php echo Meta::getButtonWording(); ?></span></a>
                    </div>
                    
                    
                    
                </li>
            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
    </div>
        <!-- END REVOLUTION SLIDER -->
    </section>
