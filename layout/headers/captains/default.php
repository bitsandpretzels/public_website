    <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <div class="title_section linkMenu"><?php if(isset($options['header_label'])) {echo $options['header_label'];} else { echo '';} ?></div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li" style="visibility: hidden">
                    <a href="<?php echo Router::getRoute('corporate'); ?>#partners" class="link_nav" data-section="investors">Startups</a>
                </li>
                <li class="hidden_li" style="visibility: hidden">
                    <a href="<?php echo Router::getRoute('corporate'); ?>#investors" class="link_nav" data-section="networking">Young Talents</a>
                </li>
                <li class="hidden_li" style="visibility: hidden">
                    <a href="<?php echo Router::getRoute('corporate'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li" style="visibility: hidden">
                    <a href="<?php echo Router::getRoute('corporate'); ?>#journalists" class="link_nav" data-section="captain">Media</a>
                </li>
                <li class="hidden_li" style="visibility: hidden">
                    <a href="<?php echo Router::getRoute('corporate'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>
                <li class="hidden_li" style="visibility: hidden">
                    <a href="<?php echo Router::getRoute('corporate'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                </li>        
                <li class="desktop_only">
                    <a class="link_nav buyticket trigger_app_nav" data-section="buyticket">Choose your Category</a>
                </li>
            </ul>
        </div>
    </div>