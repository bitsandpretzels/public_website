    <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
            <div class="title_section linkMenu"><span class="mobile_hidden">Choosen Category: </span>Startup</div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li">
                    <a href="<?php echo Router::getRoute('startup'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>                
                <li class="hidden_li">
                    <a href="<?php echo Router::getRoute('startup'); ?>#investors" class="link_nav" data-section="investors">Investors</a>
                </li>
                <li class="hidden_li">
                    <a href="<?php echo Router::getRoute('startup'); ?>#networking" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li">
                    <a href="<?php echo Router::getRoute('startup'); ?>#pitch" class="link_nav" data-section="pitc">Pitch</a>
                </li>

                <?php if(isset($options, $options['site_specific'], $options['site_specific']['show'], $options['site_specific']['show']['tc']) && $options['site_specific']['show']['tc']) : ?>
                    <li>
                        <a href="<?php echo Router::getRoute('startup'); ?>#captain" class="link_nav" data-section="captain">Table Captains</a>
                    </li>
                <?php endif; ?>

                <li class="hidden_li">
                    <a href="<?php echo Router::getRoute('startup'); ?>#academy" class="link_nav" data-section="academy">Academy</a>
                </li>
                <li class="hidden_li">
                    <a href="<?php echo Router::getRoute('startup'); ?>#business-partner" class="link_nav" data-section="connect">Connect</a>
                </li>   
                <li class="hidden_li">
                    <a href="<?php echo Router::getRoute('startup'); ?>#journalists" class="link_nav" data-section="media">Media</a>
                </li>                
                <li>
                    <a href="<?php echo Router::getRoute('buy_startup'); ?>" class="link_nav buyticket" data-section="buyticket">Buy Ticket</a>
                </li>
            </ul>
        </div>
    </div>