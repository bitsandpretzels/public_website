    <div id="sub_nav" class="sub_nav_wrap">

        <div class="wrap_subnav_inner">
            <div class="title_section linkMenu"><span class="mobile_hidden">Choosen Category: </span>Corporate</div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li">
                    <a href="<?php echo Router::getRoute('corporate'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li> 
                <li class="hidden_li">
                    <a href="<?php echo Router::getRoute('corporate'); ?>#investors" class="link_nav" data-section="networking">Young Talents</a>
                </li>                           
                <li class="hidden_li">
                    <a href="<?php echo Router::getRoute('corporate'); ?>#partners" class="link_nav" data-section="investors">Startups</a>
                </li>
                <li class="hidden_li">
                    <a href="<?php echo Router::getRoute('corporate'); ?>#agenda" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li">
                    <a href="<?php echo Router::getRoute('corporate'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                </li>                     
                <li class="hidden_li">
                    <a href="<?php echo Router::getRoute('corporate'); ?>#journalists" class="link_nav" data-section="captain">Media</a>
                </li> 
                <?php if(isset($options, $options['site_specific'], $options['site_specific']['show'], $options['site_specific']['show']['tc']) && $options['site_specific']['show']['tc']) : ?>
                    <li>
                        <a href="<?php echo Router::getRoute('corporate'); ?>#captain" class="link_nav" data-section="captain">Table Captains</a>
                    </li>
                <?php endif; ?>
                <li>
                    <a href="<?php echo Router::getRoute('buy_corporate'); ?>" class="link_nav buyticket" data-section="buyticket">Buy Ticket</a>
                </li>                
            </ul>
        </div>
    </div>