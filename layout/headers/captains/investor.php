    <div id="sub_nav" class="sub_nav_wrap">

        <div class="wrap_subnav_inner">
            <div class="title_section linkMenu"><span class="mobile_hidden">Choosen Category: </span>Investor</div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li">
                    <a href="<?php echo Router::getRoute('investor'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>            
                <li class="hidden_li">
                    <a href="<?php echo Router::getRoute('investor'); ?>#startups" class="link_nav" data-section="investors">Startups</a>
                </li>
                <?php if(isset($options, $options['site_specific'], $options['site_specific']['show'], $options['site_specific']['show']['tc']) && $options['site_specific']['show']['tc']) : ?>
                    <li>
                        <a href="<?php echo Router::getRoute('investor'); ?>#captain" class="link_nav" data-section="captain">Table Captains</a>
                    </li>
                <?php endif; ?>                
                <li class="hidden_li">
                    <a href="<?php echo Router::getRoute('investor'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li">
                    <a href="<?php echo Router::getRoute('investor'); ?>#investors" class="link_nav" data-section="pitc">Investors</a>
                </li>
                <li class="hidden_li">
                    <a href="<?php echo Router::getRoute('investor'); ?>#pitch" class="link_nav" data-section="pitch">Pitch</a>
                </li>
                <li class="hidden_li">
                    <a href="<?php echo Router::getRoute('investor'); ?>#journalists" class="link_nav" data-section="media">Media</a>
                </li>                
                <li>
                    <a href="<?php echo Router::getRoute('buy_investor'); ?>" class="link_nav buyticket" data-section="buyticket">Buy Ticket</a>
                </li>
            </ul>
        </div>
    </div>