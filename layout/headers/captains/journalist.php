    <div id="sub_nav" class="sub_nav_wrap">

        <div class="wrap_subnav_inner">
            <div class="title_section linkMenu"><span class="mobile_hidden">Choosen Category: </span>Journalist</div>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li">
                    <a href="<?php echo Router::getRoute('journalist'); ?>#speakers" class="link_nav" data-section="speakers">Speakers</a>
                </li>
                <li class="hidden_li">
                    <a href="<?php echo Router::getRoute('journalist'); ?>#attendees" class="link_nav" data-section="investors">Attendees</a>
                </li>
                <li class="hidden_li">
                    <a href="<?php echo Router::getRoute('journalist'); ?>#pitch" class="link_nav" data-section="pitch">Pitch</a>
                </li>                 
                <li class="hidden_li">
                    <a href="<?php echo Router::getRoute('journalist'); ?>#schedule" class="link_nav" data-section="networking">Matchmaking</a>
                </li>
                <li class="hidden_li">
                    <a href="<?php echo Router::getRoute('journalist'); ?>#academy" class="link_nav" data-section="speakers">Academy</a>
                </li> 
                <?php if(isset($options, $options['site_specific'], $options['site_specific']['show'], $options['site_specific']['show']['tc']) && $options['site_specific']['show']['tc']) : ?>
                    <li>
                        <a href="<?php echo Router::getRoute('journalist'); ?>#captain" class="link_nav" data-section="captain">Table Captains</a>
                    </li>
                <?php endif; ?>                               
                <li>
                    <a href="<?php echo Router::getRoute('journalist'); ?>#apply" class="link_nav buyticket" data-section="buyticket">Apply</a>
                </li>

            </ul>
        </div>
    </div>