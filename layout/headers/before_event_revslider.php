 <!-- START REVOLUTION SLIDER 5.0 -->
<section class="header-slider" style="min-height:100vh";>
    <div id="rev_slider_202_2_wrapper" class="rev_slider_wrapper fullscreen-container" 
       data-alias="concept1" style="background-color:#fff;padding:0px;">
        <!-- START REVOLUTION SLIDER 5.1.1RC fullscreen mode -->
        <div id="rev_slider_202_2" class="rev_slider fullscreenbanner" style="display:none;" 
           data-version="5.1.1RC">
            <ul>
                <!-- START: FIRST SLIDE WITH LOGO   -->
                <li 
                    data-index="rs-672" 
                    data-transition="fade" 
                    data-slotamount="default" 
                    data-easein="default" 
                    data-easeout="default" 
                    data-masterspeed="default" 
                    data-thumb="src/images/oktoberfest_thumb.jpg" 
                    data-rotate="0" 
                    data-saveperformance="off" 
                    data-title="Bits & Pretzels" 
                    data-description="">
                   
                    <!-- MAIN IMAGE -->
                    <img src="src/images/success_hero.png" alt="" 
                        data-bgposition="center center" 
                        data-bgfit="cover" 
                        data-bgrepeat="no-repeat" 
                        data-bgparallax="5" 
                        class="rev-slidebg" 
                        data-no-retina >
                        
                        <div class="rs-background-video-layer" 
                            data-forcerewind="on" 
                            data-volume="mute" 
                            data-ytid="T8--OggjJKQ" 
                            data-videoattributes="version=3&enablejsapi=1&html5=1& hd=1&wmode=opaque&showinfo=0& ref=0;;origin=http://server.local;" 
                            data-videorate="1.5" 
                            data-videowidth="100%" 
                            data-videoheight="100%" 
                            data-videocontrols="none" 
                            data-videostartat="00:00" 
                            data-videoendat="00:10" 
                            
                            data-forceCover="1"
                            data-aspectratio="16:9" 
                            data-autoplay="true" 
                            data-autoplayonlyfirsttime="false" 
                            data-nextslideatend="true"
                            data-videomp4="src/media/success.mp4"
                        ></div>
                    
                    <!-- LAYERS -->
                    <!-- LAYER B&P Logo -->
                    <div class="tp-caption tp-resizeme rs-parallaxlevel-2" id="slide-672-layer-2" 
                        data-x="['center','center','center','center']" 
                        data-hoffset="['0','0','0','0']" 
                        data-y="['middle','middle','middle','middle']" 
                         
                        data-voffset="['-100','-130','-160','-120']" 
                        data-width="none" 
                        data-height="none" 
                        data-whitespace="['nowrap','nowrap','nowrap','normal']" 
                        data-transform_idle="o:1;" 
                        data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" 
                        data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" 
                        data-start="500" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on" style="z-index: 7;">
                          <img src="/src/images/logo_bitsandpretzels_white.svg" alt=""
                            width="240" height="300"
                            data-ww="['240px','240px','240px','230px']" data-hh="300">
                    </div>
                    
                     <!-- LAYER TEXT -->
                    <div class="tp-caption Concept-SubTitle   tp-resizeme rs-parallaxlevel-2 center" id="slide-673-layer-4" 
                        data-x="['center','center','center','center']" 
                        data-hoffset="['0','0','0','0']" 
                        data-y="['middle','middle','middle','middle']" 
                         
                        data-voffset="['70','70','70','70']" 
                        data-fontsize="['60','60','50','40']" 
                        data-lineheight="['25','25','20','20']" 
                        data-width="none" 
                        data-height="none" 
                        data-whitespace="nowrap" 
                        data-transform_idle="o:1;" 
                        data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" 
                        data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" 
                        data-start="2500"
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on" style="z-index: 6; white-space: nowrap;">
                        WE ARE SOLD OUT!
                    </div>
                    
                      <!-- LAYER TEXT -->
                    <div class="tp-caption Concept-SubTitle   tp-resizeme rs-parallaxlevel-2 center" id="slide-673-layer-4" 
                        data-x="['center','center','center','center']" 
                        data-hoffset="['0','0','0','0']" 
                        data-y="['middle','middle','middle','middle']" 
                         
                        data-voffset="['150','150','150','150']" 
                        data-fontsize="['25','25','20','14']" 
                        data-lineheight="['25','25','20','20']" 
                        data-width="none" 
                        data-height="none" 
                        data-whitespace="nowrap" 
                        data-transform_idle="o:1;" 
                        data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" 
                        data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" 
                        data-start="8000"
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on" style="z-index: 6; white-space: nowrap;">
                    Don't miss the livestream: right here on Sep 25th, 10:45 am.
                    </div>
                            
                    <!-- SCROLL DOWN INDICATOR -->
                    <div class="tp-caption tp-resizeme rs-parallaxlevel-2" id="slide-672-layer-2" 
                        data-x="['center','center','center','center']" 
                        data-hoffset="['0','0','0','0']" 
                        data-y="['bottom','bottom','bottom','bottom']" 

                        data-voffset="['20px','20px','20px','0']" 
                        data-width="none" 
                        data-height="none" 
                        data-whitespace="['nowrap','nowrap','nowrap','normal']" 
                        data-transform_idle="o:1;" 
                        data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" 
                        data-transform_out="x:left(R);s:500;e:Power3.easeIn;s:500;e:Power3.easeIn;" 
                        data-start="1500" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on" style="z-index: 7;">
                         <a href="#content" target="_blank"> 
                            <img 
                            width="30" 
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAA4CAMAAACIelvlAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAXVBMVEUAAAD7+/vX19fY2Nj8/Pz5+fng4OD7+/vh4eH+/v7b29vw8PD8/PzGxsbFxcX09PTz8/Ps7OzPz8/k5OTo6OjIyMjHx8f////GxsbFxcXFxcXHx8f7+/v///8AAAD7qoQwAAAAHXRSTlMABDe+9PLN/CT+Leb9aXXs6xVI1d1eZv1ubHFi/QaY2vMAAAABYktHRACIBR1IAAAACXBIWXMAAAsSAAALEgHS3X78AAABNUlEQVRIx+3WYZuCIAwA4FEaUcu7rJTq9v//5g2QKy/A8d09fsDHvQ4RVwCJULDZNs12wwNhsGiJo5UbBTvSxMdOShTsjXFVjNkLjYIDTXGQz+yISIR4rHj8kwNEp5ol676Ivju5cHHu+3NNPoTbV9Vw+bVgjTXWSIWq+vqmbFXTR1xqf7ne5H30dr300HBTHGRGwcDJDYyoUWacQMQRLKERGS841QLcSfNgsZe6rotG090NrcREYcM6C+q4pFBDvU4xnubFW8qiiQlvEwkTzZqE+PdwnyK5QAWTXdLwojQ9Pi88vBhSN0uagsiYoojbbma8wMIWVPCcG/5l9uJZesnBdHFbLIpgfqIRiVkdofCf92Sk4mWs9ULUSrxxf8iMEYo/g3IRjOGQC29YjBXC7522ze2SXy3cHYp+DWOGAAAAAElFTkSuQmCC"
                            data-ww="['30px','30px','30px','30px']" 
                            data-hh="35px" >
                        </a>
                    </div>
                </li>
                <!-- END: FIRST SLIDE WITH LOGO   -->
                
            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
    </div>
        <!-- END REVOLUTION SLIDER -->
    </section>