 <!-- START REVOLUTION SLIDER 5.0 -->
<section class="header-slider silver" style="min-height:100vh";>
    <div id="rev_slider_202_1_wrapper" class="rev_slider_wrapper fullscreen-container" 
       data-alias="concept1" style="background-color:#fff;padding:0px;">
        <!-- START REVOLUTION SLIDER 5.1.1RC fullscreen mode -->
        <div id="rev_slider_202_3" class="rev_slider fullscreenbanner" style="display:none;" 
           data-version="5.1.1RC">
            <ul>
                <!-- START: FIRST SLIDE WITH LOGO   -->
                
               <!-- START: SPACEY SLIDE  -->
                <li data-index="rs-674" 
                    data-transition="fade" 
                    data-slotamount="default" 
                    data-easein="default" 
                    data-easeout="default" 
                    data-masterspeed="default" 
                    data-thumb="src/images/2016/spacey5.jpg?v=<?php echo $currentScriptVersion; ?>" 
                    data-rotate="0" 
                     data-saveperformance="off" 
                      data-title="Main Stage" 
                       data-description="">
                    
                    <!-- MAIN IMAGE -->
                    <img src="src/images/2016/spacey5.jpg?v=<?php echo $currentScriptVersion; ?>" alt="" 
                        data-bgposition="center center" 
                        data-bgfit="cover" 
                        data-bgrepeat="no-repeat" 
                        data-bgparallax="5" class="rev-slidebg" 
                        data-no-retina>
                    
                    <!-- LAYERS -->
                    
                    <!-- CONTENT -->
                    <div class="tp-caption Concept-SubTitle   tp-resizeme rs-parallaxlevel-2" id="slide-674-layer-4" 
                        data-x="['center','center','center','center']" 
                        data-hoffset="['0','0','0','0']" 
                        data-y="['bottom','bottom','bottom','bottom']" 
                         
                        data-voffset="['100','100','100','100']" 
                        data-fontsize="['25','25','25','25']" 
                        data-lineheight="['35','35','35','35']" 
                        data-width="['1000','600','600','400']" 
                        data-whitespace="normal" 
                        data-height="none" 
                        data-transform_idle="o:1;" 
                        data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" 
                        data-start="500" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on" style="z-index: 6;text-align:center">
<!--
                        <div style="margin-bottom:9px;">
                            <span class="text_icon" style="background-image:url('src/images/menu_icons/agenda.svg'); margin-top:15px"></span>
                             Mon, 26.09.2016 | 2:50 PM
                         </div>
-->
<!--                        "Europe`s hottest startup conference. The Olymp for entrepreneurs." – Florian H.-->
                        "Combining a startup conference with the Oktoberfest is brilliant." – Kevin Spacey
                    </div>
                </li>
                  <!-- END: SPACEY SLIDE  -->
                
                
                <!-- START: MASCHMAYER SLIDE  -->
                <li
                    data-index="rs-680"
                    data-transition="fade"
                    data-delay="6000"
                    data-slotamount="default"
                    data-easein="default"
                    data-easeout="default"
                    data-masterspeed="default"
                    data-thumb="src/images/2016/academy_hero1.jpg?v=<?php echo $currentScriptVersion; ?>"
                    data-rotate="0"
                    data-saveperformance="off"
                    data-title="Startup Academy"
                    data-description="">
                    
                    <!-- MAIN IMAGE -->
                    <img src="src/images/2016/academy_hero1.jpg?v=<?php echo $currentScriptVersion; ?>" alt="Startup Academy"
                        data-bgposition="top center"
                        data-bgfit="cover"
                        data-bgrepeat="no-repeat"
                        data-bgparallax="5" class="rev-slidebg"
                        data-no-retina>

                    <!-- LAYERS -->
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption tp-shape tp-shapewrapper " id="slide-670-layer-1"
                        data-x="['center','center','center','center']"
                        data-hoffset="['0','0','0','0']"
                        data-y="['middle','middle','middle','middle']"
                        data-voffset="['0','0','0','0']"
                        data-width="full"
                        data-height="full"
                        data-whitespace="nowrap"
                        data-transform_idle="o:1;"
                        data-transform_in="opacity:0;s:1000;e:Power2.easeInOut;"
                        data-transform_out="opacity:0;s:1000;s:1000;"
                        data-start="bytrigger"
                        data-basealign="slide"
                        data-responsive_offset="off"
                        data-responsive="off"
                        data-end="bytrigger"
                        data-lasttriggerstate="keep" style="z-index: 5;background-color:rgba(0, 0, 0, 0.35);border-color:rgba(0, 0, 0, 0);">
                    </div>

                    <!-- CONTENT -->
                    <div class="tp-caption Concept-SubTitle   tp-resizeme rs-parallaxlevel-2" id="slide-670-layer-4"
                        data-x="['right','right','middle','middle']"
                        data-hoffset="['0','0','0','0']"
                        data-y="['top','top','top','top']"

                        data-voffset="['250','149','149','169']"
                        data-fontsize="['25','25','25','25']"
                        data-lineheight="['35','35','35','35']"
                        data-width="['600','600','600','400']"
                        data-whitespace="normal"
                        data-height="none"
                        data-transform_idle="o:1;"
                        data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                        data-start="800"
                        data-splitin="none"
                        data-splitout="none"
                        data-responsive_offset="on" style="z-index: 6;">
                        "I attended this conference as a student with interest in entrepreneurship and left with the determination of founding a company one day." – Dominik Parak
                    </div>
                </li>
                <!-- END: MASCHMAYER SLIDE  -->
                
                <!-- START: AFTERPARTY SLIDE  -->
                <li
                    data-index="rs-670"
                    data-transition="fade"
                    data-delay="6000"
                    data-slotamount="default"
                    data-easein="default"
                    data-easeout="default"
                    data-masterspeed="default"
                    data-thumb="src/images/2016/afterparty.jpg?v=<?php echo $currentScriptVersion; ?>"
                    data-rotate="0"
                    data-saveperformance="off"
                    data-title="Afterparty"
                    data-description="">
                    
                    <!-- MAIN IMAGE -->
                    <img src="src/images/2016/afterparty.jpg?v=<?php echo $currentScriptVersion; ?>" alt="Kevin Spacey"
                        data-bgposition="top right"
                        data-bgfit="cover"
                        data-bgrepeat="no-repeat"
                        data-bgparallax="5" class="rev-slidebg"
                        data-no-retina>

                    <!-- LAYERS -->
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption tp-shape tp-shapewrapper " id="slide-670-layer-1"
                        data-x="['center','center','center','center']"
                        data-hoffset="['0','0','0','0']"
                        data-y="['middle','middle','middle','middle']"
                        data-voffset="['0','0','0','0']"
                        data-width="full"
                        data-height="full"
                        data-whitespace="nowrap"
                        data-transform_idle="o:1;"
                        data-transform_in="opacity:0;s:1000;e:Power2.easeInOut;"
                        data-transform_out="opacity:0;s:1000;s:1000;"
                        data-start="bytrigger"
                        data-basealign="slide"
                        data-responsive_offset="off"
                        data-responsive="off"
                        data-end="bytrigger"
                        data-lasttriggerstate="keep" style="z-index: 5;background-color:rgba(0, 0, 0, 0.35);border-color:rgba(0, 0, 0, 0);">
                    </div>

                    <!-- CONTENT -->
                    <div class="tp-caption Concept-SubTitle   tp-resizeme rs-parallaxlevel-2" id="slide-670-layer-4"
                        data-x="['right','right','middle','middle']"
                        data-hoffset="['0','0','0','0']"
                        data-y="['top','top','top','top']"

                        data-voffset="['200','149','149','169']"
                        data-fontsize="['25','25','25','25']"
                        data-lineheight="['35','35','35','35']"
                        data-width="['600','600','600','400']"
                        data-whitespace="normal"
                        data-height="none"
                        data-transform_idle="o:1;"
                        data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                        data-start="800"
                        data-splitin="none"
                        data-splitout="none"
                        data-responsive_offset="on" style="z-index: 6;">
                        "Bits &amp; Pretzels offers a friendly and exciting atmosphere where it's easy and fun to meet interesting people" – Stefan Walkner
                    </div>
                </li>
                <!-- END: AFTERPARTY SLIDE  -->
                
                <!-- START: BRANSON SLIDE  -->
                <li 
                    data-index="rs-679" 
                    data-transition="fade" 
                    data-slotamount="default" 
                    data-easein="default" 
                    data-easeout="default" 
                    data-masterspeed="default" 
                    data-thumb="src/images/2016/impression_branson1.jpg?v=<?php echo $currentScriptVersion; ?>" 
                    data-rotate="0" 
                    data-saveperformance="off" 
                    data-title="Oktoberfest" 
                    data-description="">
                    
                    <!-- MAIN IMAGE -->
                    <img src="src/images/2016/impression_branson1.jpg?v=<?php echo $currentScriptVersion; ?>" alt="" 
                        data-bgposition="center center" 
                        data-bgfit="cover" 
                        data-bgrepeat="no-repeat" 
                        data-bgparallax="5" class="rev-slidebg" 
                        data-no-retina>
                    
                    <!-- LAYERS -->
                    <!-- CONTENT -->
                    <div class="tp-caption Concept-SubTitle   tp-resizeme rs-parallaxlevel-2" id="slide-670-layer-4"
                        data-x="['right','right','middle','middle']"
                        data-hoffset="['0','0','0','0']"
                        data-y="['top','top','bottom','bottom']"

                        data-voffset="['100','100','100','100']"
                        data-fontsize="['25','25','25','25']"
                        data-lineheight="['35','35','35','35']"
                        data-width="['600','600','600','400']"
                        data-whitespace="normal"
                        data-height="none"
                        data-transform_idle="o:1;"
                        data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                        data-start="800"
                        data-splitin="none"
                        data-splitout="none"
                        data-responsive_offset="on" style="z-index: 6;">
<!--                        "Bits &amp; Pretzels is the Oktoberfest of the startup community – a must-do experience to mark in the calendar!" – Georg R.-->
                        "Bits &amp; Pretzels is a very professional event that is just as much about having fun as it is about bringing together a fascinating array of guest speakers." – Richard Branson
                    </div>
                    
                </li>
                <!-- END: BRANSON SLIDE  -->
                
                
                <!-- START: AUDIENCE SLIDE  -->
                <li 
                    data-index="rs-673" 
                    data-transition="fade" 
                    data-slotamount="default" 
                    data-easein="default" 
                    data-easeout="default" 
                    data-masterspeed="default" 
                    data-thumb="src/images/2016/exhibition_hero3.jpg?v=<?php echo $currentScriptVersion; ?>" 
                    data-rotate="0" 
                     data-saveperformance="off" 
                      data-title="Mainstage" 
                       data-description="">
                    
                    <!-- MAIN IMAGE -->
                    <img src="src/images/2016/exhibition_hero3.jpg?v=<?php echo $currentScriptVersion; ?>" alt="" 
                        data-bgposition="left top" 
                        data-bgfit="cover" 
                        data-bgrepeat="no-repeat" 
                        data-bgparallax="5" class="rev-slidebg" 
                        data-no-retina>
                    
                    <!-- CONTENT -->
                    <div class="tp-caption Concept-SubTitle   tp-resizeme rs-parallaxlevel-2" id="slide-674-layer-4" 
                        data-x="['center','center','center','center']" 
                        data-hoffset="['0','0','0','0']" 
                        data-y="['top','top','top','top']" 
                         
                        data-voffset="['100','100','100','100']" 
                        data-fontsize="['25','25','25','25']" 
                        data-lineheight="['35','35','35','35']" 
                        data-width="['800','600','600','400']" 
                        data-whitespace="normal" 
                        data-height="none" 
                        data-transform_idle="o:1;" 
                        data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" 
                        data-start="500" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on" style="z-index: 6;text-align:center">
                        "Three incredible days of great speakers and amazing opportunities – styled in Bavarian tradition, which is the best on earth." – Martin Kraft
                    </div>
                </li>
                <!-- END: AUDIENCE SLIDE  -->
                
                <!-- START: EXHIBITION SLIDE  -->
                <li 
                    data-index="rs-693" 
                    data-transition="fade" 
                    data-slotamount="default" 
                    data-easein="default" 
                    data-easeout="default" 
                    data-masterspeed="default" 
                    data-thumb="src/images/2016/exhibition_impression.jpg?v=<?php echo $currentScriptVersion; ?>" 
                    data-rotate="0" 
                    data-saveperformance="off" 
                    data-title="Exhibition" 
                    data-description="">
                    
                    <!-- MAIN IMAGE -->
                    <img src="src/images/2016/exhibition_impression.jpg?v=<?php echo $currentScriptVersion; ?>" alt="" 
                        data-bgposition="center center" 
                        data-bgfit="cover" 
                        data-bgrepeat="no-repeat" 
                        data-bgparallax="5" class="rev-slidebg" 
                        data-no-retina>
                    
                    <!-- CONTENT -->
                    <div class="tp-caption Concept-SubTitle   tp-resizeme rs-parallaxlevel-2" id="slide-674-layer-4" 
                        data-x="['right','right','center','center']" 
                        data-hoffset="['0','0','0','0']" 
                        data-y="['top','top','top','top']" 
                         
                        data-voffset="['100','100','100','100']" 
                        data-fontsize="['25','25','25','25']" 
                        data-lineheight="['35','35','35','35']" 
                        data-width="['850','600','600','400']" 
                        data-whitespace="normal" 
                        data-height="none" 
                        data-transform_idle="o:1;" 
                        data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" 
                        data-start="500" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on" style="z-index: 6;text-align:center; text-shadow: 2px 2px 15px #000">
                        "No other conference before has given my team and me such a huge number of new connections, friends and inspiration!" – Mario Juhnke
                    </div>
                </li>
                <!-- END: EXHIBITION SLIDE  -->
               
                <!-- START: AIRBNB SLIDE  -->
                <li 
                    data-index="rs-675" 
                    data-transition="fade" 
                    data-slotamount="default" 
                    data-easein="default" 
                    data-easeout="default" 
                    data-masterspeed="default" 
                    data-thumb="src/images/2016/oktoberfest_nathan.jpg?v=<?php echo $currentScriptVersion; ?>" 
                    data-rotate="0" 
                    data-saveperformance="off" 
                    data-title="Oktoberfest" 
                    data-description="">
                    
                    <!-- MAIN IMAGE -->
                    <img src="src/images/2016/oktoberfest_nathan.jpg?v=<?php echo $currentScriptVersion; ?>" alt="" 
                        data-bgposition="center center" 
                        data-bgfit="cover" 
                        data-bgrepeat="no-repeat" 
                        data-bgparallax="5" class="rev-slidebg" 
                        data-no-retina>
                    
                    <!-- LAYERS -->
                <!-- Blues Shade -->
                        <div class="tp-caption tp-shape tp-shapewrapper blue_gradient" id="slide-929-layer-2" 
                        data-x="['center','center','center','center']" 
                        data-hoffset="['0','0','0','0']" 
                        data-y="['middle','middle','middle','middle']" 
                        data-voffset="['0','0','0','0']" 
                        data-width="full" 
                        data-height="full" 
                        data-whitespace="nowrap" 
                        data-transform_idle="o:1;" 
                        data-transform_in="opacity:0;s:2000;e:Power3.easeInOut;" 
                        data-transform_out="opacity:0;s:2000;e:Power3.easeInOut;s:2000;e:Power3.easeInOut;" 
                        data-start="700" 
                        data-basealign="slide" 
                        data-responsive_offset="off" 
                        data-responsive="off" 
                        data-end="bytrigger" 
                        data-lasttriggerstate="reset" 
                        style="z-index: 7;border-color:rgba(0, 0, 0, 0);">
                        </div>
                        
                    <!-- HEADLINE -->
                    <div class="tp-caption Concept-Title   tp-resizeme rs-parallaxlevel-2" id="slide-679-layer-2" 
                        data-x="['center','center','center','center']" 
                        data-hoffset="['0','0','0','0']" 
                        data-y="['middle','middle','middle','middle']" 
                         
                        data-voffset="['150','150','150','150']" 
                        data-fontsize="['40','40','30','25']" 
                        data-lineheight="['70','70','50','40']" 
                        data-width="['600','600','600','400']" 
                        data-height="none" 
                        data-whitespace="['normal','normal','normal','normal']" 
                        data-transform_idle="o:1;" 
                        data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" 
                        data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" 
                        data-start="1600" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on" style="z-index: 7;text-align:center;text-transform:uppercase;">
                        Meet the founders of companys like <span style="font-family:sans-serif; font-size:50px; letter-spacing:.25em">Airbnb</span> at the Oktoberfest!
                    </div>
                </li>
               <!-- START: AIRBNB SLIDE  -->
                
                
            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
    </div>
        <!-- END REVOLUTION SLIDER -->
    </section>