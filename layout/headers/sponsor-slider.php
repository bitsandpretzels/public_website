<div class="white-wrap">
	<section id="section_2" class="section_n section_gallery section_transition">
		<div class="sectionContent">
                <div class="swiperSection">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide cdSlide">
                                <a href="http://www.stmwi.bayern.de/" target="_blank">
                                    <img src="/src/images/partners/staatsministerium.png">
                                </a>
                            </div>
                            <div class="swiper-slide cdSlide">
                                <a href="http://www.audi.de" target="_blank">
                                    <img src="/src/images/partners/sponsors/audi.png" alt="audi">
                                </a>
                            </div>
                            <div class="swiper-slide cdSlide">
                                <a href="http://www.wirecard.de" target="_blank">
                                    <img src="/src/images/partners/sponsors/wirecard.png" alt="wirecard">
                                </a>
                            </div>
                            <div class="swiper-slide cdSlide">
                                <a href="http://www.sage.de/" target="_blank">
                                    <img src="/src/images/partners/sponsors/sage.png" alt="sage">
                                </a>
                            </div>
                            <div class="swiper-slide cdSlide">
                                <a href="http://www.ibm.com/" target="_blank">
                                    <img src="/src/images/partners/sponsors/ibm.png" alt="ibm">
                                </a>
                            </div>
                            <div class="swiper-slide cdSlide">
                                <a href="http://www.rakuten.com" target="_blank">
                                    <img src="/src/images/partners/sponsors/rakuten.png" alt="rakuten">
                                </a>
                            </div>
                            <div class="swiper-slide cdSlide">
                                <a href="http://www.interhyp.com" target="_blank">
                                    <img src="/src/images/partners/sponsors/interhyp.png" alt="interhyp">
                                </a>
                            </div>
                            <div class="swiper-slide cdSlide">
                                <a href="http://www.onpage.org" target="_blank">
                                    <img src="/src/images/partners/sponsors/onpage_logo_blau.png" alt="onpage">
                                </a>
                            </div>
                            <div class="swiper-slide cdSlide">
                                <a href="http://www.aboalarm.de" target="_blank">
                                    <img src="/src/images/partners/sponsors/aboalarm.png" alt="aboalarm">
                                </a>
                            </div>
                            <div class="swiper-slide cdSlide">
                                <a href="http://www.allianz.com" target="_blank">
                                    <img src="/src/images/partners/sponsors/allianz.png" alt="Allianz">
                                </a>
                            </div>
                            <div class="swiper-slide cdSlide">
                                <a href="http://www.amazon.de" target="_blank">
                                    <img src="/src/images/partners/sponsors/amazon.png" alt="amazon">
                                </a>
                            </div>
                            <div class="swiper-slide cdSlide">
                                <a href="http://www.amazon.de" target="_blank">
                                    <img src="/src/images/partners/sponsors/amazonde.png" alt="amazon">
                                </a>
                            </div>
                            <div class="swiper-slide cdSlide">
                                <a href="http://www.datev.de" target="_blank">
                                    <img src="/src/images/partners/sponsors/datev.png" alt="datev">
                                </a>
                            </div>
                            <div class="swiper-slide cdSlide">
                                <a href="http://www.ey.de" target="_blank">
                                    <img src="/src/images/partners/sponsors/ey.png" alt="ey">
                                </a>
                            </div>
                            <div class="swiper-slide cdSlide">
                                <a href="http://www.gruenderland.bayern/" target="_blank">
                                    <img src="/src/images/partners/sponsors/gruenderland_bayern.png" alt="gruenderland bayern">
                                </a>
                            </div>
                            <div class="swiper-slide cdSlide">
                                <a href="http://www.ihk.de/" target="_blank">
                                    <img src="/src/images/partners/sponsors/ihk.png" alt="ihk">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="clearfix point-none"></div>    
	</section>
</div>