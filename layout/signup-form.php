<?php if(Router::renderSignUpForm()): ?>
    <div class="newsletterbar center clearfix">
      <div class="wrapper">
        <form name="" method="post" action="//bitsandpretzels.us8.list-manage.com/subscribe/post?u=59db0da2c55155bbedfa0c507&amp;id=3c5efc9e7b" id="newsletterForm">
          <div class="wrap_field">
            <input autocomplete="off" type="email" name="EMAIL" placeholder="Enter your email to get informed about important updates" class="text_field" id="emailInput">
          </div>
          <input type="hidden" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>" name="IPADRESS">
          <input type="hidden" value="NL Subscriber" name="SOURCE">
          <div style="position: absolute; left: -5000px;">
            <input type="text" name="b_59db0da2c55155bbedfa0c507_3c5efc9e7b" tabindex="-1" value="">
          </div>
          <button 
          type="submit" 
          id="emailSubmit" 
          disabled="disabled" 
          class="button disabled" 
          data-toggle="tooltip" 
          data-placement="bottom" 
          title="Sign up">Sign me up!</button>
        </form>
      </div>
    </div>
  <?php endif; ?>