<div class="list_logos logos-grid-5 clearfix">
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/media/zdf.png" alt="Z" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/media/zdf.png" alt="ZDF">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/logo_14.png" alt="GRU" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/logo_14.png" alt="GRUNDERSZENE">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/logo_15.png" alt="Han" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/logo_15.png" alt="Handelsblatt">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/logo_16.png" alt="Bus" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/logo_16.png" alt="Business Punk">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/logo_17.png" alt="t3n" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/logo_17.png" alt="t3n">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/logo_18.png" alt="wir" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/logo_18.png" alt="wired">
                </noscript>
            </div>
        </div>
    </div>

    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/media/manager_magazin_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/media/manager_magazin_neu.png" alt="Manager Magazin">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/media/tnw.png" alt="" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/media/tnw.png" alt="tnw">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/media/zeit_neu.png" alt="" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/media/zeit_neu.png" alt="Zeit">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/logo_22.png" alt="Sud" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/logo_22.png" alt="SuddeutscheZeitung">
                </noscript>
            </div>
        </div>
    </div>
</div>