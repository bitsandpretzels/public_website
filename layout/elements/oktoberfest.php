   <section id="oktoberfest" class="section section_wbox wbox_4 active section_1 section_pt" data-show-inner="false" data-show-footer="false">
        <div class="content_section paralax_section">
            <div class="wrapper_inner center">
                <div class="valign">
                    <div class="middle">
                        <div class="wbox">
                            <h1 class="main_title no-sub-title">3 days, 5000 participants, <br>countless impressions.</h1>
                            <div class="descr descr_small_margin">
                                <table class="hard-facts center">
                                    <tr>
                                    <td><span>5,000+</span> Attendees</td>
                                    <td><span>300+</span> Table Captains</td>
                                    <td class="mobile_hidden"><span>400+</span> Journalists</td>
                                    <td><span>3-day</span> Event</td>
                                    <td class="mobile_hidden"><span>1</span> Oktoberfest Tent</td>
                                    </tr>
                                </table>
                                <br>
                                <div>
                                    Bits &amp; Pretzels developed to the biggest festival for founders in Europe. But the number that gives the best testimony is the overwhelming percentage of participants that come back every year to refresh this experience.
                                </div>                                            

                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->