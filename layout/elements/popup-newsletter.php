  <div class="nav_detail rnd_page">
    <div class="wrap_nav" style="display: table; width: 100%;">
        <div class="container" style="display: table-cell; text-align: center; vertical-align: middle;">
            <div class="row clearfix">
                <div class="eight columns offset-by-two">
                    <h1 class="center" style="color:white; margin-bottom:25px; text-transform:uppercase; font-weight:800px">
                    limited to <br>
                    5,000 awesome attendees <br>
                    - become one of them -</h1>
<!--                    <img src="/src/images/2016/bird-overview.svg?v=<?php echo $currentScriptVersion; ?>" alt="Bird Overview" style="width:100%; margin-bottom:40px;">-->
                    <p class="descr_small_margin" style="color:white; margin-bottom:20px; line-height:23px; font-size:1em">
                    <strong>Enter your email address below to receive your personal invitation and price information.</strong>
                    <br>Be fast and join us for Europe‘s coolest startup event before it sells out again.
                    </p>
                </div>
            </div>
               <div class="row clearfix">
                   <div class="four columns offset-by-four">
                    <!-- Begin MailChimp Signup Form -->
                    <div id="mc_embed_signup">
                    <form action="//bitsandpretzels.us8.list-manage.com/subscribe/post?u=59db0da2c55155bbedfa0c507&amp;id=0af6c68cca" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                        <div id="mc_embed_signup_scroll">
                            <div class="mc-field-group" style="margin-top:20px; width:100%">
                                <input class="u-full-width" style="border:1px solid white !important; color:#005690 !important;font-size:17px; width:100%; border-radius:4px; height:45px; text-align:center" type="email" value="" placeholder="Email Address"name="EMAIL" class="required email" id="mce-EMAIL">
                            </div>
                            <input type="hidden" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>" name="IPADRESS">
                            <input type="hidden" value="Buy Ticket Button" name="SOURCE">
                            <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                            </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_59db0da2c55155bbedfa0c507_3c5efc9e7b" tabindex="-1" value=""></div>
                            
                            <div class="clear" style="margin-top:20px; width:100%">
                                <input style="cursor:pointer; width:100%; border-radius: 4px;height: 47px;background:#ea5c3f" type="submit" value="Send me my invitation code" name="subscribe" id="mc-embedded-subscribe" class="button">
                            </div>
                        </div>
                    </form>
                    </div>
                    <p class="descr_small_margin" style="color:white; margin-bottom:20px; line-height:23px; font-size:.7em; margin-top:10px">
                    We will never share your personal information, promise!
                    </p>
                    <!--End mc_embed_signup-->
                </div>
                
            </div>
        </div>
  	</div>
  	<div id="closenav" class="close">
  		<span class="line1"></span>
  		<span class="line2"></span>
  	</div>
  </div>