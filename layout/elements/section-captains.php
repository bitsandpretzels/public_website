    <section id="captain" class="section section_users_right white <?php echo (@$silver===true) ?  'silver' :  ''; ?> active section_1 section_pt" data-show-inner="false" data-show-footer="false">

        <div class="content_section">
            <div class="wrapper_inner">  
                <div class="valign">
                    <div class="middle">
                        <div class="grid grid-2 grid-2-simple clearfix">
                            <div class="col col-1">
                                <div class="grid-offset"> 
                                    <div class="info_box">   
                                        <h1 class="main_title">O captain! My captain!<br>May I join your table.</h1>
                                        <h2 class="down_sub_title">Join an expert-hosted table at the Oktoberfest.</h2>
                                        <div class="descr descr_small_margin">There’s this shy species everybody speaks about and only few catch a glimpse of: infuential executives. But once a year they gather at the Bits &amp; Pretzels Oktoberfest tables. Lured by socialbility and lots of fresh talents. Choose one of the CEOs, editors-in-chief or investors as your Table Captain and enjoy their experience!<div class="link_box"><a href="<?php echo Router::getRoute('captains-startup'); ?>" class="link">More info &amp; all the Table Captains <span class="icon icon-arrow_right"></span></a></div></div>
                       
                                    </div>
                                </div>
                            </div>
                            <div class="col col-2">
                                <div class="grid-offset">
                                    <div class="conteiner">
                                        <div class="list_users">
                                            <?php foreach(People::setType('table-captain')->setDataSource($tcs)->get(0, 12) as $key => $tc):  ?>

                                                <div class="user_box <?php echo ($key>7) ? 'hidden_thirteen' : ''; ?>">
                                                    <div class="user-offset">
                                                        <div class="img_user">                                                             <img class="lazyload-scroll" data-src="/src/images/table-captains/default/<?php echo $tc['image']; ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />                                                             <noscript>                                                                 <img src="/src/images/table-captains/default/<?php echo $tc['image']; ?>" alt="">                                                             </noscript>                                                         </div>
                                                        <p class="name_user"><?php echo $tc['name']; ?></p>
                                                        <div class="title"><?php echo $tc['title']; ?></div>
                                                        <div class="company"><?php echo $tc['company']; ?></div>
                                                    </div>
                                                </div>

                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->