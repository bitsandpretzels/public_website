<div class="list_logos logos-grid-5 mb0 clearfix">
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/corporate/logo_40.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/corporate/logo_40.png" alt="Allianz">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/corporate/logo_43.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/corporate/logo_43.png" alt="Audi">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/corporate/Bosch_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/corporate/Bosch_neu.png" alt="Bosch">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/corporate/logo_46.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/corporate/logo_46.png" alt="Google">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/corporate/logo_45.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/corporate/logo_45.png" alt="IBM">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/corporate/Lufthansa_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/corporate/Lufthansa_neu.png" alt="Lufthansa">
                </noscript>
            </div>
        </div>
    </div>

    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/corporate/McKinsey_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/corporate/McKinsey_neu.png" alt="McKinsey">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/corporate/RedBull_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/corporate/RedBull_neu.png" alt="Red Bull">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/corporate/Siemens_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/corporate/Siemens_neu.png" alt="Siemens">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/corporate/Telekom_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/corporate/Telekom_neu.png" alt="Telekom">
                </noscript>
            </div>
        </div>
    </div>
</div>