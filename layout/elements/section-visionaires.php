 <section id="speakers" class="section section_users silver active section_1 section_pt" data-show-inner="false" data-show-footer="false">

        <div class="content_section center">
            <div class="wrapper_inner">
                <div class="valign">
                    <div class="middle">
                        <div class="wrapp_main_text">
                            <h1 class="main_title center">Visions do come from Visionaries.</h1>
                            <h2 class="down_sub_title center">Get inspired by the speakers for Bits &amp; Pretzels 2016.</h2>
                            <div class="descr descr_small_margin center">Our Bits &amp; Pretzels stage and panel discussions host selected speakers that are able to open up new perspectives for your startup.<div class="link_box"><!--<a href="" class="link">View All Speakers<span class="icon icon-arrow_right"></span></a>--></div>                            
                            </div>  
                            
                        </div>           
                        
                        <div class="wrap_speakers">
                            <div class="conteiner">
                                <div class="list_users">
                                    <?php foreach(People::setType('speaker')->setDataSource($speakers)->get(0, 12) as $person):  ?>
                                        <div class="user_box">
                                            <div class="user-offset">
                                                <div class="img_user">
                                                    <img class="lazyload-scroll" data-src="/src/images/speakers/default/<?php echo $person['image']; ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                                                    <noscript>
                                                        <img src="/src/images/speakers/default/<?php echo $person['image']; ?>" alt="">
                                                    </noscript>
                                                </div>
                                                <p class="name_user"><?php echo $person['name']; ?></p>
                                                <div class="title"><?php echo $person['title']; ?></div>
                                                <div class="company"><?php echo $person['company']; ?></div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div class="link_box center"><a href="<?php echo Router::getRoute('speakers', true); ?>" class="link">View All Speakers<span class="icon icon-arrow_right"></span></a></div>

                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section><!-- section -->