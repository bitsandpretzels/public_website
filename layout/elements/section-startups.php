<div class="list_logos logos-grid-5 mb0 clearfix">

    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/startup/adyen_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/startup/adyen_neu.png" alt="Adyen">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/startup/Braintree_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/startup/Braintree_neu.png" alt="Braintree">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/startup/egym_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/startup/egym_neu.png" alt="Egym">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/startup/Freeletics_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/startup/Freeletics_neu.png" alt="Freeletics">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/startup/gobutler_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/startup/gobutler_neu.png" alt="Go Butler">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/startup/outfittery_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/startup/outfittery_neu.png" alt="Outfittery">
                </noscript>
            </div>
        </div>
    </div>

    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/startup/prezi_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/startup/prezi_neu.png" alt="Prezi">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/startup/stylight_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/startup/stylight_neu.png" alt="Stylight">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/startup/trivago_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/startup/trivago_neu.png" alt="Trivago">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/startup/zendesk_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/startup/zendesk_neu.png" alt="Zendesk">
                </noscript>
            </div>
        </div>
    </div>
</div>