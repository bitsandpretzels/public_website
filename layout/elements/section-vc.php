<div class="list_logos clearfix">
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/investor/500startups_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/investor/500startups_neu.png" alt="500 Startups">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/investor/Accel-Partners_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/investor/Accel-Partners_neu.png" alt="Accel Partners">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/investor/Balderton_Capital_Logo_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/investor/Balderton_Capital_Logo_neu.png" alt="Balderton Capital">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/investor/dn-capital_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/investor/dn-capital_neu.png" alt="DN Capital">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/investor/earlybird_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/investor/earlybird_neu.png" alt="Earlybird">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/investor/General_Atlantic_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/investor/General_Atlantic_neu.png" alt="General Atlantic">
                </noscript>
            </div>
        </div>
    </div>

    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/investor/Holtzbrinck_Ventures_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/investor/Holtzbrinck_Ventures_neu.png" alt="Holtzbrinck">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/investor/HTGF_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/investor/HTGF_neu.png" alt="HTGF">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/investor/index-ventures_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/investor/index-ventures_neu.png" alt="Index Ventures">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/investor/Lake_Star_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/investor/Lake_Star_neu.png" alt="Lake Star">
                </noscript>
            </div>
        </div>
    </div>
    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/investor/point_nine_neu.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/investor/point_nine_neu.png" alt="Point Nine">
                </noscript>
            </div>
        </div>
    </div>

    <div class="logo_box">
        <div class="valign">
            <div class="middle">
                <img class="lazyload-scroll" data-src="/src/images/logos/investor/logo_7.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
                <noscript>
                    <img src="/src/images/logos/investor/logo_7.png" alt="">
                </noscript>
            </div>
        </div>
    </div>
</div>