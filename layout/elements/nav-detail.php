  <div class="nav_detail rnd_page">
    <div class="container" style="display: table;width: 100%; height:100vh;">
    	<div class="row clearfix" style="display: table-cell;text-align: center;vertical-align: middle;">
			<div class="four columns">
				<div class="info">
				    <div class="title desktop_only_nav">Startup</div>
				    <div class="descr desktop_only_nav">Founder or employee of a startup that is &lt; 10 years <b>AND</b> has &lt; 300 employees</div>
				    
				    <a style="margin-bottom:10px" href="<?php echo Router::getRoute('buy_startup'); ?>" class="button light"><span>I am a Startup</span></a>
<!--				    <a style="margin-bottom:10px" href="<?php echo Router::getRoute('buy_investor'); ?>" class="button light mobile_only_nav"><span>I am an Investor</span></a>-->
<!--				    <a style="margin-bottom:10px" href="<?php echo Router::getRoute('buy_corporate'); ?>" class="button light mobile_only_nav"><span>I am a Corporate</span></a>-->
				            <!--								<a style="margin-bottom:10px" href="<?php echo Router::getRoute('buy_student'); ?>" class="button light mobile_only_nav"><span>I am a Student</span></a>-->
				            <!--								<a style="margin-bottom:10px" href="<?php echo Router::getRoute('journalist'); ?>" class="button light mobile_only_nav"><span>I am Journalist</span></a>-->
				            <!--								<a style="margin-bottom:10px" href="<?php echo Router::getRoute('buy_academia'); ?>" class="button light mobile_only_nav"><span>I am Academia</span></a>-->
				</div>
			</div>
			<div class="four columns">
				<div class="title desktop_only_nav">Investor</div>
				<div class="descr desktop_only_nav">Business Angel, Venture Capital, Private Equity, Accelerator</div>
				<a style="margin-bottom:10px" href="<?php echo Router::getRoute('buy_investor'); ?>" class="button light"><span>I am an Investor</span></a>
			</div>
			<div class="four columns">
				<div class="info">
				    <div class="title desktop_only_nav">Corporate</div>
				    <div class="descr desktop_only_nav">Founder or employee of a company > 10 Years <b>OR</b> > 300 Employees <b>OR</b> Consultant, Lawyer, Auditor, Agency, etc.</div>
				    <a style="margin-bottom:10px" href="<?php echo Router::getRoute('buy_corporate'); ?>" class="button light"><span>I am a Corporate</span></a>
				</div>
			</div>
<!--
			<div class="four columns">
				<div class="info">
				    <div class="title desktop_only_nav">Student</div>
				    <div class="descr desktop_only_nav">University student who can provide a certificate of enrollment which is valid throughout the event.</div>
				    <a style="margin-bottom:10px" href="<?php echo Router::getRoute('buy_student'); ?>" class="button light"><span>I am a Student</span></a>
				</div>
			</div>
-->
<!--
			<div class="four columns">
				<div class="info">
				    <div class="title desktop_only_nav">Journalist</div>
				    <div class="descr desktop_only_nav">Journalist who is writing on behalf of an official media. Applications will be individually reviewed regarding the concrete mandate.</div>
				    <a style="margin-bottom:10px" href="<?php echo Router::getRoute('journalist'); ?>" class="button light"><span>I am Journalist</span></a>
				</div>
			</div>
-->
<!--
			<div class="four columns">
				<div class="info">
				    <div class="title desktop_only_nav">Academia</div>
				    <div class="descr desktop_only_nav">University employees and employees of public organizations.</div>
				    <a style="margin-bottom:10px" href="<?php echo Router::getRoute('buy_academia'); ?>" class="button light"><span>I am Academia</span></a>
				</div>
			</div>
-->
    	</div>
  	</div>
  	<div id="closenav" class="close">
  		<span class="line1"></span>
  		<span class="line2"></span>
  	</div>
  </div>