<footer id="footer">
<script type="text/javascript">
_linkedin_data_partner_id = "43057";
</script><script type="text/javascript">
(function(){var s = document.getElementsByTagName("script")[0];
var b = document.createElement("script");
b.type = "text/javascript";b.async = true;
b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
s.parentNode.insertBefore(b, s);})();
</script>
<!--
  <div class="bottom_footer" id="bottom_footer" style="padding-top: 20px; padding-bottom: 10px;">
    <nav class="footer_nav">
     
      <ul class="ul-reset">
        <li>
            <a target="_blank"  href="https://www.facebook.com/bitsandpretzels/"><img style="max-width:20px;" class="IMGCluster" src="/src/images\icons\Social\fb.svg" alt="Facebook"></a>
        </li>
          <li>
            <a target="_blank"  href="https://twitter.com/bitsandpretzels"><img style="max-width:20px;" class="IMGCluster" src="/src/images\icons\Social\tw.svg" alt="Twitter"></a>
        </li>
          <li>
            <a target="_blank"  href="https://www.linkedin.com/company/bits-&-pretzels"><img style="max-width:20px;" class="IMGCluster" src="/src/images\icons\Social\in.svg" alt="Linkedin"></a>
        </li>
          <li>
            <a target="_blank"  href="https://www.instagram.com/bitsandpretzels/"><img style="max-width:20px;" class="IMGCluster" src="/src/images\icons\Social\insta.svg" alt="Instagram"></a>
        </li>
           <li>
            <a target="_blank"  href="https://www.youtube.com/channel/UCWHcCX68bpZZvx9P90DW3Eg"><img style="max-width:20px;" class="IMGCluster" src="/src/images\icons\Social\yt.svg" alt="YouTube"></a>
        </li>
       
      </ul>   style="padding-top: 10px;"
    </nav>
  </div>    -->
   <div class="bottom_footer" id="bottom_footer" >
    <nav class="footer_nav">
     
      <ul class="ul-reset">
        <li>
          <a href="<?php echo Router::getRoute('imprint'); ?>" class="link">Imprint</a>
        </li>
        <li>
          <a href="<?php echo Router::getRoute('privacy-policy'); ?>" class="link">Privacy Policy </a>
        </li>
        <li>
          <a href="<?php echo Router::getRoute('tos'); ?>" class="link">Terms of Service</a>
        </li>
        <li>
          <a href="<?php echo Router::getRoute('faq'); ?>" class="link">FAQ</a>
        </li>
      </ul>
    </nav>
  </div>

</footer>
