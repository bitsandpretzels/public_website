 <div id="sub_nav" class="sub_nav_wrap">
        <div class="wrap_subnav_inner">
<!--            <a href="/cluster" alt="All Cluster"><div class="title_section">All Clusters</div></a>-->
            <a href="<?php echo Router::getRoute('home'); ?>" class="title_section desktop_only"><div style="width:37px; height:37px; margin-top:4px; background:url('/src/images/logo_bitsandpretzels.svg')"></div></a>
            <ul class="sub_nav ul-reset">
                <li class="hidden_li">
                    <a href="/cluster/commerce" class="link_nav">Future Commerce</a>
                </li>
                <li class="hidden_li">
                    <a href="/cluster/mobility" class="link_nav">Fast Mobility</a>
                </li>
                <li class="hidden_li">
                    <a href="/cluster/lifestyle" class="link_nav">Hot Lifestyle</a>
                </li>
                <li class="hidden_li">
                    <a href="/cluster/iot" class="link_nav">Sophisticated IoT</a>
                </li>
                <li class="hidden_li">
                    <a href="/cluster/smart-company" class="link_nav">Smart Company</a>
                </li>
                <li class="hidden_li">
                    <a href="/cluster/money" class="link_nav" style="margin-right:35px">Big Money</a>
                </li>
                <li>
                    <a class="link_nav buyticket trigger_app_nav" data-section="buyticket"><?php echo Meta::getButtonWording(); ?></a>
                </li>
            </ul>
        </div>
    </div>