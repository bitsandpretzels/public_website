<?php
if(file_exists('../lib/Router.php')) {
    require_once '../lib/Router.php';
    require_once '../lib/Meta.php';
} elseif (file_exists('lib/Router.php')) {
    require_once 'lib/Router.php';
    require_once 'lib/Meta.php';    
}

// create timestamp
$currentScriptVersion = "2.17";
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title><?php echo Meta::getTitle(); ?></title>
    <meta name="description" content="<?php echo Meta::getMetaDescription(); ?>">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Cache-control" content="public">
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico?v=<?php echo $currentScriptVersion; ?>">
    <link rel="apple-touch-icon" href="/favicon.ico?v=<?php echo $currentScriptVersion; ?>"/>
    <!-- RS5.0 Main Stylesheet -->
    <link rel="stylesheet" type="text/css" href="/revolution/css/settings.css?v=<?php echo $currentScriptVersion; ?>">
    <!-- RS5.0 Layers and Navigation Styles -->
    <link rel="stylesheet" type="text/css" href="/revolution/css/layers.css?v=<?php echo $currentScriptVersion; ?>">
    <link rel="stylesheet" type="text/css" href="/revolution/css/navigation.css?v=<?php echo $currentScriptVersion; ?>"> 
    <link rel="stylesheet" href="/src/css/main.min.css?v=<?php echo $currentScriptVersion; ?>">
    
    <?php
        $host = $_SERVER['HTTP_HOST'];
        if ($host !== 'localhost:8080'):
    ?>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,700,500,600,900">
    <?php endif; ?>
    
    <?php if(Router::getCurrentRoute() === '/women_in_tech') : ?>
    <meta property="og:url" content="https://www.bitsandpretzels.com/women_in_tech" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Women in Tech: Win one of 300 free tickets!" />
    <meta property="og:description" content="Bits &amp; Pretzels is a 3 day conference for founders and people from the startup ecosystem. We bring together the world’s leading entrepreneurs and investors." />
    <meta property="og:image" content="https://www.bitsandpretzels.com/src/images/women_in_tech_meta.png" />
    
    <?php elseif(Router::getCurrentRoute() === '/exhibition'): ?>
    <meta property="og:url" content="https://www.bitsandpretzels.com/exhibition" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Startup Exhibition: Save your spot now!" />
    <meta property="og:description" content="Bits &amp; Pretzels is a 3 day conference for founders and people from the startup ecosystem. We bring together the world’s leading entrepreneurs and investors." />
    <meta property="og:image" content="https://www.bitsandpretzels.com/src/images/exhibtion_meta.png" />
    
    <?php elseif(Router::getCurrentRoute() === '/info/pitch'): ?>
    <meta property="og:url" content="https://www.bitsandpretzels.com/info/pitch" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Startup Pitch at Bits & Pretzels 2016 // Application is now open!!" />
    <meta property="og:description" content="Startups… Pay attention!! The Bits & Pretzels pitch application for the Founders Festival (25-27 September) is open. The winner joins the finals of the Extreme Tech Challenge on Sir Richard Branson’s Necker Island & gets a finalist spot in the 500 Startups accelerator!! Apply now." />
    <meta property="og:image" content="https://www.bitsandpretzels.com/src/images/pitch.png" />
    
    <?php elseif(Router::getCurrentRoute() === '/volunteers'): ?>
    <meta property="og:url" content="https://www.bitsandpretzels.com/volunteers" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Become a Volunteer at the Bits & Pretzels Founders Festival 2016!" />
    <meta property="og:description" content="Our volunteer program puts you right in the epicenter of the startup ecosystem. Apply now and make exciting experiences and valuable contacts with like-minded people and industry leaders of today and tomorrow!" />
    <meta property="og:image" content="https://www.bitsandpretzels.com/src/images/volunteers.jpg" />
    
    <?php elseif(Router::getCurrentRoute() === '/afterparty'): ?>
    <meta property="og:url" content="https://www.bitsandpretzels.com/afterparty" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="OFFICIAL BITS & PRETZELS AFTERPARTY: Apply for guest list tickets!" />
    <meta property="og:description" content="We have 500 guest list tickets for the Bits & Pretzels Afterparty" />
    <meta property="og:image" content="https://www.bitsandpretzels.com/src/images/side_events_hero-facebook.jpg" />
    
    <?php elseif(Router::getCurrentRoute() === '/jobs'): ?>
    <meta property="og:url" content="https://www.bitsandpretzels.com/jobs" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Ready for your next adventure? Become part of the Bits & Pretzels Team!" />
    <meta property="og:description" content=" We are looking for: Eventmanagement Intern" />
    <meta property="og:image" content="https://www.bitsandpretzels.com/src/images/poster_meta.png" />
    
    <?php elseif(Router::getCurrentRoute() === '/video'): ?>
    <meta property="og:url" content="https://www.bitsandpretzels.com/video" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Become a better founder with our free Startup Academy" />
    <meta property="og:description" content="Bits & Pretzels dedicates a whole stage of the founders festival solely to enlightening and relevant masterclasses that will make you become a better entrepreneur. You can watch a series of eight videos now for free." />
    <meta property="og:image" content="https://www.bitsandpretzels.com/src/images/meta/academy-video.png" />
    
    <?php else: ?>
    <meta property="og:url" content="https://www.bitsandpretzels.com/" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php echo Meta::getTitle(); ?>" />
    <meta property="og:description" content="<?php echo Meta::getMetaDescription(); ?>"/>
    <meta property="og:image" content="https://www.bitsandpretzels.com/src/images/poster_meta.png" />
    <meta property="og:image:secure_url" content="https://www.bitsandpretzels.com/src/images/poster_meta.png" />
    <meta property="og:image:type" content="image/png" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="630" />

<!--
    <meta property="og:video" content="https://www.bitsandpretzels.com/src/media/video_b_and_p.mp4" />
    <meta property="og:video:secure_url" content="https://www.bitsandpretzels.com/src/media/video_b_and_p.mp4" />
    <meta property="og:video:type" content="video/mp4" />
    <meta property="og:video:width" content="1920" />
    <meta property="og:video:height" content="1920" />
-->
    <?php endif; ?>

    <?php if(isset($options, $options['canonical']) && !empty($options['canonical'])): ?>
    <link rel="canonical" href="<?php echo $options['canonical']; ?>"/>
    <?php endif; ?>
</head>
<body>
  <div class="main">
