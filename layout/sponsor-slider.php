<div class="sectionContent">
        <div class="swiperSection">
            <div class="swiper-container swiper-container-sponsors">
                <div class="swiper-wrapper">
                    <?php for($i = 0; $i < count($partners_2016); $i++):
                    if (strpos($partners_2016[$i]['type'],"sponsor")!==false || strpos($partners_2016[$i]['type'],"patronage")!==false): ?>

                        <div class="swiper-slide cdSlide">
                            <a href="<?= $partners_2016[$i]['link'] ?>" target="_blank">
                                <img class="lazyload-scroll" src="/src/images/partners/<?= $partners_2016[$i]['logo'] ?>" alt="<?= $partners_2016[$i]['name'] ?>"/>
<!--
                                <noscript>
                                    <img src="/src/images/partners/<?= $partners_2016[$i]['logo'] ?>" alt="<?= $partners_2016[$i]['name'] ?>" alt="<?= $partners_2016[$i]['name'] ?>">
                                </noscript>
-->
                            </a>
                        </div>

                    <?php endif; endfor; ?>

                </div>
            </div>
        </div>
    </div>
<div class="clearfix point-none"></div>    