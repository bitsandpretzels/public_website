<div id="linkMenu" class="link_menu link_menu_inner linkMenu link_menu_fixed">
    <span class="line1"></span>
    <span class="line2"></span>
    <span class="line3"></span>
</div>

<div id="header_nav" class="header_nav">
	<ul class="ul-reset sub-nav-header">
        
    <?php if(Router::getCurrentRoute() === '/'): ?>
		<li>
			<a href="#content" onclick="App.closeMenu()" class="link active"><span class="menu_icon" style="background-image:url('/src/images/menu_icons/home.svg')"></span><span class="menu_text">About the Event</span></a>
		</li>
    <?php else: ?>
		<li>
			<a href="<?php echo Router::getRoute('home'); ?>" class="link active"><span class="menu_icon" style="background-image:url('/src/images/menu_icons/home.svg')"></span><span class="menu_text">About the Event</span></a>
		</li>
    <?php endif; ?>
		<li>
			<a href="<?php echo Router::getRoute('speakers'); ?>" class="link"><span class="menu_icon" style="background-image:url('/src/images/menu_icons/speakers.svg'); height:19px; top:2px"></span><span class="menu_text">Speakers</span></a>
		</li>
        <li>
			<a href="<?php echo Router::getRoute('exhibition'); ?>" class="link"><span class="menu_icon" style="background-image:url('/src/images/menu_icons/exhibition.png')"></span><span class="menu_text">Exhibition</span></a>
		</li>
		<li>
			<a href="<?php echo Router::getRoute('cluster'); ?>" class="link"><span class="menu_icon" style="background-image:url('/src/images/menu_icons/cluster.svg')"></span><span class="menu_text">Clusters</span></a>
		</li>
		<li>
			<a href="<?php echo Router::getRoute('captains'); ?>" class="link"><span class="menu_icon" style="background-image:url('/src/images/menu_icons/table_captains.png')"></span><span class="menu_text">Table Captains</span></a>
		</li>
		<li>
			<a href="<?php echo Router::getRoute('investors_stage'); ?>" class="link"><span class="menu_icon" style="background-image:url('/src/images/menu_icons/handshake.svg')"></span><span class="menu_text">Investors Stage</span></a>
		</li>
		<li>
			<a href="<?php echo Router::getRoute('pitch'); ?>" class="link"><span class="menu_icon" style="background-image:url('/src/images/menu_icons/pitch.svg')"></span><span class="menu_text">Pitch</span></a>
		</li>
<!--
		<li>
			<a href="<?php echo Router::getRoute('agenda'); ?>" class="link"><span class="menu_icon" style="background-image:url('/src/images/menu_icons/agenda.svg')"></span><span class="menu_text">Agenda</span></a>
		</li>
		<li>
			<a href="<?php echo Router::getRoute('attendees'); ?>" class="link"><span class="menu_icon" style="background-image:url('/src/images/menu_icons/attendees.svg')"></span><span class="menu_text">Attendees</span></a>
		</li>
		
		<li>
			<a href="<?php echo Router::getRoute('hotels'); ?>" class="link"><span class="menu_icon" style="background-image:url('/src/images/menu_icons/hotels.svg')"></span><span class="menu_text">Hotels</span></a>
		</li>
		<li>
			<a href="<?php echo Router::getRoute('volunteers'); ?>" class="link"><span class="menu_icon" style="background-image:url('/src/images/menu_icons/volunteers.svg');"></span><span class="menu_text">Volunteers</span></a>
		</li>-->
	
        <li>
			<a href="<?php echo Router::getRoute('women_in_tech'); ?>" class="link"><span class="menu_icon" style="background-image:url('/src/images/menu_icons/volunteers.svg');"></span><span class="menu_text">Women in Tech</span></a>
		</li>
		
		<li>
			<a href="<?php echo Router::getRoute('partners'); ?>" class="link"><span class="menu_icon" style="background-image:url('/src/images/menu_icons/partners.svg'); width:21px; margin-right:8px; top:0"></span><span class="menu_text">Partners</span></a>
		</li>
		<li>
			<a href="<?php echo Router::getRoute('video'); ?>" class="link"><span class="menu_icon" style="background-image:url('/src/images/menu_icons/school.svg')"></span><span class="menu_text">Academy Videos</span></a>
		</li>

		<li>
			<a href="<?php echo Router::getRoute('jobs'); ?>" class="link"><span class="menu_icon" style="background-image:url('/src/images/menu_icons/volunteers.svg');"></span><span class="menu_text">Jobs</span></a>
		</li>

		<li>
			<a href="<?php echo Router::getRoute('team'); ?>" class="link"><span class="menu_icon" style="background-image:url('/src/images/menu_icons/team.svg')"></span><span class="menu_text">Team</span></a>
		</li>				
	</ul>
</div>

<div class="left_nav">
	<div class="link active">
		<div class="line"></div>
	</div>
	<div class="link">
		<div class="line"></div>
	</div>
	<div class="link">
		<div class="line"></div>
	</div>
	<div class="link">
		<div class="line"></div>
	</div>
</div>
<?php //require_once 'layout/elements/nav-detail.php'; ?>
<?php require_once 'layout/elements/popup-newsletter.php'; ?>