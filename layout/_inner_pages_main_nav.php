<div id="main_nav_inner" class="main_nav_inner_wrap" style="visibility: hidden;display:none">
  <ul class="main_nav_inner ul-reset">
    <li>
      <a href="<?php echo Router::getRoute('startup'); ?>" class="link_nav <?php echo Router::getActiveRouteClass('startup') ?>">Startup</a>
    </li>
    <li>
      <a href="<?php echo Router::getRoute('investor'); ?>" class="link_nav <?php echo Router::getActiveRouteClass('investor') ?>">Investor</a>
    </li>
    <li>
      <a href="<?php echo Router::getRoute('corporate'); ?>" class="link_nav <?php echo Router::getActiveRouteClass('corporate') ?>">Corporate</a>
    </li>
    <li>
      <a href="<?php echo Router::getRoute('student'); ?>" class="link_nav <?php echo Router::getActiveRouteClass('students_in_tech') ?>">Student</a>
    </li>
    <li class="lastli">
      <a href="<?php echo Router::getRoute('journalist'); ?>" class="link_nav <?php echo Router::getActiveRouteClass('journalist') ?>">Journalist</a>
    </li>
  </ul>
</div>
