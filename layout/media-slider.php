<div class="sectionContent">
        <div class="swiperSection">
            <div class="swiper-container swiper-container-media">
                <div class="swiper-wrapper">
                   <div class="swiper-slide cdSlide">
                            <a href="https://www.sueddeutsche.de" target="_blank">
                                <img class="lazyload-scroll" src="/src/images/partners/sueddeutsche_zeitung.png" title="Süddeutsche Zeitung"/>
                            </a>
                        </div>
                    <?php for($i = 0; $i < count($partners_2016); $i++):
                    if (strpos($partners_2016[$i]['type'],"media partner")!==false || strpos($partners_2016[$i]['type'],"media partner")!==false): ?>

                        <div class="swiper-slide cdSlide">
                            <a href="<?= $partners_2016[$i]['link'] ?>" target="_blank">
                                <img class="lazyload-scroll" src="/src/images/partners/<?= $partners_2016[$i]['logo'] ?>" alt="<?= $partners_2016[$i]['name'] ?>"/>
                            </a>
                        </div>

                    <?php endif; endfor; ?>

                </div>
            </div>
        </div>
    </div>
<div class="clearfix point-none"></div>    