<?php
// local fix to match my vhost settings
$pathPrefix = '';
// $host = $_SERVER['HTTP_HOST'];
// if ($host == 'bap.local') {
//   $pathPrefix = '../';
// }
// 

require_once 'vendor/autoload.php';

// DATA
require_once $pathPrefix.'data/tcs_array_2016.php';
require_once $pathPrefix.'data/tcs_array_2017.php';
require_once $pathPrefix.'data/speakers_array_2016.php';
require_once $pathPrefix.'data/speakers_array_2015.php';
require_once $pathPrefix.'data/speakers_array_2017.php';
require_once $pathPrefix.'data/hotels_array_2016.php';
require_once $pathPrefix.'data/sideevents_array_2016.php';
require_once $pathPrefix.'data/academy_array_2016.php';
require_once $pathPrefix.'data/investor_stage_array_2017.php';
require_once $pathPrefix.'data/partners_array_2017.php';

require_once $pathPrefix.'lib/People.php';
require_once $pathPrefix.'lib/CountryNamer.php';
require_once $pathPrefix.'lib/Facebook.php';
require_once $pathPrefix.'lib/Pathable.php';

/** STUB CLASS REMOVE FOR PRODUCTION MODE */
require_once $pathPrefix.'lib/Faker.php';
require_once $pathPrefix.'lib/Router.php';

define('PASS', 'p4d');

function check_patch_trailing_slash($path, $value, $pass=false) {
	if($pass === true) {
		if(isset($_GET['secret']) && $_GET['secret'] === PASS) {
			return ($path === $value || $path === $value.'/');
		} else {
			return false;
		}
	} 
	return ($path === $value || $path === $value.'/');
}


$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$pathable = new Pathable();
error_reporting(-1);

switch(@$path):

	case '/':
            require_once 'index_.php';
        break;

	// Info
    case check_patch_trailing_slash($path, '/exhibition'):
		require_once $pathPrefix.'pages/exhibition.php';
		break;
    case check_patch_trailing_slash($path, '/exhibition_international'):
		require_once $pathPrefix.'pages/exhibition_intern.php';
		break;
    case check_patch_trailing_slash($path, '/jobs'):
		require_once $pathPrefix.'pages/jobs.php';
		break;
	case check_patch_trailing_slash($path, '/team'):
		require_once $pathPrefix.'pages/team.php';
		break;
    case check_patch_trailing_slash($path, '/faq'):
		require_once $pathPrefix.'pages/faq.php';
		break; 
    case check_patch_trailing_slash($path, '/partners'):
		require_once $pathPrefix.'pages/partners.php';
		break;
    case check_patch_trailing_slash($path, '/travel'):
		require_once $pathPrefix.'pages/hotels.php';
		break;
    case check_patch_trailing_slash($path, '/agenda'):
		require_once $pathPrefix.'pages/agenda.php';
		break;
    case check_patch_trailing_slash($path, '/cluster'):
        require_once $pathPrefix.'pages/cluster/index.php';
        break;
    case check_patch_trailing_slash($path, '/cluster/iot'):
        require_once $pathPrefix.'pages/cluster/iot.php';
        break;
    case check_patch_trailing_slash($path, '/cluster/commerce'):
        require_once $pathPrefix.'pages/cluster/commerce.php';
        break;
    case check_patch_trailing_slash($path, '/cluster/lifestyle'):
        require_once $pathPrefix.'pages/cluster/lifestyle.php';
        break;
    case check_patch_trailing_slash($path, '/cluster/mobility'):
        require_once $pathPrefix.'pages/cluster/mobility.php';
        break;
    case check_patch_trailing_slash($path, '/cluster/money'):
        require_once $pathPrefix.'pages/cluster/money.php';
        break;
    case check_patch_trailing_slash($path, '/cluster/smart-company'):
        require_once $pathPrefix.'pages/cluster/smart_company.php';
        break;
    case check_patch_trailing_slash($path, '/volunteers'):
        require_once $pathPrefix.'pages/volunteers.php';
        break;
    case check_patch_trailing_slash($path, '/raab'):
        require_once $pathPrefix.'pages/raab.php';
        break;  
    case check_patch_trailing_slash($path, '/branson-adwords'):
        require_once $pathPrefix.'pages/branson_adwords.php';
        break; 
    case check_patch_trailing_slash($path, '/side-events'):
        require_once $pathPrefix.'pages/sideevents.php';
        break;
    case check_patch_trailing_slash($path, '/afterparty'):
        require_once $pathPrefix.'pages/afterparty.php';
        break;
    case check_patch_trailing_slash($path, '/network-tables'):
        require_once $pathPrefix.'pages/network-tables.php';
        break; 
    case check_patch_trailing_slash($path, '/pitch'):
        require_once $pathPrefix.'pages/pitch.php';
        break;
    case check_patch_trailing_slash($path, '/video'):
        require_once $pathPrefix.'pages/video.php';
        break;
    case check_patch_trailing_slash($path, '/bits17'):
        require_once $pathPrefix.'pages/newsletter.php';
        break;
    case check_patch_trailing_slash($path, '/info/academy'):
        require_once $pathPrefix.'pages/academy.php';
        break;
    case check_patch_trailing_slash($path, '/investors_stage'):
        require_once $pathPrefix.'pages/investor_stage.php';
        break;


// List
    case check_patch_trailing_slash($path, '/speakers'):
		require_once $pathPrefix.'list/speakers.php';
		break;
	case check_patch_trailing_slash($path, '/table-captains'):
		require_once $pathPrefix.'list/captains.php';
		break;
//	case check_patch_trailing_slash($path, '/attendees'):
//		require_once $pathPrefix.'list/attendees.php';
//		break;


    // Tickets
	case check_patch_trailing_slash($path, '/tickets'):
		require_once $pathPrefix.'pages/tickets/tickets.php';
		break;
//    case check_patch_trailing_slash($path, '/ticket/startup'):
//		require_once $pathPrefix.'pages/buy/index.php';
//		break;
	case check_patch_trailing_slash($path, '/ticket/corporate'):
		require_once $pathPrefix.'pages/buy/buy_corporate.php';
		break;
	case check_patch_trailing_slash($path, '/ticket/investor'):
		require_once $pathPrefix.'pages/buy/buy_investor.php';
		break;
	case check_patch_trailing_slash($path, '/ticket/student'):
		require_once $pathPrefix.'pages/buy/buy_student.php';
		break;
    case check_patch_trailing_slash($path, '/ticket/academia'):
		require_once $pathPrefix.'pages/buy/buy_academia.php';
		break;
 case check_patch_trailing_slash($path, '/ticket/journalist'):
		require_once $pathPrefix.'pages/buy/journalist.php';
		break;

	// Success pages
	case check_patch_trailing_slash($path, '/success'):
		require_once $pathPrefix.'pages/success/index.php';
		break;
    case check_patch_trailing_slash($path, '/success/tickets_first'):
		require_once $pathPrefix.'pages/success/success-tickets_first.php';
		break;
    case check_patch_trailing_slash($path, '/success/tickets_final'):
		require_once $pathPrefix.'pages/success/success-tickets_final.php';
		break;

	case check_patch_trailing_slash($path, '/success/newsletter_first'):
		require_once $pathPrefix.'pages/success/newsletter_first.php';
		break;
	case check_patch_trailing_slash($path, '/success/newsletter_final'):
		require_once $pathPrefix.'pages/success/newsletter_final.php';
		break;
	case check_patch_trailing_slash($path, '/success/exhibition_first'):
		require_once $pathPrefix.'pages/success/exhibition_first.php';
		break;
	case check_patch_trailing_slash($path, '/success/exhibition_final'):
		require_once $pathPrefix.'pages/success/exhibition_final.php';
		break;
	case check_patch_trailing_slash($path, '/success/student_application'):
		require_once $pathPrefix.'pages/success/student_application.php';
		break;
	case check_patch_trailing_slash($path, '/success/journalist_application'):
		require_once $pathPrefix.'pages/success/journalist_application.php';
		break;
    case check_patch_trailing_slash($path, '/success/women_in_tech'):
		require_once $pathPrefix.'pages/success/women_in_tech.php';
		break;
    case check_patch_trailing_slash($path, '/success/pitch'):
		require_once $pathPrefix.'pages/success/pitch.php';
		break;

    case check_patch_trailing_slash($path, '/women_in_tech'):
		require_once $pathPrefix.'pages/women.php';
		break;
    case check_patch_trailing_slash($path, '/women-in-tech'):
		require_once $pathPrefix.'pages/women.php';
		break;


    case check_patch_trailing_slash($path, '/expectation'):
		require_once $pathPrefix.'list/expectation.php';
		break;
//Tickets
    case check_patch_trailing_slash($path, '/view-tickets'):
		require_once $pathPrefix.'pages/tickets/view-tickets.php';
		break;
    case check_patch_trailing_slash($path, '/ticket_corporate'):
		require_once $pathPrefix.'pages/tickets/ticket_corporate.php';
		break;
    case check_patch_trailing_slash($path, '/ticket_institutional_investor'):
		require_once $pathPrefix.'pages/tickets/ticket_inst_investor.php';
		break;
    case check_patch_trailing_slash($path, '/ticket_private_investor'):
		require_once $pathPrefix.'pages/tickets/ticket_priv_investor.php';
		break;
    case check_patch_trailing_slash($path, '/ticket_early_startup'):
		require_once $pathPrefix.'pages/tickets/ticket_ear_startup.php';
		break;
    case check_patch_trailing_slash($path, '/ticket_later_startup'):
		require_once $pathPrefix.'pages/tickets/ticket_lat_startup.php';
		break;

// Legal
	case check_patch_trailing_slash($path, '/legal/imprint'):
		require_once $pathPrefix.'imprint/index.php';
		break;
	case check_patch_trailing_slash($path, '/legal/tos'):
		require_once $pathPrefix.'imprint/tos.php';
		break;
	case check_patch_trailing_slash($path, '/legal/privacy-policy'):
		require_once $pathPrefix.'imprint/pp.php';
		break;
    case check_patch_trailing_slash($path, '/meet-kevin/conditions'):
		require_once $pathPrefix.'imprint/meet-kevin-conditions.php';
		break;

	// Ajax calls
	case '/ajax/get_last_purchase':
		require_once $pathPrefix.'ajax/AjaxHandler.php';
		break;

	// API
	case check_patch_trailing_slash($path, '/api/pathable/get-people', true):
		require_once $pathPrefix.'api/pathable.php';
		break;

	default:
		require_once 'index_.php';
		break;
endswitch;