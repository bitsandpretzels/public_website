<?php

$partners_2017 = [
    [
        'link' => 'http://www.stmwi.bayern.de/',
        'logo' => 'staatsministerium.png',
        'name' => 'Bayerisches Staatsministerium',
        'type' => 'patronage',
    ],
    
    
    
    // Sponsors
    [
        'link' => 'https://www.google.de/',
        'logo' => 'google.png',
        'name' => 'Google',
        'type' => 'sponsor',
    ],
    [
        'link' => 'http://www.ibm.com/',
        'logo' => 'ibm.png',
        'name' => 'ibm',
        'type' => 'sponsor',
    ],
    [
        'link' => 'http://www.audi.de',
        'logo' => 'audi.png',
        'name' => 'Audi',
        'type' => 'sponsor',
    ],
    [
        'link' => 'http://www.amazon.de',
        'logo' => 'amazonde.png',
        'name' => 'amazon',
        'type' => 'sponsor',
    ],
    [
        'link' => 'http://www.salesforce.com/de/',
        'logo' => 'salesforce.png',
        'name' => 'Salesforce',
        'type' => 'sponsor'
    ],
    [
        'link' => 'http://www.wirecard.de',
        'logo' => 'wirecard.png',
        'name' => 'wirecard',
        'type' => 'sponsor',
    ],
    [
        'link' => 'http://www.allianz.de',
        'logo' => 'allianz.png',
        'name' => 'Allianz',
        'type' => 'sponsor',
    ],
	[
        'link' => 'http://www.airbus.com/',
        'logo' => 'airbus.png',
        'name' => 'Airbus',
        'type' => 'sponsor'
    ],
    [
        'link' => 'http://www.sage.de/',
        'logo' => 'sage.png',
        'name' => 'sage',
        'type' => 'sponsor',
    ],
    [
        'link' => 'https://www.hybris.com/de/',
        'logo' => 'sap_hybris.png',
        'name' => 'SAP Hybris',
        'type' => 'sponsor'
    ],
    [
        'link' => 'https://home.kpmg.com/de/en/home.html',
        'logo' => 'kpmg.png',
        'name' => 'KPMG',
        'type' => 'sponsor',
    ],
	[
        'link' => 'https://www.hypovereinsbank.de',
        'logo' => 'hypovereinsbank.png',
        'name' => 'Hypo Vereinsbank',
        'type' => 'sponsor',
    ],
    [
        'link' => 'https://www.payback.de/',
        'logo' => 'payback.png',
        'name' => 'Payback',
        'type' => 'sponsor'
    ],
    [
        'link' => 'http://aws.amazon.com/de',
        'logo' => 'amazon.png',
        'name' => 'amazon',
        'type' => 'sponsor',
    ],
    [
        'link' => 'http://www.play-hub.de/',
        'logo' => 'sky.png',
        'name' => 'Sky',
        'type' => 'sponsor'
    ],
    [
        'link' => 'http://www.interhyp.com',
        'logo' => 'interhyp.png',
        'name' => 'interhyp',
        'type' => 'sponsor',
    ],
    [
        'link' => 'http://www.rakuten.com',
        'logo' => 'rakuten.png',
        'name' => 'rakuten',
        'type' => 'sponsor',
    ],
    [
        'link' => 'http://www.datev.de',
        'logo' => 'datev.png',
        'name' => 'datev',
        'type' => 'sponsor',
    ],
    [
        'link' => 'http://www.gruenderland.bayern/',
        'logo' => 'gruenderland_bayern.png',
        'name' => 'gruenderland bayern',
        'type' => 'sponsor',
    ],
    [
        'link' => 'http://www.invest-in-bavaria.com/',
        'logo' => 'investinbavaria.png',
        'name' => 'Invest in Bavaria',
        'type' => 'sponsor'
    ],
    [
        'link' => 'http://werk1.com/',
        'logo' => 'werkeins.png',
        'name' => 'Werk1',
        'type' => 'sponsor'
    ],
    [
        'link' => 'http://www.baystartup.de/',
        'logo' => 'baystartup.png',
        'name' => 'BayStartUP',
        'type' => 'sponsor'
    ],
    [
        'link' => 'http://www.ihk.de/',
        'logo' => 'ihk.png',
        'name' => 'ihk',
        'type' => 'sponsor',
    ],
    [
        'link' => 'http://www.ebv.com',
        'logo' => 'ebv_elektro.png',
        'name' => 'EBV Elektronik',
        'type' => 'sponsor'
    ],
    [
        'link' => 'http://www.pmi.com/deu/',
        'logo' => 'philip_morris.png',
        'name' => 'Philip Morris',
        'type' => 'sponsor',
    ],
    [
        'link' => 'http://www.munich-startup.de/',
        'logo' => 'munich_startup.png',
        'name' => 'Munich Startup',
        'type' => 'sponsor',
    ],
    [
        'link' => 'http://www.muenchen.de/',
        'logo' => 'City_Of_Munich.png',
        'name' => 'City of Munich',
        'type' => 'sponsor',
    ],
	[
        'link' => 'https://www.next47.com/',
        'logo' => 'nextviersieben.png',
        'name' => 'Next47',
        'type' => 'sponsor'
    ],
	[
        'link' => 'http://nunatak.com/',
        'logo' => 'tng.png',
        'name' => 'The Nunatak Group',
        'type' => 'sponsor'
    ],
    [
        'link' => 'http://www.onpage.org',
        'logo' => 'onpage_logo_blau.png',
        'name' => 'onpage',
        'type' => 'sponsor',
    ],
    [
        'link' => 'http://www.aboalarm.de',
        'logo' => 'aboalarm.png',
        'name' => 'aboalarm',
        'type' => 'sponsor',
    ],
    [
        'link' => 'https://www.linkedin.com/in/felixhaas',
        'logo' => 'felixhaas.png',
        'name' => 'felix haas',
        'type' => 'sponsor'
    ],
    [
        'link' => 'http://www.lfa.de/',
        'logo' => 'lfa.png',
        'name' => 'lfa Förderbank Bayern',
        'type' => 'sponsor',
    ], 
	[
        'link' => 'http://bayernkapital.de/',
        'logo' => 'bayernkapital.png',
        'name' => 'BayernKapital',
        'type' => 'sponsor',
    ],
	[
        'link' => 'http://www.baybg.de/',
        'logo' => 'baybg.png',
        'name' => 'BayernBG',
        'type' => 'sponsor',
    ],
    [
        'link' => 'https://piabo.net/',
        'logo' => 'piabo_pr.png',
        'name' => 'Piabo PR',
        'type' => 'sponsor'
    ],
    



	
    // Premium Media Partners
	[
        'link' => 'http://www.sueddeutsche.de/',
        'logo' => 'sueddeutsche_zeitung.png',
        'name' => 'Sueddeutsche Zeitung', 
        'type' => 'media partner', //premium mpartner
    ],
	
	
	
	
	
    // Media Partners
    
    [
        'link' => 'http://www.berlinvalley.com',
        'logo' => 'berlin_valley.png',
        'name' => 'Berlin Valley',
        'type' => 'media partner',
    ],
    [
        'link' => 'http://www.gruenderszene.de/',
        'logo' => 'gruenderszene.png',
        'name' => 'Gruenderszene',
        'type' => 'media partner',
    ],
    [   
        'link' => 'http://www.n-tv.de/',
        'logo' => 'ntv.png',
        'name' => 'ntv',
        'type' => 'media partner',
    ],
     [
        'link' => 'http://www.t3n.de/',
        'logo' => 't3n.png',
        'name' => 't3n',
        'type' => 'media partner',
    ],
     [
        'link' => 'http://www.vc-magazin.de/',
        'logo' => 'vc_magazin.png',
        'name' => 'fuer gruender',
        'type' => 'media partner',
    ],
    
    /*
    
   
    [
        'link' => 'http://www.absatzwirtschaft.de/',
        'logo' => 'absatzwirtschaft.png',
        'name' => 'absatzwirtschft',
        'type' => 'media partner',
    ], 
    [
        'link' => 'http://www.starting-up.de/',
        'logo' => 'startingup.png',
        'name' => 'startingup',
        'type' => 'media partner',
    ],
    [
        'link' => 'http://www.ispo.com/',
        'logo' => 'ispo.png',
        'name' => 'ispo',
        'type' => 'media partner',
    ],
    [
        'link' => 'http://www.internetworld.de/',
        'logo' => 'internetworld.png',
        'name' => 'internetworld',
        'type' => 'media partner',
    ],
    [
        'link' => 'https://www.wired.de/',
        'logo' => 'wired.png',
        'name' => 'Wired',
        'type' => 'media partner',
    ],
    
    [
        'link' => 'http://www.horizont.net/',
        'logo' => 'horizont.png',
        'name' => 'Horizont',
        'type' => 'media partner',
    ],
    [
        'link' => 'http://www.fuer-gruender.de/',
        'logo' => 'fuer_gruender.png',
        'name' => 'fuer gruender',
        'type' => 'media partner',
    ],
    [
		'link' => 'http://www.wuv.de/',
		'logo' => 'wundv.png',
		'name' => 'W und V',
		'type' => 'media partner',
	],
    
    [
		'link' => 'http://www.computerbild.de/',
		'logo' => 'computerbild.png',
		'name' => 'Computerbild',
		'type' => 'media partner',
	],
	
	[
        'link' => 'http://www.thenordicweb.com/',
        'logo' => 'thenordicweb.png',
        'name' => 'The Nordic Web',
        'type' => 'media partner',
    ],
    [
        'link' => 'http://cointelegraph.com/en',
        'logo' => 'cointelegraph.png',
        'name' => 'CoinTelegraph',
        'type' => 'media partner',
    ],*/
	
	/*
	[
        'link' => 'http://www.thehundert.com/',
        'logo' => 'hundert.png',
        'name' => 'The Hundert',
        'type' => 'media partner',
    ],
	*/
    /*
	[
        'link' => 'http://www.frenchweb.fr',
        'logo' => 'frenchweb.png',
        'name' => 'Frenchweb',
        'type' => 'media partner',
    ],
	[
        'link' => 'http://www.startupvalley.news',
        'logo' => 'startup_valley.png',
        'name' => 'StartupValley',
        'type' => 'media partner',
    ],
	[
        'link' => 'http://www.deal-magazin.com/',
        'logo' => 'dealmagazin.png',
        'name' => 'Dealmagazin',
        'type' => 'media partner',
    ],
	[
        'link' => 'https://www.derbrutkasten.com/',
        'logo' => 'brutkasten.png',
        'name' => 'Brutkasten',
        'type' => 'media partner',
    ],
	[
		'link' => 'http://www.helsinkitimes.fi/',
		'logo' => 'helsinki_times.png',
		'name' => 'Helsinki Times',
		'type' => 'media partner',
	],
    [
		'link' => 'http://gruenderfreunde.de/',
		'logo' => 'gruenderfreunde.png',
		'name' => 'Gründerfreunde',
 	    'type' => 'media partner',
	],*/
/*
    [
        'link' => 'http://www.testbild.de/',
        'logo' => 'test-bild.png',
        'name' => 'Test Bild',
        'type' => 'media partner',
    ],*/

// Product Partners

    [
        'link' => 'http://www.dealmatrix.io/#startupportal ',
        'logo' => 'dealmatrix.png',
        'name' => 'eventbrite',
        'type' => 'product partner',
    ],
    [
        'link' => 'https://www.eventbrite.com/',
        'logo' => 'eventbrite.png',
        'name' => 'eventbrite',
        'type' => 'product partner',
    ],
    /*[
        'link' => 'http://www.sofitel.com/de/deutschland/index.shtml ',
        'logo' => 'sofitel.png',
        'name' => 'sofitel',
        'type' => 'product partner',
    ],*/
    /*[
        'link' => 'https://en.99designs.de/',
        'logo' => '99designs.png',
        'name' => '99designs',
        'type' => 'product partner',
    ],*/
    [
        'link' => 'http://www.candis.io/',
        'logo' => 'candis.png',
        'name' => 'candis',
        'type' => 'product partner',
    ],
	/*[
        'link' => 'http://gaudiherz.de/',
        'logo' => 'gaudiherz_logo.png',
        'name' => 'GaudiHerz',
        'type' => 'product partner',
    ],
	[
        'link' => 'https://www.microsoft.com/microsoft-surface-hub/de-de',
        'logo' => 'microsoft_surface_hub.png',
        'name' => 'Microsoft Surface Hub',
        'type' => 'product partner',
    ],*/
	/*[
        'link' => 'http://www.networktables.com/',
        'logo' => 'networktables_logo.png',
        'name' => 'Networktables',
        'type' => 'product partner',
    ],*/
    [
        'link' => 'http://www.daller-tracht.de/',
        'logo' => 'daller.png',
        'name' => 'Trachten Daller',
        'type' => 'product partner',
    ],
    [
        'link' => 'https://www.roccofortehotels.com/de/hotels-and-resorts/the-charles-hotel/',
        'logo' => 'charles_hotel.png',
        'name' => 'Charles Hotel',
        'type' => 'product partner',
    ],
    [
        'link' => 'http://www.fabrik19.de/',
        'logo' => 'fabrick_19.png',
        'name' => 'Fabrik 19',
        'type' => 'product partner',
    ],

// Network Partners

//Wichtige Reihenfolge

    [
        'link' => 'http://www.venionaire.com/',
        'logo' => 'venionaire_capital.png',
        'name' => 'venionaire_capital',
        'type' => 'network partner'
    ],
	[
		'link' => 'http://www.miitya.com/',
		'logo' => 'miitya.png',
		'name' => 'Social Event GmbH',
		'type' => 'network partner',
	],
	[
        'link' => 'http://www.gruenderkueche.de/',
        'logo' => 'gruenderkueche.png',
        'name' => 'Gründerküche',
        'type' => 'network partner'
    ],
	
	//Gründerfreunde
	//Climate_KIC
	//12minme
	[
		'link' => 'http://www.unternehmer.de/',
		'logo' => 'unternehmer-de.png',
		'name' => 'unternehmer.de',
		'type' => 'network partner',
	],
	[
        'link' => 'http://www.startups-allgaeu.de/',
        'logo' => 'startup_allgaeu.png',
        'name' => 'startup_allgaeu.png',
        'type' => 'network partner'
    ],
	[
        'link' => 'http://www.xlhealth.de',
        'logo' => 'xl_health_ag.png',
        'name' => 'XL Health AG',
        'type' => 'network partner'
    ],
	[
        'link' => 'http://www.startupradio.de',
        'logo' => 'startupradio.png',
        'name' => 'Startupradio.de',
        'type' => 'network partner'
    ],
	[
        'link' => 'http://www.teg-ev.de/',
        'logo' => 'teg.png',
        'name' => 'TEG - The entrepreneurial group',
        'type' => 'network partner'
    ],
	[
        'link' => 'http://www.startupbrett.de/',
        'logo' => 'startupbrett.png',
        'name' => 'startupbrett',
        'type' => 'network partner'
    ],
	//Scale 11
	[
        'link' => 'http://www.fempreneur.de/',
        'logo' => 'fempreneur.png',
        'name' => 'fermpreneur',
        'type' => 'network partner'
    ],
	[
		'link' => 'http://www.unternehmertum.de',
		'logo' => 'unternehmer-tum.png',
		'name' => 'UnternehmerTUM',
		'type' => 'network partner',
	],
	 [
		'link' => 'http://www.sce.de/',
		'logo' => 'center_for_entrepreneurship.png',
		'name' => 'Strascheg Center for Entrepreneuship (SCE)',
		'type' => 'network partner',
	],
	
	
//Sortierung nach Anmeldung
    
    [
        'link' => 'http://www.thebithouse.org/',
        'logo' => 'bithouse.png',
        'name' => 'bithouse.png',
        'type' => 'network partner'
    ],

    [
        'link' => 'http://www.dvtm.net/',
        'logo' => 'dvtm.png',
        'name' => 'DVTM',
        'type' => 'network partner'
    ],
    [
        'link' => 'http://www.face-entrepreneurship.eu/en/',
        'logo' => 'FACE.png',
        'name' => 'face',
        'type' => 'network partner'
    ],
    [
        'link' => 'https://www.founderio.com/',
        'logo' => 'founderio.png',
        'name' => 'founderio',
        'type' => 'network partner'
    ],
    [
        'link' => 'http://www.junge-gruender.de/',
        'logo' => 'junge_gruender.png',
        'name' => 'junge gruender',
        'type' => 'network partner'
    ],
    [
        'link' => 'http://startup-stuttgart.de/',
        'logo' => 'startup_stuttgart.png',
        'name' => 'startup stuttgart',
        'type' => 'network partner'
    ],
    [
        'link' => 'https://www.venturate.com/',
        'logo' => 'venturate.png',
        'name' => 'venturate',
        'type' => 'network partner'
    ],
    [
        'link' => 'http://www.yearofthegoat.co/',
        'logo' => 'yotg.png',
        'name' => 'year of the goat',
        'type' => 'network partner'
    ],
    [
        'link' => 'http://www.young-targets.com/',
        'logo' => 'young_targets.png',
        'name' => 'young targets  ',
        'type' => 'network partner'
    ],
    [
        'link' => 'http://www.gruenderwettbewerb.de/',
        'logo' => 'gruenderwettbewerb.png',
        'name' => 'gruenderwettbewerb',
        'type' => 'network partner'
    ],
    [
        'link' => 'https://www.f6s.com/',
        'logo' => 'f6s.png',
        'name' => 'f6s',
        'type' => 'network partner'
    ],
    [
        'link' => 'http://event0.com/',
        'logo' => 'event0.png',
        'name' => 'event0',
        'type' => 'network partner'
    ],
    [
        'link' => 'http://projecter.de/',
        'logo' => 'projecter.png',
        'name' => 'projecter',
        'type' => 'network partner'
    ],
    [
        'link' => 'http://www.idm-suedtirol.com/en/home.html',
        'logo' => 'idm_suedtirol.png',
        'name' => 'IDM Suedtirol',
        'type' => 'network partner'
    ],

    [
        'link' => 'http://www.hamburg-startups.net/',
        'logo' => 'hamburg_startup.png',
        'name' => 'Hamburg Startup',
        'type' => 'network partner'
    ],
    [
        'link' => 'http://medialab-bayern.de/',
        'logo' => 'medialab_bayern.png',
        'name' => 'Medialab Bayern',
        'type' => 'network partner'
    ],
    [
        'link' => 'http://fempreneur-summit.com/',
        'logo' => 'fempreneur_summit.png',
        'name' => 'fempreneur_summit',
        'type' => 'network partner'
    ],
    [
        'link' => 'http://www.startstories.de/',
        'logo' => 'startup_stories.png',
        'name' => 'startup_stories',
        'type' => 'network partner'
    ],
    [
        'link' => 'http://www.axelspringerplugandplay.com/',
        'logo' => 'axel_springer_plugandplay.png',
        'name' => 'axel_springer_plugandplay',
        'type' => 'network partner'
    ],
    [
        'link' => 'http://fielfalt.de/',
        'logo' => 'fielfalt.png',
        'name' => 'fielfalt',
        'type' => 'network partner'
    ],
	[
        'link' => 'http://www.munich-business-school.de/',
        'logo' => 'munich_business_school.png',
        'name' => 'Munich Business School',
        'type' => 'network partner'
    ],[
        'link' => 'http://build.or.at/',
        'logo' => 'build_gruenderzentrum.png',
        'name' => 'build! Gründerzentrum Kärnten GmbH',
        'type' => 'network partner'
    ],
	[
        'link' => 'http://www.deutschland-startet.de',
        'logo' => 'ewd_expertennetzwerk.png',
        'name' => 'EWD Expertennetzwerk Deutschland GmbH',
        'type' => 'network partner'
    ],
	[
        'link' => 'http://www.fintech-law.de/',
        'logo' => 'fintechlaw.png',
        'name' => 'FinTech Law',
        'type' => 'network partner'
    ],
	[
        'link' => 'https://www.stift-thueringen.de/',
        'logo' => 'stift.png',
        'name' => 'Stiftung für Technologie Innovation und Forschung Thüringen',
        'type' => 'network partner'
    ],
	[
        'link' => 'http://www.done.by',
        'logo' => 'done.by.png',
        'name' => 'done.by GmbH',
        'type' => 'network partner'
    ],
	[
        'link' => 'http://www.austrianstartups.com',
        'logo' => 'austrain_startups.png',
        'name' => 'AustrianStartups',
        'type' => 'network partner'
    ],
	[
        'link' => 'http://www.bmwstartupgarage.com',
        'logo' => 'bmw_garage.png',
        'name' => 'BMW Startup Garage',
        'type' => 'network partner'
    ],
	[
        'link' => 'http://www.skinnovation.io',
        'logo' => 'skinnovation_web.png',
        'name' => 'Skinnovation',
        'type' => 'network partner'
    ],
	[
        'link' => 'http://innovation-entrepreneurship.at/',
        'logo' => 'uni_insbruck.png',
        'name' => 'Universität Innsbruck - Innovation & Entrepreneurship',
        'type' => 'network partner'
    ],
	[
        'link' => 'http://www.meinstartup.com',
        'logo' => 'meinstartup.png',
        'name' => 'MeinStartUp',
        'type' => 'network partner'
    ],
	[
        'link' => 'http://www.medianet-bb.de',
        'logo' => 'media-net-berlin-brandenburg.png',
        'name' => 'media.net berlinbrandenburg e.V.',
        'type' => 'network partner'
    ],
	[
        'link' => 'https://startalps.co/',
        'logo' => 'startalps-co.png',
        'name' => 'startalps.co',
        'type' => 'network partner'
    ],
	[
        'link' => 'http://www.digital-analytics-association.de/index.php',
        'logo' => 'digital_analytics_association.png',
        'name' => 'Digital Analytics Association Germany',
        'type' => 'network partner'
    ],
/*	[
        'link' => 'http://www.meetup.com/IoTMunich/',
        'logo' => '',
        'name' => 'IoT Munich Meetup',
        'type' => 'network partner'
    ],*/
	[
        'link' => 'http://www.meetup.com/de-DE/Instant-Start-Up-Munich/',
        'logo' => 'instant-startup_munich_web.png',
        'name' => 'Instant Start-Up Munich',
        'type' => 'network partner'
    ],
	[
        'link' => 'https://www.xing.com/communities/groups/start-up-muenchen-4014-1040711',
        'logo' => 'xing_gruppe_start_up_munich.png',
        'name' => 'XING Gruppe Start-Up München',
        'type' => 'network partner'
    ],
    [
        'link' => 'https://www.uni.li/de/studium/master/masterstudiengang-entrepreneurship',
        'logo' => 'universitaet_liechtenstein.png',
        'name' => 'Universität Liechtenstein',
        'type' => 'network partner',
    ],
    [
        'link' => 'http://theartsplus.com/',
        'logo' => 'the_arts.png',
        'name' => 'THE ARTS+',
        'type' => 'network partner',
    ],
     [
        'link' => 'http://www.iamwire.com/',
        'logo' => 'iamwire.png',
        'name' => 'iamwire',
        'type' => 'network partner',
    ],
    [
        'link' => 'http://d.tales.de/',
        'logo' => 'dtales.png',
        'name' => 'd.Tales',
        'type' => 'network partner',
    ],
    [
        'link' => 'http://www.smartworkers.net/',
        'logo' => 'smartworkers.png',
        'name' => 'smartworkers',
        'type' => 'network partner',
    ],
    [
        'link' => 'http://www.fuckupnights.cz/',
        'logo' => 'fuckup.png',
        'name' => 'FuckUp Nights Prague',
        'type' => 'network partner',
    ],
    [
        'link' => 'http://innovationfactory.bayern/',
        'logo' => 'inno-fac.png',
        'name' => 'InnovationFactory GmbH',
        'type' => 'network partner',
    ],
    [
        'link' => 'http://www.mcbw.de/',
        'logo' => 'mcbw.png',
        'name' => 'Munich Creative Business Week (MCBW)',
        'type' => 'network partner',
    ],
	[
        'link' => 'http://www.foundersfight.club',
        'logo' => 'founders_fight_club_ug.png',
        'name' => 'Founders Fight Club UG',
        'type' => 'network partner'
    ],
	[
        'link' => 'https://getstarted.de',
        'logo' => 'get _started_by_bitkom.png',
        'name' => 'Get Started by Bitkom',
        'type' => 'network partner'
    ],
	[
        'link' => 'http://www.startupnetzwerk.org',
        'logo' => 'stipendiatisches_startup_netzwerk.png',
        'name' => 'Stipendiatisches Startup-Netzwerk SUN',
        'type' => 'network partner'
    ],
	[
        'link' => 'https://factorycampus.de/',
        'logo' => 'factory_campus.png',
        'name' => 'Factory Campus',
        'type' => 'network partner'
    ],
	[
        'link' => 'http://hamburg.betahaus.de/',
        'logo' => 'betahaus_hamburg.png',
        'name' => 'betahaus Hamburg',
        'type' => 'network partner'
    ],
    [
        'link' => 'http://www.netznews.org/',
        'logo' => 'netz_news.png',
        'name' => 'Netz News',
        'type' => 'network partner',
    ],
	[
        'link' => 'http://www.siliconvilstal.de/',
        'logo' => 'silicon_vilstal.png',
        'name' => 'Silicon Vilstal',
        'type' => 'network partner'
    ],
	[
		'link' => 'http://www.fham.de',
		'logo' => 'hochs_angewende_managment.png',
		'name' => 'Hochschule für angewandtes Management GmbH',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.tech2b.at',
		'logo' => 'tech2be.png',
		'name' => 'tech2b Inkubator GmbH',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.europeaninnovationhub.com',
		'logo' => 'europea_innovation_hub.png',
		'name' => 'European Innovation Hub',
		'type' => 'network partner',
	],
    [
		'link' => 'http://paranoid-internet.de/',
		'logo' => 'paranoid.png',
		'name' => 'Paranoid Internet',
 	    'type' => 'network partner',
    ],
	[
		'link' => 'http://www.bvkap.de',
		'logo' => 'german_private_equity.png',
		'name' => 'Bundesverband Deutscher Kapitalbeteiligungsgesellschaften',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.innovation-village.com',
		'logo' => 'innovation_village.png',
		'name' => 'Innovation Village',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.dresden-exists.de',
		'logo' => 'dresdenexists.png',
		'name' => 'dresden|exists',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.prnews.io',
		'logo' => 'prnews.png',
		'name' => 'prnewsio',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.barcinno.com/',
		'logo' => 'barcinno.png',
		'name' => 'Barcinno',
		'type' => 'network partner',
	],

	[
		'link' => 'http://www.capscovil.com',
		'logo' => 'capscovil.png',
		'name' => 'Capscovil',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.diversity-natives.com',
		'logo' => 'diversity_natives.png',
		'name' => 'Diversity Natives',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.hightech-venture-days.com',
		'logo' => 'hightech_venture_days.png',
		'name' => 'HIGHTECH VENTURE DAYS',
		'type' => 'network partner',
	],

	
	[
		'link' => 'http://www.afce.co/',
		'logo' => 'crop_entrepn.png',
		'name' => 'Corporate Entrepreneurship',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.gruendermetropole-berlin.de',
		'logo' => 'gruendermetro.png',
		'name' => 'Gründermetro',
		'type' => 'network partner',
	],
	[
		'link' => 'https://www.hubraum.com/',
		'logo' => 'hubraum.png',
		'name' => 'Hubraum',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.konferenz-fotografie.de',
		'logo' => 'konferenz_fotografie.png',
		'name' => 'Konferenz Fotografie',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.humboldt-innovation.de/',
		'logo' => 'humbold_innovation.png',
		'name' => 'humbold innovation',
		'type' => 'network partner',
	],
	[
		'link' => 'http://munich-datageeks.de',
		'logo' => 'datageeks.png',
		'name' => 'munich datageeks',
		'type' => 'network partner',
	],
	[
		'link' => 'https://startups.ch/de/',
		'logo' => 'startups-ch.png',
		'name' => 'Startups-Ch',
		'type' => 'network partner',
	],
	[
		'link' => 'http://selbststaendigkeit.de/',
		'logo' => 'selbststaendigkeit.png',
		'name' => 'selbststaedigkeit de',
		'type' => 'network partner',
	],
	[
		'link' => 'https://www.gruenderlexikon.de/',
		'logo' => 'gruenderlexikon.png',
		'name' => 'Gründerlexikon',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.webmontag-berlin.de/',
		'logo' => 'webmontag.png',
		'name' => 'webmontag',
		'type' => 'network partner',
	],	
	[
		'link' => 'http://dmexco.de/',
		'logo' => 'dmexco.png',
		'name' => 'dmexco',
		'type' => 'network partner',
	],
	
	[
		'link' => 'http://clutch.frauwenk.de/',
		'logo' => 'clutch.png',
		'name' => 'Clutch',
		'type' => 'network partner',
	],
	[
		'link' => 'https://startupgermany.org/',
		'logo' => 'startup_ger.png',
		'name' => 'Startup Germany e.V.',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.nexussquared.co/',
		'logo' => 'nexussquared.png',
		'name' => 'nexussquared',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.egz.de/',
		'logo' => 'exgruezent.png',
		'name' => 'Existenzgründerzentrum Ingolstadt GmbH',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.ebs-site.de',
		'logo' => 'strascheg_instit.png',
		'name' => 'Strascheg Institute for Innovation, Transformation & Entrepreneurship',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.gruenderimpuls.de/',
		'logo' => 'smart_concept.png',
		'name' => 'Smart Concept GmbH',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.startupheatmap.eu/',
		'logo' => 'euro_startup_init.png',
		'name' => 'European Startup Initiative',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.cebit.de/scale11',
		'logo' => 'deut_messe.png',
		'name' => 'Deutsche Messe AG (CeBIT)',
		'type' => 'network partner',
	],
	[
		'link' => 'http://high-tech-gruenderfonds.de/de/',
		'logo' => 'high_tech_gruenfond.png',
		'name' => 'High-Tech Gründerfonds GmbH',
		'type' => 'network partner',
	],
	[
        'link' => 'http://www.entrepreneurship-center.uni-muenchen.de/',
        'logo' => 'uni_lmu.png',
        'name' => 'LMU',
        'type' => 'network partner',
    ],
	[
        'link' => 'http://www.geektime.com/',
        'logo' => 'geektime-icon.png',
        'name' => 'Geektime',
        'type' => 'network partner',
    ],
	
	//Neu seit 29.08
	
	[
		'link' => 'http://www.igz.wuerzburg.de/',
		'logo' => 'igz_wuerzburg.png',
		'name' => 'IGZ Würzburg',
		'type' => 'network partner',
	],
	[
		'link' => 'http://talentgarden.org/',
		'logo' => 'talent_garden.png',
		'name' => 'Talent Garden',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.gruendungsengel.org',
		'logo' => 'gruendungsengel.png',
		'name' => 'Gründungsengel e.V.',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.sirius-venture.com',
		'logo' => 'sirius_venture.png',
		'name' => 'Sirius Venture Partners GmbH',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.blinkked.com',
		'logo' => 'blinkked.png',
		'name' => 'Blinkked',
		'type' => 'network partner',
	],
	[
		'link' => 'https://geistreich78.info',
		'logo' => 'geistreich.png',
		'name' => 'geistreich78 - Annette und Stefan Hoffmeister GbR',
		'type' => 'network partner',
	],
	[
		'link' => 'http://ms-spacelab.com',
		'logo' => 'spacelab_tech_accelerator.png',
		'name' => 'Media-Saturn SPACELAB Tech Accelerator',
		'type' => 'network partner',
	],
	[
		'link' => 'http://deutscher-gruenderverband.de',
		'logo' => 'deutscher_ gruenderverband.png',
		'name' => 'Deutscher Gründerverband',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.labfolder.com',
		'logo' => 'labfolder_gmbh.png',
		'name' => 'labfolder GmbH',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.gruenderfonds.at',
		'logo' => 'aws_gruenderfonds.png',
		'name' => 'aws Gründerfonds',
		'type' => 'network partner',
	],
	[
		'link' => 'http://factoryberlin.com/',
		'logo' => 'factory_berlin.png',
		'name' => 'Factory Berlin',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.business-angels.de/',
		'logo' => 'business_angels_netzwerk_deutschland.png',
		'name' => 'Business Angels Netzwerk Deutschland',
		'type' => 'network partner',
	],
	[
		'link' => 'http://beta-i.pt/',
		'logo' => 'beta-i.png',
		'name' => 'Beta-i',
		'type' => 'network partner',
	],
	[
		'link' => 'https://www.conda.eu/en/',
		'logo' => 'conda_ag.png',
		'name' => 'CONDA AG',
		'type' => 'network partner',
	],
	[
		'link' => 'https://www.rkw-kompetenzzentrum.de/',
		'logo' => 'rkw_kompetenzzentrum.png',
		'name' => 'RKW Kompetenzzentrum',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.germanystartupjobs.com/',
		'logo' => 'germany_startup_jobs.png',
		'name' => 'Germany Startup Jobs',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.mates-muenchen.de/',
		'logo' => 'mates.png',
		'name' => 'MATES',
		'type' => 'network partner',
	],
	[
		'link' => 'http://hack.institute/en/home/',
		'logo' => 'hack.institute.png',
		'name' => 'hack.institute',
		'type' => 'network partner',
	],
	[
		'link' => 'http://www.art-vibes.com/',
		'logo' => 'artvibes.png',
		'name' => 'ArtVibes',
		'type' => 'network partner',
	],
    //Neu seit 12.09
    [
        'link' => 'http://www.czechinvest.org/en',
        'logo' => 'czechinvest.png',
        'name' => 'CzechInvest',
        'type' => 'network partner',
    ],
    [
        'link' => 'http://www.seedstarsworld.com',
        'logo' => 'seedstars_world.png',
        'name' => 'Seedstars World',
        'type' => 'network partner',
    ],
    [
        'link' => 'https://www.czechstartups.org/',
        'logo' => 'czechstartups-org.png',
        'name' => 'czechstartups.org',
        'type' => 'network partner',
    ],
    [
        'link' => 'http://wannabiz.com.ua/',
        'logo' => 'wannabiz.png',
        'name' => 'WannaBiz',
        'type' => 'network partner',
    ],
    [
        'link' => 'http://startupukraine.com/',
        'logo' => 'startup_ukraine.png',
        'name' => 'Startup Ukraine',
        'type' => 'network partner',
    ],
    [
        'link' => 'http://www.hs-kempten.de/home.html',
        'logo' => 'hochschule_kempten.png',
        'name' => 'Hochschule Kempten',
        'type' => 'network partner',
    ],
    [
        'link' => 'http://www.esa-bic.de/',
        'logo' => 'esa_bic_bavaria.png',
        'name' => 'ESA BIC Bavaria',
        'type' => 'network partner',
    ],
    [
        'link' => 'http://www.anwendungszentrum.de/',
        'logo' => 'azo.png',
        'name' => 'Space of Innovation',
        'type' => 'network partner',
    ],
    [
        'link' => 'http://www.startupolic.com',
        'logo' => 'startupolic-com.png',
        'name' => 'Startupolic.com',
        'type' => 'network partner',
    ],
    
    [
        'link' => 'http://www.ideentriebwerkgraz.com/',
        'logo' => 'ideen_triebwerk_graz.png',
        'name' => 'Ideen Triebwerk Graz',
        'type' => 'network partner',
    ],
    
    [
        'link' => 'http://muenchner-webwoche.de/',
        'logo' => 'muc_webwoche.png',
        'name' => 'Webwoche München',
        'type' => 'network partner',
    ],
    [
        'link' => 'http://isarnetz.de/',
        'logo' => 'isarnetz.png',
        'name' => 'Isarnetz',
        'type' => 'network partner',
    ],
        
    [
        'link' => 'http://reaktorwarsaw.com/',
        'logo' => 'reaktorwarsaw.png',
        'name' => 'ReaktorWarsaw',
        'type' => 'network partner',
    ],
    [
        'link' => 'http://women2.com/',
        'logo' => 'women2.png',
        'name' => 'Women 2.0',
        'type' => 'network partner',
    ],
    /*
    [
        'link' => 'www.webdecologne.de',
        'logo' => 'web_de_cologne_e.v..png',
        'name' => 'Web de Cologne e.V.',
        'type' => 'network partner',
    ],*/
    
    
    [
        'link' => 'http://www.startingthingsup.com/',
        'logo' => 'startingthingsup.png',
        'name' => 'StartingThingsUp',
        'type' => 'network partner',
    ],
    
   [
        'link' => 'http://www.aspire.org.pl/',
        'logo' => 'aspire.png',
        'name' => 'Aspire',
        'type' => 'network partner',
    ],






	
	
/*	
	[
		'link' => 'https://www.entrepreneurship.de/summit/',
		'logo' => 'stift_entrepreneurship.png',
		'name' => 'Stiftung Entrepreneurship',
		'type' => 'network partner',
	],
*/	
]
?>