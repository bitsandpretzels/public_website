<?php 
static $expec = array(

	 array( 
        'title' => 'World Class Keynote',
        'sort_name' => '01_Keynote',
        'description' => 'Some of the most successful national and international entrepreneurs, investors and CEOs shared ',
        'link_text' => 'View all speakers',
        'link_page' => 'speakers',
        'image' => 'kevin_spacey.png',
        'type' => 'founder',
    ), 
	/*
    Types: founder | investor | corporate
    */
    array( 
        'title' => 'Table Captains',
        'sort_name' => '02_Keynote',
        'description' => 'Every table at the grand finale is hosted by a Table Captain and all attendees can choose one of those handpicked CEOs',
        'link_text' => 'View all captains',
        'link_page' => 'speakers',
        'image' => 'kevin_spacey.png',
        'type' => 'founder',
    ), 
    array( 
        'title' => 'Academy Stage',
        'sort_name' => '03_Keynote',
        'description' => 'Partnerships with the aim of forming long-lasting relationships with mutual benefits forms the basis for the',
        'link_text' => 'View academy program',
        'link_page' => 'speakers',
        'image' => 'kevin_spacey.png',
        'type' => 'investor',
    ), 
    array( 
        'title' => 'Oktoberfest',
        'sort_name' => '04_Keynote',
        'description' => 'Bits & Pretzels creates unique networking around Munich Oktoberfest and encourages meaningful relationships',
        'link_text' => 'View the Bierzelt',
        'link_page' => 'speakers',
        'image' => 'kevin_spacey.png',
        'type' => 'founder',
    ), 
	/*
    Types: founder | investor | corporate
    */
    array( 
        'title' => 'Networking App',
        'sort_name' => '05_Keynote',
        'description' => 'Als Mobile App (auf Deutsch meist in der Kurzform die App [ɛp], eine Abkürzung für den Fachbegriff Applikation; teils auch',
        'link_text' => 'View bits&pretzels Apps',
        'link_page' => 'speakers',
        'image' => 'kevin_spacey.png',
        'type' => 'founder',
    ), 
    array( 
        'title' => 'The Bavarian Flare',
        'sort_name' => '06_Keynote',
        'description' => 'Das Oktoberfest in München ist das größte Volksfest der Welt. Es findet seit 1810 auf der i..',
        'link_text' => 'View academy program',
        'link_page' => 'speakers',
        'image' => 'kevin_spacey.png',
        'type' => 'founder',
    ), 

// outer array
);





static $tcs_2017 = array(
	
    array(
        'name' => 'Dorothee Bär',
        'sort_name' => 'Bär',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'State Secretary',
        'company' => 'Federal Ministry of Transport and Digital Infrastructure',
        'image' => 'dorothee_baer.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Ralf Kleber',
        'sort_name' => 'Kleber',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'VP, Country Manager',
        'company' => 'Amazon.com',
        'image' => 'ralf_kleber.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Stephanie Czerny',
        'sort_name' => 'Czerny',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'Managing Director',
        'company' => 'DLD Media GmbH',
        'image' => 'stephanie_czerny.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Max Wittrock',
        'sort_name' => 'Wittrock',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'CEO / Co-Founder',
        'company' => 'mymuesli GmbH',
        'image' => 'max_wittrock.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Dr. Paul-Bernhard Kallen',
        'sort_name' => 'Kallen',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'CEO',
        'company' => 'Hubert Burda Media',
        'image' => 'paul-bernhard_kallen.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Sebastian Diemer',
        'sort_name' => 'Diemer',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'Founder & CEO',
        'company' => 'Blackbill',
        'image' => 'sebastian_diemer.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Kristin Müller',
        'sort_name' => 'Müller',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'Investment Manager',
        'company' => 'High-Tech Gründerfonds Management GmbH',
        'image' => 'kristin_mueller.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Lukasz Gadowski',
        'sort_name' => 'Gadowski',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'Entrepreneur & Investor',
        'company' => '',
        'image' => 'lukasz_gadowski.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Nora Heer',
        'sort_name' => 'Heer',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'Managing Director',
        'company' => 'Loopline Systems',
        'image' => 'nora_heer.png',
        'network_table' => '',
    ),


    array(
        'name' => 'Wieland Holfelder',
        'sort_name' => 'Holfelder',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'Engineering Director & Site Lead',
        'company' => 'Google',
        'image' => 'wieland_holfelder.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Lars Jankowfsky',
        'sort_name' => 'Jankowfsky',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'General Partner',
        'company' => 'NFQ',
        'image' => 'lars_jankowfsky.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Heiko Hubertz',
        'sort_name' => 'Hubertz',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'Founder & CEO',
        'company' => 'WHOW Games',
        'image' => 'heiko_hubertz.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Titus Dittmann',
        'sort_name' => 'Dittmann',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'Social Entrepreneur',
        'company' => 'titus GmbH',
        'image' => 'titus_dittmann.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Christian Ramme',
        'sort_name' => 'Ramme',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'Managing Director',
        'company' => 'ACXIT Capital Partners',
        'image' => 'christian_ramme.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Martin Sinner',
        'sort_name' => 'Sinner',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'Managing Director',
        'company' => 'Electronic Online Services',
        'image' => 'martin_sinner.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Hendrik Brandis',
        'sort_name' => 'Brandis',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'Partner',
        'company' => 'Earlybird Venture Capital',
        'image' => 'hendrik_brandis.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Valentin Stalf',
        'sort_name' => 'Stalf',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'Founder & CEO',
        'company' => 'N26',
        'image' => 'valentin_stalf.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Dr. Philipp Kreibohm',
        'sort_name' => 'Kreibohm',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'Founder / Co-CEO',
        'company' => 'Home24 AG',
        'image' => 'dr_philipp_kreibohm.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Rob Moffat',
        'sort_name' => 'Moffat',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'Partner',
        'company' => 'Balderton Capital',
        'image' => 'rob_moffat.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Thomas Preuß',
        'sort_name' => 'Preuß',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'VC Partner',
        'company' => 'Deutsche Telekom Capital Partners Management GmbH',
        'image' => 'thomas_preuss.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Miriam Wohlfarth',
        'sort_name' => 'Wohlfarth',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'Founder and Managing Director',
        'company' => 'RatePAY',
        'image' => 'miriam_wohlfahrt.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Dr. Florian Langenscheidt',
        'sort_name' => 'Langenscheidt',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'Business Angel',
        'company' => 'Langenscheidt Verlagsgruppe',
        'image' => 'dr_florian_langenscheidt.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Matthias Müller',
        'sort_name' => 'Müller',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'Partner',
        'company' => 'Global Founders Capital',
        'image' => 'matthias_mueller.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Julian Riedlbauer',
        'sort_name' => 'Riedlbauer',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'Partner',
        'company' => 'GP Bullhound',
        'image' => 'julian_riedlbauer.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Nick Caldwell',
        'sort_name' => 'Caldwell',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'VP of Engineering',
        'company' => 'Reddit',
        'image' => 'nick_caldwell.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Alexander Kudlich',
        'sort_name' => 'Kudlich',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'Member of the Management Board',
        'company' => 'Rocket Internet SE',
        'image' => 'alexander_kudlich.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Udo Schloemer',
        'sort_name' => 'Schloemer',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'CEO & Founder',
        'company' => 'Factory Works GmbH',
        'image' => 'udo_schloemer.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Gabriele Böhmer',
        'sort_name' => 'Böhmer',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'Director',
        'company' => 'Munich Startup',
        'image' => 'boehmer_gabriele.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Joachim Müller',
        'sort_name' => 'Müller',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'CEO',
        'company' => 'Allianz Beratungs- und Vertriebs-AG / Allianz Versicherungs-AG',
        'image' => 'joachim_mueller.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Alex Spain',
        'sort_name' => 'Spain',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'Senior Associate',
        'company' => 'Atomico',
        'image' => 'alex_spain.png',
        'network_table' => '',
    ),

    array(
        'name' => 'Olivier Schuepbach',
        'sort_name' => 'Schuepbach',
        'type' => 'entrepreneur, press, industry_leader, investor, forbes',
        'title' => 'General Partner',
        'company' => 'Partech Ventures',
        'image' => 'olivier_schuepbach.png',
        'network_table' => '',
    ),

// outer array
);

/*
foreach($tcs as $key => $value) {
    $value['sort_name'] = strtolower($value['sort_name']);
    $tcs[$key] = $value;
}
*/