<?php 

static $tcs_2016 = array(

	 array( 
        'name' => 'Nathan Blecharczyk',
        'sort_name' => 'Blecharczyk',
        'type' => 'entrepreneur',
        'title' => 'Co-Founder & CTO',
        'company' => 'airbnb',
        'image' => 'blecharczky_nathan.png',
        'network_table' => '',
    ), 
	/*
    array( 
        'name' => 'Florian Gschwandtner',
        'sort_name' => 'gschwandtner',
        'type' => 'entrepreneur',
        'title' => 'CEO & Co-Founder',
        'company' => 'Runtastic',
        'image' => 'gschwandtner_florian.png',
        'network_table' => ''
    ),*/
    array(
        'name' => 'Jess Erickson',
        'sort_name' =>'Erickson',
        'type' => 'investor',
        'title' => 'Programm Director',
        'company' => '500 Startups',
        'image' => 'erickson_jess.png',
        'network_table' => null,
    ),
/*
    array( 
        'name' => 'Dr. Jens Müffelmann',
        'sort_name' => 'mueffelmann',
        'type' => 'press',
        'title' => 'President',
        'company' => 'Axel Springer USA',
        'image' => 'mueffelmann_jens.png',
        'network_table' => ''
    ) ,*/
    array( 
        'name' => 'Katharina Borchert',
        'sort_name' => 'borchert',
        'type' => 'industry_leader',
        'title' => 'Chief Innovation Officer ',
        'company' => 'Mozilla',
        'image' => 'borchert_katharina.png',
        'network_table' => '',
    ),
    array( 
        'name' => 'Dorothee Bär',
        'sort_name' => 'baer',
        'type' => 'industry_leader',
        'company' => 'Federal Ministry of Transport and Digital Infrastructure',
        'title' => 'State Secretary',
        'image' => 'baer_dorothee.png',
        'network_table' => ''
    ),
    array(
        'name' => 'Andrei Brasoveanu',
        'sort_name' =>'Brasoveanu',
        'type' => 'investor',
        'title' => 'Investment Team',
        'company' => 'Accel Partners',
        'image' => 'brasoveanu_andrei.png',
        'network_table' => null,
    ),
    array( 
        'name' => 'Sonja Schwetje',
        'sort_name' => 'schwertje',
        'type' => 'press',
        'title' => 'Editor in Chief',
        'company' => 'n-tv',
        'image' => 'schwetje_sonja.png',
        'network_table' => ''
    ),
    
/*
    array(  
        'name' => 'Frank Thelen',
        'sort_name' => 'Thelen',
        'type' => 'Investors',
        'title' => 'CEO',
        'company' => 'e42',
        'image' => 'thelen_frank.png',
        'network_table' => ''
     )
    array( 
        'name' => 'Dr. Dr. Alexander Görlach',
        'sort_name' => 'goerlach',
        'type' => 'press',
        'title' => 'Founder, Publisher & Editor in Chief',
        'company' => 'The European',
        'image' => 'goerlach_alexander.png',
        'network_table' => ''
    ),
*/
    array( 
        'name' => 'Dr. Joana Breidenbach',
        'sort_name' => 'breidenbach',
        'type' => 'entrepreneur',
        'title' => 'Co-Founder',
        'company' => 'Betterplace.org',
        'image' => 'breidenbach_joana.png',
        'network_table' => ''
    ),
    array( 
        'name' => 'Konstantin Urban',
        'sort_name' => 'urban',
        'type' => 'entrepreneur',
        'title' => 'Co-Founder & CEO',
        'company' => 'Windeln.de',
        'image' => 'urban_konstantin.png',
        'network_table' => ''
    ),
    /*
    array( 
        'name' => 'Ruth Fend',
        'sort_name' => 'fend',
        'type' => 'press',
        'title' => 'Editor in Chief',
        'company' => 'Business Punk',
        'image' => 'fend_ruth.png',
        'network_table' => ''
    ),   */
    array( 
        'name' => 'Florian Langenscheidt',
        'sort_name' => 'langenscheidt',
        'type' => 'investor',
        'title' => 'Publisher, Business Angel',
        'company' => null,
        'image' => 'langenscheidt_florian.png',
        'network_table' => ''
    ),
	
/*
// Hat abgesagt
    array( 
        'name' => 'Wolfgang Grupp',
        'sort_name' => 'grupp',
        'type' => 'industry_leader',
        'title' => 'Managing Director',
        'company' => 'Trigema',
        'image' => 'grupp_wolfgang.png',
        'network_table' => ''
    ),
*/
    array( 
        'name' => 'Tobias Ragge',
        'sort_name' => 'ragge',
        'type' => 'industry_leader',
        'title' => 'CEO',
        'company' => 'HRS',
        'image' => 'ragge_tobias.png',
        'network_table' => ''
    ),
    array( 
        'name' => 'Magnus Graf Lambsdorff',
        'sort_name' => 'graf_lambsdorff',
        'type' => 'industry_leader',
        'title' => 'Partner & Chief People Officer',
        'company' => 'HitFox Group',
        'image' => 'lambsdorff_magnus.png',
        'network_table' => ''
    ),
    array( 
        'name' => 'Florian Leibert',
        'sort_name' => 'leibert',
        'type' => 'entrepreneur',
        'title' => 'Co-Founder',
        'company' => 'Mesosphere',
        'image' => 'leibert_florian.png',
        'network_table' => ''
    )
,
/*
    array( 
        'name' => 'Andreas Thümmler',
        'sort_name' => 'thuemmler',
        'type' => 'investor',
        'title' => 'Managing Director',
        'company' => 'AXCIT Capital Partners',
        'image' => 'thuemmler_andreas.png',
        'network_table' => ''
    ) ,*/
	
    array( 
        'name' => 'Richard Gutjahr',
        'sort_name' => 'gutjahr',
        'type' => 'press',
        'title' => 'Journalist',
        'company' => 'Bayerisches Fernsehen',
        'image' => 'gutjahr_richard.png',
        'network_table' => ''
    )
     ,
    array( 
        'name' => 'Curt Simon Harlinghausen',
        'sort_name' => 'harlinghausen',
        'type' => 'press',
        'title' => 'CDO EMEA',
        'company' => 'Starcom Mediavest Group',
        'image' => 'harlinghausen_curt_simon.png',
        'network_table' => ''
    )  
     ,
	 
/*
//Hat abgesagt
    array( 
        'name' => 'Dominik Wichmann',
        'sort_name' => 'wichmann',
        'type' => 'press',
        'title' => 'Editor in Chief & Co-CEO',
        'company' => 'DLD',
        'image' => 'wichmann_dominik.png',
        'network_table' => ''
    ),  
*/
     
    array( 
        'name' => 'Jörg Binnenbrücker',
        'sort_name' => 'binnenbruecker',
        'type' => 'investor',
        'title' => 'Founding Partner',
        'company' => 'Capnamic Ventures',
        'image' => 'binnenbruecker_joerg.png',
        'network_table' => ''
    )  
         ,
    array( 
        'name' => 'Lars Jankowfsky',
        'sort_name' => 'jankowfsky',
        'type' => 'investor',
        'title' => 'General Partner',
        'company' => 'NFQ Capital',
        'image' => 'jankowfsky_lars.png',
        'network_table' => ''
    )   ,
    array( 
        'name' => 'Christian Mangstl',
        'sort_name' => 'mangstl',
        'type' => 'investor',
        'title' => 'Owner',
        'company' => 'cmco ventures',
        'image' => 'mangstl_christian.png',
        'network_table' => ''
    )    ,
    array( 
        'name' => 'Birgit Ströbel',
        'sort_name' => 'stroebel',
        'type' => 'entrepreneur',
        'title' => 'Co-Founder',
        'company' => 'Immobilienscout',
        'image' => 'stroebel_birgit.png',
        'network_table' => ''
    ) 

   ,
    array( 
        'name' => 'Axel Telzerow',
        'sort_name' => 'telzerow',
        'type' => 'press',
        'title' => 'Editor in Chief',
        'company' => 'Computer Bild',
        'image' => 'telzerow_axel.png',
        'network_table' => ''
    ) 
   ,
    array( 
        'name' => 'Manfred Klaus',
        'sort_name' => 'klaus',
        'type' => 'press',
        'title' => 'Managing Partner',
        'company' => 'Plan.net Group',
        'image' => 'klaus_manfred.png',
        'network_table' => ''
    )     ,
    array( 
        'name' => 'Julian Riedlbauer',
        'sort_name' => 'riedlbauer',
        'type' => 'investor',
        'title' => 'Managing Partner',
        'company' => 'GP Bullhound',
        'image' => 'riedlbauer_julian.png',
        'network_table' => ''
    )     ,
	/*
    array( 
        'name' => 'Franziska Bluhm',
        'sort_name' => 'bluhm',
        'type' => 'press',
        'title' => 'Head of Digital Networking',
        'company' => 'Verlagsgruppe Handelsblatt',
        'image' => 'bluhm_franziska.png',
        'network_table' => ''
    )     ,*/
    array( 
        'name' => 'Christian Nagel',
        'sort_name' => 'nagel',
        'type' => 'investor',
        'title' => 'Managing Partner',
        'company' => 'Earlybird',
        'image' => 'nagel_christian.png',
        'network_table' => ''
    )     ,
    array( 
        'name' => 'Nora-Vanessa Wohlert',
        'sort_name' => 'wohlert',
        'type' => 'entrepreneur',
        'title' => 'Co-Founder & Managing Partner',
        'company' => 'Edition F ',
        'image' => 'wohlert_nora_vanessa.png',
        'network_table' => ''
    )     ,
    array( 
        'name' => 'Marcus Tandler',
        'sort_name' => 'tandler',
        'type' => 'entrepreneur',
        'title' => 'Co-Founder',
        'company' => 'OnPage.org',
        'image' => 'tandler_marcus.png',
        'network_table' => ''
    ),
    /*     ,
    array( 
        'name' => 'Ruth Fend',
        'sort_name' => 'fend',
        'type' => 'press',
        'title' => 'Editor in Chief',
        'company' => 'Business Punk',
        'image' => 'fend_ruth.png',
        'network_table' => ''
    ),     
    
    array( 
        'name' => 'Peter Borchers',
        'sort_name' => 'borchers',
        'type' => 'investor',
        'title' => 'Founder & Head',
        'company' => 'HUB:RAUM',
        'image' => 'borchers_peter.png',
        'network_table' => ''
    )      ,*/ 
    array( 
        'name' => 'Harald Braunstein',
        'sort_name' => 'braunstein',
        'type' => 'investor',
        'title' => 'Head of Investment',
        'company' => 'NFQ Capital',
        'image' => 'braunstein_harald.png',
        'network_table' => ''
    )      ,
    array( 
        'name' => 'Patrick Warnking',
        'sort_name' => 'warnking',
        'type' => 'global_disruptor',
        'title' => 'Country Director',
        'company' => 'Google Switzerland',
        'image' => 'warnking_patrick.png',
        'network_table' => ''
    )        ,
    array( 
        'name' => 'Michael Brehm',
        'sort_name' => 'brehm',
        'type' => 'industry_leader',
        'title' => 'Partner',
        'company' => 'Redstone Digital',
        'image' => 'brehm_michael.png',
        'network_table' => ''
    )       ,
    array( 
        'name' => 'Sebastian Diemer',
        'sort_name' => 'diemer',
        'type' => 'entrepreneur',
        'title' => 'Founder',
        'company' => 'Kreditech',
        'image' => 'diemer_sebastian.png',
        'network_table' => ''
    )       ,
    array( 
        'name' => 'Maximilian von der Ahé',
        'sort_name' => 'von',
        'type' => 'entrepreneur',
        'title' => 'Founder & CEO',
        'company' => 'Betahaus',
        'image' => 'von_der_ahe_maximilian.png',
        'network_table' => ''
    )  
    /*      ,
    array( 
        'name' => 'Valentin Stalf',
        'sort_name' => 'stalf',
        'type' => 'entrepreneur',
        'title' => 'Founder & CEO',
        'company' => 'Number26',
        'image' => 'stalf_valentin.png',
        'network_table' => ''
    )  */      ,
    array( 
        'name' => 'Niels Doerje',
        'sort_name' => 'doerje',
        'type' => 'industry_leader',
        'title' => 'Digital Entrepreneur | Serial Board Advisor',
        'company' => null,
        'image' => 'doerje_niels.png',
        'network_table' => ''
    )        ,
    array( 
        'name' => 'Jan Hendrik Merlin Jacob',
        'sort_name' => 'jacob',
        'type' => 'entrepreneur',
        'title' => 'Co-Founder & CTO',
        'company' => 'OnPage.org',
        'image' => 'jan_jacob.png',
        'network_table' => ''
    )        ,
    array( 
        'name' => 'Wolf Ingomar Faecks',
        'sort_name' => 'faecks',
        'type' => 'industry_leader',
        'title' => 'Managing Director',
        'company' => 'Sapient',
        'image' => 'faecks_wolf_ingomar.png',
        'network_table' => ''
    )         ,
    array( 
        'name' => 'Pia Victoria Poppenreiter',
        'sort_name' => 'poppenreiter',
        'type' => 'entrepreneur',
        'title' => 'CEO & Co-Founder',
        'company' => 'Ohlala.com',
        'image' => 'poppenreiter_pia_victoria.png',
        'network_table' => ''
    )           ,
   /* array( 
        'name' => 'Christoph Janz',
        'sort_name' => 'janz',
        'type' => 'investor',
        'title' => 'Managing Partner',
        'company' => 'Point Nine Capital',
        'image' => 'janz_christoph.png',
        'network_table' => ''
    )           , */
    array( 
        'name' => 'Christian Saller',
        'sort_name' => 'saller',
        'type' => 'investor',
        'title' => 'Partner',
        'company' => 'Holtzbrinck Ventures',
        'image' => 'saller_christian.png',
        'network_table' => ''
    )           ,
    /*
    array( 
        'name' => 'Arnd Benninghoff',
        'sort_name' => 'benninghoff',
        'type' => 'industry_leader',
        'title' => 'Deputy CEO',
        'company' => 'MTGx',
        'image' => 'benninghof_arnd.png',
        'network_table' => ''
    )           ,*/
    
    array( 
        'name' => 'Christoph Jung',
        'sort_name' => 'jung',
        'type' => 'investor',
        'title' => 'General Partner',
        'company' => 'Holzbrink Ventures ',
        'image' => 'jung_christoph.png',
        'network_table' => ''
    )           ,
    array( 
        'name' => 'Michael Oschmann',
        'sort_name' => 'oschmann',
        'type' => 'press',
        'title' => 'CEO',
        'company' => 'Müller Medien',
        'image' => 'oschmann_michael.png',
        'network_table' => ''
    )           ,
    array( 
        'name' => 'Michiel Goris',
        'sort_name' => 'goris',
        'type' => 'industry_leader',
        'title' => 'CEO',
        'company' => 'Interhyp',
        'image' => 'goris_michiel.png',
        'network_table' => ''
    )           ,
    array( 
        'name' => 'Jan Becker',
        'sort_name' => 'becker',
        'type' => 'entrepreneur',
        'title' => 'Founding Partner',
        'company' => '10x',        
        'image' => 'becker_jan.png',
        'network_table' => ''
    )           ,
    array( 
        'name' => 'Bernhard Brugger',
        'sort_name' => 'brugger',
        'type' => 'industry_leader',
        'title' => 'Co-Founder & CEO Europe',
        'company' => 'PAYBACK',
        'image' => 'brugger_bernhard.png',
        'network_table' => ''
    )            ,
    array( 
        'name' => 'René Seifert',
        'sort_name' => 'seifert',
        'type' => 'entrepreneur',
        'title' => 'Entrepreneur',
        'company' => 'Venturate',
        'image' => 'seifert_rene.png',
        'network_table' => ''
    ),
	/*
     array( 
            'name' => 'Dr. Rainer Kiefer',
            'sort_name' => 'kiefer',
            'type' => null,
            'title' => 'CEO',
            'company' => 'TRUIZE (former Cyberport)',
            'image' => 'kiefer_rainer.png',
            'network_table' => ''
        ),
	*/
	
// 17.03.2016

array( 'name' => 'Adam Kostyal',
   	'sort_name' => 'Kostyal',
   	'type' => 'Global Disruptors',
   	'title' => 'Senior Vice President',
   	'company' => 'Nasdaq',
   	'image' => 'kostyal_adam.png',
   	'network_table' => ''),

array( 'name' => 'Andreas Harting',
   	'sort_name' => 'Harting',
   	'type' => 'Industry Leaders',
   	'title' => 'Co-Founder & Managing Director',
   	'company' => 'Deloitte Digital',
   	'image' => 'harting_andreas.png',
   	'network_table' => ''),

array( 'name' => 'Chris Öhlund',
   	'sort_name' => 'Oehlund',
   	'type' => 'Industry Leaders',
   	'title' => 'CEO',
   	'company' => 'Verivox',
   	'image' => 'oehlund_chris.png',
   	'network_table' => ''),
/*
array(  'name' => 'Christian Henschel',
   	'sort_name' => 'Henschel',
   	'type' => 'Entrepreneur',
   	'title' => 'Co-Founder & CEO',
   	'company' => 'adjust.com',
   	'image' => 'henschel_christian.png',
   	'network_table' => ''),

array(  'name' => 'Christian Tiessen',
   	'sort_name' => 'Tiessen',
   	'type' => 'Entrepreneur, forbes',
   	'title' => 'Co-Founder',
   	'company' => 'Savedo',
   	'image' => 'tiessen_christian.png',
   	'network_table' => ''),*/

array(  'name' => 'Christoph Schuh',
   	'sort_name' => 'Schuh',
   	'type' => 'Investor',
   	'title' => 'Owner',
   	'company' => 'Calceus Invest',
   	'image' => 'schuh_christoph.png',
   	'network_table' => ''),

array(  'name' => 'Martina Weiner',
   	'sort_name' => 'Weiner',
   	'type' => 'industry_leader',
   	'title' => 'Managing Director',
   	'company' => 'i-Potentials',
   	'image' => 'weiner_martina.png',
   	'network_table' => ''),

array(  'name' => 'Florian Huber',
   	'sort_name' => 'Huber',
   	'type' => 'Entrepreneur',
   	'title' => 'Co-Founder & CEO',
   	'company' => 'united-domains',
   	'image' => 'huber_florian.png',
   	'network_table' => ''),

array(  'name' => 'Florian Weiß',
   	'sort_name' => 'Weiß',
   	'type' => 'Industry Leaders',
   	'title' => 'CEO',
   	'company' => 'jameda',
   	'image' => 'weiss_florian.png',
   	'network_table' => ''),

    /*
array(  'name' => 'Frank Schmiechen',
   	'sort_name' => 'Schmiechen',
   	'type' => 'Media',
   	'title' => 'Editor in Chief',
   	'company' => 'Gründerszene',
   	'image' => 'schmiechen_frank.png',
   	'network_table' => ''),*/
	
/*    
array(  'name' => 'Gerhard Thomas',
   	'sort_name' => 'Thomas',
   	'type' => 'Industry Leaders',
   	'title' => 'CEO',
   	'company' => 'Burda Digital Systems',
   	'image' => 'thomas_gerhard.png',
   	'network_table' => ''),
*/
/*
array(  'name' => 'Holger Felgner',
   	'sort_name' => 'Felgner',
   	'type' => 'Global Disruptors',
   	'title' => 'Board Member',
   	'company' => 'TeamViewer',
   	'image' => 'felgner_holger.png',
   	'network_table' => ''),

*/

array(  'name' => 'Iskender Dirik',
   	'sort_name' => 'Dirik',
   	'type' => 'Industry Leaders',
   	'title' => 'Managing Director',
   	'company' => 'Bauer Venture Partners',
   	'image' => 'dirik_iskender.png',
   	'network_table' => ''),

array(  'name' => 'Gleb Tritus',
   	'sort_name' => 'Tritus',
   	'type' => 'Industry Leaders',
   	'title' => 'Co-Creator & Director Venture Development',
   	'company' => 'Lufthansa Innovation Hub',
   	'image' => 'tritus_gleb.png',
   	'network_table' => ''),

/*
array(  'name' => 'Jana Kusick',
   	'sort_name' => 'Kusick',
   	'type' => 'Industry Leaders',
   	'title' => 'Managing Director',
   	'company' => 'Plista',
   	'image' => 'kusick_jana.png',
   	'network_table' => ''),
*/

array(  'name' => 'Jan Christe',
   	'sort_name' => 'Christe',
   	'type' => 'Media',
   	'title' => 'Editor in Chief',
   	'company' => 't3n',
   	'image' => 'christe_jan.png',
   	'network_table' => ''),

array(  'name' => 'Jan Ippen',
   	'sort_name' => 'Ippen',
   	'type' => 'Industry Leaders',
   	'title' => 'Founder & CEO',
   	'company' => 'Ippen Digital',
   	'image' => 'ippen_jan.png',
   	'network_table' => ''),
/*
array(  'name' => 'Jan Thomas',
   	'sort_name' => 'Thomas',
   	'type' => 'Industry Leaders',
   	'title' => 'Founder & CEO',
   	'company' => 'NKF Media',
   	'image' => 'thomas_jan.png',
   	'network_table' => ''),
*/
array(  'name' => 'Judith Williams',
   	'sort_name' => 'Williams',
   	'type' => 'Investors',
   	'title' => 'Founder',
   	'company' => 'Judith Williams GmbH',
   	'image' => 'williams_judith.png',
   	'network_table' => ''),

array(  'name' => 'Lamine Cheloufi',
   	'sort_name' => 'Cheloufi',
   	'type' => 'Entrepreneurs, forbes',
   	'title' => 'Co-Founder & Head of Product',
   	'company' => 'Cookies',
   	'image' => 'cheloufi_lamine.png',
   	'network_table' => ''),

array(  'name' => 'Markus Knall',
   	'sort_name' => 'Knall',
   	'type' => 'Media',
   	'title' => 'Editor in Chief',
   	'company' => 'merkur.de / tz.de',
   	'image' => 'knall_markus.png',
   	'network_table' => ''),

array(  'name' => 'Markus Pöhlmann',
   	'sort_name' => 'Pöhlmann',
   	'type' => 'Industry Leaders',
   	'title' => 'Managing Director',
   	'company' => 'gutefrage.net',
   	'image' => 'poehlmann_markus.png',
   	'network_table' => ''),

array(  'name' => 'Nicolas Brand',
   	'sort_name' => 'Brand',
   	'type' => 'Investors',
   	'title' => 'Partner',
   	'company' => 'Lakestar',
   	'image' => 'brand_nicolas.png',
   	'network_table' => ''),

array(  'name' => 'Nikolaus Röttger',
   	'sort_name' => 'Röttger',
   	'type' => 'Media',
   	'title' => 'Editor in Chief',
   	'company' => 'WIRED Germany',
   	'image' => 'roettger_nikolaus.png',
   	'network_table' => ''),

array(  'name' => 'Nils Seebach',
   	'sort_name' => 'Seebach',
   	'type' => 'Entrepreneurs',
   	'title' => 'Co-Founder & CEO',
   	'company' => 'Spryker Systems',
   	'image' => 'seebach_nils.png',
   	'network_table' => ''),
/*
array(  'name' => 'Philipp Man',
   	'sort_name' => 'Man',
   	'type' => 'Entrepreneurs',
   	'title' => 'Co-Founder & CEO',
   	'company' => 'Chronext',
   	'image' => 'man_philipp.png',
   	'network_table' => ''),*/

array(  'name' => 'Dr. Ralf Baumann',
   	'sort_name' => 'Baumann',
   	'type' => 'Industry Leaders',
   	'title' => 'Managing Director',
   	'company' => 'Digital Media Products',
   	'image' => 'baumann_ralf.png',
   	'network_table' => ''),

array(  'name' => 'Stefan Muff',
   	'sort_name' => 'Muff',
   	'type' => 'Entrepreneur',
   	'title' => 'Founder',
   	'company' => 'Axon Active',
   	'image' => 'muff_stefan.png',
   	'network_table' => ''),

array(  'name' => 'Stefan Heilmann',
   	'sort_name' => 'Heilmann',
   	'type' => 'Industry Leaders',
   	'title' => 'Managing Director',
   	'company' => 'IEG - Investment Banking Group',
   	'image' => 'heilmann_stefan.png',
   	'network_table' => ''),

array(  'name' => 'Tobias Flaitz',
   	'sort_name' => 'Flaitz',
   	'type' => 'Industry Leaders',
   	'title' => 'CEO',
   	'company' => 'Sedo Holding',
   	'image' => 'flaitz_tobias.png',
   	'network_table' => ''),

array(  'name' => 'Valentin Stalf',
   	'sort_name' => 'Stalf',
   	'type' => 'Entrepreneurs',
   	'title' => 'Founder & CEO',
   	'company' => 'Number26',
   	'image' => 'stalf_valentin.png',
   	'network_table' => ''),
/*
array(  'name' => 'Volker Schütz',
   	'sort_name' => 'Schütz',
   	'type' => 'Media',
   	'title' => 'Editor in Chief',
   	'company' => 'Horizont',
   	'image' => 'schuetz_volker.png',
   	'network_table' => ''),*/

array(  'name' => 'Anna Ly',
   	'sort_name' => 'Ly',
   	'type' => 'Global Disruptors, forbes',
   	'title' => 'Senior Manager of Creative Tech Partnerships/Ventures',
   	'company' => 'Sesame Workshop',
   	'image' => 'ly_anna.png',
   	'network_table' => ''),
/*
array(  'name' => 'Chris Giliberti',
   	'sort_name' => 'Giliberti',
   	'type' => 'Media, forbes',
   	'title' => 'Chief of Staff',
   	'company' => 'Gimlet Media',
   	'image' => 'giliberti_chris.png',
   	'network_table' => ''),
*//*
array(  'name' => 'Jennifer Schenker',
   	'sort_name' => 'Schenker',
   	'type' => 'Media',
   	'title' => 'Veteran Technology Journalist',
   	'company' => '',
   	'image' => 'schenker_jennifer.png',
   	'network_table' => ''),*/

array(  'name' => 'Lydia Benkö',
   	'sort_name' => 'Benkoe',
   	'type' => 'Industry Leaders',
   	'title' => 'Managing Partner',
   	'company' => 'Digital Capital Advisors',
   	'image' => 'benkoe_lydia.png',
   	'network_table' => ''),

array(  'name' => 'Nimrod Lehavi',
   	'sort_name' => 'Lehavi',
   	'type' => 'Entrepreneurs',
   	'title' => 'Co-Founder & CEO',
   	'company' => 'Simplex',
   	'image' => 'lehavi_nimrod.png',
   	'network_table' => ''),

array(  'name' => 'Peggy Reichelt',
   	'sort_name' => 'Reichelt',
   	'type' => 'Industry Leaders',
   	'title' => 'CEO',
   	'company' => 'Ströer Media Brands',
   	'image' => 'reichelt_peggy.png',
   	'network_table' => ''),

array(  'name' => 'Polina Raygorodskaya',
   	'sort_name' => 'Raygorodskaya',
   	'type' => 'Entrepreneurs, forbes',
   	'title' => 'Co-Founder & CEO',
   	'company' => 'Wanderu',
   	'image' => 'raygorodskaya_polina.png',
   	'network_table' => ''),

array(  'name' => 'Birte Gall',
   	'sort_name' => 'Gall',
   	'type' => 'Entrepreneurs',
   	'title' => 'Founder & CEO',
   	'company' => 'Berlin School of Digital Business',
   	'image' => 'gall_birte.png',
   	'network_table' => ''),
	
/*
// Hat abgesagt
array(  'name' => 'Hans-Christian Zappel',
   	'sort_name' => 'Zappel',
   	'type' => 'Entrepreneurs, forbes',
   	'title' => 'Co-Founder',
   	'company' => 'Knomi',
   	'image' => 'zappel_hans-christian.png',
   	'network_table' => ''),
*/

array(  'name' => 'Daniel Grözinger',
   	'sort_name' => 'Groezinger',
   	'type' => 'investor',
   	'title' => 'Partner',
   	'company' => 'Parklane Captital',
   	'image' => 'goetzinger_daniel.png',
   	'network_table' => ''),


array(  'name' => 'Catharina van Delden',
   	'sort_name' => 'van Delden',
   	'type' => 'Entrepreneurs',
   	'title' => 'Founder & CEO',
   	'company' => 'innosabi',
   	'image' => 'van_delden_catherina.png',
   	'network_table' => ''),


// 10.05.2016

array(
'name' => 'Alessio Avellan Borgmeyer',
'sort_name' =>'Avellan Borgmeyer',
'type' => 'entrepreneur',
'title' => 'CEO & Founder',
'company' => 'Jodel',
'image' => 'borgmeyer_alessio.png',
'network_table' => null,
),
array(
'name' => 'Sebastian Bärhold',
'sort_name' =>'Baerhold',
'type' => 'entrepreneur',
'title' => 'Co-Founder',
'company' => 'ID Now',
'image' => 'baerhold_sebastian.png',
'network_table' => null,
),
array(
'name' => 'Filip Felician Dames',
'sort_name' =>'Dames',
'type' => 'entrepreneu, investor',
'title' => 'Founding Partner',
'company' => 'Cherry Ventures',
'image' => 'dames_filip.png',
'network_table' => null,
),
array(
'name' => 'Urs Haeusler',
'sort_name' =>'Haeusler',
'type' => 'entrepreneur',
'title' => 'CEO',
'company' => 'DealMarket',
'image' => 'haeusler_urs.png',
'network_table' => null,
),
array(
'name' => 'Markus Hallermann',
'sort_name' =>'Hallermann',
'type' => 'entrepreneur',
'title' => 'Co-Founder & CEO',
'company' => 'komoot',
'image' => 'hallermann_markus.png',
'network_table' => null,
),
array(
'name' => 'Fredrik Harkort',
'sort_name' =>'Harkort',
'type' => 'entrepreneur',
'title' => 'CEO & Co-Founder',
'company' => 'BodyChange',
'image' => 'harkort_fredrik.png',
'network_table' => null,
),
array(
'name' => 'Tanja Kufner',
'sort_name' =>'Kufner',
'type' => 'investor, entrepreneur',
'title' => 'Managing Director',
'company' => 'Startup Bootcamp',
'image' => 'kufner_tanja.png',
'network_table' => null,
),
    
    /*
array(
'name' => 'Christian Leybold',
'sort_name' =>'Leybold',
'type' => 'investor, entrepreneur',
'title' => 'General Partner',
'company' => 'e.ventures',
'image' => 'leybold_christian.png',
'network_table' => null,
),*/
array(
'name' => 'Nico Lumma',
'sort_name' =>'Lumma',
'type' => 'investor, entrepreneur',
'title' => 'COO',
'company' => 'Next Media Accelerator',
'image' => 'lumma_nico.png',
'network_table' => null,
),
array(
'name' => 'Christoph Magnussen',
'sort_name' =>'Magnussen',
'type' => 'entrepreneur',
'title' => 'Founder & CEO',
'company' => 'Blackboat',
'image' => 'magnussen_christoph.png',
'network_table' => null,
),
    
    /*
array(
'name' => 'Max-Josef Meier',
'sort_name' =>'Meier',
'type' => 'entrepreneur',
'title' => 'Founder',
'company' => 'Stylight',
'image' => 'meier_max_josef.png',
'network_table' => null,
),*/
    
array(
'name' => 'Janis Meyer-Plath',
'sort_name' =>'Meyer-Plath',
'type' => 'entrepreneur',
'title' => 'Founder',
'company' => 'Friendsurance',
'image' => 'meyer_plath_janis.png',
'network_table' => null,
),
array(
'name' => 'Fabien Nestmann',
'sort_name' =>'Nestmann',
'type' => 'industry leader',
'title' => 'Public Policy Lead DACH',
'company' => 'Uber',
'image' => 'nestmann_fabien.png',
'network_table' => null,
),
array(
'name' => 'Fritz Oidtmann',
'sort_name' =>'Oidtmann',
'type' => 'investor',
'title' => 'Managing Partner',
'company' => 'Acton Capital Partners',
'image' => 'oidtmann_fritz.png',
'network_table' => null,
),
array(
'name' => 'Dr. Jens Pippig',
'sort_name' =>'Pippig',
'type' => 'investor',
'title' => 'Managing Director P7S1 Accelerator',
'company' => 'ProSiebenSat.1',
'image' => 'pippig_jens.png',
'network_table' => null,
),
array(
'name' => 'Sven Rittau',
'sort_name' =>'Rittau',
'type' => 'entrepreneur',
'title' => 'Founder & CEO',
'company' => 'K5',
'image' => 'rittau_sven.png',
'network_table' => null,
),
array(
'name' => 'Niklas Raberg',
'sort_name' =>'Raberg',
'type' => 'investor',
'title' => 'Investment Manager',
'company' => 'High-Tech Gründerfonds',
'image' => 'raberg_niklas.png',
'network_table' => null,
),
array(
'name' => 'Max Wittrock',
'sort_name' =>'Wittrock',
'type' => 'entrepreneur',
'title' => 'Co-Founder',
'company' => 'mymüsli',
'image' => 'wittrock_max.png',
'network_table' => null,
),
array(
'name' => 'Carsten Rudolph',
'sort_name' =>'Rudolph',
'type' => 'investor',
'title' => 'Managing Director',
'company' => 'BayStartUp',
'image' => 'rudolph_carsten.png',
'network_table' => null,
),
array(
'name' => 'Daniel Steil',
'sort_name' =>'Steil',
'type' => 'media',
'title' => 'Editor in Chief',
'company' => 'Focus Online',
'image' => 'steil_daniel.png',
'network_table' => null,
),
array(
'name' => 'Franz Glatz',
'sort_name' =>'Glatz',
'type' => 'media',
'title' => 'Managing Partner',
'company' => 'Werk 1',
'image' => 'glatz_franz.png',
'network_table' => null,
),
array(
'name' => 'Mario Witte',
'sort_name' =>'Witte',
'type' => 'industry_leader',
'title' => 'CTO',
'company' => 'Team Internet',
'image' => 'witte_mario.png',
'network_table' => null,
),
array(
'name' => 'Niklas Veltkamp',
'sort_name' =>'Veltkamp',
'type' => 'media',
'title' => 'Managing Director',
'company' => 'BITKOM',
'image' => 'veltkamp_niklas.png',
'network_table' => null,
),
    /*
array(
'name' => 'Stefan Wiegard',
'sort_name' =>'Wiegard',
'type' => 'entrepreneur',
'title' => 'COO',
'company' => 'Team Internet',
'image' => 'wiegard_stefan.png',
'network_table' => null,
),*/
array(
'name' => 'Tilo Bonow',
'sort_name' =>'Bonow',
'type' => 'entrepreneur',
'title' => 'CEO & Founder',
'company' => 'PIABO PR',
'image' => 'bonow_tilo.png',
'network_table' => null,
),
array(
'name' => 'Dr. Helmut Schönenberger',
'sort_name' =>'Schönenberger',
'type' => 'investor',
'title' => 'Managing Director',
'company' => 'UnternehmerTUM',
'image' => 'schoenenberger_helmut.png',
'network_table' => null,
),
array(
'name' => 'Nenad Marovac',
'sort_name' =>'Marovac',
'type' => 'investor',
'title' => 'Partner',
'company' => 'DN Capital',
'image' => 'marovac_nenad.png',
'network_table' => null,
),


// 27.05.2016

array(
'name' => 'Alexander Artope',
'sort_name' =>'Artope',
'type' => 'entrepreneur',
'title' => 'Co-Founder & CEO',
'company' => 'Smava',
'image' => 'artope_alexander.png',
'network_table' => null,
),
array(
'name' => 'Dr. Annette Pawlu',
'sort_name' =>'Pawlu',
'type' => 'entrepreneur',
'title' => 'Editor at Large',
'company' => 'BILANZ',
'image' => 'pawlu_annette.png',
'network_table' => null,
),
array(
'name' => 'Berthold von Freyberg',
'sort_name' =>'von Freyberg',
'type' => 'investor, entrepreneur',
'title' => 'Partner',
'company' => 'Target Partners',
'image' => 'von_freyberg_berthold.png',
'network_table' => null,
),
array(
'name' => 'Jens Hoffmann',
'sort_name' =>'Hoffmann',
'type' => 'entrepreneur',
'title' => 'Founder',
'company' => 'Berlinstartup',
'image' => 'hoffmann_jens.png',
'network_table' => null,
),
array(
'name' => 'Jonathan Becker',
'sort_name' =>'Becker',
'type' => 'investor, forbes, entrepreneur',
'title' => 'Vice President',
'company' => 'e.Ventures',
'image' => 'becker_jonathan.png',
'network_table' => null,
),
array(
'name' => 'Kanika Tekriwal',
'sort_name' =>'Tekriwal',
'type' => 'entrepreneur, forbes',
'title' => 'Cofounder & CEO',
'company' => 'JetSetGo',
'image' => 'tekriwal_kanika.png',
'network_table' => null,
),
/*
array(
'name' => 'Koh Martinez Onozawa',
'sort_name' =>'Onozawa',
'type' => 'entrepreneur, forbes',
'title' => 'President & CEO',
'company' => 'Loudbasstard',
'image' => 'onozawa_koh_martinez.png',
'network_table' => null,
),*/
array(
'name' => 'Michael Vietze',
'sort_name' =>'Vietze',
'type' => 'entrepreneur',
'title' => 'Co-Founder & Managing Partner',
'company' => 'stylefruits',
'image' => 'vietze_michael.png',
'network_table' => null,
),
array(
'name' => 'Myriam Locher',
'sort_name' =>'Locher',
'type' => 'entrepreneur',
'title' => 'Partner',
'company' => 'Blackboat',
'image' => 'locher_myriam.png',
'network_table' => null,
),
array(
'name' => 'Philip Kamp',
'sort_name' =>'Kamp',
'type' => 'entrepreneur',
'title' => 'CMO & Co-Founder',
'company' => 'auxmoney',
'image' => 'kamp_philip.png',
'network_table' => null,
),
array(
'name' => 'Philipp Roesch-Schlanderer',
'sort_name' =>'Roesch-Schlanderer',
'type' => 'entrepreneur',
'title' => 'Co-Founder & CEO',
'company' => 'Egym',
'image' => 'roesch_schlanderer_philipp.png',
'network_table' => null,
),
array(
'name' => 'Rouven Dresselhaus',
'sort_name' =>'Dresselhaus',
'type' => 'investor, entrepreneur',
'title' => 'Managing Partner',
'company' => 'Cavalry Ventures',
'image' => 'dresselhaus_rouven.png',
'network_table' => null,
),
array(
'name' => 'Savannah Peterson',
'sort_name' =>'Peterson',
'type' => 'entrepreneur, forbes',
'title' => 'Founder & Chief Unicorn',
'company' => 'Savvy Millennial',
'image' => 'peterson_savannah.png',
'network_table' => null,
),
/*
array(
'name' => 'Stephan Uhrenbacher',
'sort_name' =>'Uhrenbacher',
'type' => 'entrepreneur',
'title' => 'CEO',
'company' => 'FLIO',
'image' => 'uhrenbacher_stephan.png',
'network_table' => null,
),*/
array(
'name' => 'Till Faida',
'sort_name' =>'Faida',
'type' => 'entrepreneur',
'title' => 'CEO',
'company' => 'Eyeo (Adblock Plus)',
'image' => 'faida_till.png',
'network_table' => null,
),
/*
array(
'name' => 'Tim Pointer',
'sort_name' =>'Pointer',
'type' => 'entrepreneur, forbes',
'title' => 'Managing Director',
'company' => 'Uprise Digital',
'image' => 'pointer_tim.png',
'network_table' => null,
),
*/
array(
'name' => 'Gabriele Böhmer',
'sort_name' =>'boehmer',
'type' => 'press',
'title' => 'Chief Editor',
'company' => 'Munich Startup',
'image' => 'boehmer_gabriele.png',
'network_table' => null,
),
/*
array(
'name' => 'Alexander Hüsing',
'sort_name' =>'Hüsing',
'type' => 'media',
'title' => 'Editor in Chief',
'company' => 'Deutsche Startups',
'image' => 'huesing_alexander.png',
'network_table' => null,
),
*/
array(
'name' => 'Alexander Piutti',
'sort_name' =>'Piutti',
'type' => 'entrepreneur',
'title' => 'CEO & Founder',
'company' => 'GameGenetics',
'image' => 'piutti_alexander.png',
'network_table' => null,
),
array(
'name' => 'Claire England',
'sort_name' =>'England',
'type' => 'investor',
'title' => 'Executive Director',
'company' => 'Central Texas Angel Network',
'image' => 'england_claire.png',
'network_table' => null,
),
array(
'name' => 'Elizabeth Yin',
'sort_name' =>'Yin',
'type' => 'investor, entrepreneur',
'title' => 'Partner',
'company' => '500 Startups',
'image' => 'yin_elizabeth.png',
'network_table' => null,
),
array(
'name' => 'Falk Mueller-Veerse',
'sort_name' =>'Mueller-Veerse',
'type' => 'investor',
'title' => 'Managing Partner',
'company' => 'Cartagena Capital',
'image' => 'mueller_veerse_falk.png',
'network_table' => null,
),
//
array(
'name' => 'Felix Huber',
'sort_name' =>'Huber',
'type' => 'industry_leader',
'title' => 'Country Lead Germany',
'company' => 'Stripe',
'image' => 'huber_felix.png',
'network_table' => null,
),
/*
// Hat abgesagt
array(
'name' => 'Frank Seehaus',
'sort_name' =>'Seehaus',
'type' => 'investor',
'title' => 'Managing Partner',
'company' => 'Action Capital',
'image' => 'seehaus_frank.png',
'network_table' => null,
),
*/
array(
'name' => 'Lina Wüller',
'sort_name' =>'Wüller',
'type' => 'entrepreneur',
'title' => 'Founder',
'company' => 'Rebel at Heart',
'image' => 'wueller_lina.png',
'network_table' => null,
),

/*
array(
'name' => 'Marcus W. Mosen',
'sort_name' =>'Mosen',
'type' => 'industry_leader',
'title' => 'CEO',
'company' => 'Concardis GmbH',
'image' => 'mosen_marcus.png',
'network_table' => null,
),*/

array(
'name' => 'Tomas Peeters',
'sort_name' =>'Peeters',
'type' => 'industry_leader',
'title' => 'Chief Strategy Officer',
'company' => 'ING Bank',
'image' => 'peteers_tomas.png',
'network_table' => null,
),
//
array(
'name' => 'Peter Gerich',
'sort_name' =>'Gerich',
'type' => 'media',
'title' => 'Publishing Director',
'company' => 'HORIZONT-Medien',
'image' => 'gerich_peter.png',
'network_table' => null,
),
    /*
array(
'name' => 'Ramanan Sivalingam',
'sort_name' =>'Sivalingam',
'type' => 'industry_leader, forbes',
'title' => 'Institutional Sales',
'company' => 'Deutsche Bank Securities',
'image' => 'sivalingam_ramanan.png',
'network_table' => null,
),*/
array(
'name' => 'Sean Seton-Rogers',
'sort_name' =>'Seton-Rogers',
'type' => 'investor',
'title' => 'Partner',
'company' => 'Profounders Capital',
'image' => 'sean_seton_rogers.png',
'network_table' => null,
),
array(
'name' => 'Sebastian Konrad',
'sort_name' =>'Konrad',
'type' => 'industry_leader',
'title' => 'Regional Manager VIP Sales',
'company' => 'Audi AG',
'image' => 'konrad_sebastian.png',
'network_table' => null,
),
    /*
array(
'name' => 'Sheel Mohnot',
'sort_name' =>'Mohnot',
'type' => 'investor',
'title' => 'Partner - Fintech',
'company' => '500 Startups',
'image' => 'mohnot_sheel.png',
'network_table' => null,
),*/
array(
'name' => 'Cosmin Ene',
'sort_name' =>'Ene',
'type' => 'entrepreneur',
'title' => 'Founder & CEO',
'company' => 'LaterPlay',
'image' => 'ene_cosmin.png',
'network_table' => null,
),
array(
'name' => 'Garan Goodman',
'sort_name' =>'Goodman',
'type' => 'industry_leader',
'title' => 'Managing Director',
'company' => 'Wayra Munich',
'image' => 'goodman_garan.png',
'network_table' => null,
),
array(
'name' => 'Frank Schneider',
'sort_name' =>'Schneider',
'type' => 'entrepreneur',
'title' => 'Co-Founder',
'company' => 'dmexco',
'image' => 'schneider_frank.png',
'network_table' => null,
),

/*
array(
'name' => 'Jörg Kachelmann',
'sort_name' =>'kachelmann',
'type' => 'entrepreneur',
'title' => 'Entrepreneur & meteorologist',
'company' => '',
'image' => 'kachelmann_joerg.png',
'network_table' => null,
),
*/
array(
'name' => 'Heiner Kroke',
'sort_name' =>'kroke',
'type' => 'entrepreneur',
'title' => 'CEO',
'company' => 'Momox.de',
'image' => 'kroke_heiner.png',
'network_table' => null,
),

array(
'name' => 'Michael Brinkmann',
'sort_name' =>'brinkmann',
'type' => 'industry_leader',
'title' => 'EVP Marketing & Partnermanagement',
'company' => 'Wirecard',
'image' => 'brinkmann_michael.png',
'network_table' => null,
),

//Neu am 17.08 hinzugefügt

array(
'name' => 'Benjamin Heimlich',
'sort_name' =>'heimlich',
'type' => 'industry_leader',
'title' => 'Chief Editor',
'company' => 'VentureCapital Magazin',
'image' => 'heimlich_bejamin.png',
'network_table' => null,
),

array(
'name' => 'Christian Figge',
'sort_name' =>'figge',
'type' => 'industry_leader',
'title' => 'Vice President',
'company' => 'General Atlantic',
'image' => 'figge_christian.png',
'network_table' => null,
),

array(
'name' => 'Dirk Graber',
'sort_name' =>'graber',
'type' => 'entrepreneur',
'title' => 'CEO',
'company' => 'Mister Spex',
'image' => 'dirk_graber.png',
'network_table' => null,
),
array(
'name' => 'Stefanie Peters',
'sort_name' =>'peters',
'type' => 'entrepreneur',
'title' => 'CEO',
'company' => 'enable2grow',
'image' => 'stefanie-peters-foto.png',
'network_table' => null,
),

array(
'name' => 'Felix Reinshagen',
'sort_name' =>'reinshagen',
'type' => 'entrepreneur',
'title' => 'Co-Founder & CEO',
'company' => 'NavVis',
'image' => 'reinhagen_felix.png',
'network_table' => null,
),
    /*
array(
'name' => 'Meelan Radia',
'sort_name' =>'radia',
'type' => 'entrepreneur',
'title' => 'Co-Founder & Corp Dev',
'company' => 'Yieldify',
'image' => 'radia_meelan.png',
'network_table' => null,
),*/
    
array(
'name' => 'Michael Karb',
'sort_name' =>'karb',
'type' => 'industry_leader',
'title' => 'Chief Financial Officer Europe',
'company' => 'Lazada Group',
'image' => 'karb_michael.png',
'network_table' => null,
),
/*
array(
'name' => 'Gerrit Seidel',
'sort_name' =>'seidel',
'type' => 'investor',
'title' => 'Managing Director & Partner',
'company' => 'yabeo Capital Gmbh',
'image' => 'seidel_gerrit.png',
'network_table' => null,
),*/
array(
'name' => 'Marie-Helene Ametsreiter',
'sort_name' =>'ametsreiter',
'type' => 'investor',
'title' => 'Partner',
'company' => 'Speedinvest',
'image' => 'ametsreiter_marie-helene.png',
'network_table' => null,
),

array(
'name' => 'Rainer Downar',
'sort_name' =>'downar',
'type' => 'industry_leader',
'title' => 'Executive Vice President Central Europe',
'company' => 'Sage',
'image' => 'downar_rainer.png',
'network_table' => null,
),



array(
'name' => 'Christian von Hammel-Bonten',
'sort_name' =>'hammelbonten',
'type' => 'industry_leader',
'title' => 'Executive Vice President Global Product Strategy',
'company' => 'Wirecard',
'image' => 'von-hammel-bonten_christian.png',
'network_table' => null,
),

array(
'name' => 'Charlie Kindel',
'sort_name' =>'kindel',
'type' => 'industry_leader',
'title' => 'Director',
'company' => 'Smart Home, Amazon',
'image' => 'kindel_charlie.png',
'network_table' => null,
),



array(
'name' => 'Torsten Blaschke',
'sort_name' =>'blaschke',
'type' => 'industry_leader',
'title' => 'Startup Initiative',
'company' => 'EY',
'image' => 'blaschke_torsten.png',
'network_table' => null,
),
/*
array(
'name' => 'Alex McLaren',
'sort_name' =>'McLaren',
'type' => 'industry_leader',
'title' => 'FinTech Manager',
'company' => 'EY',
'image' => 'mclaren_alex.png',
'network_table' => null,
),*/

//Neu am 22.08 hinzugefügt
/*
array(
'name' => 'Niclas Rohrwacher',
'sort_name' =>'rohrwacher',
'type' => 'industry_leader',
'title' => 'Chief Relationship Officer',
'company' => 'Factory',
'image' => 'rohrwacher_niclas.png',
'network_table' => null,
),*/

array(
'name' => 'Dr. Lothar Stein',
'sort_name' =>'stein',
'type' => 'investor',
'title' => 'Director Emeritus McKinsey & Company',
'company' => 'Innovative Ventures',
'image' => 'stein_lothar.png',
'network_table' => null,
),

array(
'name' => 'Dirk Heitmann',
'sort_name' =>'heitmann',
'type' => 'industry_leader',
'title' => 'Director of Cognitive Solutions DACH',
'company' => 'IBM',
'image' => 'heitmann_dirk.png',
'network_table' => null,
),

array(
'name' => 'Sanjay Brahmawar',
'sort_name' =>'brahmawar',
'type' => 'industry_leader',
'title' => 'Global Head of Sales & Managing Partner',
'company' => 'IBM Watson IoT',
'image' => 'sanjay_brahmawar.png',
'network_table' => null,
),
/*
array(
'name' => 'Andreas Unseld',
'sort_name' =>'unseld',
'type' => 'investor',
'title' => 'Partner',
'company' => 'Unternehmertum Venture Capital Partners GmbH',
'image' => 'andreas_unseld.png',
'network_table' => null,
),
*/
//Neu seit 29.08

array(
    'name' => 'Jan-Eric Peters',
    'sort_name' => 'Peters',
    'type' => 'press',
    'title' => 'Editor in Chief',
    'company' => 'Upday',
    'image' => 'jan-eric_peters.png',
    'network_table' => '',
),

array(
    'name' => 'Leif Pellikan',
    'sort_name' => 'Pellikan',
    'type' => 'press',
    'title' => 'Editor',
    'company' => 'W&V',
    'image' => 'leif_pellikan.png',
    'network_table' => '',
),

array(
    'name' => 'Cherno Jobatey',
    'sort_name' => 'Jobatey',
    'type' => 'press',
    'title' => 'Editorial Director',
    'company' => 'Huffington Post',
    'image' => 'cherno_jobatey.png',
    'network_table' => '',
),
/*
array(
    'name' => 'Andreas Kunze',
    'sort_name' => 'Kunze',
    'type' => 'entrepreneur',
    'title' => 'CEO & Co-Founder',
    'company' => 'KONUX',
    'image' => 'andreas_kunze.png',
    'network_table' => '',
),*/

array(
    'name' => 'Michael Stephanblome',
    'sort_name' => 'Stephanblome',
    'type' => 'investor',
    'title' => ' Entrepreneur & Venture Partner',
    'company' => 'Eight Rounds',
    'image' => 'michael_stephanblome.png',
    'network_table' => '',
),

array(
    'name' => 'Benedikt Franke',
    'sort_name' => 'Franke',
    'type' => 'entrepreneur',
    'title' => 'Co-Founder',
    'company' => 'Helpling',
    'image' => 'helping_franke.png',
    'network_table' => '',
),

array(
    'name' => 'Marko Wenthin',
    'sort_name' => 'Wenthin',
    'type' => 'industry_leader',
    'title' => 'Vorstand',
    'company' => 'SolarisBank',
    'image' => 'marko_wenthin.png',
    'network_table' => '',
),

array(
    'name' => 'Birgit Storz',
    'sort_name' => 'Storz',
    'type' => 'entrepreneur',
    'title' => 'Founder Director',
    'company' => 'Main Incubator',
    'image' => 'birgit_storz.png',
    'network_table' => '',
),

array(
    'name' => 'Andreas Unseld',
    'sort_name' => 'Unseld',
    'type' => 'investor',
    'title' => 'Investment Manager',
    'company' => 'UnternehmerTUM Fonds Management',
    'image' => 'andreas_unseld.png',
    'network_table' => '',
),

array(
    'name' => 'Dan Maag',
    'sort_name' => 'Maag',
    'type' => 'entrepreneur',
    'title' => 'CEO',
    'company' => 'Pantaflix',
    'image' => 'dan_maag.png',
    'network_table' => '',
),

array(
    'name' => 'Eran Davidson',
    'sort_name' => 'Davidson',
    'type' => 'entrepreneur',
    'title' => 'Founder',
    'company' => 'Davidson Technology Growth Debt',
    'image' => 'eran_davidson.png',
    'network_table' => '',
),

array(
    'name' => 'Frauke Mispagel',
    'sort_name' => 'Mispagel',
    'type' => 'investor',
    'title' => 'Managing Director',
    'company' => 'Axel Springer Plug and Play Accelerator',
    'image' => 'frauke_mispagel.png',
    'network_table' => '',
),

array(
    'name' => 'Frank Engelhardt',
    'sort_name' => 'Engelhardt',
    'type' => 'industry_leader',
    'title' => 'VP Enterprise Strategy',
    'company' => 'Salesforce',
    'image' => 'frank_engelhardt.png',
    'network_table' => '',
),

array(
    'name' => 'Igor Bratnikov',
    'sort_name' => 'Bratnikov',
    'type' => 'entrepreneur, forbes',
    'title' => 'Co-founder',
    'company' => 'Wanderu',
    'image' => 'igor_bratnikov.png',
    'network_table' => '',
),

array(
    'name' => 'Rainer Kiefer',
    'sort_name' => 'Kiefer',
    'type' => 'entrepreneur',
    'title' => 'CEO',
    'company' => 'TRUIZE (former Cyberport)',
    'image' => 'rainer_kiefer.png',
    'network_table' => '',
),

array(
    'name' => 'Hermann Eul',
    'sort_name' => 'Eul',
    'type' => 'investor',
    'title' => 'Independent Board Member',
    'company' => 'Eul Ventures',
    'image' => 'hermann_eul.png',
    'network_table' => '',
),

array(
    'name' => 'Daniel Grassinger',
    'sort_name' => 'Grassinger',
    'type' => 'entrepreneur',
    'title' => 'Co-Founder',
    'company' => 'nexussquared',
    'image' => 'daniel_grassinger.png',
    'network_table' => '',
),
/*
array(
    'name' => 'Joshua Scigala',
    'sort_name' => 'Scigala',
    'type' => 'entrepreneur',
    'title' => 'CEO & Co-Founder',
    'company' => 'Vaultoro',
    'image' => 'joshua_scigala.png',
    'network_table' => '',
),
*/
array(
    'name' => 'Erik Voorhees',
    'sort_name' => 'Voorhees',
    'type' => 'entrepreneur',
    'title' => 'CEO & Co-Founder',
    'company' => 'Shapeshift',
    'image' => 'erik_voorhees.png',
    'network_table' => '',
),

array(
    'name' => 'Lisa Brack',
    'sort_name' => 'Brack',
    'type' => 'press',
    'title' => 'Editor in Chief',
    'company' => 'Chip Online',
    'image' => 'lisa_brack.png',
    'network_table' => '',
),

array(
    'name' => 'Joshua Scigala',
    'sort_name' => 'Scigala',
    'type' => 'entrepreneur',
    'title' => 'CEO & Co - Founder',
    'company' => 'Vaulttoro',
    'image' => 'joshua_scigala.png',
    'network_table' => '',
),

array(
    'name' => 'Jerome Cochet',
    'sort_name' => 'Cochet',
    'type' => 'entrepreneur',
    'title' => 'SVP Advertising & Sales',
    'company' => 'Zalando SE',
    'image' => 'jerome_cochet.png',
    'network_table' => '',
),

array(
    'name' => 'Julia Bönisch',
    'sort_name' => 'Bönisch',
    'type' => 'press',
    'title' => 'Deputy Editor-in-Chief',
    'company' => 'Süddeutsche.de',
    'image' => 'julia_boenisch.png',
    'network_table' => '',
),

array(
    'name' => 'Marc-Oliver Jauch',
    'sort_name' => 'Jauch',
    'type' => 'investor',
    'title' => 'Partner & Managing Director',
    'company' => 'Castik Capital Partners',
    'image' => 'marc-oliver_jauch.png',
    'network_table' => '',
),

array(
    'name' => 'Marion Horstmann',
    'sort_name' => 'Horstmann',
    'type' => 'industry_leader',
    'title' => 'Strategy Partner',
    'company' => 'next47 – a Siemens Company',
    'image' => 'marion_horstmann.png',
    'network_table' => '',
),

array(
    'name' => 'Markus Fuhrmann',
    'sort_name' => 'Fuhrmann',
    'type' => 'investor',
    'title' => 'Venture Partner',
    'company' => 'DOJO Madness',
    'image' => 'markus_fuhrmann.png',
    'network_table' => '',
),

array(
    'name' => 'Markus Schöberl',
    'sort_name' => 'Schöberl',
    'type' => 'industry_leader',
    'title' => 'Director Seller Services Germany',
    'company' => 'Amazon Deutschland Services',
    'image' => 'markus_schoeberl.png',
    'network_table' => '',
),

array(
    'name' => 'Martin Krämer',
    'sort_name' => 'Krämer',
    'type' => 'industry_leader',
    'title' => 'Member of the Management Board',
    'company' => 'Datev',
    'image' => 'martin_kraemer.png',
    'network_table' => '',
),

array(
    'name' => ' Niels Brants',
    'sort_name' => 'Brants',
    'type' => 'entrepreneur',
    'title' => 'Global Head of Marketplace Operations',
    'company' => 'foodpanda',
    'image' => 'user_placeholder.png',
    'network_table' => '',
),

array(
    'name' => 'André Bajorat',
    'sort_name' => 'Bajorat',
    'type' => 'entrepreneur',
    'title' => 'CEO',
    'company' => 'Figo',
    'image' => 'andre_bajorat.png',
    'network_table' => '',
),

array(
    'name' => 'Nicolas Wittenborn',
    'sort_name' => 'Wittenborn',
    'type' => 'investor, forbes',
    'title' => '',
    'company' => 'Insight Venture Partners',
    'image' => 'nicolas_wittenborn.png',
    'network_table' => '',
),

array(
    'name' => 'Michael Mollath',
    'sort_name' => 'Mollath',
    'type' => 'industry_leader',
    'title' => 'CDO',
    'company' => 'Deutsche Messe AG/CeBit',
    'image' => 'michael_mollath.png',
    'network_table' => '',
),
/*
array(
    'name' => 'Grzegorz Czapla',
    'sort_name' => 'Czapla',
    'type' => 'entrepreneur',
    'title' => 'Founder & CEO',
    'company' => 'INDATA S.A.',
    'image' => 'grzegorz_czapla.png',
    'network_table' => '',
),*/
/*
array(
    'name' => 'Philippe Crochet',
    'sort_name' => 'Crochet',
    'type' => 'investor',
    'title' => 'Partner at Keensight Capital',
    'company' => 'Keensight Capital',
    'image' => 'philippe_crochet.png',
    'network_table' => '',
),*/

array(
    'name' => 'Pina Albo',
    'sort_name' => 'Albo',
    'type' => 'industry_leader',
    'title' => 'Member of Board of Management',
    'company' => 'MunichRe',
    'image' => 'pina_albo.png',
    'network_table' => '',
),

array(
    'name' => 'Ralph Chromik',
    'sort_name' => 'Chromik',
    'type' => 'entrepreneur',
    'title' => 'CEO & Founder',
    'company' => 'ScaleUp.de',
    'image' => 'ralph_chromik.png',
    'network_table' => '',
),

array(
    'name' => 'Florian Pauthner',
    'sort_name' => 'Pauthner',
    'type' => 'investor',
    'title' => 'CFO',
    'company' => 'SevenVentures',
    'image' => 'florian_pauthner.png',
    'network_table' => '',
),

array(
    'name' => 'Sepita Ansari',
    'sort_name' => 'Ansari',
    'type' => 'industry_leader',
    'title' => 'Managing Director',
    'company' => 'Catbird Seat',
    'image' => 'sepita_ansari.png',
    'network_table' => '',
),


array(
    'name' => 'Thomas Preuß',
    'sort_name' => 'Preuß',
    'type' => 'investor',
    'title' => 'Partner',
    'company' => 'Telekom Captital Partners',
    'image' => 'thomas_preus.png',
    'network_table' => '',
),

array(
    'name' => 'Kaya Taner',
    'sort_name' => 'Taner',
    'type' => 'entrepreneur',
    'title' => 'Honeypot',
    'company' => 'Founder & CEO',
    'image' => 'kaya_taner.png',
    'network_table' => '',
),

array(
    'name' => 'Ulrich Schmitz',
    'sort_name' => 'Schmitz',
    'type' => 'investor',
    'title' => 'Managing Director',
    'company' => 'Axel Springer Digital Ventures',
    'image' => 'urlich_schmitz.png',
    'network_table' => '',
),


array(
    'name' => 'Aurore Belfrage',
    'sort_name' => 'Belfrage',
    'type' => 'investor',
    'title' => 'Head of Together',
    'company' => 'EQT Ventures',
    'image' => 'aurore_belfrage.png',
    'network_table' => '',
),


array(
    'name' => 'Florian Lehwald',
    'sort_name' => 'Lehwald',
    'type' => 'industry_leader',
    'title' => 'Managing Director',
    'company' => 'Ströer Mobile Performance GmbH',
    'image' => 'lehwald_florian.png',
    'network_table' => '',
),


array(
    'name' => 'Klaus Dittrich',
    'sort_name' => 'Dittrich',
    'type' => 'industry_leader',
    'title' => 'Chairman and CEO',
    'company' => 'Messe München',
    'image' => 'dittrich_klaus.png',
    'network_table' => '',
),


array(
    'name' => 'Andreas Zipser',
    'sort_name' => 'Zipser',
    'type' => 'industry_leader',
    'title' => 'Vice President Sales',
    'company' => 'Sage',
    'image' => 'zipser_andreas.png',
    'network_table' => '',
),
    

    
array(
    'name' => 'Ronny Fehling',
    'sort_name' => 'Fehling',
    'type' => 'industry_leader',
    'title' => 'Vice President and Head of Data Driven Technologies',
    'company' => 'Airbus',
    'image' => 'fehling_ronny.png',
    'network_table' => '',
),

array(
    'name' => 'Sascha  van Holt',
    'sort_name' => 'van Holt',
    'type' => 'industry_leader',
    'title' => 'CEO',
    'company' => 'SevenVentures',
    'image' => 'van-holt-sascha-.png',
    'network_table' => '',
),

array(
    'name' => 'Alexander Kudlich',
    'sort_name' => 'Kudlich',
    'type' => 'investor',
    'title' => 'Managing Director',
    'company' => 'Rocket Internet',
    'image' => 'kudlich-alexander.png',
    'network_table' => '',
),

array(
    'name' => 'David Rosskamp',
    'sort_name' => 'Rosskamp',
    'type' => 'investor',
    'title' => 'General Partner',
    'company' => 'KRW Schindler',
    'image' => 'rosskamp-david.png',
    'network_table' => '',
),

array(
    'name' => 'Finn Age Hänsel',
    'sort_name' => 'Hänsel',
    'type' => 'entrepreneur',
    'title' => 'Managing Director',
    'company' => 'Movinga',
    'image' => 'haensel-finn-age.png',
    'network_table' => '',
),

array(
    'name' => 'Julian Kawohl',
    'sort_name' => 'Kawohl',
    'type' => 'industry_leader',
    'title' => 'Professor for Strategic Management',
    'company' => 'HTW Berlin',
    'image' => 'kawohl-julian.png',
    'network_table' => '',
),

array(
    'name' => 'Helmut Gassel',
    'sort_name' => 'Gassel',
    'type' => 'industry_leader',
    'title' => 'Division President',
    'company' => 'Infineon Technologies',
    'image' => 'gassel-helmut.png',
    'network_table' => '',
),

array(
    'name' => 'John Caspers',
    'sort_name' => 'Caspers',
    'type' => 'entrepreneur',
    'title' => 'Co-founder & Head of Innovation',
    'company' => 'Adyen Global Payments',
    'image' => 'user_placeholder.png',
    'network_table' => '',
),

array(
    'name' => 'Thijn Lamers',
    'sort_name' => 'Lamers',
    'type' => 'industry_leader',
    'title' => 'Executive Vice President (EVP)',
    'company' => 'Adyen Global Payments',
    'image' => 'lamers-thijn.png',
    'network_table' => '',
),

array(
    'name' => 'Anton von Ingelheim',
    'sort_name' => 'von Ingelheim',
    'type' => 'industry_leader',
    'title' => 'VP Sales Germany',
    'company' => 'Adyen',
    'image' => 'von-ingelheim-anton.png',
    'network_table' => '',
),

array(
    'name' => 'Marc Kottmann',
    'sort_name' => 'Kottmann',
    'type' => 'investor',
    'title' => 'Practice lead connected car & mobility',
    'company' => 'Allianz Ventures',
    'image' => 'kottmann-marc.png',
    'network_table' => '',
),

array(
    'name' => 'Jörg Richtsfeld',
    'sort_name' => 'Richtsfeld',
    'type' => 'investor',
    'title' => 'Head',
    'company' => 'Allianz Ventures',
    'image' => 'richtsfeld-joerg.png',
    'network_table' => '',
),

array(
    'name' => 'Alex Hofmann',
    'sort_name' => 'Hofmann',
    'type' => 'press',
    'title' => 'Deputy Editor-in-Chief',
    'company' => 'Gruenderszene / Vertical Media',
    'image' => 'hofmann-alex.png',
    'network_table' => '',
),

array(
    'name' => 'Rolf Dienst',
    'sort_name' => 'Dienst',
    'type' => 'investor',
    'title' => 'General Partner',
    'company' => 'WESSEL MANAGEMENT GMBH',
    'image' => 'dienst-rolf.png',
    'network_table' => '',
),

array(
    'name' => 'Titus Dittmann',
    'sort_name' => 'Dittmann',
    'type' => 'industry_leader',
    'title' => 'skate-aid-Anstifter',
    'company' => 'titus GmbH / Titus Dittmann Stiftung',
    'image' => 'titus_dittmann.png',
    'network_table' => '',
),

array(
    'name' => 'Mark Bentall',
    'sort_name' => 'Bentall',
    'type' => 'industry_leader',
    'title' => 'Head of Automatic Flight and Comms Research',
    'company' => 'Airbus',
    'image' => 'bentall-mark.png',
    'network_table' => '',
),

array(
    'name' => 'Alexander Brand',
    'sort_name' => 'Brand',
    'type' => 'entrepreneur',
    'title' => 'Founder & Co-CEO',
    'company' => 'windeln.de AG',
    'image' => 'brand-alexander.png',
    'network_table' => '',
),

array(
    'name' => 'Benedikt Daller',
    'sort_name' => 'Daller',
    'type' => 'investor',
    'title' => 'CEO',
    'company' => 'Daller Tracht',
    'image' => 'daller-benedikt.png',
    'network_table' => '',
),

array(
    'name' => 'Christin Eisenschmid',
    'sort_name' => 'Eisenschmid',
    'type' => 'industry_leader',
    'title' => 'Managing DIrector, VP & General Manager',
    'company' => 'Intel Deutschland GmbH',
    'image' => 'eisenschmid-christin.png',
    'network_table' => '',
),

array(
    'name' => 'Roland Grenke',
    'sort_name' => 'Grenke',
    'type' => 'entrepreneur',
    'title' => 'Co-Founder',
    'company' => 'Dubsmash',
    'image' => 'grenke-roland.png',
    'network_table' => '',
),

array(
    'name' => 'Marc Hoenke',
    'sort_name' => 'Hoenke',
    'type' => 'industry_leader',
    'title' => 'Director Product Marketing',
    'company' => 'Salesforce',
    'image' => 'hoenke-marc.png',
    'network_table' => '',
),

array(
    'name' => 'Matthias Hunecke',
    'sort_name' => 'Hunecke',
    'type' => 'entrepreneur',
    'title' => 'Founder & Co-CEO',
    'company' => 'Brille24 Group',
    'image' => 'hunecke-matthias.png',
    'network_table' => '',
),

array(
    'name' => 'Nikolas Krawinkel',
    'sort_name' => 'Krawinkel',
    'type' => 'investor',
    'title' => 'VC Principal',
    'company' => 'Mangrove Capital Partners',
    'image' => 'krawinkel-nikolas.png',
    'network_table' => '',
),

array(
    'name' => 'Burton Lee',
    'sort_name' => 'Lee',
    'type' => 'industry_leader',
    'title' => 'Senior Advisor',
    'company' => 'Stanford University - Engineering School',
    'image' => 'lee-burton.png',
    'network_table' => '',
),

array(
    'name' => 'Conrad Pozsgai',
    'sort_name' => 'Pozsgai',
    'type' => 'industry_leader',
    'title' => 'Managing DIrecto, CIO',
    'company' => 'PAYBACK',
    'image' => 'pozsgai-conrad.png',
    'network_table' => '',
),

array(
    'name' => 'Bernhard Schmid',
    'sort_name' => 'Schmid',
    'type' => 'investor',
    'title' => 'Managing Partner',
    'company' => 'XAnge',
    'image' => 'schmid-bernhard.png',
    'network_table' => '',
),

array(
    'name' => 'Tassilo Bindl',
    'sort_name' => 'Bindl',
    'type' => 'industry_leader',
    'title' => 'Head of Design',
    'company' => 'Genesis Mining',
    'image' => 'user_placeholder.png',
    'network_table' => '',
),
    
array(
    'name' => 'Magdalena Oehl',
    'sort_name' => 'Oehl',
    'type' => 'entrepreneur',
    'title' => 'Co-Founder',
    'company' => 'CATCHYS',
    'image' => 'oehl_magdalena.png',
    'network_table' => '',
),
/*
array(
    'name' => 'Vinay Sankarapu',
    'sort_name' => 'Sankarapu',
    'type' => 'entrepreneur',
    'title' => 'Cofounder',
    'company' => 'Arya.ai',
    'image' => 'sankarapu-vinay.jpg',
    'network_table' => '',
),*/

array(
    'name' => 'Erik Podzuweit',
    'sort_name' => 'Podzuweit',
    'type' => 'entrepreneur',
    'title' => 'Founder, Co-CEO',
    'company' => 'Scalable Capital',
    'image' => 'podzuweit-erik.png',
    'network_table' => '',
),

array(
    'name' => 'David Roldan',
    'sort_name' => 'Roldan',
    'type' => 'investor',
    'title' => 'VC & Startup Leader - Europe',
    'company' => 'Google Cloud Platform',
    'image' => 'user_placeholder.png',
    'network_table' => '',
),
    
array(
    'name' => 'Ulrich Schäfer',
    'sort_name' => 'Schäfer',
    'type' => 'press',
    'title' => 'Head of the Business Section',
    'company' => 'Süddeutsche Zeitung',
    'image' => 'ulrich_schaefer.png',
    'network_table' => '',
),
array(
    'name' => 'Sam Crawshay Jones',
    'sort_name' => 'Jones',
    'type' => 'industry_leader',
    'title' => 'Digital Transformation',
    'company' => 'Airbus Group',
    'image' => 'user_placeholder.png',
    'network_table' => '',
),
    
    array(
    'name' => 'Andreas Etten',
    'sort_name' => 'Etten',
    'type' => 'investor',
    'title' => 'Founding Partner',
    'company' => '10x',
    'image' => 'etten_andreas.png',
    'network_table' => '',
),
  
    array(
    'name' => 'Martin Wild',
    'sort_name' => 'Wild',
    'type' => 'industry_leader',
    'title' => 'Chief Digital Officer',
    'company' => 'Media-Saturn-Holding',
    'image' => 'wild_martin.png',
    'network_table' => '',
),
    array(
    'name' => 'Nina Zimmermann',
    'sort_name' => 'Zimmermann',
    'type' => 'entrepreneur',
    'title' => 'Managing Director Publishing',
    'company' => 'Hubert Burda Media',
    'image' => 'zimmermann_nina.png',
    'network_table' => '',
),
    
    
    
array(
    'name' => 'Jochen Wegner',
    'sort_name' => 'Wegner',
    'type' => 'press',
    'title' => 'Editor in Chief',
    'company' => 'ZEIT ONLINE GmbH',
    'image' => 'wegner_jochen.png',
    'network_table' => '',
),

array(
    'name' => 'Robin Wauters',
    'sort_name' => 'Wauters',
    'type' => 'press',
    'title' => 'Co-Founder and editor-in-chief',
    'company' => 'Tech.eu',
    'image' => 'wauters-robin.png',
    'network_table' => '',
),

array(
    'name' => 'Lu Li',
    'sort_name' => 'Li',
    'type' => 'press',
    'title' => 'Founder & CEO',
    'company' => 'Blooming Founders',
    'image' => 'li_lu.png',
    'network_table' => '',
),

array(
    'name' => 'Christian Scherrer',
    'sort_name' => 'Scherrer',
    'type' => 'investor',
    'title' => 'Investment Team',
    'company' => 'Kinnevik',
    'image' => 'user_placeholder.png',
    'network_table' => '',
),

array(
    'name' => 'Lukas Bennemann',
    'sort_name' => 'Bennemann',
    'type' => 'investor',
    'title' => 'Seed+Speed / Maschmeyer Group',
    'company' => 'Investment Manager',
    'image' => 'benemann-lukas.png',
    'network_table' => '',
),

array(
    'name' => 'Florian Erber',
    'sort_name' => 'Erber',
    'type' => 'investor',
    'title' => 'Managing Partner',
    'company' => 'Social Venture Fund',
    'image' => 'erber_florian.png',
    'network_table' => '',
),

array(
    'name' => 'Christian Miele',
    'sort_name' => 'Miele',
    'type' => 'investor',
    'title' => 'Vice President',
    'company' => 'e.ventures',
    'image' => 'miele_christian.png',
    'network_table' => '',
),

array(
    'name' => 'Tobias Jahn',
    'sort_name' => 'Jahn',
    'type' => 'investor',
    'title' => 'Investments',
    'company' => 'BMW i Ventures',
    'image' => 'jahn-tobias.png',
    'network_table' => '',
),

array(
    'name' => 'Christian Cohrs',
    'sort_name' => 'Cohrs',
    'type' => 'press',
    'title' => 'Editor in Chief',
    'company' => 'Business Punk',
    'image' => 'cohrs_christian.png',
    'network_table' => '',
),

array(
    'name' => 'Wolfgang Ziebart',
    'sort_name' => 'Ziebart',
    'type' => 'industry_leader',
    'title' => 'Technical Design Director',
    'company' => 'Jaguar Land Rover',
    'image' => 'ziebart_wolfgang.png',
    'network_table' => '',
),

array(
    'name' => 'Oliver Bohl',
    'sort_name' => 'Bohl',
    'type' => 'industry_leader',
    'title' => 'Director Digital Business Developpment',
    'company' => 'PAYBACK',
    'image' => 'bohl_oliver.png',
    'network_table' => '',
),
    
/*
array(
    'name' => 'Michael Treskow',
    'sort_name' => 'Treskow',
    'type' => 'industry_leader',
    'title' => 'Principal',
    'company' => 'Eight Roads Ventures',
    'image' => '',
    'network_table' => '',
),*/


// outer array
);

/*
foreach($tcs as $key => $value) {
    $value['sort_name'] = strtolower($value['sort_name']);
    $tcs[$key] = $value;
}
*/