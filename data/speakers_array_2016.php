<?php 
$speakers_2016 = array(
	
    array( 
        'name' => 'Kevin Spacey',
        'sort_name' => '0',
        'track' => 'main',
        'title' => 'Oscar-winning actor,',
        'company' => 'producer, startup investor',
        'image' => 'spacey.png',
        'cluster' => '',
    ),
    array( 
        'name' => 'Richard Branson',
        'sort_name' => '1',
        'track' => 'main',
        'title' => 'Founder',
        'company' => 'Virgin Group',
        'image' => 'branson.jpg',
        'cluster' => '',
    ),
    array( 
        'name' => 'Ilse Aigner',
        'sort_name' => '3',
        'track' => 'main',
        'title' => 'Bavarian State Minister of Economic Affairs and Media, Energy and Technology Deputy Minister-President',
        'company' => '',
        'image' => 'ilse_aigner.png',
        'cluster' => '',
    ), 
    array( 
        'name' => 'Nathan Blecharczyk',
        'sort_name' => '5',
        'track' => 'main',
        'title' => 'Co-Founder & CTO',
        'company' => 'airbnb',
        'image' => 'blecharczky_nathan.png',
        'cluster' => '',
    ), 
    array( 
        'name' => 'Mitchell Baker',
        'sort_name' => '10',
        'track' => 'main',
        'title' => 'Executive Chairwoman',
        'company' => 'Mozilla Foundation & Corporation',
        'image' => 'baker_mitchell.png',
        'cluster' => 'smart company',
    ),
	
    array( 
        'name' => 'Neil Patel',
        'sort_name' => '100',
        'track' => 'side, academy',
        'title' => 'Co-Founder',
        'company' => 'Crazy Egg, Hello Bar, KISSmetrics',
        'image' => 'patel_neil.png',
        'cluster' => 'academy',
    ),
    array( 
        'name' => 'Steve Hafner',
        'sort_name' => '200',
        'track' => 'main',
        'title' => 'Co-Founder & CEO',
        'company' => 'KAYAK',
        'image' => 'hafner_steve.png',
        'cluster' => '',
    ),
	array( 
        'name' => 'Sportfreunde Stiller',
        'sort_name' => '300',
        'track' => 'main',
        'title' =>  'Live in concert',
        'company' => '',
        'image' => 'sportfreunde_stiller.png',
        'cluster' => '',
    ),
    array(
        'name' => 'Tony Fadell',
        'sort_name' => '350',
        'track' => 'main',
        'title' => 'Founder',
        'company' => 'Nest & Creator iPod',
        'image' => 'fadell_tony.png',
        'cluster' => '',
    ),
    array( 
        'name' => 'Riccardo Zacconi',
        'sort_name' => '375',
        'track' => 'main',
        'title' => 'Co-Founder & CEO',
        'company' => 'King Digital Entertainment PLC',
        'image' => 'zacconi_riccardo.png',
        'cluster' => '',
    ),

    array( 
        'name' => 'Leila Janah',
        'sort_name' => '400',
        'track' => 'main',
        'title' => 'CEO',
        'company' => 'Sama and Laxmi',
        'image' => 'leila_janah.png',
        'cluster' => '',
    ), 
    
    //Löwen
    
    
     array( 
        'name' => 'Jochen Schweizer',
        'sort_name' => '700',
        'track' => 'main',
        'title' => 'Founder & Chairman',
        'company' => 'Jochen Schweizer Group',
        'image' => 'schweizer_jochen.png',
        'cluster' => 'commerce',
    ),
    array( 
        'name' => 'Judith Williams',
        'sort_name' => '900',
        'track' => 'main',
        'title' => 'Founder',
        'company' => 'Judith Williams GmbH',
        'image' => 'williams_judith.png',
        'cluster' => 'money',
    ),
    array( 
        'name' => 'Carsten Maschmeyer',
        'sort_name' => '600',
        'track' => 'main, academy',
        'title' => 'CEO',
        'company' => 'Maschmeyer Group',
        'image' => 'maschmeyer_carsten.png',
        'cluster' => 'academy',
    ),
    array( 
        'name' => 'Frank Thelen',
        'sort_name' => '800',
        'track' => 'main',
        'title' => 'CEO',
        'company' => 'e42',
        'image' => 'thelen_frank.png',
        'cluster' => 'money',
    ),
	/*
     array( 
        'name' => 'Daniel Graf',
        'sort_name' => '500',
        'track' => 'main',
        'title' => 'Head of Marketplace',
        'company' => 'Uber',
        'image' => 'graf_daniel.png',
        'cluster' => '',
    ),*/
    
    // Löwen Ende
     array( 
        'name' => 'Daniel Graf',
        'sort_name' => '500',
        'track' => 'main',
        'title' => 'Head of Marketplace',
        'company' => 'Uber',
        'image' => 'graf_daniel.png',
        'cluster' => '',
    ),
	array(
		'name' => 'Kara Swisher',
		'sort_name' => 'Swisher',
		'track' => 'main',
		'title' => 'Tech journalist & Co-Founder',
		'company' => 'Re/code',
		'image' => 'swisher-kara.png',
		'cluster' => '',
	),
    
    array( 
        'name' => 'Christos Tsolkas',
        'sort_name' => 'Tsolkas',
        'track' => 'main',
        'title' => 'Vice President of Global Sales Strategy',
        'company' => 'Philip Morris International',
        'image' => 'tsolkas-shristos.png',
        'cluster' => '',
    ), 
    
	array(
		'name' => 'Daniel Ramamoorthy',
		'sort_name' => 'Ramamoorthy',
		'track' => 'main',
		'title' => 'Entrepreneur | Startup & Govt Advisor',
		'company' => 'Treehouse',
		'image' => 'ramamoorthy-daniel.png',
		'cluster' => '',
	),
    
    array(
        'name' => 'Boris Scukanec Hopinski',
        'sort_name' => 'Hopinski',
        'track' => 'side , money, fintech',
        'title' => ' Chief Digital Officer',
        'company' => 'HypoVereinsbank',
        'image' => 'boris-scukanec-hopinski.png',
        'cluster' => 'money',
    ),
    
     array( 
        'name' => 'Philipp Kriependorf',
        'sort_name' => 'kriependorf',
        'track' => 'side , money, fintech',
        'title' => 'Co-Founder',
        'company' => 'Auxmoney',
        'image' => 'kriependorf_philipp.png',
        'cluster' => 'money',
    ), 
      array( 
        'name' => 'Mike Butcher',
        'sort_name' => 'Butcher',
        'track' => 'main',
        'title' => 'Editor-in-Chief',
        'company' => 'TechCrunch',
        'image' => 'mike-butcher.png',
        'cluster' => '',
    ), 
    array( 
        'name' => 'Nick Bortot',
        'sort_name' => 'Bortot',
        'track' => 'side, money, fintech',
        'title' => 'CEO & Founder',
        'company' => 'Bux',
        'image' => 'bortot_nick.png',
        'cluster' => 'money',
    ), 
    array( 
        'name' => 'Valentin Stalf',
        'sort_name' => '9999',
        'track' => 'side, money, fintech',
        'title' => 'Co-Founder & CEO',
        'company' => 'NUMBER 26',
        'image' => 'stalf_valentin.png',
        'cluster' => 'money',
    ),
    array( 
        'name' => 'Stephan Schambach',
        'sort_name' => 'schambach',
        'track' => 'main',
        'title' => 'Founder & CEO',
        'company' => 'NewStore',
        'image' => 'schambach_stephan.png',
        'cluster' => '',
    ),     
    array( 
        'name' => 'Heiko Hubertz',
        'sort_name' => 'hubertz',
        'track' => 'main, academy',
        'title' => 'Founder & CEO',
        'company' => 'WHOW Games',
        'image' => 'hubertz_heiko.png',
        'cluster' => 'academy',
    ),
    /*
    array( 
        'name' => 'Lu Li',
        'sort_name' => 'li',
        'track' => 'side, smart_company',
        'title' => 'Founder & CEO',
        'company' => 'Blooming Founders',
        'image' => 'li_lu.png',
        'cluster' => 'smart_company',
    ), */
    array( 
        'name' => 'Robin Wauters',
        'sort_name' => 'wauters',
        'track' => 'main',
        'title' => 'Founder & Editor',
        'company' => 'Tech.eu',
        'image' => 'wauters_robin.png',
        'cluster' => '',
    ),  
    array( 
        'name' => 'Patrick de Laive',
        'sort_name' => 'de laive',
        'track' => 'main',
        'title' => 'Co-Founder',
        'company' => 'The Next Web',
        'image' => 'de_laive_patrick.png',
        'cluster' => '',
    ), 
/*	
    array( 
        'name' => 'Christina Kehl',
        'sort_name' => 'kehl',
        'track' => 'main',
        'title' => 'Co-Founder',
        'company' => 'Knip AG',
        'image' => 'kehl_christina.png',
        'cluster' => 'money',
    ),
*/	
    array( 
        'name' => 'Konstantin Urban',
        'sort_name' => 'urban',
        'track' => 'side, commerce',
        'title' => 'Co-Founder & CEO',
        'company' => 'Windeln.de AG',
        'image' => 'urban_konstantin.png',
        'cluster' => 'ecommerce',
    ), 
    array( 
        'name' => 'Seven Rittau',
        'sort_name' => 'rittau',
        'track' => 'side, commerce',
        'title' => 'CEO',
        'company' => 'K5',
        'image' => 'user_placeholder.png',
        'cluster' => 'ecommerce',
    ), 
    array( 
        'name' => 'Lukasz Gadowski',
        'sort_name' => 'Gadowski',
        'track' => 'side, academy',
        'title' => 'Chairman, Delivery Hero',
        'company' => 'Founder, Spreadshirt',
        'image' => 'hadowski_lukasz.png',
        'cluster' => 'academy',
    ), 
    array( 
        'name' => 'Claire England',
        'sort_name' => 'England',
        'track' => 'side, academy',
        'title' => 'Executive Director',
        'company' => 'Central Texas Angel Network (CTAN)',
        'image' => 'england_claire.png',
        'cluster' => 'academy',
    ), 
    // 27-06-2016
    array( 
        'name' => 'Fabrice Grinda',
        'sort_name' => 'Grinda',
        'track' => 'main',
        'title' => 'Internet Entrepreneur & Investor',
        'company' => '',
        'image' => 'grinda_fabrice.png',
        'cluster' => '',
    ), /*
    array( 
        'name' => 'Christian Vollmann',
        'sort_name' => 'vollmann',
        'track' => 'side, smart_company',
        'title' => 'Founder',
        'company' => 'Nebenan.de',
        'image' => 'vollmann_christian.png',
        'cluster' => 'smart_company',
    ), */
    array( 
        'name' => 'Johannes Reck',
        'sort_name' => 'Reck',
        'track' => 'main',
        'title' => 'CEO',
        'company' => 'GetYourGuide',
        'image' => 'reck_johannes.png',
        'cluster' => '',
    ), 
    array( 
        'name' => 'Fabian Thylmann',
        'sort_name' => 'Thylamm',
        'track' => 'main',
        'title' => 'Founder',
        'company' => 'SN-Invests, Manwin (YouPorn)',
        'image' => 'thylmann_fabian.png',
        'cluster' => '',
    ), 
/*
    array( 
        'name' => 'Dr. Florian Heinemann',
        'sort_name' => 'Heinemann',
        'track' => 'side, smart_company',
        'title' => 'Co-Founder & Managing Director',
        'company' => 'Project A Ventures',
        'image' => 'heinemann_florian.png',
        'cluster' => 'smart_company',
    ),
*/	
    array( 
        'name' => 'Erik Voorhees',
        'sort_name' => 'Voorhees',
        'track' => 'side, blockchain, money',
        'track_role' => 'Speaker',
        'title' => 'CEO & Co-Founder',
        'company' => 'Shapeshift',
        'image' => 'voorhees_erik.png',
        'cluster' => 'money',
    ), 
    array( 
        'name' => 'Joshua Scigala',
        'sort_name' => 'Scigala',
        'track' => 'side, blockchain, money',
        'track_role' => 'Speaker',
        'title' => 'CEO & Co-Founder',
        'company' => 'Vaulttoro',
        'image' => 'scigala_joshua.png',
        'cluster' => 'money',
    ),
/*
    array( 
        'name' => 'Calogero Scibetta',
        'sort_name' => '0',
        'track' => 'side, blockchain, money',
        'track_role' => 'Speaker / Podium Discussion',
        'title' => 'COO & Co - Founder',
        'company' => 'Everledger',
        'image' => 'scibetta_calogero.png',
        'cluster' => 'money',
    ),
*/
    array( 
        'name' => 'Bruce Pon',
        'sort_name' => 'Pon',
        'track' => 'side, blockchain, money',
        'track_role' => 'Speaker / Podium Discussion',
        'title' => 'CEO & Co-Founder',
        'company' => 'Ascribe / BigChain DB',
        'image' => 'pon_bruce.png',
        'cluster' => 'money',
    ), 
	
	array( 
        'name' => 'Adam Stradling',
        'sort_name' => 'Stradling',
        'track' => 'side, blockchain, money',
        'track_role' => 'Speaker / Podium Discussion',
        'title' => 'Co-Owner',
        'company' => 'Blockchain Entrepreneur and Investor',
        'image' => 'adam_stradling.png',
        'cluster' => 'money',
    ),
	
    array( 
        'name' => 'Axel Apfelbacher',
        'sort_name' => 'Apfelbacher',
        'track' => 'side, blockchain, money',
        'track_role' => 'Moderator',
        'title' => 'Digital Banking Strategist & Co-Author',
        'company' => 'The FinTech Book',
        'image' => 'apfelbacher_axel.png',
        'cluster' => 'money',
    ), 
    array( 
        'name' => 'Siån Jones',
        'sort_name' => 'Jones',
        'track' => 'side, blockchain, money',
        'track_role' => 'Podium Discussion',
        'title' => 'CEO & Co-Founder',
        'company' => 'Coinsult',
        'image' => 'jones_sian.png',
        'cluster' => 'money',
    ), 
    array( 
        'name' => 'Angel González',
        'sort_name' => 'González',
        'track' => 'side, blockchain, money',
        'track_role' => 'Speaker',
        'title' => 'Executive IT Specialist',
        'company' => 'IBM',
        'image' => 'gonzalez_angel.png',
        'cluster' => 'money, blockchain'
    ), 
    array( 
        'name' => 'Till Faida',
        'sort_name' => 'Faida',
        'track' => 'main',
        'title' => 'CEO',
        'company' => 'Eyeo (Adblock Plus)',
        'image' => 'faida_till.png',
        'cluster' => '',
    ), 
    array( 
        'name' => 'Loic Le Meur',
        'sort_name' => 'Le Meur',
        'track' => 'main',
        'title' => 'CEO',
        'company' => 'Leade.rs',
        'image' => 'le_meur_loic.png',
        'cluster' => '',
    ), 
/*
    array( 
        'name' => 'Gil Penchina',
        'sort_name' => 'Penchina',
        'track' => 'main',
        'title' => 'Angel Investor',
        'company' => '',
        'image' => 'penchina_gil.png',
        'cluster' => '',
    ),
*/	
    
//    array( 
//        'name' => 'Leanne Kemp',
//        'sort_name' => 'Kemp',
//        'track' => 'main',
//        'title' => 'CEO & Co-Founder',
//        'company' => 'Everledger',
//        'image' => 'kemp_leanne.png',
//    'cluster' => '',
//    ), 
    array( 
        'name' => 'Hakan Koç',
        'sort_name' => 'Koç',
        'track' => 'main',
        'title' => 'Founder',
        'company' => 'AUTO1 Group (WirKaufenDeinAuto)',
        'image' => 'koc_hakan.png',
        'cluster' => '',
    ), 
/*    array( 
        'name' => 'Dr. Birte Gall',
        'sort_name' => 'Gall',
        'track' => 'side, smart_company',
        'title' => 'Founder & CEO',
        'company' => 'Berlin School of Digital Business',
        'image' => 'gall_birte.png',
        'cluster' => '',
    ), */
    array( 
        'name' => 'Pia Poppenreiter',
        'sort_name' => 'poppenreiter',
        'track' => 'side, commerce',
        'title' => 'CEO & Co-Founder',
        'company' => 'Ohlala.com',
        'image' => 'poppenreiter_pia.png',
        'cluster' => 'ecommerce',
    ), 
	array( 
        'name' => 'Pamela Reif',
        'sort_name' => 'reif',
        'track' => 'side, lifestyle',
        'title' => 'Social Media Star',
        'company' => '',
        'image' => 'reif_pamela.png',
        'cluster' => 'lifestyle',
    ), 
	array( 
        'name' => 'Dan Maag',
        'sort_name' => 'maag',
        'track' => 'main',
        'title' => 'Film producer, Founder & CEO',
        'company' => 'Pantaflix',
        'image' => 'dan_maag.png',
        'cluster' => '',
    ), 
/*
	array( 
        'name' => 'Justin Coghlan',
        'sort_name' => 'coghlan',
        'track' => 'main',
        'title' => 'Co-Founder',
        'company' => 'Movember Foundation',
        'image' => 'coghlan_justin.png',
        'cluster' => '',
    ),
*/	
	array( 
        'name' => 'Philipp Kreibohm',
        'sort_name' => 'kreibohm',
        'track' => 'side, commerce',
		'title' => 'Co-Founder & Co-Ceo',
        'company' => 'Home24',
        'image' => 'kreibohm_philipp.png',
        'cluster' => 'ecommerce',
    ), 
	array( 
        'name' => 'Renaud Visage',
        'sort_name' => 'visage',
        'track' => 'side, academy',
        'title' => 'Co-Founder & CTO',
        'company' => 'Eventbrite',
        'image' => 'visage_renaud.png',
        'cluster' => 'academy',
    ), 
		array( 
        'name' => 'Chris Kastenholz',
        'sort_name' => 'kastenholz',
        'track' => 'lifestyle',
        'title' => 'Co-Founder & CEO',
        'company' => 'PulseAdvertising',
        'image' => 'kastenholz_chris.png',
        'cluster' => 'lifestyle',
    ),
	
	array( 
        'name' => 'Jörg Kachelmann',
        'sort_name' => 'kachelmann',
        'track' => 'main',
        'title' => 'Unternehmer & Meteorologe',
        'company' => 'kachelmannwetter.com',
        'image' => 'kachelmann_joerg.png',
        'cluster' => '',
    ),
		array( 
        'name' => 'Max Wittrock',
        'sort_name' => 'wittrock',
        'track' => 'side, academy',
        'title' => 'Founder & CEO',
        'company' => 'mymuesli GmbH',
        'image' => 'wittrock_max.png',
        'cluster' => 'academy',
    ),
	array( 
        'name' => 'Roman Kirsch',
        'sort_name' => 'kirsch',
        'track' => 'side, commerce',
        'title' => 'Co-Founder & CEO',
        'company' => 'Lesara',
        'image' => 'kirsch_roman.png',
        'cluster' => 'ecommerce',
    ),
	array( 
        'name' => 'Elicia Bravo',
        'sort_name' => 'bravo',
        'track' => 'main',
        'title' => 'Chief Strategy Officer',
        'company' => 'Lottoland',
        'image' => 'bravo_elicia.png',
        'cluster' => '',
    ),
	array( 
        'name' => 'Florian Gschwandtner',
        'sort_name' => 'gschwandtner',
        'track' => 'side, academy',
        'title' => 'Co-Founder & CEO',
        'company' => 'Runtastic',
        'image' => 'gschwandtner_florian.png',
        'cluster' => '',
    ),
	array( 
        'name' => 'Lars Jankowfsky',
        'sort_name' => 'jankowfsky',
        'track' => 'side, academy',
        'title' => 'General Partner',
        'company' => 'NFQ',
        'image' => 'jankowfsky_lars.png',
        'cluster' => 'academy',
    ),
	array( 
        'name' => 'Ola Ahlvarsson',
        'sort_name' => 'ahlvarsson',
        'track' => 'side, academy',
        'title' => 'Co-Founder & Chairman',
        'company' => 'Result',
        'image' => 'ahlvarsson_ola.png',
        'cluster' => 'academy',
    ),
	array( 
        'name' => 'Heiner Kroke',
        'sort_name' => 'Kroke',
        'track' => 'side, commerce',
        'title' => 'CEO',
        'company' => 'Momox',
        'image' => 'kroke_heiner.png',
        'cluster' => 'ecommerce',
    ),
	array( 
        'name' => 'Jochen Wegner',
        'sort_name' => 'Wegner',
        'track' => 'main',
        'title' =>  'Chefredakteur',
        'company' => 'ZEIT Online',
        'image' => 'wegner_jochen.png',
        'cluster' => '',
    ),

	array( 
        'name' => 'Ulrich Schäfer',
        'sort_name' => 'schaefer',
        'track' => 'main',
        'title' =>  'Head of the Business Section',
        'company' => 'Süddeutsche Zeitung',
        'image' => 'schaefer_ulrich.png',
        'cluster' => '',
    ),
	array( 
        'name' => 'Fabian Westerheide',
        'sort_name' => 'westerheide',
        'track' => 'side, smart_company',
        'title' =>  'Founder & CEO',
        'company' => 'Asgard',
        'image' => 'westerheide_fabian.png',
        'cluster' => 'smart_company',
    ),
	array( 
        'name' => 'Dr. Ines Fritz',
        'sort_name' => 'fritz',
        'track' => 'side, academy',
        'title' =>  'Head of Startup Legal',
        'company' => 'EY',
        'image' => 'ines_fritz.png',
        'cluster' => 'academy',
    ),	
	
	//iot
	array( 
        'name' => 'Patrick Schwarzkopf',
        'sort_name' => 'schwarzkopf',
        'track' => 'side, iot',
        'title' =>  'Managing Director',
        'company' => 'VDMA Robotics + Automation',
        'image' => 'schwarzkopf_patrick.png',
        'cluster' => 'iot',
    ),
	array( 
        'name' => 'Benno Pichlmaier',
        'sort_name' => 'pichlmaier',
        'track' => 'side, iot',
        'title' =>  'Head of Research & Advanced Engineering',
        'company' => 'AGCO / Fendt',
        'image' => 'pichlmaier_benno.png',
        'cluster' => 'robotics, iot',
    ),
	array( 
        'name' => 'Gudrun Litzenberger',
        'sort_name' => 'litzenberger',
        'track' => 'side, iot',
        'title' =>  'General Secretary',
        'company' => 'IFR, Author of World Robotics ',
        'image' => 'llitzenberger_gudrun.png',
        'cluster' => 'robotics, iot',
    ),
	array( 
        'name' => 'Larry Jasinski',
        'sort_name' => 'jasinski',
        'track' => 'main, iot',
        'title' =>  'CEO',
        'company' => 'ReWalk Robotics, Inc.',
        'image' => 'jasinski_larry.png',
        'cluster' => 'robotics, iot',
    ),
    array( 
        'name' => 'Nicki Donnelly',
        'sort_name' => 'Nicki Donnelly',
        'track' => 'main, iot',
        'title' =>  'ReWalker',
        'company' => 'ReWalk Robotics, Inc.',
        'image' => 'donelly.png',
        'cluster' => 'robotics, iot',
    ),
    
    array( 
        'name' => 'Nikolas Engelhard',
        'sort_name' => 'Engelhard',
        'track' => 'side, iot',
        'title' =>  'Co-Founder',
        'company' => 'Magazino',
        'image' => 'nikolas-engelhard.png',
        'cluster' => 'robotics, iot',
    ),
    
	
	array(
		'name' => 'Charlie Kindel',
		'sort_name' =>'kindel',
		'track' => 'side, iot',
		'title' => 'Director',
		'company' => 'Smart Home, Amazon',
		'image' => 'kindel_charlie.png',
        'cluster' => 'iot',
	),
	
	array( 
        'name' => 'Sanjay Brahmawar',
        'sort_name' => 'Brahmawar',
        'track' => 'main',
        'title' =>  'CEO  & Managing Partner',
        'company' => 'IBM Watson Internet of Things',
        'image' => 'sanjay_brahmawar.png',
        'cluster' => '',
    ),
	array( 
        'name' => 'Joel Kaczmarek',
        'sort_name' => 'Kaczmarek',
        'track' => 'main',
        'title' =>  'Editor-in-Chief',
        'company' => 'digital kompakt',
        'image' => 'joel-kaczmarek.png',
        'cluster' => '',
    ),
    
    
    
    
    // Allianz Leute ;)
    
    
	array( 
        'name' => 'Christof Mascher',
        'sort_name' => 'Mascher',
        'track' => 'main, side , money, insurtech',
        'title' =>  'COO',
        'company' => 'Allianz Group',
        'image' => 'christof_mascher.png',
        'cluster' => 'money',
    ),
    
    array(
        'name' => 'Solmaz Altin',
        'sort_name' => 'Altin',
        'track' => 'side , money, insurtech',
        'title' => 'CDO',
        'company' => 'Allianz',
        'image' => 'altin-solmaz.png',
        'cluster' => 'money',
    ),
    array(
        'name' => 'Teja Pauli',
        'sort_name' => 'Pauli',
        'track' => 'side , money, insurtech',
        'title' => 'Tied Agent',
        'company' => 'Allianz Generalagentur',
        'image' => 'pauli-teja.png',
        'cluster' => 'money',
    ),
    
    
    
	
// Neu seit 26.08.2016
	array( 
        'name' => 'Christoph Hering',
        'sort_name' => 'Hering',
        'track' => 'side, money',
        'title' =>  'Crypto Coach',
        'company' => 'BitShares',
        'image' => 'hering_christoph.png',
        'cluster' => 'money',
    ),
	array( 
        'name' => 'André Bajorat',
        'sort_name' => 'Bajorat',
        'track' => 'side, money',
        'title' =>  'CEO',
        'company' => 'Figo',
        'image' => 'bajorat_andre_marcel.png',
        'cluster' => 'money',
    ),	
	array( 
        'name' => 'Christian Stammel',
        'sort_name' => 'Stammel',
        'track' => 'side, iot',
        'title' =>  'Founder & CEO',
        'company' => 'Wearable Technologies Group',
        'image' => 'stammel_christian.png',
        'cluster' => 'iot',
    ),
	
	array( 
        'name' => 'Tobias Gröber',
        'sort_name' => 'Gröber',
        'track' => '',
        'title' =>  'Head of ISPO Munich',
        'company' => 'ISPO Group',
        'image' => 'groeber_tobias.png',
        'cluster' => 'lifestyle',
    ),	
	array(
		'name' => 'Alexander Piutti',
		'sort_name' =>'Piutti',
		'track' => 'side, mobility',
		'title' => 'Serial Entrepreneur & Angel Investor',
		'company' => '',
		'image' => 'piutti_alexander.png',
        'cluster' => '',
	),
	
	array(
		'name' => 'Daniel Sobhani',
		'sort_name' =>'Sobhani',
		'track' => 'side, lifestyle',
		'title' => 'CEO',
		'company' => 'Freeletics',
		'image' => 'sobhani_daniel.png',
        'cluster' => 'lifestyle',
	),
	array(
		'name' => 'André Schwämmlein',
		'sort_name' =>'Schwämmlein',
		'track' => 'side, mobility',
		'title' => 'Founder',
		'company' => 'Flixbus',
		'image' => 'schwaemmlein_andre.png',
        'cluster' => 'mobility',
	),
	
	//Neu seit 01.09.2016
	array(
		'name' => 'Mauricio Piper',
		'sort_name' => 'Piper',
		'track' => 'wearables',
		'title' => 'CEO Wotch',
		'company' => 'Partner Smartwatchgroup',
		'image' => 'piper-mauricio.png',
		'cluster' => 'iot',
	),

	array(
		'name' => 'Simon Bartmann',
		'sort_name' => 'Bartmann',
		'track' => 'wearables',
		'title' => 'CEO & Founder',
		'company' => 'blinkked Technologies',
		'image' => 'bartmann-simon.png',
		'cluster' => 'iot',
	),

	array(
		'name' => 'Erwan Colin',
		'sort_name' => 'Colin',
		'track' => 'wearables',
		'title' => 'President & Co-Founder',
		'company' => 'Shammane',
		'image' => 'colin-erwan.png',
		'cluster' => 'iot',
	),

	array(
		'name' => 'Arnaud Lancelot',
		'sort_name' => 'Lancelot',
		'track' => 'wearables',
		'title' => 'CEO & Co-Founder',
		'company' => 'Shammane',
		'image' => 'lancelot-arnaud.png',
		'cluster' => 'iot',
	),

	/*
	array(
		'name' => 'Christian Stammel',
		'sort_name' => 'Stammel',
		'track' => 'wearables',
		'title' => 'CEO & Founder',
		'company' => 'Wearable Technologies AG',
		'image' => 'stammel-christian.png',
		'cluster' => 'iot',
	),*/

	array(
		'name' => 'Todd Glider',
		'sort_name' => 'Glider',
		'track' => 'wearables',
		'title' => 'CEO',
		'company' => 'BaDoink',
		'image' => 'glider-todd.png',
		'cluster' => 'iot',
	),

	array(
		'name' => 'Tania Boler',
		'sort_name' => 'Boler',
		'track' => 'wearables',
		'title' => 'CEO & Co-Founder',
		'company' => 'Elvie',
		'image' => 'boler-tania.png',
		'cluster' => 'iot',
	),

	array(
		'name' => 'Stephanie Alys',
		'sort_name' => 'Alys',
		'track' => 'wearables',
		'title' => 'Co-Founder, Chief Pleasure Officer',
		'company' => 'MysterVibe',
		'image' => 'alys_stephanie.png',
		'cluster' => 'iot',
	),

	array(
		'name' => 'Jiang Li',
		'sort_name' => 'Li',
		'track' => 'wearables',
		'title' => 'CEO',
		'company' => 'VivaInk',
		'image' => 'li-jiang.png',
		'cluster' => 'iot',
	),
    
    array(
		'name' => 'Rob Goudswaard',
		'sort_name' => 'Goudswaard',
		'track' => 'wearables',
		'title' => 'Business Development Leader Europe',
		'company' => 'Royal Philips BV',
		'image' => 'goud_rob.png',
		'cluster' => 'iot',
	),



	array(
		'name' => 'Jochen Krisch',
		'sort_name' => 'Krisch',
		'track' => 'commerce',
		'title' => 'Publisher',
		'company' => 'Exciting Commerce',
		'image' => 'krisch-jochen.png',
		'cluster' => 'ecommerce',
	),

	array(
		'name' => 'Christopher Grätz',
		'sort_name' => 'Grätz',
		'track' => 'money, fintech',
		'title' => 'CEO & Co-Founder',
		'company' => 'kapilendo ',
		'image' => 'graetz-christopher.png',
		'cluster' => 'money',
	),

	array(
		'name' => 'Florian Miguet',
		'sort_name' => 'Miguet',
		'track' => 'side, lifestyle',
		'title' => 'CEO & Founder',
		'company' => 'Clim8',
		'image' => 'miguet-florian.png',
		'cluster' => 'lifestyle',
	),

	array(
		'name' => 'Maximilian Vollenbroich',
		'sort_name' => 'Vollenbroich',
		'track' => 'side, mobility',
		'title' => 'CEO & Co-Founder',
		'company' => 'Carspring',
		'image' => 'vollenbroich-maximilian.png',
		'cluster' => 'mobility',
	),
	
	array(
		'name' => 'Florian Prucker',
		'sort_name' => 'Prucker',
		'track' => 'side, money, fintech',
		'title' => 'Co-Founder and Co-CEO',
		'company' => 'Scalable Capital',
		'image' => 'prucker_florian.png',
		'cluster' => 'money',
	),
    
    
    //Neu seit 07.09.2015
    
    array(
        'name' => 'Sebastian Bärhold',
        'sort_name' => 'Bärhold',
        'track' => 'side , money, fintech',
        'title' => 'Co-Founder',
        'company' => 'ID Now',
        'image' => 'baerhold-sebastian.png',
        'cluster' => 'money',
    ),

    array(
        'name' => 'Marcus Börner',
        'sort_name' => 'Börner',
        'track' => 'side , money, fintech',
        'title' => 'CEO',
        'company' => 'Optiopay',
        'image' => 'boerner-marcus.png',
        'cluster' => 'money',
    ),

    array(
        'name' => 'Chris Scheuermann',
        'sort_name' => 'Scheuermann',
        'track' => 'side , money, fintech',
        'title' => 'Head of Sales',
        'company' => 'figo',
        'image' => 'scheuermann-chris.png',
        'cluster' => 'money',
    ),

    array(
        'name' => 'Marko Wenthin',
        'sort_name' => 'Wenthin',
        'track' => 'side , money, fintech',
        'title' => 'Vorstand',
        'company' => 'SolarisBank',
        'image' => 'wenthin-marko.png',
        'cluster' => 'money',
    ),

    array(
        'name' => 'Michael Koch',
        'sort_name' => 'Koch',
        'track' => 'side , money, fintech',
        'title' => 'Head of digital Factory',
        'company' => 'Deutsche Bank',
        'image' => 'koch-michael.png',
        'cluster' => 'money',
    ),

    array(
        'name' => 'Nektarios Liolios',
        'sort_name' => 'Liolios',
        'track' => 'side , money, insurtech',
        'title' => 'Co-founder & CEO',
        'company' => 'Startupbootcamp',
        'image' => 'liolios-nektarios.png',
        'cluster' => 'money',
    ),

    array(
        'name' => 'Olaf Acker',
        'sort_name' => 'Acker',
        'track' => 'side , money, insurtech',
        'title' => 'Partner',
        'company' => 'PwC Strategy',
        'image' => 'acker-olaf.png',
        'cluster' => 'money',
    ),



    array(
        'name' => 'Christopher Oster',
        'sort_name' => 'Oster',
        'track' => 'side , money, insurtech',
        'title' => 'CEO',
        'company' => 'Clark',
        'image' => 'oster-christopher.png',
        'cluster' => 'money',
    ),

    array(
        'name' => 'David Stubbs',
        'sort_name' => 'Stubbs',
        'track' => 'side , money, insurtech',
        'title' => 'CEO',
        'company' => 'RightIndem',
        'image' => 'stubbs-david.png',
        'cluster' => 'money',
    ),

    array(
        'name' => 'Annette C. Leonhard MacDonald',
        'sort_name' => 'C. Leonhard MacDonald',
        'track' => 'side , money, insurtech',
        'title' => 'CEO',
        'company' => 'Leonhard-MacDonald Ventures',
        'image' => 'c-leonhard-macdonald-annette.png',
        'cluster' => 'money',
    ),


    array(
        'name' => 'Paolo Galvani',
        'sort_name' => 'Galvani',
        'track' => 'side , money, insurtech',
        'title' => 'CEO',
        'company' => 'Moneyfarm',
        'image' => 'galvani-paolo.png',
        'cluster' => 'money',
    ),

    array(
        'name' => 'Robin von Hein',
        'sort_name' => 'von Hein',
        'track' => 'side , money, insurtech',
        'title' => 'CEO',
        'company' => 'Simplesurance',
        'image' => 'vonhein-robin.png',
        'cluster' => 'money',
    ),

    array(
        'name' => 'Victor van Tol',
        'sort_name' => 'van Tol',
        'track' => 'side , money, insurtech',
        'title' => 'CEO',
        'company' => 'Snappcar',
        'image' => 'vantol-victor.png',
        'cluster' => 'money',
    ),

    array(
        'name' => 'Nils v. Dellingshausen',
        'sort_name' => 'v. Dellingshausen',
        'track' => 'side , money, insurtech',
        'title' => 'CEO',
        'company' => 'BetterDoc',
        'image' => 'v-dellingshausen-nils.png',
        'cluster' => 'money',
    ),

    array(
        'name' => 'Florian Rothfuss',
        'sort_name' => 'Rothfuss',
        'track' => 'side, mobility',
        'title' => 'Manager Digital Business',
        'company' => 'Porsche',
        'image' => 'rothfuss-florian.png',
        'cluster' => 'mobility',
    ),

    array(
        'name' => 'Gregor Gimmy',
        'sort_name' => 'Gimmy',
        'track' => 'side, mobility',
        'title' => 'Founder and Head',
        'company' => 'BMW Startup Garage',
        'image' => 'gimmy-gregor.png',
        'cluster' => 'mobility',
    ),

    array(
        'name' => 'Sven Lackinger',
        'sort_name' => 'Lackinger',
        'track' => 'side, mobility',
        'title' => 'Founder & Managing Director',
        'company' => 'evopark',
        'image' => 'lackinger-sven.png',
        'cluster' => 'mobility',
    ),

    array(
        'name' => 'Maxim Nohroudi',
        'sort_name' => 'Nohroudi',
        'track' => 'side, mobility',
        'title' => 'CEO & Co-Founder',
        'company' => 'door2door / ally / allygator',
        'image' => 'nohroudi-maxim.png',
        'cluster' => 'mobility',
    ),

    array(
        'name' => 'Stephan Leppler',
        'sort_name' => 'Leppler',
        'track' => 'side, mobility',
        'title' => 'CEO & Co-Founder',
        'company' => 'MotionTag',
        'image' => 'leppler-stephan.png',
        'cluster' => 'mobility',
    ),
/*
    array(
        'name' => 'Jörg Lamprecht',
        'sort_name' => 'Lamprecht',
        'track' => 'side, mobility',
        'title' => 'CEO & Co-Founder',
        'company' => 'dedrone',
        'image' => 'lamprecht-joerg.png',
        'cluster' => 'mobility',
    ),*/

    array(
        'name' => 'Mat Schubert',
        'sort_name' => 'Schubert',
        'track' => 'side, mobility',
        'title' => 'CEO & Founder',
        'company' => 'Coup & BBM Mobility, Robert Bosch',
        'image' => 'schubert-mat.png',
        'cluster' => 'mobility',
    ),

    array(
        'name' => 'Stefan Mennerich',
        'sort_name' => 'Mennerich',
        'track' => 'side, lifestyle',
        'title' => 'Prokurist, Direktor Medien, Digital und Kommunikation',
        'company' => 'FC Bayern',
        'image' => 'mennerich-stefan.png',
        'cluster' => 'lifestyle',
    ),

    array(
        'name' => 'Gunnar Jans',
        'sort_name' => 'Jans',
        'track' => 'side, lifestyle',
        'title' => 'Chefredakteur Sport & Sport Business',
        'company' => 'THE DIGITALE',
        'image' => 'jans-gunnar.png',
        'cluster' => 'lifestyle',
    ),
    
    array(
        'name' => 'Peter Bilz-Wohlgemuth',
        'sort_name' => 'Bilz-Wohlgemuth',
        'track' => 'side, lifestyle',
        'title' => 'CTO',
        'company' => 'THE DIGITALE',
        'image' => 'bilz_peter.png',
        'cluster' => 'lifestyle',
    ),
    
    array(
        'name' => 'Stefan Wolf',
        'sort_name' => 'Wolf',
        'track' => 'side, lifestyle',
        'title' => 'CEO & Founder',
        'company' => 'pop that tag',
        'image' => 'wolf-stefan.png',
        'cluster' => 'lifestyle',
    ),

    array(
        'name' => 'Daniel  Krauss',
        'sort_name' => 'Krauss',
        'track' => 'side, smart_comp2',
        'title' => 'Co-Founder & CIO',
        'company' => 'FlixBus',
        'image' => 'krauss-daniel.png',
        'cluster' => 'smart_company',
    ),

    array(
        'name' => 'Fabian Wesemann',
        'sort_name' => 'Wesemann',
        'track' => 'side, smart_comp2',
        'title' => 'Co-Founder',
        'company' => 'FinanceFox',
        'image' => 'wesemann-fabian.png',
        'cluster' => 'smart_company',
    ),
    
    array(
        'name' => 'Nora Jakob',
        'sort_name' => 'Jakob',
        'track' => 'side, smart_comp2',
        'title' => 'Freelance Journalist',
        'company' => '',
        'image' => 'generic-avatar-female.png',
        'cluster' => 'smart_company',
    ),
    
    array(
        'name' => 'Bernd Wagner',
        'sort_name' => 'Wagner',
        'track' => 'side, smart_comp2',
        'title' => 'Vice President Marketing Cloud',
        'company' => 'Salesforce',
        'image' => 'user_placeholder.png',
        'cluster' => 'smart_company',
    ),
    
    
    array(
        'name' => 'Niels Doerje',
        'sort_name' => 'Doerje',
        'track' => 'side, smart_comp1',
        'title' => 'Digital Entrepreneur | Serial Board Advisor',
        'company' => null,
        'image' => 'doerje_niels.png',
        'cluster' => 'smart_company',
    ),
    
    array(
        'name' => 'Jörg Lamprecht',
        'sort_name' => 'Lamprecht',
        'track' => 'side, smart_comp1, mobility',
        'title' => 'Co-Founder & CEO',
        'company' => 'Dedrone',
        'image' => 'joerg-lamprecht-ceo-dedrone.png',
        'cluster' => 'smart_company, mobility',
    ),
    
    array(
        'name' => 'Ferry Heilemann',
        'sort_name' => 'Heilemann',
        'track' => 'side, smart_company',
        'title' => 'Co-Founder & CEO',
        'company' => 'Freighthub',
        'image' => 'ferry_heilemann.png',
        'cluster' => 'smart_company',
    ),

/*    array(
        'name' => 'Frank Engelhardt',
        'sort_name' => 'Engelhardt',
        'track' => 'side, smart_comp2',
        'title' => 'Vice President Enterprise Strategy',
        'company' => 'Salesforce',
        'image' => 'engelhardt-frank.png',
        'cluster' => 'smart_company',
    ),*/

    array(
        'name' => 'Dr. Carsten Rudolph',
        'sort_name' => 'Rudolph',
        'track' => 'side, gruenderland',
        'title' => 'CEO',
        'company' => 'BayStartUP',
        'image' => 'rudolph-dr-carsten.png',
        'cluster' => '',
    ),

    array(
        'name' => 'Thorsten Gröne',
        'sort_name' => 'Gröne',
        'track' => 'side, gruenderland',
        'title' => 'Managing Director & Co-Founder',
        'company' => 'Cevotec',
        'image' => 'groene-thorsten.png',
        'cluster' => '',
    ),

    array(
        'name' => 'Sebastian Jagsch',
        'sort_name' => 'Jagsch',
        'track' => 'side, gruenderland',
        'title' => 'Founder & CEO',
        'company' => 'eluminocity',
        'image' => 'jagsch-sebastian.png',
        'cluster' => '',
    ),

    array(
        'name' => 'Hanno Renner',
        'sort_name' => 'Renner',
        'track' => 'side, gruenderland, smart_company',
        'title' => 'Co-Founder & CEO',
        'company' => 'Personio',
        'image' => 'renner-hanno.png',
        'cluster' => 'smart_company',
    ),

    array(
        'name' => 'Philpp G. Schwarz',
        'sort_name' => 'Schwarz',
        'track' => 'side, gruenderland',
        'title' => 'Co-Founder & CEO',
        'company' => 'Wearable Life Science',
        'image' => 'schwarz-philppg.png',
        'cluster' => '',
    ),

    array(
        'name' => 'Markus Bohl',
        'sort_name' => 'Bohl',
        'track' => 'side, gruenderland',
        'title' => 'Co-Founder & CEO',
        'company' => 'Fineway',
        'image' => 'bohl-markus.png',
        'cluster' => '',
    ),

    array(
        'name' => 'Dr. Felix Reinshagen',
        'sort_name' => 'Reinshagen',
        'track' => 'side, gruenderland',
        'title' => 'Co-Founder & CEO',
        'company' => 'NavVis',
        'image' => 'reinshagen-dr-felix.png',
        'cluster' => '',
    ),

    array(
        'name' => 'David Heiny',
        'sort_name' => 'Heiny',
        'track' => 'side, gruenderland',
        'title' => 'Managing Director',
        'company' => 'SimScale',
        'image' => 'heiny-david.png',
        'cluster' => '',
    ),

    array(
        'name' => 'Matthew Ulbrich',
        'sort_name' => 'Ulbrich',
        'track' => 'side, gruenderland',
        'title' => 'Co-Founder & Chief Creative Officer',
        'company' => 'Tickaroo',
        'image' => 'ulbrich-matthew.png',
        'cluster' => '',
    ),

    array(
        'name' => 'Dr. Markus Knapek',
        'sort_name' => 'Knapek',
        'track' => 'side, gruenderland',
        'title' => 'Co-Founder & CEO',
        'company' => 'ViaLight Communications',
        'image' => 'knapek-dr-markus.png',
        'cluster' => '',
    ),

    array(
        'name' => 'Andreas Fruth',
        'sort_name' => 'Fruth',
        'track' => 'side, gruenderland',
        'title' => 'Co-Founder',
        'company' => 'Global Savings Group',
        'image' => 'fruth-andreas.png',
        'cluster' => '',
    ),
    array(
        'name' => 'Alexander Ullmann',
        'sort_name' => 'Ullmann',
        'track' => 'side, gruenderland',
        'title' => 'Venture Team',
        'company' => 'BayBG',
        'image' => 'user_placeholder.png',
        'cluster' => '',
    ),
/*
    array(
        'name' => 'Andreas Kunze',
        'sort_name' => 'Kunze',
        'track' => 'side, gruenderland',
        'title' => 'Co-Founder & CEO',
        'company' => 'Konux',
        'image' => 'kunze-andreas.png',
        'cluster' => 'mobility',
    ),*/
    
    array(
        'name' => 'Torben Schreiter',
        'sort_name' => 'Schreiter',
        'track' => 'side, smart_company',
        'title' => 'Executive Advisor',
        'company' => 'Signavio',
        'image' => 'torben_schreiter.png',
        'cluster' => 'smart_company',
    ),
    
     array(
        'name' => 'Matthias Eireiner',
        'sort_name' => 'Eireiner',
        'track' => 'side , money, fintech',
        'title' => 'Managing Director',
        'company' => 'FinReach',
        'image' => 'matthias_eireiner.png',
        'cluster' => 'money',
    ),
    
    
    //Kein Bild
    
    
    
    array(
        'name' => 'Tomas Peeters',
        'sort_name' => 'Peeters',
        'track' => 'side , money, fintech',
        'title' => 'CSO',
        'company' => 'Ing diba',
        'image' => 'peteers_tomas.png',
        'cluster' => 'money',
    ),
    
    
       
    
    
    
    array(
        'name' => 'Raphael Fellmer',
        'sort_name' => 'Fellmer',
        'track' => 'main',
        'title' => 'Co-Founder',
        'company' => 'foodsharing & Sharecy',
        'image' => 'fellmer_raphael.png',
        'cluster' => '',
    ),
    
    
    // Kara Swisher
	
	/*
	array(
		'name' => 'Ilse Aigner',
		'sort_name' =>'Aigner',
		'track' => '',
		'title' => '',
		'company' => '',
		'image' => '',
        'cluster' => '',
	),
    
    */
    
    array(
        'name' => 'Jan Hendrik Merlin Jacob',
        'sort_name' => 'Jacob',
        'track' => 'side, smart_company',
        'title' => 'Co-Founder & CTO',
        'company' => 'OnPage.org',
        'image' => 'jan_hendrik_merlin_jacob.png',
        'cluster' => 'smart_company',
    ),
    
	
	
	//Jury

    array( 
        'name' => 'Andreas Etten',
        'sort_name' => 'Wegner',
        'track' => 'jury, main',
        'title' =>  'Founding Partner',
        'company' => '10x',
        'image' => 'etten_andreas.png',
        'cluster' => '',
    ),
    array( 
        'name' => 'Christian Saller',
        'sort_name' => 'XXXXX',
        'track' => 'jury, main',
        'title' =>  'General Partner',
        'company' => 'Holtzbrinck',
        'image' => 'christian_saller.png',
        'cluster' => '',
    ),
    array( 
        'name' => 'Daniel Glasner',
        'sort_name' => 'XXXXX',
        'track' => 'jury, main',
        'title' =>  'Founding Partner',
        'company' => 'Cherry Ventures',
        'image' => 'glasner_daniel.png',
        'cluster' => '',
    ),
    array( 
        'name' => 'Hendrik Brandis',
        'sort_name' => 'XXXXX',
        'track' => 'jury, main',
        'title' =>  'Partner',
        'company' => 'Earlybird',
        'image' => 'hendrik_brandis.png',
        'cluster' => '',
    ),
    array( 
        'name' => 'Rob Moffat',
        'sort_name' => 'XXXXX',
        'track' => 'jury, main',
        'title' =>  'Partner',
        'company' => 'Balderton Capital',
        'image' => 'rob_moffat.png',
        'cluster' => '',
    ),
    array( 
        'name' => 'Philipp Hartmann',
        'sort_name' => 'XXXXX',
        'track' => 'jury, main',
        'title' =>  'Principal',
        'company' => 'Index Ventures',
        'image' => 'philipp_hartmann.png',
        'cluster' => '',
    ),
    array( 
        'name' => 'Christopher Steinau',
        'sort_name' => 'XXXXX',
        'track' => 'jury, main',
        'title' =>  'Investment Manager',
        'company' => 'Northzone',
        'image' => 'christopher_steinau.png',
        'cluster' => '',
    ),


    
    
    
    
    
    
    /*
    array( 
        'name' => 'Elizabeth Yin',
        'sort_name' => 'XXXXX',
        'track' => 'jury, main',
        'title' =>  'Partner',
        'company' => '500 Startups',
        'image' => 'yin_elizabeth.png',
        'cluster' => '',
    ),
	array( 
        'name' => 'Marie-Helene Ametsreiter',
        'sort_name' => 'XXXXX',
        'track' => 'jury, main',
        'title' =>  'Partner',
        'company' => 'Speedinvest',
        'image' => 'ametsreiter_marie_helene.png',
        'cluster' => '',
    ),
	array( 
        'name' => 'Nenad Marovac',
        'sort_name' => 'XXXXX',
        'track' => 'jury, main',
        'title' =>  'Managing Partner & CEO',
        'company' => 'DN Capital',
        'image' => 'marovac_nenad.png',
        'cluster' => '',
    ),
	
	array( 
        'name' => 'Christian Miele',
        'sort_name' => 'XXXXX',
        'track' => 'jury, main',
        'title' =>  'Vice President',
        'company' => 'eventures',
        'image' => 'miele_christian.png',
        'cluster' => '',
    ),
	array( 
        'name' => 'Fritz Oidtman',
        'sort_name' => 'XXXXX',
        'track' => 'jury, main',
        'title' =>  'Managing Partner',
        'company' => 'Acton Capital',
        'image' => 'oidtmann_fritz.png',
        'cluster' => '',
    ),

	array( 
        'name' => 'Markus Hefter',
        'sort_name' => 'XXXXX',
        'track' => 'jury, main',
        'title' =>  'Exhibition Group Director',
        'company' => 'ISPO',
        'image' => 'hefter_markus.png',
        'cluster' => '',
    ),

    
	
	array( 
        'name' => 'Ulrich Schäfer',
        'sort_name' => 'schaefer',
        'track' => 'main',
        'title' =>  'Head of the Business Section',
        'company' => 'Süddeutsche Zeitung',
        'image' => '',
        'cluster' => '',
    ),
	//iot
	array( 
        'name' => 'Patrick Schwarzkopf',
        'sort_name' => 'schwarzkopf',
        'track' => 'main',
        'title' =>  'Managing Director',
        'company' => 'VDMA Robotics + Automation',
        'image' => '',
        'cluster' => '',
    ),
	array( 
        'name' => 'Benno Pichlmaier',
        'sort_name' => 'pichlmaier',
        'track' => 'main',
        'title' =>  'Head of Research & Advanced Engineering',
        'company' => 'AGCO / Fendt',
        'image' => '',
        'cluster' => '',
    ),
	array( 
        'name' => 'Gudrun Litzenberger',
        'sort_name' => 'litzenberger',
        'track' => 'main',
        'title' =>  'General Secretary',
        'company' => 'IFR, Author of World Robotics ',
        'image' => '',
        'cluster' => '',
    ),
	array( 
        'name' => 'Larry Jasinski',
        'sort_name' => 'jasinski',
        'track' => 'main',
        'title' =>  'CEO',
        'company' => 'ReWalk Robotics, Inc.',
        'image' => '',
        'cluster' => '',
    ),
	
*/
	

	
);