<?php

$hotels_2016 = [
    [
        'hotel' => 'Sofitel Munich Bayerpost',
        'stars' => 5,
        'price' => 500,
        'main_image' => 'sofitel-main.png',
        'first_image' => 'sofitel-1.png',
        'second_image' => 'sofitel-2.png',
        'adress' => 'Bayerstraße 12, 80335 M&uumlnchen',
        'location' => '10 km from ICM <br> 1.3 km from Oktoberfest',
        'price_detail' => 'Single Room: 500€/night incl. VAT <br> Double Room: 540€/night incl. VAT',
        'booking_link' => 'mailto:hotel@bitsandpretzels.com&Subject=Booking%20Request%3A%20Bits%26Bretzels%202016&Body=Please%20provide%20your%20personal%20data%20here%20%28do%20not%20change%20the%20title%29%3A%0A%0ABilling%20Address%3A%0ACompany%3A%0AFirst%20Name%3A%0ASurname%3A%0AStreet%20/%20Nr.%3A%0AZip%20Code%20/%20City%3A%0ACountry%3A%0A%0AIntended%20check-in%20date%3A%0AIntended%20check-out%20date%3A%0A%0AName%20of%20Guest%20%231%3A%0AName%20of%20Guest%20%232%3A'
    ],
    [
        'hotel' => 'INNSIDE M&uumlnchen Neue Messe',
        'stars' => 4,
        'price' => 129, 
        'main_image' => 'neuemesse-main.png',
        'first_image' => 'neuemesse-1.png',
        'second_image' => 'neuemesse-2.png',
        'adress' => 'Humboldtstraße 12, 85609 Aschheim',
        'location' => '2.1 km from ICM <br> 11 km from Oktoberfest',
        'price_detail' => '24.-25.09. <br>
            Single Room: 199€/night incl. VAT  <br>
            Double Room: 229€/night incl. VAT<br>
             <br>
            25.-26.09<br>
            Single Room: 129€/night incl. VAT <br>
            Double Room: 159€/night incl. VAT<br>
             <br>
            26.-27.09.<br>
            Single Room: 239€/night incl. VAT <br>
            Double Room: 269€/night incl. VAT<br>
             <br>
            27.-28.09<br>
            Single Room: 239€/night incl. VAT <br>
            Double Room: 269€/night incl. VAT<br>
             <br>
            Premium Kategorie +15€<br>
            Preise inkl. Frühstück und Minibar<br>',
        'booking_link' => 'mailto:reservierung.innside.neumesse@melia.com?BCC=hotel@bitsandpretzels.com&Subject=Booking%20Request%3A%20Bits%26Bretzels%202016-inside-neue-messe&Body=Please%20provide%20your%20personal%20data%20here%20%28do%20not%20change%20the%20title%29%3A%0A%0ABilling%20Address%3A%0ACompany%3A%0AFirst%20Name%3A%0ASurname%3A%0AStreet%20/%20Nr.%3A%0AZip%20Code%20/%20City%3A%0ACountry%3A%0A%0AIntended%20check-in%20date%3A%0AIntended%20check-out%20date%3A%0A%0AName%20of%20Guest%20%231%3A%0AName%20of%20Guest%20%232%3A'
    ],
    [
        'hotel' => 'INNSIDE M&uumlnchen Schwabing',
        'stars' => 4,
        'price' => 169, 
        'main_image' => 'parkstadtschwabing-main.png',
        'first_image' => 'parkstadtschwabing-1.png',
        'second_image' => 'parkstadtschwabing-2.png',
        'adress' => 'Mies-van-der-Rohe-Straße 10, 80807 M&uumlnchen',
        'location' => '10.5 km from ICM <br>7.5 km from Oktoberfest',
        'price_detail' => '24.-25.09. <br>
            Single Room: 269€/night incl. VAT <br>
            Double Room: 299€/night incl. VAT<br>
             <br>
            25.-26.09<br>
            Single Room: 169€/night incl. VAT <br>
            Double Room: 199€/night incl. VAT<br>
             <br>
            26.-27.09<br>
            Single Room: 239€/night incl. VAT <br>
            Double Room: 269€/night incl. VAT<br>
            
             <br>
            27.-28.09<br>
            Single Room: 249€/night incl. VAT <br>
            Double Room: 279€/night incl. VAT <br>
             <br>
            Preise inkl. Frühstück und Minibar<br>',
        'booking_link' => 'mailto:reservierung.innside.neumesse@melia.com?BCC=hotel@bitsandpretzels.com&Subject=Booking%20Request%3A%20Bits%26Bretzels%202016-inside-schwabing&Body=Please%20provide%20your%20personal%20data%20here%20%28do%20not%20change%20the%20title%29%3A%0A%0ABilling%20Address%3A%0ACompany%3A%0AFirst%20Name%3A%0ASurname%3A%0AStreet%20/%20Nr.%3A%0AZip%20Code%20/%20City%3A%0ACountry%3A%0A%0AIntended%20check-in%20date%3A%0AIntended%20check-out%20date%3A%0A%0AName%20of%20Guest%20%231%3A%0AName%20of%20Guest%20%232%3A'
    ],
    [
        'hotel' => 'GHOTEL hotel & living München-City',
        'stars' => '3',
        'price' => 269,
        'main_image' => 'ghotel-main.png',
        'first_image' => 'ghotel-1.png',
        'second_image' => 'ghotel-2.png',
        'adress' => 'Landwehrstrasse 77, 80336 München',
        'location' => '',
        'price_detail' => 'Single Room: from 269€/night incl. VAT <br>Double Room: from 299€/night incl. VAT',
        'booking_link' => 'mailto:reservierung.muenchen@ghotel.de?BCC=hotel@bitsandpretzels.com&Subject=Booking%20Request%3A%20Bits%26Bretzels%202016&Body=Please%20provide%20your%20personal%20data%20here%20%28do%20not%20change%20the%20title%29%3A%0A%0ABilling%20Address%3A%0ACompany%3A%0AFirst%20Name%3A%0ASurname%3A%0AStreet%20/%20Nr.%3A%0AZip%20Code%20/%20City%3A%0ACountry%3A%0A%0AIntended%20check-in%20date%3A%0AIntended%20check-out%20date%3A%0A%0AName%20of%20Guest%20%231%3A%0AName%20of%20Guest%20%232%3A'
    ],
    [
        'hotel' => 'Wies‘n Loft',
        'stars' => '',
        'price' => 99,
        'main_image' => 'wiesnloft-main.png',
        'first_image' => 'wiesnloft-1.png',
        'second_image' => 'wiesnloft-2.png',
        'adress' => 'Schichtlstraße 46, 81929 M&uumlnchen',
        'location' => '2.8 km from ICM <br>10.6 km from Oktoberfest',
        'price_detail' => 'Double Room: from 99€/night incl. VAT',
        'booking_link' => 'mailto:INFO@WIESNLOFT.DE?BCC=hotel@bitsandpretzels.com&Subject=Booking%20Request%3A%20BUP16&Body=Please%20provide%20your%20personal%20data%20here%20%28do%20not%20change%20the%20subject%20or%20title%29%3A%0A%0ABilling%20Address%3A%0ACompany%3A%0AFirst%20Name%3A%0ASurname%3A%0AStreet%20/%20Nr.%3A%0AZip%20Code%20/%20City%3A%0ACountry%3A%0A%0AIntended%20check-in%20date%3A%0AIntended%20check-out%20date%3A%0A%0AName%20of%20Guest%20%231%3A%0AName%20of%20Guest%20%232%3A'
    ],
];

?>