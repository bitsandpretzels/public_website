<?php

$sideevents_2016 = [
    /* Premium Side Events */
    [
        'premium' => 'yes',
        'headline' => 'Media-Saturn SPACELAB Pitch Battle',
        'main_image' => 'spacelab_pitch.jpg',
        'info' => 'Registration necessary - Free for B&P attendees',
        'adress' => 'Nymphenburger Straße 86, 80636 Munich',
        'location' => 'Along with a surprise keynote speech we’ll be hosting a Pitch Battle @SPACELAB and you are invited to join us and share a pretzel with fellow start-ups and start-up enthusiasts!<br>
        The winner will not only be awarded a € 200 Saturn voucher but also an exclusive wild card to pitch at SPACELAB’s Demo Day in November.<br>
        The start-ups aka contestants will compete in an Oktoberfest-themed setting trying to convince the audience of their business in three knockout rounds.<br><br>

        <b>Round 1</b><br>
        4 elevator pitch battles: 2 min each, no pitch deck > 4 winners move on to the next round<br><br>

        <b>Round 2</b><br>
        2 pitch battles: 3 min pitch + 2 min Q&A each, max 3 slides > 2 winners proceed to round 3<br><br>

        <b>Round 3</b><br>
        1 final battle between the best two start-ups: 5 min pitch + 5 min Q&A each, full pitch deck allowed<br><br>

        If you’d like to participate in the battle, please send your pitch deck to spacelab@eog.de before 21 September 2016. If you’d like to view the spectacle, just register via Eventbrite. 
        ',
        'button' => 'Register',
        'booking_link' => 'https://www.eventbrite.de/e/ms-spacelab-pitch-battle-tickets-27351775898'
	],
    [
        'premium' => 'yes',
        'headline' => 'BMW Startup Garage Biergarten - Bits & Pretzels After Hour',
        'main_image' => 'sideevent_bmw_garage_v6.jpg',
        'info' => 'Application is needed - Free for B&P attendees',
        'adress' => 'BMW, Parkring 19, 85748 Garching Munich',
        'location' => 'When night falls, the real excitement begins at a place that can make all the difference to your startup! 
            Meet the BMW Startup Garage - your gateway into the automotive industry!<br>

            Find out how you can succeed in the trillion dollar automotive industry in a relaxed Biergarten atmosphere. And have one (or more...) beers  with us - Hendl included. <br><br>

            <b>What to expect:</b><br>

            Your fasttrack into the automotive industry! Tell us about your startup and learn how the BMW Startup Garage can help you succeed in the automotive industry! In our pit stop sessions we focus 100% on you 

            Network with BMW innovation managers and investors!<br>

            Up for a ride? Then test drive a ?! ... i8 ... maybe! :)<br>

            Hungry... and of course thirsty? We &apos;ve got you covered with beer, wine, non-alcoholics and Biergarten food!<br>

            Additional goodie! Get a €29 voucher to register for free to DriveNow!<br>
            
            <b>Agenda:</b><br>

            <b>7:00 PM:</b> Shuttle from ICM to BMW Startup Garage<br>

            <b>7:20 PM:</b> Shuttle from Odeonsplatz to BMW Startup Garage<br>

            <b>8:00 PM:</b> Registration<br>

            <b>9:00 PM:</b> Welcome by Gregor Gimmy and Matthias Meyer (founders of BMW Startup Garage)<br><br>

            <b>DRINKS & FOOD & NETWORK</b><br>

            <b>10:00 PM - 12:00 PM:</b> Pit Stop Sessions. Tell us about your startup in 5 minute, personal face-to-face chats with the founders of the BMW Startup Garage. Please register using the Eventbrite contact form. Let us know your name, company name and how your startup &apos;s technology, product or service boosts the mobility experience.',
        'button' => 'Register',
        'booking_link' => 'https://www.eventbrite.com/e/bmw-startup-garage-biergarten-bits-pretzels-after-hour-tickets-26948711321'
    ],
	[
        'premium' => 'yes',
        'headline' => 'Media Startup Night',
        'main_image' => 'media-lab-bayern.jpg',
        'info' => 'Registration necessary - Free for BP attendees',
        'adress' => 'Brienner Straße 45, 80333 Munich',
        'location' => 'Media and Startup people, come together for a night of drinks, trends and innovation in media!<br>
        Media Lab Bayern and Mediaplus invite you to an awesome networking night with the most forward-thinking people from the media industry and startup scene.<br>
        Whether you &apos;re an editor-in-chief or a media founder, a publisher, broadcaster, startup or media enthusiast - this night is for you. Get to know each other and your ideas around innovative content and technology in media.<br><br>
        We provide drinks, food and some special surprises. Let &apos;s have some fun!',
        'button' => 'Register',
        'booking_link' => 'https://www.eventbrite.de/e/media-startup-night-bits-pretzels-tickets-27171299087'
    ],
    [
        'premium' => 'yes',
        'headline' => 'Wirecard Coding Dojo Session',
        'main_image' => 'wirecard.jpg',
        'info' => 'Application is needed - Free for B&P attendees',
        'adress' => 'Einsteinring 35, 85609 Aschheim',
        'location' => '<p><b>Invitation to the
            Wirecard Coding Dojo Session</b><o:p></o:p></p>
            <p>&nbsp;<o:p></o:p></p>
            <p>Hello Ladies and
            Gentlemen, <o:p></o:p></p>
            <p><o:p>&nbsp;</o:p></p>
            <p>Wirecard, the leading
            specialist for payment processing and issuing, is happy to invite you to its
            first-ever coding dojo session on the 26<sup>th</sup> of September, as a side
            event of Bits &amp; Pretzels.<o:p></o:p></p>
            <p>&nbsp;<o:p></o:p></p>
            <p><b>What is a coding
            dojo?</b><o:p></o:p></p>
            <p>A coding dojo is a
            meeting where a bunch of coders get together to work on a programming
            challenge. They are there to have fun and to engage in <span lang="DE"><a href="http://codingdojo.org/cgi-bin/index.pl?DeliberatePractice"><span lang="EN-US" style="color:#0070C0;mso-ansi-language:EN-US">Deliberate Practice</span></a></span><span style="color:#0070C0;mso-ansi-language:EN-US"> </span>in order to improve their skills. The <span lang="DE"><a href="http://codingdojo.org/cgi-bin/index.pl?RandoriKata"><span lang="EN-US" style="color:windowtext;mso-ansi-language:EN-US">RandoriKata</span></a></span><span class="MsoHyperlink"> </span>focuses on coding in front of others, most
            often something from scratch, in a very short amount of time (1 to 1.5 hours).
            They use various languages, various tools, various exercise formats. They
            consider the outcome of an exercise successful when it is completed within the allocated
            time AND the audience can repeat the exercise at home by themselves.<o:p></o:p></p>
            <p>&nbsp;<o:p></o:p></p>
            <p><b>What does the
            agenda look like?</b><o:p></o:p></p>
            <p>If you are one of the
            25 coders to register for the Wirecard coding dojo session, a shuttle bus will
            pick you up in front of the Bits &amp; Pretzels building and bring you to
            Wirecard Headquarters in Aschheim. Once you have arrived, we ask you to please
            register at the front desk for the session. Don’t worry, snacks and drinks will
            be offered ;-)&nbsp; <o:p></o:p></p>
            <p>&nbsp;<o:p></o:p></p>
            <p><b>Agenda: </b><o:p></o:p></p>
            <p>7:15pm: Departure from
            Bits &amp; Pretzels (in front of the Bits &amp; Pretzels building) to Wirecard
            Headquarters<o:p></o:p></p>
            <p>7:30pm: Welcome to
            Wirecard (registration)<o:p></o:p></p>
            <p>8:00pm: Coding dojo
            begins (snacks &amp; drinks)<o:p></o:p></p>
            <p>9:30pm: Shuttle bus
            will bring you back to Bits &amp; Pretzels <o:p></o:p></p>
            <p>&nbsp;<o:p></o:p></p>
            <p><b>What do I need
            to participate?</b><o:p></o:p></p>
            <p></span>Register/Apply
            as soon as possible:<o:p></o:p></p>
          
            </span></span>We have
            allotted 25 spots for Bits &amp; Pretzels participants.<o:p></o:p></p>
            <p>
            <p>- Bring your own laptop</p>
            <p>- Technical requirements:</p>
            <p>An IDE, Intellij Community Edition or Eclipse Mars are strongly recommended</p>
            <p>Java 8</p>
            <p>A <a href="http://codingdojo.org/cgi-bin/index.pl?DeliberatePractice"><span lang="EN-US" style="color:#0070C0;mso-ansi-language:EN-US">Bitbucket Account</span></a></p>
            <p>As soon as you are successfully registered, you will receive a confirmation email from us with the required software for the session, as well as further details.</p><br>
            <p><b>The problem will be a surprise – looking forward to seeing you all there!</b></p><br>
            
            <br><br>
            </span></span>',
        'button' => 'Apply Now',
        'booking_link' => 'mailto:heidi.thiede@wirecard.com?subject=I would like to attend the Wirecard Dojo Session'
    ],
	[
        'premium' => 'yes',
        'headline' => 'Pokernight @ Bits & Pretzels',
        'main_image' => 'gruender_poker_event.png',
        'info' => 'Application is needed - Free for B&P attendees',
        'adress' => ' Grafinger Str. 6, 81671 München',
        'location' => 'One of Germany’s exclusive networking formats meets Munich’s leading space for digital entrepreneurs: Gründerpokern at WERK1 Café<br><br>
            Come and join an exclusive crowd of entrepreneurs and innovative thinkers. Play poker and get a chance to win some of the great prices offered to the winners.<br>
            Doesn’t matter if you are a beginner or connoisseur of the game, you will get the chance to have a new networking experience with all participants accompanied by great food, drink and music.<br>
            You can join, either as a poker player by choosing the poker ticket, or just to network through registering for the networking pass.<br><br>
            We hope to poker with you soon!',
        'button' => 'Apply Now',
        'booking_link' => 'mailto:pokernight@werk1.com?subject=I would like to attend the Pokernight at Werk1'
    ],
	
	
	/* Side Events */
	[
        'premium' => 'no',
        'headline' => '7. IHK Crowdfunding Night@WERK1',
        'main_image' => 'side_events_ihk.jpg',
        'info' => 'Registration necessary - Free for B&P attendees',
        'adress' => 'Grafinger Straße 6, 81671 Munich',
        'location' => '2. Stockwerk',
        'button' => 'Register',
        'booking_link' => 'https://www.ihk-muenchen.de/veranstaltung/detail.jsp?id=10907&ccache=Y'
    ],
    [
        'premium' => 'no',
        'headline' => '48forward - After-Hour at Bits & Pretzels',
        'main_image' => 'afterhour-eventpic.jpg',
        'info' => 'Registration necessary - Prices: €13.15 – €19.43',
        'adress' => '',
        'location' => '',
        'button' => 'Register',
        'booking_link' => 'http://www.eventbrite.com/e/48forward-after-hour-at-bits-pretzels-tickets-26173744374?aff=bitspretzels'
    ],

	[
        'premium' => 'no',
        'headline' => 'Bits & BBQ @ ABC Venture Gates',
        'main_image' => 'abc_event.png',
        'info' => 'Free & Registration is not mandatory',
        'adress' => 'Konrad-Zuse-Platz 8-9, 81829 Munich',
        'location' => 'Registration necessary',
        'button' => 'Register',
        'booking_link' => 'https://www.eventbrite.com/e/bits-bbq-abc-venture-gates-tickets-27521357120'
    ],
	
	[
        'premium' => 'no',
        'headline' => 'OMBash – Networking',
        'main_image' => 'ombusi.png',
        'info' => 'Ticket necessary: 30€ instead of 39€',
        'adress' => '',
        'location' => 'Coupon Code: bits16',
        'button' => 'Register',
        'booking_link' => 'http://www.ombash.com/teilnehmer/anmeldung'
    ],
    [
        'premium' => 'no',
        'headline' => 'IXDS labs - OktoberPITCH',
        'main_image' => 'oktob_pitchmin.png',
        'info' => 'Registration is not mandatory',
        'adress' => 'Ganghoferstrasse 68b, 80339 Munich',
        'location' => '',
        'button' => 'Register',
        'booking_link' => 'https://www.eventbrite.co.uk/e/oktoberpitch-tickets-26803127877'
    ],
    [
        'premium' => 'no',
        'headline' => 'Exclusive Bits & Pretzels Blockchain After Party',
        'main_image' => 'blockchain_afterparty.png',
        'info' => 'Registration is not mandatory',
        'adress' => 'Theresienhöhe 15, 80339 Munich',
        'location' => '',
        'button' => 'Register',
        'booking_link' => 'https://www.meetup.com/de-DE/BitShares-Munich/events/234116257/'
    ],

];

?>