<?php

$academy_2016 = [
    [
        'headline' => '<strong>Masterclass 1:</strong>  How to Start a Startup',
        'speaker_image' =>'hadowski_lukasz.png',
        'description' => 'From ideation to execution ',
        'description_more' =>'Lukasz is the co-founder of Team Europe, Spreadshirt, and Point Nine Capital (early stage VC). Through Team Europe, he is now an extremely active venture capitalist in the German start-up scene, and currently supports more than 15 portfolio companies including Delivery Hero Sponsorpay, MisterSpex just to name a few.',
        'speaker' => 'Lukasz Gadowski'
    ],
    [
        'headline' => '<strong>Masterclass 2:</strong>  How to master legal basics',
        'speaker_image' =>'fritz_ines.png',
        'description' => 'Best practices about the set-up of your start-up and the ten biggest mistakes made and challenges facing you in practice',
        'description_more' =>'Ines is Executive Director of Ernst & Young Law and recommended lawyer for Venture Capital since 2010. She has extensive experience in advising founders and start-ups on all question relating to the set-up of a company and the creation of a professional legal structure eligible for future funding. Furthermore, Ines led various financing rounds and advised on the drafting and negotiation of the investment documentation as well as she is experienced in structuring and implementing employee participations programs.',
        'speaker' => 'Dr. Ines Fritz'
    ],
	[
        'headline' => '<strong>Masterclass 3:</strong>  How to approach the big players',
        'speaker_image' =>'wittrock_max.png',
        'description' => 'Find out how to build longlasting corporate-startup partnerships',
        'description_more' =>'In 2007 the trained journalist and lawyer founded mymuesli with two fellow students and with a float of only 3500 euro. Now the company employs over 150 staff members and are counted among the most successsful startups in Germany.',
        'speaker' => 'Max Wittrock'
    ],
    [
        'headline' => '<strong>Masterclass 4:</strong>  How to not screw up your technology',
        'speaker_image' =>'jankowfsky_lars.png',
        'description' => 'Find the right CTO and get your dev power on the street',
        'description_more' =>'Lars is the co-founder and CTO of OXID eSales and also of swoodoo AG which he successfully sold to kayak.com. At the moment he is focused on NFQ.com – EMEA´s leading software & product company with a focus on high growth startups. He helps tech startups around the world to scale - mostly by creating and scaling powerful tech teams. Lars also provide interim management for high-tech companies as a ‘turn-around’ CTO with a focus on solving growth and scaling challenges. ',
        'speaker' => 'Lars Jankowfsky'
    ],
    [
        'headline' => '<strong>Masterclass 5:</strong>  How to raise money',
        'speaker_image' =>'england_claire.png',
        'description' => 'Investors vs. bootstrapping vs. crowdfunding – find your way to finance your startup',
        'description_more' =>'Claire England is Executive Director of Central Texas Angel Network (CTAN), the most active angel investing organization in the United States, having invested over $70 million USD in more than 130 startups since 2006. She worked on local and international startup ecosystem projects for clients including SXSW Interactive, Tech Ranch, and St. Edward’s University before joining CTAN. She is also judging pitch competitions at SXSW Festival, the international Rice Business Plan Competition, and many more.',
        'speaker' => 'Claire England'
    ],
    [
        'headline' => '<strong>Masterclass 6:</strong> How to promote your product and make people love it',
        'speaker_image' =>'patel_neil.png',
        'description' => 'Insights into mindblowing marketing & PR strategies',
        'description_more' =>'Neil is a successful serial entrepreneur and online marketer. Entrepreneur.com ranked him the number 1 marketer in the world and among the 100 most innovative companies. He is the co-founder of both KISSMetrics, a customer intelligence and web analytics company and Crazy Egg, a tool for analyzing the behavior of your site visitors. Currently, he helps companies with customer acquisition. Among those companies are Google and General Motors as well as several other massive corporations.',
        'speaker' => 'Neil Patel'
    ],
    [
        'headline' => '<strong>Masterclass 7:</strong>  How to recruit an awesome team',
        'speaker_image' =>'gschwandtner_florian.png',
        'description' => 'Hire, develop & retain the best possible staff',
        'description_more' =>'Florian is the co-founder and CEO of Runtastic, an Austrian company which is specialized in the development of hard- and software products for the fitness sector. He also works as an advisor for the non-profit platform AustrianStartups and is a board member of Tractive GmbH. Lars focuses on mobile technologies & apps, entrepreneurship, building products, communication and start-ups.',
        'speaker' => 'Florian Gschwandtner'
    ],
	[
        'headline' => '<strong>Masterclass 8:</strong>  How to master KPI driven growth',
        'speaker_image' =>'visage_renaud.png',
        'description' => 'Different approaches to scale growing',
        'description_more' =>'Renaud is the co-founder and CTO of Eventbrite, a global marketplace for live experiences that allows people to find and create events. He anticipated several fundamental shifts in the way we use the internet – including the arrival of social media, big data analysis, the shift to mobile devices as first screens and the ubiquity of APIs– and leveraged their disruptive power to Eventbrite’s advantage, fuelling the company’s growth into the globally leading self-service ticketing platform it is today. In 2014, Renaud was included in Wired UKs Top 100 digital influencers in Europe. When he is not writing code, he’s mentoring and investing in startups in several European countries.',
        'speaker' => 'Renaud Visage'
    ],
	[
        'headline' => '<strong>Masterclass 9:</strong>  How to deal with challenges',
        'speaker_image' =>'hubertz_heiko.png',
        'description' => 'All about organizational change & leadership',
        'description_more' =>'Heiko is the founder and managing director of the Hamburg-based Bigpoint GmbH. Today, the company is the world’s largest portal for browser games and counts among the top 3 gaming portals in the world with 240 employees from 20 nations who develop and operate the games to be on the cutting-edge of technology.',
        'speaker' => 'Heiko Hubertz'
    ],
    [
        'headline' => '<strong>Masterclass 10:</strong>  How to go global',
        'speaker_image' =>'ahlvarsson_ola.png',
        'description' => 'Learn proven strategies for internationalization',
        'description_more' =>'Ola is the co-founder and chairman of Result, a global system for corporate growth which helps companies accelerate success through expansion and innovation. He has founded over 20 companies and has been working with and managing leading internet ventures ever since the early days with Spray, Boxman, Blocket, Letsbuyit, SIME to later ventures like FON, Best Secret, Blocket, Epicenter and many more. Ola has a background as an entrepreneur working as an Internet strategist, business accelerator and a managerial advisor for Fortune 500. He is also a frequent speaker in conferences and industry events (for example Le Web Paris and DLD) and has recieved numerous recongition and awards including Wired Magazines top technology thinkers 2015',
        'speaker' => 'Ola Ahlvarsson'
    ],
	[
        'headline' => '<strong>Masterclass 11:</strong>  How to become great at selling',
        'speaker_image' =>'maschmeyer_carsten.png',
        'description' => 'Sales strategies for startups',
        'description_more' =>'Carsten Maschmeyer works as entrepreneur, investor, consultant, author and is juror in the famous TV-show „Die Höhle der Löwen“. The Maschmeyer Group comprises all his investment activities. Together with the early-stage investor seed&speed and the investment company ALSTIN he supports startups from different sectors. Carsten Maschmeyer is moreover inventor of the independant financal consultancy.',
        'speaker' => 'Carsten Maschmeyer'
    ],

];
?>