<?php

$investor_stage_2017 = [
    [
        'headline' => '<strong>Company 1:</strong> Home24',
        'speaker_image' =>'Philip_Kreibohm.png',
        'description' => '',
        'description_more' =>'',
        'speaker' => 'Speaker: Dr. Philipp Kreibohm'
    ],
    [
        'headline' => '<strong>Company 2:</strong> Movinga',
        'speaker_image' =>'Finn_Age_Haensel.png',
        'description' => '',
        'description_more' =>'',
        'speaker' => 'Speaker: Finn Age Hänsel'
    ],
    [
        'headline' => '<strong>Company 3:</strong> Dreamlines',
        'speaker_image' =>'Felix_Schneider.png',
        'description' => '',
        'description_more' =>'',
        'speaker' => 'Speaker: Felix Schneider'
    ],
    [
        'headline' => '<strong>Company 4:</strong> Book a tiger',
        'speaker_image' =>'Nikita_Fahrenhorst.png',
        'description' => '',
        'description_more' =>'',
        'speaker' => 'Speaker: Nikita Fahrenholz'
    ],
    [
        'headline' => '<strong>Company 5:</strong> Whitebox',
        'speaker_image' =>'Salome_Preiswerk.png',
        'description' => '',
        'description_more' =>'',
        'speaker' => 'Speaker: Salome Preiswerk'
    ],
    [
        'headline' => '<strong>Company 6:</strong>  Blackbill',
        'speaker_image' =>'sebastian_diemer.png',
        'description' => '',
        'description_more' =>'',
        'speaker' => 'Speaker: Sebastian Diemer'
    ],
    [
        'headline' => '<strong>Company 7:</strong>   Airhelp',
        'speaker_image' =>'user_placeholder.png',
        'description' => '',
        'description_more' =>'',
        'speaker' => 'Speaker: Henrik Zillmer'
    ],
    [
        'headline' => '<strong>Company 8:</strong>  Shopgate',
        'speaker_image' =>'Andrea_Anderheggen.png',
        'description' => '',
        'description_more' =>'',
        'speaker' => 'Speaker: Andrea Anderheggen'
    ],
    [
        'headline' => '<strong>Company 9:</strong>  OptioPay',
        'speaker_image' =>'Marcus_Boerner.png',
        'description' => '',
        'description_more' =>'',
        'speaker' => 'Speaker: Marcus Börner'
    ],
    [
        'headline' => '<strong>Company 10:</strong>  Signavio',
        'speaker_image' =>'gero_decker.png',
        'description' => '',
        'description_more' =>'',
        'speaker' => 'Speaker: Gero Decker'
    ],
    [
        'headline' => '<strong>Company 11:</strong>  ePages',
        'speaker_image' =>'Wilfried_Beeck.png',
        'description' => '',
        'description_more' =>'',
        'speaker' => 'Speaker: Wilfried Beeck'
    ],
    [
        'headline' => '<strong>Company 12:</strong>  Tis (Treasury Intelligence  Solutions)',
        'speaker_image' =>'joerg_wiemer.png',
        'description' => '',
        'description_more' =>'',
        'speaker' => 'Speaker: Jörg Wiemer'
    ],
    [
        'headline' => '<strong>Company 13:</strong>  IDnow',
        'speaker_image' =>'Sebastian_Baerhold.jpg',
        'description' => '',
        'description_more' =>'',
        'speaker' => 'Speaker: Sebastian Bärhold'
    ],
    [
        'headline' => '<strong>Company 14:</strong>  Open-Xchange',
        'speaker_image' =>'Rafael_Laguna_de_la_Vera.png',
        'description' => '',
        'description_more' =>'',
        'speaker' => 'Speaker: Rafael Laguna de la Vera'
    ],
    [
        'headline' => '<strong>Company 15:</strong>  esome advertising technologies',
        'speaker_image' =>'user_placeholder.png',
        'description' => '',
        'description_more' =>'',
        'speaker' => 'Speaker: Falk Bielesch'
    ],
    [
        'headline' => '<strong>Company 16:</strong>  Proven Expert',
        'speaker_image' =>'Remo_Fyda.png',
        'description' => '',
        'description_more' =>'',
        'speaker' => 'Speaker: Remo Fyda'
    ],
    [
        'headline' => '<strong>Host:</strong>  Julian Riedlbauer',
        'speaker_image' =>'Julian_Riedlbauer.png',
        'description' => '',
        'description_more' =>'',
        'speaker' => 'Company: GP Bullhound'
    ],
    

];
?>