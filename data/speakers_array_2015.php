<?php 
$speakers_2015 = array(array( 
        'name' => 'Phil Libin',
        'track' => 'main',
        'title' => 'Co-founder &amp; Executive Chairman',
        'company' => 'Evernote',
        'image' => 'phil_libin.png',
    ),array( 
        'name' => 'Herbert Hainer',
        'track' => 'main',
        'title' => 'CEO',
        'company' => 'Adidas-Group',
        'image' => 'herbert_hainer.png',
    ),array( 
        'name' => 'Joko Winterscheidt',
        'track' => 'main',
        'title' => 'Co-Founder',
        'company' => 'GoButler',
        'image' => 'joko_winterscheidt.png',
    ),array( 
        'name' => 'Navid Hadzaad',
        'track' => 'main',
        'title' => 'Founder &amp; CEO',
        'company' => 'GoButler',
        'image' => 'navid_hazaad.png',
    ),
    array( 
        'name' => 'Reshma Sohoni',
        'track' => 'investor',
        'title' => 'Co-Founder &amp; Partner',
        'company' => 'Seedcamp',
        'image' => 'sohoni.png',
    ),    
    array( 
        'name' => 'Niklas Oestberg',
        'track' => 'main',
        'title' => 'Co-Founder &amp; CEO',
        'company' => 'Delivery Hero',
        'image' => 'oestberg.png',
    ),array( 
        'name' => 'Peter Arvai',
        'track' => 'main',
        'title' => 'Co-Founder &amp; CEO',
        'company' => 'Prezi',
        'image' => 'arvai.png',
    ),array( 
        'name' => 'Gil Penchina',
        'track' => 'main',
        'title' => 'Angel investor',
        'company' => '',
        'image' => 'penchina.png',
    ),array( 
        'name' => 'Mikkel Svane',
        'track' => 'main',
        'title' => 'Co-Founder &amp; CEO',
        'company' => 'Zendesk',
        'image' => 'svane.png',
    ),array( 
        'name' => 'Anna Alex',
        'track' => 'main',
        'title' => 'Founder and MD',
        'company' => 'Outfittery',
        'image' => 'anna_alex.png',
    ),array( 
        'name' => 'Marvin Liao',
        'track' => 'main',
        'title' => 'Partner',
        'company' => '500Startups',
        'image' => 'marvin_liao.png',
    ),array( 
        'name' => 'Steffi Czerny',
        'track' => 'main',
        'title' => 'Managing Director',
        'company' => 'DLD &amp; Member of the HVB women&rsquo;s council',
        'image' => 'steffi_czerny.png',
    ),array( 
        'name' => 'Stefan Mennerich',
        'track' => 'main',
        'title' => 'Director Media Rights, New Media and IT',
        'company' => 'FC Bayern M&uuml;nchen',
        'image' => 'stefan_mennerich.png',
    ),array( 
        'name' => 'Florian Gschwandtner',
        'track' => 'main',
        'title' => 'Co-Founder &amp; CEO',
        'company' => 'Runtastic',
        'image' => 'gschwandtner.png',
    ),array( 
        'name' => 'Pieter van der Does',
        'track' => 'main',
        'title' => 'Co-Founder &amp; CEO',
        'company' => 'Adyen',
        'image' => 'does.png',
    ),array( 
        'name' => 'Aleyda Solis',
        'track' => 'online-marketing',
        'title' => 'Founder',
        'company' => 'Orainti',
        'image' => 'solis.png',
    ),array( 
        'name' => 'Ben Parr',
        'track' => 'main',
        'title' => 'Managing Partner',
        'company' => 'DominateFund',
        'image' => 'parr.png',
    ),array( 
        'name' => 'Sam Shank',
        'track' => 'main',
        'title' => 'Co-Founder &amp; CEO',
        'company' => 'HotelTonight',
        'image' => 'shank.png',
    ),array( 
        'name' => 'Joe Green',
        'track' => 'main',
        'title' => 'Founder &amp; President',
        'company' => 'FWD.us',
        'image' => 'joe_green.png',
    ),
    array( 
        'name' => 'Sitar Teli',
        'track' => 'investor',
        'title' => 'Managing Partner',
        'company' => 'Connect Ventures',
        'image' => 'sitar_teli.png',
    ),

   array( 
        'name' => 'Katharina Klausberger',
        'track' => 'rising-stars',
        'title' => 'Co-Founder',
        'company' => 'Shpock',
        'image' => 'katharina_klausberger.png',
    ), 

   array( 
        'name' => 'Mengting Gao',
        'track' => 'rising-stars',
        'title' => 'Founder & Managing Director',
        'company' => 'Kitchen Stories',
        'image' => 'mengting_gao.png',
    ),
                                                 
   array( 
        'name' => 'Ray Chan',
        'track' => 'main',
        'title' => 'Founder & CEO',
        'company' => '9GAG',
        'image' => 'ray_chan.jpg',
    ),

   array( 
        'name' => 'Colette Ballou',
        'track' => 'main',
        'title' => 'President of UK, France & Germany',
        'company' => 'Ballou PR',
        'image' => 'colette_ballou.jpg',
    ),

   array( 
        'name' => 'Avery Wang',
        'track' => 'main',
        'title' => 'Founder and Chief Scientist',
        'company' => 'Shazam',
        'image' => 'avery_wang.jpg',
    ),

    );