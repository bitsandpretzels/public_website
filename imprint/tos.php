<?php
// local fix to match my vhost settings
$pathPrefix = '';
$host = $_SERVER['HTTP_HOST'];
if ($host == 'bap.local') {
  $pathPrefix = '../';
}
include_once realpath($pathPrefix.'layout/top.php');
include_once realpath($pathPrefix.'layout/header.php');
?>

<div class="inner_sections">

  <?php include_once realpath($pathPrefix.'layout/_inner_pages_main_nav.php');?>

  <!--
          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
          !! The sub menus have a different arrangement of pages !!
          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
   -->

  <div id="sub_nav" class="sub_nav_wrap">
    <div class="wrap_subnav_inner">
      <div class="title_section">T&amp;C</div>
    </div>
  </div>

  <section class="section silver active section_1 section_1_adjust">
    <div class="content_section center">
      <div class="wrapper_inner">
        <div class="valign">
          <div class="wrapp_main_text">
            <h1 class="main_title">General Terms and Conditions</h1>
          </div>
          <div class="tos-box">
            <h2 class="center">IMPORTANT! PLEASE NOTE:</h2><br>
			You agree to the <a class="link" href="#german-tos">German General Terms and Conditions</a> which have been translated to English for your convenience only. The German General Terms and Conditions are binding.
		  </div>
          <div class="grid clearfix white imprint_wrapper">
            <div class="valign">
              <div class="middle">
                    <div class="imprint">
                          <h2>Scope, object and formation of the contract</h2>
                          <p>The following General Terms and Conditions (T&amp;Cs) shall exclusively govern the contractual relationship between Startup Events UG (limited liability), represented by its Directors Andreas Bruckschlögl, Dr Bernd Storm van's Gravesande and Felix Haas, Gärtnerplatz 5, 80469 Munich, hereinafter referred to as the "Organiser", and the customer. <br><br>
                          These T&amp;Cs shall apply exclusively. Terms and conditions of the customer which contradict with or depart from these T&amp;Cs shall not be deemed incorporated into the contract, unless the Organiser has expressly agreed their applicability in an individual case. <br><br>
                          These T&amp;Cs shall apply exclusively to entrepreneurs. As defined by these T&amp;Cs, an entrepreneur is any natural person or legal entity or Partnership with legal personality who or which, when placing an order, acts in exercise of a trade, business or profession. <br><br>
                          The object of the contract shall be the sale of tickets by the Organiser to the customer for the specific Bits & Pretzels event advertised in each case. <br><br>
                          The customer may buy tickets for the event from the Organiser's website. A valid contract is formed between the parties at such time as the order process is completed and full payment for the tickets is made. <br><br>
                          Formation of contract takes place in German only. German law shall apply, to the extent the customer is an entrepreneur. The German language version of these T&amp;Cs shall always control in the event of ambiguities</p>
                    </div>
                    <div class="imprint">
                          <h2>Performance of the contract</h2>
                          <p>The purchase price shall fall due for payment at such time as the tickets are ordered. The customer shall choose between different payment methods. Tickets shall be deemed merely reserved until such time as the customer has paid the purchase price in full. The reservation shall lapse at the end of 14 days from the time of the order. <br><br>
                          Following receipt of payment in full by the Organiser, the Organiser shall forward the tickets to the customer at his specified email address. The customer shall receive no additional tickets by post or fax. The customer shall be solely responsible for safe-guarding the tickets and for bringing them with him to the event. In the event that the customer has purchased concession rate tickets (e.g. Early Stage Startup, Later Stage Startup), the customer shall produce the necessary proof of eligibility when entering the event.</p>
                    </div>
                    <div class="imprint">
                          <h2>Rights and obligations</h2>
                          <p>The Organiser shall have the right to alter the content, running and locations of events at short notice. The Organiser shall source an equivalent substitute, if possible. This shall not give rise to any right on the part of the customer to rescind the contract and/or cancel the tickets. <br><br>
                          The customer shall assure that the person, which will be using the ticket (e.g. Early Stage Startup ticket), fulfils the requirements to do so. In case the person is not fulfilling the requirements, a right to participate will not be purchased and entrance will be refused. <br><br>
                          Name changes will be facilitated up to 7 days prior to the commencement of the event. The new ticket user has to fulfil the requirements in 3.2. No name changes will be facilitated 7 days prior to the commencement of the event. <br><br>
                          The parties agree that Bits &amp; Pretzels is a global networking event, which lives on the names of its participants. Appropriate to this fact the organiser is allowed to publish the information provided by the customer or to forward this data to third parties.</p>
                    </div>
                    <div class="imprint">
                          <h2>Cancellation by the Organiser</h2>
                          <p>The Organiser shall be released from its duty to perform the contract in the event of force majeure. Force majeure shall be deemed to include all unforeseen events and events as to which neither party bears responsibility for the effects thereof on the performance of the contract. In the event of cancellation, any fees already paid shall be refunded; liability for any further claims is hereby disclaimed, except in cases of intentional acts or omissions or gross negligence.</p>
                    </div>
                    <div class="imprint">
                          <h2>Photographs and recordings</h2>
                          <p>The customer hereby expressly confirms his agreement that the Organiser shall have the right to take photographs or make recordings of the customer before and during the entire event, and to publish, disseminate and circulate the same on the Internet, in print media and other media.</p>
                    </div>
                    <div class="imprint">
                          <h2>Final provisions</h2>
                          <p>
                             These T&amp;Cs and contracts concluded based on these T&amp;Cs shall be governed exclusively by German law unless the customer is a consumer. <br><br>
                            Where the parties are general merchants [Vollkaufleute], jurisdiction and venue for disputes arising from or in connection with this contract shall be vested in the competent court located within the city where the Organiser has its registered office. <br><br>
                            Should one or more provisions of these T&amp;Cs be deemed invalid in whole or in part, this shall not affect the validity of the remaining provisions hereof.</p>
                    </div>
                    <a id="german-tos"></a>
              </div>              
            </div>
          </div>

          <div class="wrapp_main_text">
            <h1 class="main_title">Allgemeine Geschäftsbedingungen</h1>
          </div>
          <div class="grid clearfix white imprint_wrapper">
            <div class="valign">
              <div class="middle">
                    <div class="imprint">
                          <h2>§ 1 Geltungsbereich, Gegenstand und Abschluss des jeweiligen Vertrages</h2>
                          <p>Die folgenden Bedingungen regeln abschließend das Vertragsverhältnis zwischen Startup Events UG (haftungsbeschränkt), vertreten durch die Geschäftsführer Andreas Bruckschlögl, Dr. Bernd Storm van's Gravesande und Felix Haas, Gärtnerplatz 5, 80469 München, im Folgenden „Veranstalter“ genannt und dem jeweiligen Kunden. <br><br>
                          Diese Allgemeinen Geschäftsbedingungen gelten ausschließlich. Entgegenstehende oder von diesen Geschäftsbedingungen abweichende Bedingungen des Kunden werden nicht anerkannt, es sei denn, der Veranstalter hat diesen im Einzelfall ausdrücklich zugestimmt.<br><br>
                          Sie gelten ausschließlich gegenüber Unternehmern. Unternehmer im Sinne dieser AGB ist eine natürliche oder juristische Person oder eine rechtsfähige Personengesellschaft, die bei der Bestellung in Ausübung ihrer gewerblichen oder selbständigen beruflichen Tätigkeit handelt.<br><br>
                          Gegenstand des jeweiligen Vertrages ist der Ticketverkauf vom Veranstalter an den Kunden für die jeweils aktuell angebotene Veranstaltung Bits &amp; Pretzels.<br><br>
                          Der Kunde kann über die Website des Veranstalters einen Kauf von Tickets für die jeweilige Veranstaltung vornehmen. Mit dem Abschluss des Bestellvorgangs und der vollständigen Bezahlung der Tickets kommt ein wirksamer Vertrag zwischen den Parteien zustande.<br><br>
                          Der Vertragsschluss findet ausschließlich in deutscher Sprache statt. Es ist deutsches Recht anwendbar, soweit der Kunde Unternehmer ist. Bei Auslegungsschwierigkeiten gelten immer diese deutschsprachigen AGB.</p>
                    </div>
                    <div class="imprint">
                          <h2>§ 2 Abwicklung des Vertrages</h2>
                          <p>Mit der Bestellung der Tickets wird die Zahlung des Kaufpreises fällig. Der Kunde kann zwischen verschiedenen Zahlungsvarianten wählen. Bis zur vollständigen Bezahlung des Kaufpreises ist das Ticket für den Kunden nur reserviert. Nach Ablauf von 14 Tagen ab dem Bestellungszeitpunkt verfällt die Reservierung wieder. <br><br>
                          Im Anschluss an dem Eingang der vollständigen Bezahlung bei dem Veranstalter erhält der Kunde die Tickets an die von ihm angegebene Email-Adresse übersandt. Der Kunde erhält keine weiteren Tickets per Post oder Fax. Er ist selbst für die Aufbewahrung und Mitführung der Tickets zur Veranstaltung verantwortlich. Der Kunde ist bei der Inanspruchnahme von vergünstigten Tickets (z.B. Early Stage Startup, Later Stage Startup) ist der Kunde verpflichtet, die entsprechenden Nachweise bei den Zugangskontrollen zur Veranstaltung vorzulegen.</p>
                    </div>
                    <div class="imprint">
                          <h2>§ 3 Rechte und Pflichten</h2>
                          <p>Der Veranstalter hat das Recht, den Ablauf, den Inhalt und die Orte der jeweiligen Veranstaltungen kurzfristig zu ändern. Der Veranstalter wird soweit möglich für gleichwertigen Ersatz sorgen. Hieraus ergibt sich für den Kunden kein Recht zum Rücktritt und/oder zur Stornierung seiner Tickets. <br><br>
                          Der Erwerber versichert, dass die Person, für die er das Ticket erwirbt, tatsächlich die Anforderungen (z.B. Early Stage Startup) an das erworbene Ticket erfüllt. Für den Fall, dass die persönlichen Anforderungen nicht zu der Art des Tickets passen, wird kein Teilnahmerecht an der Veranstaltung erworben, weswegen der Einlass verweigert werden wird.<br><br>
                          Der Erwerber ist berechtigt die Namen, auf die die von ihm gekauften Tickets ausgestellt sind, bis 7 Tage vor der Veranstaltung zu ändern, insofern die Nutzer die Bedingungen aus §3 Abs. 2 erfüllen. Mit Ablaufen dieser Frist ist eine Änderung der Namen nicht mehr möglich.<br><br>
                          Die Parteien sind sich darüber im Klaren, dass die Veranstaltung ein globales Networking-Event ist, welches auch von den Namen der Teilnehmer lebt. Entsprechend ist der Veranstalter berechtigt, die vom Teilnehmer zur Verfügung gestellten Daten zu veröffentlichen und/oder an Dritte weiter zu geben.</p>
                    </div>
                    <div class="imprint">
                          <h2>§ 4 Absage, Stornierung durch den Veranstalter</h2>
                          <p>Der Veranstalter ist von der Leistungspflicht in Fällen höherer Gewalt befreit. Als höhere Gewalt gelten alle unvorhergesehenen Ereignisse sowie solche Ereignisse, deren Auswirkungen auf die Vertragserfüllung von keiner Partei zu vertreten sind. Bei Absage, werden bereits bezahlte Gebühren erstattet; weitergehende Ansprüche sind ausgeschlossen, außer in Fällen vorsätzlichen oder grob fahrlässigen Verhaltens.</p>
                    </div>
                    <div class="imprint">
                          <h2>§ 5 Fotos und Aufzeichnungen</h2>
                          <p>Die Kunden erklären sich ausdrücklich damit einverstanden, dass vor und während der gesamten Veranstaltungen Fotos und Aufnahmen von ihm gemacht und von dem Veranstalter im Internet, den Printmedien und anderen Medien veröffentlicht, verbreitet und weitergegeben werden dürfen.</p>
                    </div>
                    <div class="imprint">
                          <h2>§ 6 Schlussbestimmungen</h2>
                          <p>
                             Auf die vorliegenden Allgemeinen Geschäftsbedingungen und auf den jeweils geschlossenen Kaufvertrag ist ausschließlich deutsches Recht anwendbar, wenn der Kunde kein Verbraucher ist.<br><br>
                             Sofern die Parteien Vollkaufleute sind, wird für alle Streitigkeiten, die sich aus oder im Zusammenhang mit dem vorliegenden Vertrag ergeben, die Stadt des Sitzes des Veranstalters als Gerichtsstand vereinbart.<br><br>
                             Sollten eine oder mehrere Klauseln dieser Geschäftsbedingungen ganz oder teilweise unwirksam sein, so soll hierdurch die Gültigkeit der übrigen Bestimmungen nicht berührt sein.</p>
                    </div>
              </div>
            </div>
          </div>


        </div>
      </div>
    </div>
  </section>
</div>

<?php
include realpath($pathPrefix.'layout/footer.php');
include realpath($pathPrefix.'layout/bottom.php');
