<?php
// local fix to match my vhost settings
$pathPrefix = '';
$host = $_SERVER['HTTP_HOST'];
if ($host == 'bap.local') {
  $pathPrefix = '../';
}
include_once realpath($pathPrefix.'layout/top.php');
include_once realpath($pathPrefix.'layout/header.php');
?>

<div class="inner_sections">

  <?php include_once realpath($pathPrefix.'layout/_inner_pages_main_nav.php');?>

  <!--
          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
          !! The sub menus have a different arrangement of pages !!
          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
   -->

  <div id="sub_nav" class="sub_nav_wrap">
    <div class="wrap_subnav_inner">
      <div class="title_section">Students in Tech</div>
    </div>
  </div>

  <section class="section silver active section_1 section_1_adjust">
    <div class="content_section center">
      <div class="wrapper_inner">
        <div class="valign">
          <div class="wrapp_main_text">
            <h1 class="main_title">Teilnahmebedingungen</h1>
          </div>

          <div class="grid clearfix white imprint_wrapper">
            <div class="valign">
              <div class="middle">
                    <div class="imprint">
                          <h2>§1 Gewinn</h2>
                          <p>(1) Der Gewinner erhält ein kostenloses Studententicket für die Veranstaltung Bits & Pretzels 2016 (25. – 27. September 2016). Das Ticket berechtigt zur Teilnahme an allen drei Veranstaltungstagen.</p>
                          <br>
                          <p>
                            (2) Der Gewinn ist nicht übertragbar und das Ticket wird namentlich auf den Gewinner ausgestellt. 
                          </p>
                    </div>
                    <div class="imprint">
                          <h2>§ 2 Teilnahme</h2>
                          <p>
                          (1) Teilnahmeberechtigt sind volljährige Personen, die zum Zeitpunkt der Veranstaltung Bits & Pretzels (25. – 27. September 2016) über eine Immatrikulation als Student an einer Hochschule verfügen. Minderjährige bedürfen zu ihrer Teilnahme der Zustimmung ihrer/ihres Erziehungsberechtigten.
                          </p>
                          <br>
                          <p>
                          (2) Eine Person nimmt am Gewinnspiel teil, indem sie das Anmeldeformular ausfüllt und mittels des Buttons „send“ an Startup Events UG abschickt. Der Teilnehmer ist für die Richtigkeit seiner Angaben, insbesondere seiner E-Mail selbst verantwortlich. Der Eingang des Anmeldeformulars hat innerhalb der im Gewinnspiel genannten Frist zu erfolgen. Zur Überprüfung der Fristwahrung dient der elektronisch protokollierte Eingang der E-Mail bei der Startup Events UG (haftungsbeschränkt).
                          </p>
                          <br>
                          <p>
                          (3) Zur Teilnahme am Gewinnspiel ist unbedingt erforderlich, dass sämtliche Personenangaben der Wahrheit entsprechen. Andernfalls kann ein Ausschluss gemäß § 3 (4) erfolgen.
                          </p>
                          <br>
                          <p>
                          (4) Die Teilnahme muss bis zum 30. Juni erfolgen.
                          </p>
                    </div>
                    <div class="imprint">
                          <h2>§ 3 Ausschluss vom Gewinnspiel</h2>
                          <p>(1) Bei einem Verstoß gegen diese Teilnahmebedingungen behält sich Startup Events UG das Recht vor, Personen vom Gewinnspiel auszuschließen.</p>
                          <br><p>(2) Teilnehmer, die bei der Teilnahme keine für den Zeitraum des Events gültige Immatrikulationsbescheinigung hochladen, werden vom Gewinnspiel ausgeschlossen.</p>
                          <br><p>(3) Ausgeschlossen werden auch Personen, die sich unerlaubter Hilfsmittel bedienen oder sich anderweitig durch Manipulation Vorteile verschaffen. Gegebenenfalls können in diesen Fällen auch nachträglich Gewinne aberkannt und zurückgefordert werden.</p> <br>
                          <p>(4) Wer unwahre Personenangaben macht, kann vom Gewinnspiel ausgeschlossen werden
                          </p>
                    </div>
                    <div class="imprint">
                          <h2>§ 4 Durchführung und Abwicklung</h2>
                          <p>
                            (1) Die Gewinner werden nach erfolgter Ziehung per E-Mail über ihren Gewinn benachrichtigt.
                          </p><br>
                          <p>(2) Der Gewinner erhält ein Zugangscode mit dem er seinen Gewinn selbstständig einlösen kann. Wird das kostenlose Ticket nicht innerhalb von 7 Tagen nach dem Absenden der Benachrichtigung bestellt, so verfällt der Anspruch auf den Gewinn. </p><br>
                          <p>(3) Eine Barauszahlung der Gewinne oder eines etwaigen Gewinnersatzes ist in keinem Falle möglich.</p><br>
                          <p>
                            (4) Der Anspruch auf den Gewinn oder Gewinnersatz kann nicht abgetreten werden.
                          </p>
                    </div>
                    <div class="imprint">
                          <h2>§ 5 Vorzeitige Beendigung des Gewinnspiels</h2>
                          <p>Die Startup Events UG behält sich vor, das Gewinnspiel zu jedem Zeitpunkt ohne Vorankündigung und ohne Angabe von Gründen abzubrechen oder zu beenden. </p>
                    </div>
                    <div class="imprint">
                          <h2>§ 6 Datenschutz</h2>
                          <p>
                             Startup Events UG haftet nur für Schäden, welche von der Startup Events UG oder einem seiner Erfüllungsgehilfen vorsätzlich oder grob fahrlässig oder durch die Verletzung von Kardinalspflichten verursacht wurde. Dies gilt nicht für Schäden durch die Verletzung von Leben, Körper und/oder Gesundheit. Voranstehende Haftungsbeschränkung gilt insbesondere für Schäden durch Fehler, Verzögerungen oder Unterbrechungen in der Übermittlung von Daten o. ä., bei Störungen der technischen Anlagen oder des Services, unrichtige Inhalte, Verlust oder Löschung von Daten, Viren.
                          </p>
                    </div>
                    <div class="imprint">
                          <h2>§ 8 Sonstiges</h2>
                          <p>
                            (1) Der Rechtsweg ist ausgeschlossen.
                          </p>
                          <br>
                          <p>
                            (2) Es ist ausschließlich das Recht der Bundesrepublik Deutschland anwendbar.
                          </p>
                          <br>
                          <p>
                            (3) Sollten einzelne dieser Bestimmungen ungültig sein oder werden, bleibt die Gültigkeit der übrigen Nutzungsbedingungen hiervon unberührt.
                          </p>
                          <br>
                          <p>
                            (4) Diese Nutzungsbedingungen können jederzeit von der Startup Events UG ohne gesonderte Benachrichtigung geändert werden.   
                          </p>
                    </div>                    
                    <a id="german-tos"></a>
              </div>              
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>
</div>

<?php
include realpath($pathPrefix.'layout/footer.php');
include realpath($pathPrefix.'layout/bottom.php');
