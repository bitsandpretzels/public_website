<?php
// local fix to match my vhost settings
$pathPrefix = '';
$host = $_SERVER['HTTP_HOST'];
if ($host == 'bap.local') {
  $pathPrefix = '../';
}
include_once realpath($pathPrefix.'layout/top.php');
include_once realpath($pathPrefix.'layout/header.php');
?>

<div class="inner_sections">

  <?php include_once realpath($pathPrefix.'layout/_inner_pages_main_nav.php');?>

  <!--
          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
          !! The sub menus have a different arrangement of pages !!
          \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
   -->

  <div id="sub_nav" class="sub_nav_wrap">
    <div class="wrap_subnav_inner">
      <div class="title_section">Meet Kevin Conditions</div>
    </div>
  </div>

  <section class="section silver active section_1 section_1_adjust">
    <div class="content_section center">
      <div class="wrapper_inner">
        <div class="valign">
          <div class="wrapp_main_text">
            <h1 class="main_title">Conditions of Participation for Competitions/Prize Draws</h1>
          </div>
          <div class="tos-box">
            <h2 class="center">IMPORTANT! PLEASE NOTE:</h2><br>
			You agree to the <a class="link" href="#german-tos">German General Terms and Conditions</a> which have been translated to English for your convenience only. The German General Terms and Conditions are binding.
		  </div>
          <div class="grid clearfix white imprint_wrapper">
            <div class="valign">
              <div class="middle">
                    <div class="imprint">
                        <p>Startup Events UG (limited liability) (named SE UG in the following) offers online-competitions and price draws occasionally. The participation in this competitions/price draws is subject to the following Conditions of Participation. </p>
                    </div>
                    <div class="imprint">
                        <h2>Organizer</h2>
                        <p>The online-competitions and price draws are organized by Startup Events UG, Paul-Heyse-Straße 27, 80336 Munich. </p>
                    </div>
                        <div class="imprint">
                            <h2>Prize and duration of the competition</h2>
                            <p>Details of the prize:
                                <br>An exclusive lunch with Kevin Spacey at his table (together with other people)
                                <br>A full 3-day conference ticket for Bits &amp; Pretzels
                                <br>2 nights in a superior room in the Sofitel luxury hotel (5-Star) from 24.09-26.09.2016
                                <br>The competition starts September 13th 2016 at 09.00 CET and ends September 16th at 00:00 CET. The prize drawing and the announcement oft he winner will start September 16th 14 CET.
                            </p>
                        </div>
                           <div class="imprint">
                            <h2>Conditions of Participation/Price draws</h2>
                            <p>The participation in the competition is free of charge and independent from the purchases of goods or services. Only those participants who registered with all necessary data with the form will be taken into account for the price draw. Lots are drawn among all the replies, guaranteeing the random principle. Disbursement or transferability of the win is excluded. The winner will be informed by SE UG via mail. In case that the winner doesn’t confirm the win in written form (via letter, fax, mail) within 5 days of being notified, the claim for the win will expire and a substitute winner will be drawn after the same principle. The winner won’t be informed about this requirement in the winning notification again. The participant is responsible for the correctness of the given mail address. </p>
                        </div>
                        <div class="imprint">
                            <h2>Eligibility </h2>
                            <p>Any person over the age of 18 is entitled to participate. For participating in the competition/price draw, true statements about personal data of the participant are required. Employees of SE UG, their affiliated companies and partners of the competition, including relatives of the named group are excluded from participating.  SE UG is authorized to exclude individual persons if there are justifying reasons, e.g. violation of the Conditions of Participation, double participation, illegal influence of the competition, manipulation etc. In such cases, winnings can subsequently be denied and reclaimed, if necessary. </p>
                        </div>
                        <div class="imprint">
                            <h2>Amendment of the participation rules and termination of the competition</h2>
                            <p>SE UG reserves the right to to terminate or interrupt the competition or the price draw for important reasons without preannouncement. This applies particularly to any reasons that would disrupt or prevent the scheduled course of the lottery. If such a termination should be caused by negligence of a participant, SE UG has the right to claim damages from such person.</p>
                        </div>
                        <div class="imprint">
                            <h2>Limitations of liability </h2>
                            <p>SE UG shall be liable only for damages which are caused intentionally or by gross negligence or by a breach of the material obligation by the company itsel, by its legal representatives or its vicarious agents within the competition/the price draw (so-called cardinal obligation) . Material contractual obligations are those whose performance to achieve the goal of the competition/the price draw is necessary. In these cases, our liability is limited to the foreseeable, typically occurring damage.The liability of SE UG for damages resulting from the violation of life, body or health remains unaffected thereby. SE UG reserves the right to refuse people.</p>
                        </div>
                        <div class="imprint">
                           <h2>Data protection </h2>
                            <p>All personal data of the participant will be saved and used exclusively for the purpose of the execution and processing of the competition/price draw and will be delteted not later than two months after termination of the competition. A participant will be at liberty at any time to withdraw his participation in the competition/price draw. The revocation is to be sent to: Startup Events UG (haftungsbeschränkt), Paul-Heyse-Straße 27, 80336 Munich or to hello@bitsandpretzels.com.</p>  </div>
                        <div class="imprint"><h2>Final clauses </h2>
                            <p>The law of the Federal Republic of Germany has validity. If certain terms of these Conditions of Participation should be or become invalid, the validity of the remaining Conditions of Participation is not changed. The legal procedure of competitions and price draws is impossible.</p>
                        </div>
                    <a id="german-tos"></a>
              </div>              
            </div>
          </div>

          <div class="wrapp_main_text">
            <h1 class="main_title">Teilnahmebedingungen für Gewinnspiele/Verlosungen </h1>
          </div>
          <div class="grid clearfix white imprint_wrapper">
            <div class="valign">
              <div class="middle">
                    <div class="imprint">
                        <p>Die Startup Events UG (haftungsbeschränkt) (im folgenden SE UG genannt), bietet zeitweise Online-Gewinnspiele und Verlosungen an. Die Teilnahme an diesen Gewinnspielen/Verlosungen richtet sich nach folgenden Teilnahmebedingungen.</p>
                    </div>
                    <div class="imprint">
                        <h2>Veranstalter</h2>
                        <p>Die Online-Gewinnspiele und Verlosungen werden von der Startup Events UG, Paul-Heyse-Straße 27, 80336 München veranstaltet. </p>
                    </div>
                       <div class="imprint">
                        <h2>Gewinn und Gewinnspieldauer</h2>
                        <p>Wir verlosen folgenden Preis: Du kannst am 25.09.2016 an Kevin Spacey’s Tisch sitzen zu einem exklusiven Lunch an einem geheimen Ort! 
                        <br>Details:
                        <br>Exklusiver Lunch am Tisch von Kevin Spacey (zusammen mit anderen Personen) 
                        <br>Ein Drei-Tages-Conference-Ticket für Bits &amp; Pretzels 2016
                        <br>2 kostenlose Übernachtungen in einem „superior room“ im Sofitel Luxury Hotel in München vom 24.09.2016 bis zum 26.09.2016
                        <br>Das Gewinnspiel startet am 13.09.2016 um 09.00 Uhr CET und endet am 16.09.2016 um 00:00 Uhr CET. Die Verlosung und die Bekanntgabe des Gewinners/der Gewinnerin beginnt am 16.09.2016 um 14 Uhr CET.
                        </p>
                    </div>

                    <div class="imprint">
                        <h2>Teilnahmevoraussetzungen/Verlosung</h2>
                        <p>Die Teilnahme am Gewinnspiel ist kostenlos und unabhängig von dem Erwerb einer Ware oder Dienstleistung. Für die Verlosung werden nur Teilnehmer berücksichtigt, die sich mit allen erforderlichen Angaben im jeweiligen Formular registriert haben. Unter allen Einsendungen entscheidet das Los unter Gewährleistung des Zufallsprinzips. Nur ein Teilnehmer gewinnt. Eine Barauszahlung oder Übertragbarkeit des Gewinns auf andere Personen ist ausgeschlossen. Der Gewinner wird von SE UG via E-Mail benachrichtigt. Sollte ein Gewinner nicht innerhalb von 5 Tagen nach der Benachrichtigung gegenüber der SGM den Gewinn in Textform (per Brief, Fax, E-Mail) bestätigen, so verfällt der Anspruch auf den Gewinn und es wird nach demselben Vorgehen ein Ersatzgewinner ausgelost. Der Gewinner wird in der Gewinnbenachrichtigung nicht nochmals ausdrücklich auf dieses Erfordernis hingewiesen. Für die Richtigkeit der angegebenen E-Mail Adresse ist der Teilnehmer verantwortlich.</p>
                    </div>

                    <div class="imprint">
                        <h2>Teilnahmeberechtigung</h2>
                        <p>Teilnahmeberechtigt sind alle Personen ab 18 Jahren. Zur Teilnahme am Gewinnspiel/Verlosung ist die wahrheitsgemäße Angabe der personenbezogenen Daten des Teilnehmers erforderlich. Mitarbeiter von SE UG, ihrer verbundenen Unternehmen sowie Gewinnspielpartner sind von der Teilnahme ausgeschlossen. Die SE UG ist berechtigt, einzelne Personen von der Teilnahme auszuschließen, sofern berechtigte Gründe, wie z. B. Verstoß gegen die Teilnahmebedingungen, doppelte Teilnahme, unzulässige Beeinflussung des Gewinnspiels, Manipulation etc., vorliegen. Gegebenenfalls können in diesen Fällen auch nachträglich der Gewinn aberkannt und zurückgefordert werden.</p>
                    </div>
                  
                    <div class="imprint">
                        <h2>Änderungen der Teilnahmeregeln und Beendigung des Gewinnspiels</h2>
                        <p>SE UG behält sich vor, das Gewinnspiel bzw. die Verlosung jederzeit aus wichtigem Grund ohne Vorankündigung zu beenden oder zu unterbrechen. Dies gilt insbesondere für solche Gründe, die einen planmäßigen Ablauf des Gewinnspiels bzw. der Verlosung stören oder verhindern würden. Sofern eine derartige Beendigung durch das Verhalten eines Teilnehmers schuldhaft verursacht wird, ist SE UG berechtigt, von dieser Person den entstandenen Schaden ersetzt zu verlangen. Zudem behält sich die SE UG das Recht vor Personen abzulehnen.</p>
                    </div>

                    <div class="imprint">
                        <h2>Haftungsbeschränkungen</h2>
                        <p>Die SE UG haftet nur für Schäden, die von ihr, ihren gesetzlichen Vertretern oder Erfüllungsgehilfen vorsätzlich oder grob fahrlässig oder durch die Verletzung wesentlicher Pflichten im Rahmen des Gewinnspiels/der Verlosung (sog. Kardinalspflichten) verursacht wurden. Wesentliche Pflichten sind solche, deren Erfüllung zur Erreichung des Ziels des Gewinnspiels/der Verlosung notwendig ist. In diesen Fällen ist die Haftung auf den vorhersehbaren, typischerweise eintretenden Schaden beschränkt. Die Haftung der SE UG für Schäden aus der Verletzung des Lebens, des Körpers oder der Gesundheit bleibt davon unberührt.</p>
                    </div>

                    <div class="imprint">
                        <h2>Datenschutz</h2>
                        <p>Alle personenbezogenen Daten des Teilnehmers werden ausschließlich zum Zwecke der Durchführung und Abwicklung des Gewinnspiels/ der Verlosung gespeichert und genutzt und spätestens 2 Monate nach Beendigung des Gewinnspiels gelöscht. Es steht dem Teilnehmer jederzeit frei, seine Teilnahme am Gewinnspiel/ an der Verlosung zu widerrufen. Der Widerruf ist zu richten an: Startup Events UG (haftungsbeschränkt), Paul-Heyse-Straße 27, 80336 München oder an hello@bitsandpretzels.com.</p>
                    </div>

                    <div class="imprint">
                        <h2>Schlussbestimmungen</h2>
                        <p>Es gilt das Recht der Bundesrepublik Deutschland. Sollten einzelne Bestimmungen der Teilnahmebedingungen unwirksam sein oder werden, bleibt die Gültigkeit der übrigen Teilnahmebedingungen davon unberührt. Bei den Gewinnspielen und Verlosungen ist der Rechtsweg ausgeschlossen.</p>
                    </div>
              </div>
            </div>
          </div>


        </div>
      </div>
    </div>
  </section>
</div>

<?php
include realpath($pathPrefix.'layout/footer.php');
include realpath($pathPrefix.'layout/bottom.php');