<?php
// local fix to match my vhost settings
$pathPrefix = '';
$host = $_SERVER['HTTP_HOST'];
if ($host == 'bap.local') {
  $pathPrefix = '../';
}
include_once realpath($pathPrefix.'layout/top.php');
include_once realpath($pathPrefix.'layout/header.php');
?>

<div class="inner_sections">

    <?php include_once realpath($pathPrefix.'layout/_inner_pages_main_nav.php');?>

    <!--
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            !! The sub menus have a different arrangement of pages !!
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     -->

    <div id="sub_nav" class="sub_nav_wrap">

        <div class="wrap_subnav_inner">
            <div class="title_section">Imprint</div>
        </div>
    </div>

    <section class="section silver active section_1 section_1_adjust">
       <div class="content_section center">
        <div class="wrapper_inner">
            <div class="wrapp_main_text">
                <h1 class="main_title">Imprint</h1>
            </div>
            <div class="grid clearfix white imprint_wrapper">
                <div class="imprint">
                      <h2>Responsible i.S.d. </h2>
                      <p>Startup Events UG (haftungsbeschränkt)</p>
                      <p>Gärtnerplatz 5</p>
                      <p>80469 Munich</p>
                      <p>Germany</p>
                </div>
                <div class="imprint">
                      <h2>Represented by Managing Directors:</h2>
                      <p>Andreas Bruckschlögl</p>
                      <p>Dr. Bernd Storm van's Gravesande</p>
                      <p>Felix Haas</p>
                </div>
                                             <div class="imprint">
                      <h2>Contact</h2>
                      <p>E-Mail: hello@bitsandpretzels.com</p>
                      <p>Phone:+ 49 089 21554414</p>
                </div>
                  <div class="imprint">
                      <h2>Entry in the commercial register:</h2>
                      <p>Register court: Amtsgericht Munich</p>
                      <p>Register number: HRB 213466</p>
                </div>
                <div class="imprint">
                      <h2>VAT identification number. gem. § 27a UStG:</h2>
                      <p>DE296380022</p>
                </div>
                  <div class="imprint">
                      <h2>Responsible for content according to § 55 paragraph 2 <br>Interstate Broadcasting Agreement:</h2>
                      <p>Andreas Bruckschlögl</p>
                      <p>Dr. Bernd Storm van's Gravesande</p>
                </div>
            </div>
            <div class="wrapp_main_text">
                <h1 class="main_title">Disclaimer</h1>
            </div>
            <div class="grid clearfix white imprint_wrapper">
                <div class="imprint">
                      <h2>Liability for content</h2>
                      <p>The contents of these pages were created with great care. For the correctness, completeness and topicality of contents we can not take any responsibility. As a service provider we are responsible according to § 7 paragraph 1 of TMG for own contents on these pages under the general laws. After §§ 8 to 10 TMG we are not obliged as a service provider to monitor transmitted or stored foreign information or to investigate circumstances that indicate illegal activity. Obligations to remove or block the use of information under the general laws remain unaffected. However, a relevant liability is only possible from the date of knowledge of a specific infringement. Upon notification of such violations, we will remove the content immediately.</p>
                </div>
                <div class="imprint">
                      <h2>Liability for Links</h2>
                      <p>Our offer contains links to external websites over which we have no control. Therefore we can not accept any responsibility for their content. The respective provider or operator is always responsible for the contents of any Linked Site. The linked sites were checked at the time of linking for possible legal violations. Illegal contents were at the time of linking. A permanent control of the linked pages is unreasonable without concrete evidence of a violation. Upon notification of violations, we will remove such links immediately.</p>
                </div>
                                             <div class="imprint">
                      <h2>Copyright</h2>
                      <p>The contents and works on these pages created by the site operator are subject to German copyright law. Duplication, processing, distribution and any kind of exploitation outside the limits of copyright require the written consent of the respective author or creator. Downloads and copies of these pages are only permitted for private, non-commercial use. As far as the content is not created by the website operator, the copyrights of third parties. In particular, third party content as such. Should you nevertheless become aware of copyright infringement, we ask for a hint. Upon notification of violations, we will remove such content immediately.</p>
                </div>
            </div>
            <div class="wrapp_main_text">
                <h1 class="main_title">Credits</h1>
            </div>
            <div class="grid clearfix white imprint_wrapper">
                <div class="imprint">
                      <h2>Pictures:</h2>
                      <p>Dan Taylor - Heisenberg Media</p>
                      <p>Stefan Hobmaier - Stefan Hobmaier Photography</p>
                      <p>Andreas Gebert - Andreas Gebert Photography</p>
                      <p>Miller Mobley (Kevin Spacey Portrait)</p>
                </div>
                <div class="imprint">
                      <h2>Logos:</h2>
                      <p>Company logos shown refer to companies that have been attending Bits &amp; Pretzels events or will be attending the next Bits &amp; Pretzels. All trademarks and logos belong to their respective owners. All rights reserved.</p>
                </div>    
            </div>
        </div>
    </div>
    </section>
</div>

<?php
include realpath($pathPrefix.'layout/footer.php');
include realpath($pathPrefix.'layout/bottom.php');
