<?php 
// Pad
class Router {

	private static $routes = [];
	private static $eventbriteRoutes = [];


	public static function init() {
		self::$routes = [
			'home' => '',

		    // Ticket routes
			'student' => 'student',
            'students_in_tech' => 'student',
			'investor' => 'investor',
			'corporate' => 'corporate',
			'journalist' => 'journalist',
			'startup' => 'startup',
			'academia' => 'academia',

			// List
			'speakers' => 'speakers',
			'captains' => 'table-captains',

			// Info
			'exhibition' => 'exhibition',
            'exhibition_int' => 'exhibition_international',
            'investors_stage' => 'investors_stage',
			'faq' => 'faq',
			'partners' => 'partners',
			'women_in_tech' => 'women-in-tech',
			'hotels' => 'travel',		
			'schedule' => 'agenda',
			'video' => 'video',
			'agenda' => 'agenda',
			'attendees' => 'attendees',
			'pitch' => 'pitch',
			'onepager' => 'onepager',
			'side-events' => 'side-events',
			'afterparty' => 'afterparty',
            'volunteers' => 'volunteers',
            
            // Buy
            'tickets' => 'tickets',
			'buy_startup' => 'ticket/startup',
			'buy_student' => 'ticket/student',
			'buy_corporate' => 'ticket/corporate',
			'buy_investor' => 'ticket/investor',
			'buy_academia' => 'ticket/academia',

			// Success
			'success' => 'success',
			'success_newsletter_first' => 'success/newsletter_first',
			'success_newsletter_final' => 'success/newsletter_final',
            
            // Cluster
            'cluster' => 'cluster',

			/// Academy
			'academy' => 'info/academy',	

			// Legal
			'imprint' => 'legal/imprint',
			'privacy-policy' => 'legal/privacy-policy',
			'tos' => 'legal/tos',
			'team' => 'team',
			'jobs' => 'jobs',
			'tos-students-in-tech' => 'info/students-in-tech/conditions',
			'meet-kevin/conditions' => 'meet-kevin/conditions',

			

		];

		self::$eventbriteRoutes = [
			'startup_early' => 'https://www.eventbrite.com/e/bits-pretzels-2016-tickets-19797638269?discount=BIT_B_ES',
			'startup_later' => 'https://www.eventbrite.com/e/bits-pretzels-2016-tickets-19797638269?discount=BIT_B_LS',
			'investor_private' => 'https://www.eventbrite.com/e/bits-pretzels-2016-tickets-19797638269?discount=BIT_C_PI',
			'investor_institutional' => 'https://www.eventbrite.com/e/bits-pretzels-2016-tickets-19797638269?discount=BIT_C_II',
			'corporate' => 'https://www.eventbrite.com/e/bits-pretzels-2016-tickets-19797638269?discount=BIT_D_C',
			'academia' => 'http://www.eventbrite.com/e/bits-pretzels-2016-tickets-19797638269?discount=Academia_BP',
			'startup' => 'https://www.eventbrite.com/e/bits-pretzels-2016-tickets-19797638269?discount=Student_B1_YR81DI73',
		];
	}


	/**
	* Returns the current base url 
	**/
	private static function getCurrentBaseUrl($atRoot=FALSE, $atCore=FALSE, $parse=FALSE){
	        if (isset($_SERVER['HTTP_HOST'])) {
	            $http = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
	            $hostname = $_SERVER['HTTP_HOST'];
	            $dir =  str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

	            $core = preg_split('@/@', str_replace($_SERVER['DOCUMENT_ROOT'], '', realpath(dirname(__FILE__))), NULL, PREG_SPLIT_NO_EMPTY);
	            $core = $core[0];

	            $tmplt = $atRoot ? ($atCore ? "%s://%s/%s/" : "%s://%s/") : ($atCore ? "%s://%s/%s/" : "%s://%s%s");
	            $end = $atRoot ? ($atCore ? $core : $hostname) : ($atCore ? $core : $dir);
	            $base_url = sprintf( $tmplt, $http, $hostname, $end );
	        }
	        else $base_url = 'http://localhost/';

	        if ($parse) {
	            $base_url = parse_url($base_url);
	            if (isset($base_url['path'])) if ($base_url['path'] == '/') $base_url['path'] = '';
	        }

	        return $base_url;
	    }   

	/**
	* Returns the route specified in $name
	**/
	public static function getRoute($name, $auto_category=false) {
		self::init();
		//var_dump(self::$routes);
		$url = self::getCurrentBaseUrl(true) . self::$routes[$name];

		// hacky atm
		if($auto_category) {
			$route = ltrim(self::getCurrentRoute(), '/');
			$route = self::$routes[$name.'-'.$route];
			$url = self::getCurrentBaseUrl(true) . $route;
		}

		return $url;
		// return self::getCurrentBaseUrl();
	}


	public static function getEventbriteRoute($routeName) {
		return @self::$eventbriteRoutes[$routeName];
	}


	public static function getCurrentRoute() {
		return parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
	}


	/**
	* Get active route class
	*/
	public static function getActiveRouteClass($routeName) {
		$route = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
		if(strpos($route, $routeName) !== false) {
			return 'active';
		}
		return '';
	}

	/**
	 * Basically checks if the current route contains a speicifc keywords
	 */
	public static function isRouteGroup($group) {
		$url = self::getCurrentRoute();
		if(strpos($url, $group) !== false) {
			return true;
		}
		return false;		
	}

	/**
	* Boolean that indicates if sign up form in footer should be rendered or not
	**/
	public static function renderSignUpForm() {
		$url = self::getCurrentBaseUrl();
		if(strpos($url, 'list') !== false) {
			return false;
		}
		return true;
	}


	// eoc
} 
