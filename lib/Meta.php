<?php 

require_once 'Router.php';

// Pad
class Meta {
	public static function getTitle($routeName='') {
		return 'Bits & Pretzels | The Founders Festival: 3 Days, 5000 Participants.';
	}
	public static function getMetaDescription($routeName='') {
		return 'Bits & Pretzels is a 3 day conference for founders and people from the startup ecosystem. We bring together the world’s leading entrepreneurs and investors.';
	}
    public static function getButtonWording($routeName='') {
		return 'Tickets';
	}
    
} 
