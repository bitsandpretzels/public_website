<?php

/**
* 
*/
class Pathable 
{
	private $API_TOKEN = '7a857f2d-75b9-448b-9d13-603ff4397af8';
	private $base_get_user_url = 'http://api.pathable.com/v1/communities/374893/users?api_token=';
	private $pathable_users_tbl = 'pathable_users';	
	private $db;
	private $base_url = 'https://bitsandpretzels.pathable.com';

	public function __construct() {
		$whitelist = array(
		    '127.0.0.1',
		    '::1'
		);

		/*
		$mysqli_connection = new MySQLi('130.211.74.230:3306', 'root', '05e133fd05ca6385dc992180965690b3', 'bitsandpretzels');
		if ($mysqli_connection->connect_error) {
		   echo "Not connected, error: " . $mysqli_connection->connect_error;
		}
		else {
		   echo "Connected.";
		}
		exit;
		*/
	
		$hostname = null; //Defaults to mysqli.default_host
		$username = "root";
		$password = "05e133fd05ca6385dc992180965690b3";
		$database = "bitsandpretzels"; //Defaults to "" 
		$port = null; //Defaults to mysqli.default_port
		$socket = "/cloudsql/bitspretzels-website-1075:europe-west1:bpwebsitesql";
		$mysqli = new mysqli($hostname, $username, $password, $database, $port, $socket);

	
	//$mysqli = new mysqli('/cloudsql/bitspretzels-website-1075:europe-west1:bpwebsitesql', 'root', '', 'bitsandpretzels');

	/*

	if ($mysqli->connect_errno) {
	    printf("Connect failed: %s\n", $mysqli->connect_error);
	    exit();
	}


	if ($mysqli->ping()) {
	    printf ("Our connection is ok!\n");
	} else {
	    printf ("Error: %s\n", $mysqli->error);
	}

	
	$mysqli->close();
	*/




		if(in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
		   $this->db = new MysqliDb ($mysqli);
		   //echo '1';
		   //$this->db =new MysqliDb ('/cloudsql/bitspretzels-website-1075:europe-west1:bpwebsitesql', 'root', '05e133fd05ca6385dc992180965690b3', 'bitsandpretzels');
		   //$this->db =new MysqliDb ('localhost', 'root', '', 'bitsandpretzels');
		} else {
		   //echo '2';
		   $this->db = new MysqliDb ($mysqli);
		   //$this->db =new MysqliDb ('130.211.74.230', 'root', '05e133fd05ca6385dc992180965690b3', 'bitsandpretzels');
		   // $this->db =new MysqliDb ('130.211.74.230:3306', 'root', '05e133fd05ca6385dc992180965690b3', 'bitsandpretzels');
		}	
	}


	private function removeLevel($array) {
	      $result = array();
	      foreach ($array as $key => $value) {
	        if (is_array($value)) {	          	         
	          $result = array_merge($result, $value);
	        }
	      }
	      return $result;
	}

	private function getUsersUrl() {
		return $this->base_get_user_url . $this->API_TOKEN;
	}

	private function getPageData() {
		$url = $this->getUsersUrl();
	    $content = file_get_contents($url);
		$obj = json_decode($content);
		return ['total_pages' => $obj->total_pages];
	}


	public function refreshPeopleDatabase() {
		error_reporting(-1);
		$pages = $this->getPageData();
		$pages = $pages['total_pages'];	

		$data = [];
		$id = 0;

		// Iterate over all pages
		$i = 1;
		while($i <= $pages) {
			// Download page
			$content = $this->downloadPage($i);

	
			// Insert into database
			$data = Array (
						"url" => $content['url'],
						"page" => $i,
			            "data" => $content['content'],
			            "updated_at" => \Carbon\Carbon::now('Europe/Berlin')->toDateTimeString(),
			            "created_at" => \Carbon\Carbon::now('Europe/Berlin')->toDateTimeString()

			);
			$updateColumns = Array ("data", "updated_at");
			$lastInsertId = "id";
			$this->db->onDuplicate($updateColumns, $lastInsertId);
			$id = $this->db->insert($this->pathable_users_tbl, $data);

			$i++;
		}



		/*
		echo '<pre>';
		var_dump($data);
		echo '</pre>';	
		*/	
		return $id;
	}


	/**
	 * This method downloads the json string of the given page
	 * @param  int $page page number
	 * @return array
	 */
	public function downloadPage($page) {
		$page = (int)$page;
		$url = $this->getUsersUrl() . '&page=' . $page; 
	    $content = file_get_contents($url);
		return ['url' => $url, 'content' => $content];
	}


	/**
	 * This method returns a complete people array based on the json files in the database
	 * @param  array  $options [description]
	 * @return boolean
	 */
	public function getPeople($options=[]) {
		$people = $this->db->get($this->pathable_users_tbl); 
		$result = [];
		// Merge json data to create a people array
		$i = 0;
		foreach ($people as $key => $entry) {
			$continue = false;
			$json = json_decode($entry['data']);	
			foreach ($json->results as $key => $user) {
						// Add values
						$user->image = $this->base_url . $user->photo_url_full;
						$user->company = $user->organization_name;
						$user->sort_name = strtolower($user->last_name);
						// Check options people type
						if(isset($options['people_type'])) {
							if(!isset($user->ribbons[0]) && isset($options['people_type'])) {
								$continue = true;
								continue;								
							}
							if(isset($options['people_type']) && $options['people_type'] !== $user->ribbons[0]->name) {
								$continue = true;
								continue;
							}

							// Assign type (e.g global disruptor etc.)
							foreach ( $user->answers as $key => $value) {
								switch($value->question->question) {
									case 'person_type_tags':
										$user->type = $value->groups[0]->name;
										break;
									default:
										$user->type = 'N/A';
										break;
								}
							}
						}
						$json->results[$key] = (array)$user;
					}
			if($continue) {
				continue;
			}		
			$result[$i] = $json->results;
			$i++;
		}

		$result = $this->removeLevel($result);
		return $result;
	}



}