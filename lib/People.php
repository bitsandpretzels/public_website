<?php 


// Pad
class People {

	static $type = '';
	static $source = [];

	public static function setType($type) {
		switch($type):
			case 'table-captain':
				self::$type = 'table-captain';
				break;
			case 'speaker':
				self::$type = 'speaker';
				break;
            case 'expectation':
				self::$type = 'expectation';
				break;

			default:
				self::$type = 'table-captain';
				break;	

		endswitch;

		return new static;
	}

	/**
	Sort multi dimensional array by key
	**/
	private static function arrayOrderBy()
	{
	    $args = func_get_args();
	    $data = array_shift($args);
	    foreach ($args as $n => $field) {
	        if (is_string($field)) {
	            $tmp = array();
	            foreach ($data as $key => $row) {	  
	                $tmp[$key] = strtolower($row[$field]);
	            }
	            $args[$n] = $tmp;
	            }
	    }
	    $args[] = &$data;
	    call_user_func_array('array_multisort', $args);
	    return array_pop($args);
	}


	private static function mSort() {   // Sort the multidimensional array
	     usort(self::$source, "sort_name");
	     // Define the custom sort function

 	}


	public static function setDataSource($source) {
		self::$source = $source;
		return new static;
	}	


	public static function get($start=0, $num='all', $break=true, $sort=false, $filter=[]) {

		$i = 0;
		$result = [];

		if($sort) {
			if(isset(self::$source[0][$sort])) {
				self::$source = self::arrayOrderBy(self::$source, $sort, SORT_ASC, SORT_STRING);	
			} else {
				self::$source = self::arrayOrderBy(self::$source, 'sort_name', SORT_ASC, SORT_STRING);
			}   			
		}

		//echo '<pre>';
		//var_dump(self::$source);
		//exit;

		switch(self::$type):
			case 'table-captain':

				foreach (self::$source as $key => $value) {


					if($start > $i) {
						$i++;
						continue;
					}
				

					if($i == $num && $num !== 'all') {
						return $result;
					}

					if($break) {
						$value['name'] = substr_replace($value['name'], '<br>', strrpos($value['name'], ' '), strlen(' '));
					}


					$result[] = $value;
					$i++;
				}
        
            case 'expectation':

				foreach (self::$source as $key => $value) {


					if($start > $i) {
						$i++;
						continue;
					}
				

					if($i == $num && $num !== 'all') {
						return $result;
					}

					if($break) {
						//$value['name'] = substr_replace($value['name'], '<br>', strrpos($value['name'], ' '), strlen(' '));
					}


					$result[] = $value;
					$i++;
				}



				return $result;
				break;
			case 'speaker':
				$skip = false;
				foreach (self::$source as $key => $value) {

					if(isset($value['filter']) && !empty($value['filter']) && is_array($value['filter'])) {
						foreach($value['filter'] as $item) {
							if(in_array($item, $filter)) {
								$skip = true;
							}
						}
					}

					if($skip) {
						$skip = false;
						continue;
					}

					if($start > $i) {
						$i++;
						continue;
					}

					if($i == $num && $num !== 'all') {
						return $result;
					}

					if($break) {
						$value['name'] = substr_replace($value['name'], '<br>', strrpos($value['name'], ' '), strlen(' '));
					}


					$result[] = $value;
					$i++;
				}
				return $result;
				break;

			default:
				return array_slice(self::$source, $num);
				break;

		endswitch;

	}





	// eoc
} 
