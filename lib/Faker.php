<?php

error_reporting(0); // Important don't show something like error in Faker... to the user!
require_once 'Eventbrite.php';
require 'vendor/autoload.php';

use Carbon\Carbon;




class Faker
{

	public static function getLastBuyTime($category) {
		$eventbrite = new Eventbrite('MEJYHGGYBMZDZKTAJMGP');
		 $args = array('id' => 19797638269, 'data' => 'orders', 'page' => 19);
  		 $orders = $eventbrite->events($args);
  		 $last_order = array_pop($orders['orders']);

  		 //echo '<PRE>';
  		 //var_dump($last_order['created']);
  		 

  		 $now = Carbon::now('Europe/Berlin');
  		 $carbon = Carbon::parse($last_order['created']);
  		 // Parser detects a wrong timezone
  		 $carbon->timezone = 'Europe/Berlin';
  		 $carbon->hour = $carbon->hour - 1;

  		 //var_dump($carbon);
  		 //var_dump($now);

		 //echo '</pre>';

  		 return gmdate("H:i", $now->diffInMinutes($carbon)*60);
	}



	public static function getLastBuyTimeEstimation($category) {

		$c_rand = Carbon::now();
		// Add random factor that stays the same for each day
		$random_factor = ((($c_rand->day)/$c_rand->month)*abs($c_rand->month-12)*$c_rand->month)*(1+(($c_rand->day*$c_rand->day)/$c_rand->month)*0.0009);
		$random_factor = min($random_factor, 800);


		// return $random_factor;
		

		switch (@$category) {
			case 'startup':
				$period = new DatePeriod(
					     new DateTime('2016-02-16 03:04:10'),
					     new DateInterval('PT14H'),
					     new DateTime('2016-06-20 16:09:10')
					);
				break;

			case 'corporate':
				$period = new DatePeriod(
					     new DateTime('2016-02-16 14:53:50'),
					     new DateInterval('PT8H'),
					     new DateTime('2016-06-20 09:21:30')
				);
				break;

			case 'investor':
				$period = new DatePeriod(
				     new DateTime('2016-02-16 18:33:54'),
				     new DateInterval('PT19H'),
				     new DateTime('2016-06-20 23:12:11')
				);	
				break;		


			case 'student':
				$period = new DatePeriod(
				     new DateTime('2016-02-16 13:09:54'),
				     new DateInterval('PT11H'),
				     new DateTime('2016-06-20 14:43:11')
				);	
				break;		

			
			default:
				$period = new DatePeriod(
					     new DateTime('2016-02-16 03:04:10'),
					     new DateInterval('PT14H'),
					     new DateTime('2016-06-20 16:09:10')
					);
				break;
		}




		$arr = iterator_to_array($period);

		$now = Carbon::now();
		$old_buy = null;
		foreach ($arr as $key => $value) {
			$buy = Carbon::instance($value);

			// Check if is in future
			if($now->lt($buy)) {	
				$old_buy->subMinutes(ceil($random_factor));
				//return $now->diffInMinutes($old_buy);
				return gmdate("H:i", $now->diffInMinutes($old_buy)*60);
			}

			// save old buy
			$old_buy = $buy;
		}

	}


	public static function views($category) {
		$carbon = Carbon::now();

		$correction = 1;
		if($carbon->hour > 17) {
			if(mt_rand(0, 100) < 15) {
				$correction = $carbon->hour - mt_rand(3,6) + mt_rand(3,6) + mt_rand(3,6)*(0.1*$carbon->hour);
			} else {
				$correction = $carbon->hour - 5+5*(0.1*$carbon->hour);
			}
		} 

		if($carbon->hour >= 10 && $carbon->hour <= 17) {
			if(mt_rand(0, 100) < 15) {
				$correction = $carbon->hour + mt_rand(3,6) + mt_rand(3,6) + mt_rand(3,6)*(0.1*$carbon->hour);
			} else {
				$correction = $carbon->hour + 5+5*(0.1*$carbon->hour);
			}
		}

		if($carbon->hour < 8) {
			if(mt_rand(0, 100) < 15) {
				$correction = -mt_rand(45,59);
			} else {
				$correction = -53;
			}
		}

		$views = $correction + mt_rand(10, 12);

		return floor(abs($views));
	}
	

}

