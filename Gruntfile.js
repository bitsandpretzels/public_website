module.exports = function(grunt) {
  // JS files
  var app = [
    '<%= paths.js %>app.js',
    '<%= paths.js %>app.*.js',
    '<%= paths.js %>init.js'
  ],
  vendor = [
    '<%= paths.vendor %>jquery-1.11.3.min.js',
    '<%= paths.vendor %>TweenMax.1.11.8.min.js',
    '<%= paths.vendor %>ismobile.js',
    '<%= paths.vendor %>jquery.validate.min.js',
    '<%= paths.plugins %>helpers.custom.js',
    '<%= paths.plugins %>jquery.helpers.js',
    '<%= paths.plugins %>plugins.form.util.js',
    '<%= paths.plugins %>plugins.cookie.util.js',
    '<%= paths.plugins %>plugins.keyboard.util.js',
    '<%= paths.plugins %>plugins.section.js',
    '<%= paths.plugins %>plugins.parallax.util.js',
    '<%= paths.plugins %>CSSPlugin.min.js',
    '<%= paths.plugins %>DrawSVGPlugin.min.js',
    '<%= paths.plugins %>newSmoothScroll.js',
    '<%= paths.plugins %>jquery.unveil.min.js',
    '<%= paths.plugins %>featherlight.min.js',
    '<%= paths.plugins %>swiper.min.js',
    '<%= paths.plugins %>jquery.browser.min.js',
    '<%= paths.plugins %>jquery.visible.min.js',
    '<%= paths.plugins %>jquery.mobile.custom.min.js',
    '<%= paths.plugins %>plugins.videoBackground.js',
    '<%= paths.plugins %>lity.js',
    '<%= paths.plugins %>jquery.countdown.min.js',
    '<%= paths.plugins %>jquery.countdown.min.js',
    '<%= paths.plugins %>jquery.countdown.min.js',
    'revolution/js/jquery.themepunch.tools.min.js?rev=5.0',
    'revolution/js/jquery.themepunch.revolution.min.js?rev=5.0'
  ];

  grunt.initConfig({
    paths : {
      js : 'src/js/',
      css : 'src/css/',
      less : 'src/less/',
      vendor : 'src/js/vendor/',
      plugins : 'src/js/plugins/'
    },

    less : {
      app : {
        files : {'<%= paths.css %>main.css':'<%= paths.less %>main.less'},
        options : {
          compress: true,
          yuicompress: true,
          optimization: 2,
          dumpLineNumbers: 'all',
          sourceMap: true,

          sourceMapFilename: 'src/css/main.css.map', // where file is generated and located
          sourceMapURL: 'src/css/main.css.map', // the complete url and filename put in the compiled css file
          sourceMapBasepath: '/', // Sets sourcemap base path, defaults to current working directory.
          sourceMapRootpath: '/', // adds this path onto the sourcemap filename and less file paths

          // sourceMapBasepath : 'http://localhost:8080/git/edesign.bg',
          // sourceMapRootpath : '/'
        }
      },
      production : {
        files : {
          '<%= paths.css %>main.css'  : '<%= paths.less %>main.less'
        },
        options : {
          cleancss : true,
          compress : true
        }
      }
    },

    cssmin: {
      target: {
        files: {
          '<%= paths.css %>main.min.css': '<%= paths.css %>main.css'
        }
      }
    },

    watch : {
      less : {
        files : ['<%= paths.less %>*.less'],
        tasks : ['less:app', 'cssmin']
      },
      grunt : {
        files : ['Gruntfile.js']
      },
      scripts : {
        files : app,
        tasks : [
          'concat:dev',
          'concat:vendor'
        ]
      },
      vendor : {
        files : ['<%= paths.vendor %>*.js', '<%= paths.plugins %>*.js'],
        tasks : ['concat:vendor']
      },
    },

    concat : {
      dev : {
        files : {
          '<%= paths.js %>main.js' : app
        }
      },
      vendor : {
        files : {
          '<%= paths.js %>vendor.js' : vendor
        }
      },
      dist: {
        src: [
          '<%= paths.js %>vendor.js',
          '<%= paths.js %>main.js'
        ],
        dest: '<%= paths.js %>/dist.js'
      },
    },

    uglify : {
      vendor : {
        options : {
          sourceMap : false,
          sourceMapName : '<%= paths.js %>vendor.map'
        },
        files :{ '<%= paths.js %>vendor.js' : vendor }
      },
      app : {
        options : {
          sourceMap : false,
          sourceMapName : '<%= paths.js %>scripts.map'
        },
        files : {
          '<%= paths.js %>main.js' : app
        }
      },
      dist: {
        files: {
          '<%= paths.js %>/dist.min.js': ['<%= paths.js %>/dist.js']
        }
      }
    },
    //
    // htmlmin: {                                     // Task
    //   dist: {                                      // Target
    //     options: {                                 // Target options
    //       removeComments: true,
    //       collapseWhitespace: true
    //     },
    //     files: {                                   // Dictionary of files
    //       'dist/index.html': 'src/index.html',     // 'destination': 'source'
    //       'dist/contact.html': 'src/contact.html'
    //     }
    //   },
    //   dev: {                                       // Another target
    //     files: {
    //       'dist/index.html': 'src/index.html',
    //       'dist/contact.html': 'src/contact.html'
    //     }
    //   }
    // },

    cacheBust: {
      taskName: {
        options: {
          assets: ['<%= paths.js %>vendor.js', '<%= paths.js %>main.js', ]
        },
        src: ['layout/**/*.php']
      }
    }

  });

  grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-cache-bust');
  //grunt.loadNpmTasks('grunt-contrib-htmlmin');

  grunt.registerTask('default', ['less', 'watch', 'concat', 'uglify', 'cacheBust', 'cssmin']);
  grunt.registerTask('vendor', ['concat:vendor']);
  grunt.registerTask('dist', ['concat:dist', 'uglify:dist', 'cssmin']);
}
