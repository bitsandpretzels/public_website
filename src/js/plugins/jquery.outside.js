(function (){

    jQuery.fn.outside = function (callback)
    {
        Outside.add ( $(this), callback );
        return this;
    }

    jQuery.fn.removeOutside = function ()
    {
        Outside.remove ( $(this) );
        return this;
    }

    var Outside = {
        map : [],
        have_win_click: false,
        add : function (target, callback){
            target.each( function (){
                Outside.map.push( {target: $(this), callback:callback });
            })

            if( !Outside.have_win_click )
            {
                $(window).on('click' , Outside.winClick );
                Outside.have_win_click = true;
            }
        },

        remove : function ( target ){
            for (var i = Outside.map.length-1; i > -1; i--) {
                if( target.is( Outside.map[i].target) ){
                    Outside.map.splice(i,1);    
                }   
            }
        },

        winClick : function ( e ){

            for (var i = 0; i < Outside.map.length; i++) {
                var target = Outside.map[i].target;
                if(target.css('display') == 'none')continue;

                var l = target.offset().left,
                t = target.offset().top,
                r = target.width() + l,
                b = target.height() + t,
                ex = e.pageX,
                ey = e.pageY;
                
                if( ex < l || ex > r || ey < t || ey > b ){
                    Outside.map[i].callback.apply( target );
                }
                
            };
        } 
    }

})();
