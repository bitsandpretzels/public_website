/*
	sections: '.section_transition',
	change: function,
	beforeChange : function
	afterChange : function
	zIndex: true,
	enableKeyboard : true,
	loop: false
*/

var SectionTransition = function (props){
	var obj = {},
	is_busy,settings,sections = [],
	current_section = 0,
	current_step= 0,
	prev_step=0,
	prev_section = -1,
	dest,list = {},lock_id,
	min_lock_time = .1,
	is_lock = false;
	settings = props || {};

	if(settings.zIndex == undefined)settings.zIndex = true; 
	if(!settings.change ) settings.change = function (c, p, current_index, prev_index, dest, all){
		var self = this,current_id, prev_id;
		var is_same_section = current_step != prev_step;
		self.lock(true);	
		clearTimeout( lock_id );
		
		if( c ) {
			current_id = c.attr('id').replace(/#/);
			c.addClass('active');
		}

		if( p ) prev_id = p.attr('id').replace(/#/);
		var e = { index:list[current_id].index, transition:list[current_id], incomingSection:list[current_id].section , step: current_step ,currentId:current_id, prevId: prev_id,  destination: dest, all:all, currentIndex: current_index, prevIndex: prev_index };		
		if(prev_id)
			e.outcomingSection = list[prev_id].section;
		if ( settings.beforeChange) settings.beforeChange ( e );  
		var lock_time =  self.showTransition (current_id, prev_id, e, is_same_section);
		
		lock_id = setTimeout( function (){
			if( p && !is_same_section)
				p.removeClass('active');
			self.lock(false);
		}, lock_time*1000);

		e.lockTime = lock_time;
		if ( settings.afterChange) settings.afterChange ( e ); 
	}

	/*if(!settings.sections) throw new Error('');
	sections = typeof settings.sections == 'string'? $(settings.sections) : settings.sections;*/

	if( settings.enableKeyboard ) enableKeyboard ( true );
	$(window).on("mousewheel DOMMouseScroll", wheelHandler);	

	function wheelHandler (event){
		var delta = event.originalEvent.wheelDeltaY || -event.originalEvent.detail;
		/*if(event.originalEvent.wheelDeltaX !== 0) return;*/
		if(is_lock)return;
		is_lock = true;

		setTimeout(function (){
			is_lock = false;
		},min_lock_time*1000);	

		if( !is_busy ){
			prev_section = current_section;
		}else
			return;

		delta < 1? next() : prev();
	}

	function enableKeyboard (value) {
		if( value ){
			$(window).off('keydown', keyDown);
			$(window).on('keydown', keyDown);
		}else{
			$(window).off('keydown', keyDown);
		}
	}	

	function keyDown (e){
		if( e.keyCode == 40){
			if( !is_busy )
				prev_section = current_section;
			next();
		}
		if( e.keyCode == 38){
			if( !is_busy )
				prev_section = current_section;
			prev();
		}
	}

	function next (){
		if(is_busy)return;
		var n = current_section;
		++current_section;		
		if( current_section > sections.length-1)
			current_section = settings.loop ? 0 : sections.length-1;		

		if( n == current_section)return;
		dest = 1;
		change ();
	}

	function prev (){
		if(is_busy)return;
		var n = current_section;
		--current_section;
		if( current_section < 0)
			current_section = settings.loop ?  sections.length-1 : 0;
		
		if( n == current_section)return;
		dest = -1;
		change ();
	}

	function change (){
		var l1 = sections[current_section],
			l2 = sections[prev_section] 
		var c = l1.section;
		var p = l2.section;

		//console.log ( l1.name , l2.name )
		if( l1.name == l2.name && l1.steps ){
			prev_step = current_step;
			current_step += dest;
		}else
			current_step = prev_step = 0;

		if( settings.zIndex  ) 
		{
			c.css('zIndex', 1);
			p.css('zIndex', 0);
		}

		if( current_section == prev_section ) p = null;
		settings.change.apply ( obj ,[ c, p, current_section, prev_section, dest, sections.length]);
	}

	function lock (value){
		is_busy = value;
	}

	function show ( value ){
		if( typeof value == 'string'){
			for (var i = 0; i < sections.length; i++) {
				if (sections[i].section.attr('id') == value){
					value = i;
					break;
				}
			};
		}

		dest = value > current_section? 1 : -1;
		prev_section = current_section;

		current_section = value;
		change();
	}			

	var section_index = -1;
	function add ( transition ){
		transition.index = ++section_index;
		list[ transition.name ] = transition;		

		for (var i = 0; i < transition.steps; i++) 
			sections.push( transition );
	}

	function showTransition ( incoming, outcoming, obj, change_step ){
		var i = 0.5,o = 0;
		if(!change_step){
			if( incoming in list ) i = list[incoming].incoming(obj);
			if( outcoming in list ) o = list[outcoming].outcoming(obj);
		}else{
			i = list[incoming].changeStep(obj);
		}

		//console.log( 'incoming: '+ incoming, 'outcoming: '+ outcoming )
		return Math.max(i,o);
	}

	obj.change = change;
	obj.lock   = lock;
	obj.show   = show;
	obj.next   = next;
	obj.prev   = prev;
	obj.addTransition    = add;
	obj.showTransition   = showTransition;
	obj.enableKeyboard 	 = enableKeyboard;

	return obj;
};

var Transition = function (props)
{
	if(!props)return;
	props = props || {}
	if(props.changeStep == undefined) props.changeStep = function (obj){}

	var sec_id = props.section.replace(/#/,''),
		section =  $(props.section),
		index = 0,
		steps = props.steps || 1;


	return {
		incoming : props.incoming,
		outcoming : props.outcoming,
		changeStep  : props.changeStep,
		name : sec_id,
		section : section,
		steps : steps,
		index : index,
		id : function (value){ sec_id = value; }
	}
};