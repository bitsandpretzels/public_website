/*
    props : {
        alignX : 0 - 1, default 0.5 center 
        alignY : 0 - 1, default 0.5 center
        scale  : 'fill' || 'fit' || 'stretch' 
        viewport : jQuery Object || 'parent' || { width , height } || function (target) { return jQuery Object }, default 'parent'
        alignTo : jQuery Object || 'parent' || { width , height }, default 'parent'
    }
*/

jQuery.fn.scale = function(props) {
    // VERSION 1.0.1
    var self = this;
    var support_transform = Modernizr.csstransforms;

    self.each(function() {
        var select_image = $(this);
        var is_none = select_image.css('display') == 'none';
        var viewport_width, 
            viewport_height, 
            settings,
            use_transform; 
            
        settings = props || {
            viewport: 'parent',
            transform : true
        };

        if( settings.transform == undefined )
            settings.transform = true;

        use_transform = settings.transform;


        if( (settings.transform && !support_transform) || select_image.css('position') == 'absolute' ) {
            use_transform = false;
        }   

        if (settings.alignX == undefined) settings.alignX = 0.5;
        if (settings.alignY == undefined) settings.alignY = 0.5;

        if (settings.viewport == 'parent' || settings.viewport == undefined) {
            var $bgImgParent = select_image.parent();

            viewport_width = $bgImgParent.width();
            viewport_height = $bgImgParent.height();
        } else if (settings.viewport instanceof jQuery) {
            viewport_width = settings.viewport.width();
            viewport_height = settings.viewport.height();
        } else if (settings.viewport.width) {
            viewport_width = settings.viewport.width;
            viewport_height = settings.viewport.height;
        } else if ( typeof settings.viewport  == 'function' ){
            var $bgImgParent = settings.viewport( select_image );

            viewport_width = $bgImgParent.width();
            viewport_height = $bgImgParent.height()
        }         

        var img_width = settings.image ? settings.image.width : select_image.attr('width') || select_image.width();
        var img_height = settings.image ? settings.image.height : select_image.attr('height') || select_image.height();

        img_width = parseInt(img_width)
        img_height = parseInt(img_height)
        
        if (img_width * img_height <= 0 && select_image.attr('src')){
            if(select_image.prop('complete')) return;
            else select_image.on('load', function (){
                img_width = parseInt(select_image.width());
                img_height = parseInt(select_image.height());

                scale();
            } )
        }else
            scale ();

        function scale ()
        {
            var _css = {};

            var master_ratio = 1;
            var coef,
                new_width,
                new_height,
                left_position,
                top_position,
                is_stretch;

            switch (settings.scale) {
                case 'fit':
                    master_ratio = Math.min(viewport_width / img_width, viewport_height / img_height);
                    new_width = Math.round(master_ratio * img_width);
                    new_height = Math.round(master_ratio * img_height);
                    break;
                case 'stretch':
                    new_width = viewport_width;
                    new_height = viewport_height;
                    is_stretch = true;
                    break;
                case 'fill':
                default:
                    master_ratio = Math.max(viewport_width / img_width, viewport_height / img_height);
                    new_width = Math.round(master_ratio * img_width);
                    new_height = Math.round(master_ratio * img_height);
                    break;
            }

            var align_x,align_y;
            if(typeof settings.alignX == 'string')
            {
              switch (settings.alignX) {
                  case 'right': align_x = 1.0;break;
                  case 'left': align_x = 0.0;break;
                  case 'center': default:align_x = 0.5;break;
              }
            }else
              align_x = settings.alignX; 

            left_position = use_transform ?  
              ( (new_width - img_width) * .5) *(1-align_x) +  (viewport_width - ( (new_width + img_width) * .5 ) ) * align_x : 
              (viewport_width - new_width) * align_x; 
            
            if(typeof settings.alignY == 'string')
            {
              switch (settings.alignY) {
                  case 'top': align_y = 0.0; break;
                  case 'bottom': align_y = 1.0; break;
                  case 'center': default: align_y = 0.5;break;
              }
            }else
              align_y = settings.alignY; 
            

            top_position = use_transform ? 
                 ( (new_height - img_height) * .5) *(1-align_y) +  (viewport_height - ( (new_height + img_height) * .5 ) ) * align_y : 
                 (viewport_height - new_height)*settings.alignY;


            if(select_image.css('position') == 'absolute')
            {
                _css = {
                        width: new_width,
                        height: new_height,
                        left: left_position,
                        top: top_position
                    }
            }else{     
                if (use_transform) {

                    if(is_stretch)
                    {
                      _css = {
                        width: new_width,
                        height: new_height
                      }
                    }else{
                      _css = {
                          transform: ' translate(' + left_position + 'px, ' + top_position + 'px) scale(' + master_ratio + ')'
                      }
                    }
                } else {
                    _css = {
                        width: new_width,
                        height: new_height,
                        marginLeft: left_position,
                        marginTop: top_position
                    }
                }
            }

            select_image.css(_css);

            if (is_none)
                select_image.css({
                    display: 'none'
                });
        }
    });

    return this;
}

;window.Modernizr=function(a,b,c){function w(a){i.cssText=a}function x(a,b){return w(prefixes.join(a+";")+(b||""))}function y(a,b){return typeof a===b}function z(a,b){return!!~(""+a).indexOf(b)}function A(a,b){for(var d in a){var e=a[d];if(!z(e,"-")&&i[e]!==c)return b=="pfx"?e:!0}return!1}function B(a,b,d){for(var e in a){var f=b[a[e]];if(f!==c)return d===!1?a[e]:y(f,"function")?f.bind(d||b):f}return!1}function C(a,b,c){var d=a.charAt(0).toUpperCase()+a.slice(1),e=(a+" "+m.join(d+" ")+d).split(" ");return y(b,"string")||y(b,"undefined")?A(e,b):(e=(a+" "+n.join(d+" ")+d).split(" "),B(e,b,c))}var d="2.8.3",e={},f=b.documentElement,g="modernizr",h=b.createElement(g),i=h.style,j,k={}.toString,l="Webkit Moz O ms",m=l.split(" "),n=l.toLowerCase().split(" "),o={},p={},q={},r=[],s=r.slice,t,u={}.hasOwnProperty,v;!y(u,"undefined")&&!y(u.call,"undefined")?v=function(a,b){return u.call(a,b)}:v=function(a,b){return b in a&&y(a.constructor.prototype[b],"undefined")},Function.prototype.bind||(Function.prototype.bind=function(b){var c=this;if(typeof c!="function")throw new TypeError;var d=s.call(arguments,1),e=function(){if(this instanceof e){var a=function(){};a.prototype=c.prototype;var f=new a,g=c.apply(f,d.concat(s.call(arguments)));return Object(g)===g?g:f}return c.apply(b,d.concat(s.call(arguments)))};return e}),o.csstransforms=function(){return!!C("transform")};for(var D in o)v(o,D)&&(t=D.toLowerCase(),e[t]=o[D](),r.push((e[t]?"":"no-")+t));return e.addTest=function(a,b){if(typeof a=="object")for(var d in a)v(a,d)&&e.addTest(d,a[d]);else{a=a.toLowerCase();if(e[a]!==c)return e;b=typeof b=="function"?b():b,typeof enableClasses!="undefined"&&enableClasses&&(f.className+=" "+(b?"":"no-")+a),e[a]=b}return e},w(""),h=j=null,e._version=d,e._domPrefixes=n,e._cssomPrefixes=m,e.testProp=function(a){return A([a])},e.testAllProps=C,e}(this,this.document);

jQuery.fn.clickOutside = function (callback)
{
    var self = $(this)
    $(window).on('click', function (e){
        self.each (function (index,target){
            var target = $(target);
            if(target.css('display') == 'none')return;
            if( e.pageX >= target.offset().left && e.pageX <= target.offset().left + target.width() && e.pageY >= target.offset().top && e.pageY <= target.offset().top + target.height()){
            
            }
            else
                callback(target)
        })

    })

    return this;
}
