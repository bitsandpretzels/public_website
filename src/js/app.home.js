App.Home = {
	mobile: navigator.userAgent.match(/Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile/i) || (window.outerWidth  - 10) < 1030 ? true : false,
	sec: $('#section_1'),
	sec_2: $('#section_2'),
	init : function (){

		App.Home.createLeftNav();
		App.Home.bind();
		App.Home.initialAnimation();
	},
	bind : function (){
		$('.left_nav .link').on('click', function (){
			var _this = $(this).index();
			//App.SectionsTransition.transition.show ( $(this).index() );
			$('html, body').animate({
		        scrollTop: $('#section_' + _this).offset().top
		    }, 1000);
		})

	},initialAnimation:function(){
		var sec = $('#section_0');

		if(!App.mobile_ver_tb)
		{

			$(document).ready(function() {
				TweenMax.fromTo(sec.find('.logo'),1,{y:-100,autoAlpha:0},{y:0,autoAlpha:1});
				TweenMax.fromTo(sec.find('.main_title'),1,{y:-100,autoAlpha:0},{y:0,delay:.3,autoAlpha:1});
				TweenMax.fromTo(sec.find('.sub_title'),1,{autoAlpha:0,y:-100},{autoAlpha:1,y:0});
				TweenMax.fromTo($('#main_nav'),1,{y:200,autoAlpha:0},{y:0,autoAlpha:1});
			});

			App.Home.scrollTemplate();
			$(window).on("scroll wheel",App.Home.handler);
		}
	},
	unsetAnimationPositions:function(){
		var rect = document.getElementById('white-rect');
		var rect1 = $("#rect-1");

		App.all_tl = new TimelineLite({paused:true});
		//App.all_tl.set('#mask-gevreche',{transfromOrigin:"50% 50%",yPercent:0});
		App.all_tl.set('#mask-gevreche',{transfromOrigin:"50% 50%",yPercent:50});
		App.all_tl.set('#video_tag' ,{ y:0});
		App.all_tl.set('.video_layer' ,{ y:0});
		App.all_tl.set(rect1,{yPercent:0,transfromOrigin:"50% 50%"});
		App.all_tl.set('#white-rect',{opacity:1});
		App.all_tl.set(rect,{x:0});
		App.all_tl.play();
	},

	createLeftNav : function (){
		var left_nav = $('.left_nav').css('zIndex',104).html('');
		var sections = $('.section_transition');
		for (var i = 0; i < sections.length; i++) {
			left_nav.append('<div class="link"><div class="line"></div></div>');
		};
	},
	getProgress : function() {
		var _progress = getScrollTop() /  App.viewport*0.3;
		_progress = Math.min(1, Math.max(0, _progress));

		return _progress;
	},
	handler : function(e) {
		_event = 'scroll';

			setTimeout(function() {
				App.all_tl.progress(App.Home.getProgress());
			}, 16);
	},
	scrollTemplate: function(){

		var rect = document.getElementById('white-rect');
		var rect1 = $("#rect-1");

		App.all_tl = new TimelineLite({paused:true});
		//App.all_tl.set('#mask-gevreche',{transfromOrigin:"50% 50%",yPercent:-20},"start");
		//App.all_tl.set('#video_home' ,{ y:0},"start+=0.5");



		//App.all_tl.set(rect1,{yPercent:100,transfromOrigin:"50% 50%"},"start");
		App.all_tl.set('#white-rect',{opacity:1},"start+=0.3");
		App.all_tl.set(rect,{x:485});

		App.all_tl.set('#svg',{autoAlpha:1},"end");


		App.all_tl.addLabel('start')
        App.all_tl.to('#video_home' ,1,{ y:App.viewport, ease:Linear.easeNone},"start-=0.5");

	    App.all_tl.to(rect1, 1,{yPercent:0,ease:Linear.easeNone},"start");
        //App.all_tl.to("#mask-gevreche",1,{yPercent:0,ease:Linear.easeNone},"start");


        /*
        App.all_tl.to(rect,1,{x:485,ease:Linear.easeNone,onStart:function (){
        	//TweenMax.set('#white-rect',{opacity:1});
        },onUpdate:function() {
        	var xval = rect._gsTransform.x;
        	//Changing the timeout interval
        	setTimeout(function() {
        		TweenLite.set("#mask-gevreche2", {x:-(xval)});
        	}, 1);
        }},"start+=0.6")
		*/




/*		App.all_tl.addLabel('end')
		App.all_tl.fromTo(App.Home.sec.find('.sub_title'),0.8,{x:-100,autoAlpha:0},{x:0,autoAlpha:1, ease:Linear.easeNone},"end-=1.2");
		App.all_tl.fromTo(App.Home.sec.find('.descr'),0.8,{x:-100,autoAlpha:0},{x:0,autoAlpha:1, ease:Linear.easeNone},"end-=1.2");
		App.all_tl.fromTo(App.Home.sec.find('.link_box'),0.8,{x:-100,autoAlpha:0},{x:0, autoAlpha:1, ease:Linear.easeNone},"end-=1.0");*/

		// App.all_tl.addLabel('three')


		/*App.all_tl.fromTo(App.Home.sec_2.find('.main_title_out'),1,{y:100,autoAlpha:0}, {y:0,autoAlpha:1, ease:Linear.easeNone}, "three+=1");	*/
/*		App.all_tl.staggerFromTo(App.Home.sec_2.find('.grid-n_one .el'),1,{y:100,autoAlpha:0}, {y:0,autoAlpha:1, ease:Linear.easeNone}, "three+=1.2");
		App.all_tl.addLabel('last')*/
/*		App.all_tl.staggerFromTo(App.Home.sec_2.find('.grid-n_two .el'),1,{y:100,autoAlpha:0}, {y:0,autoAlpha:1,  ease:Linear.easeNone}, "last+=6.2");
		App.all_tl.staggerFromTo(App.Home.sec_2.find('.grid-n_three .el'),1,{y:100,autoAlpha:0}, {y:0,autoAlpha:1,  ease:Linear.easeNone}, "last+=8.2");*/


	},
}
