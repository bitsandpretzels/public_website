var _controller = $('#video').attr('data-index'),
    _buy_section = $('#buy_section').attr('data-index'),
    x = 252;

App = {
  w : null,
  h : null,
  is_mobile : false,
  all_tl:null,
  viewport: getViewportSize().height,
  mobile_ver : $(window).width() <= 1006,
  mobile_ver_tb : $(window).width() <= 1024,
  videoFlag : false,
  vid_h_addon : 0,

  init: function() {
    "use strict";
    // Anchor smooth scroll
    $('a[href*="#"]:not([href="#"])').click(function() {
      if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 1000);
          return false;
        }
      }
    });

    //Check if page is home/index else dont't execute
    if (_controller === "home") {
      App.Home.init();
    }

    App.MainPages.init();
    App.bind();

    $(document).ready(function() {
        App.validateEmail();
        App.positionArrowDown();
        App.initUserAction();
        App.initLazyLoading();
//        App.initMediaSwiper();
//        App.initSwiper();
//        App.initCounter();
//        App.initSecondCounter();
//        App.initThirdCounter();
        App.initFirstRevolutionSlider();


      // Add iPhone check class to html root element
      // iPhone minimal ui
      if(isMobile.apple.phone) {
        $('html').addClass('is-iphone');
      }
      //App.unityForm();
    });

    $('.svg_name_section').on('load', function() {
//      App.sublineSVGText();
    });

    App.loadEventbriteLastPurchase();
  },

  initUserAction: function(){
    $(window).off('scroll.userAction').on('scroll.userAction', App.doUserAction);
    $(window).off('load.userAction').on('load.userAction', App.doUserAction);

    $(window).off('scroll.scrollAction').on('scroll.scrollAction', function(){
        $(window).off('scroll.scrollAction');

      $('body').addClass('app-scrolled');
    });
  },
  doUserAction: function(){
    $(window).off('scroll.userAction');
    $(window).off('load.userAction');

    $('body').addClass('app-ready');
  },

  initLazyLoading: function(){
    var $lazyload = $('img.lazyload'),
        $lazyLoadScroll = $('img.lazyload-scroll'),
        threshold = $(window).height() / 4;

    if($lazyload[0]) {
      App.lazyLoadingLoaded = 0;
      App.lazyLoadingTotal = $lazyload.length;
      $lazyload.unveil(threshold, App.lazyLoadingComplete);
    }

    if($lazyLoadScroll[0]) {
      $(window).off('scroll.lazyLoadScroll').on('scroll.lazyLoadScroll', function(){
        $(window).off('scroll.lazyLoadScroll');

        App.lazyLoadingScrollLoaded = 0;
        App.lazyLoadingScrollTotal = $lazyLoadScroll.length;
        $lazyLoadScroll.unveil(threshold, App.lazyLoadingScrollComplete);
      });
    }
  },
  lazyLoadingComplete: function(){
    App.lazyLoadingLoaded ++;
    if(App.lazyLoadingLoaded != App.lazyLoadingTotal) {
      return true;
    }


  },
  lazyLoadingScrollComplete: function(){
    App.lazyLoadingScrollLoaded ++;
    if(App.lazyLoadingScrollLoaded != App.lazyLoadingScrollTotal) {
      return true;
    }

      
  },

  initSwiper: function(){
    var mySwiper = new Swiper ('.swiper-container-sponsors', {
      loop: true,
      autoplayStopOnLast: false,
      autoplayDisableOnInteraction: false,
      speed: 1000,
      autoplay: 1500,
      slidesPerView: 6,
      spaceBetween: 40,
      breakpoints: {
        2000: {
          slidesPerView: 6,
          spaceBetween: 35
        },
        1500: {
          slidesPerView: 6,
          spaceBetween: 30
        },
        1024: {
          slidesPerView: 6,
          spaceBetween: 25
        },
        768: {
          slidesPerView: 5,
          spaceBetween: 20
        },
        640: {
          slidesPerView: 4,
          spaceBetween: 15
        },
        450: {
          slidesPerView: 3,
          spaceBetween: 10
        }
      }
    })
    },
    
    initMediaSwiper: function(){
    var mySwiper = new Swiper ('.swiper-container-media', {
      loop: true,
      autoplayStopOnLast: false,
      autoplayDisableOnInteraction: false,
      speed: 1000,
      autoplay: 1500,
      slidesPerView: 6,
      spaceBetween: 40,
      breakpoints: {
        2000: {
          slidesPerView: 6,
          spaceBetween: 35
        },
        1500: {
          slidesPerView: 6,
          spaceBetween: 30
        },
        1024: {
          slidesPerView: 6,
          spaceBetween: 25
        },
        768: {
          slidesPerView: 5,
          spaceBetween: 20
        },
        640: {
          slidesPerView: 4,
          spaceBetween: 15
        },
        450: {
          slidesPerView: 3,
          spaceBetween: 10
        }
      }
    })
  },
    
//    initDiashow: function(){
//    var mySwiper = new Swiper ('.swiper-container-diashow', {
//        nextButton: '.swiper-button-next',
//        prevButton: '.swiper-button-prev',
//        pagination: '.swiper-pagination',
//        paginationType: 'progress',
//        loop: true,
//        autoplayStopOnLast: false,
//        autoplayDisableOnInteraction: false,
//        speed: 1500,
//        autoplay: 1500,
//        effect: 'fade',
//        slidesPerView: 1,
//        autoHeight: true,
//        preloadImages: false,
//        lazyLoading: true,
//        lazyLoadingInPrevNext: true,
//        lazyLoadingOnTransitionStart: true,
//        
//    })
//  },
    initCounter: function() {
        $('#countdown').countdown('2016/09/23 18:00:00', function(event) {
            $(this).html(event.strftime('<td class="data" style="font-size:3.1em">%d</td> \
                                        <td class="data" style="font-size:3.1em">%H</td> \
                                        <td class="data" style="font-size:3.1em">%M</td> \
                                        <td class="data" style="font-size:3.1em">%S</td>'));
//            $(this).html(event.strftime('<br>%w weeks %d days and %H:%M:%S')); 
        }) 
    },
    initSecondCounter: function() {
        $('#second-countdown').countdown('2016/09/21 18:00:00', function(event) {
            $(this).html(event.strftime('<td class="data" style="font-size:3.1em">%d</td> \
                                        <td class="data" style="font-size:3.1em">%H</td> \
                                        <td class="data" style="font-size:3.1em">%M</td> \
                                        <td class="data" style="font-size:3.1em">%S</td>'));
        }) 
    }, 
    initThirdCounter: function() {
        $('#third-countdown').countdown('2016/09/16', function(event) {
            $(this).html(event.strftime('%d days and %H:%M:%S'));
        }) 
    },

  bind: function() {
    "use strict";
    // Underline navigation fields
    var aChildren = $('ul.sub_nav li').children(); // find the a children of the list items
    var aArray = []; // create the empty aArray
    for (var i = 0; i < aChildren.length; i++) {
      var aChild = aChildren[i];
      var ahref = $(aChild).attr('href');
      aArray.push(ahref);
    } // this for loop fills the aArray with attribute href values

    $(window).scroll(function() {
      var windowPos = $(window).scrollTop(); // get the offset of the window from the top of page
      var windowHeight = $(window).height(); // get the height of the window
      var docHeight = $(document).height();

      for (var i = 0; i < aArray.length; i++) {
        var theID = aArray[i];
        // see http://stackoverflow.com/questions/28304454/uncaught-error-syntax-error-unrecognized-expression-jquery-selector-single-vs
        try {
          var divPosid = $(theID);
          if (!divPosid.length) {
            return;
          }
          var divPos = divPosid.offset().top - 200; // get the offset of the div from the top of page
          var divHeight = $(theID).height(); // get the height of the div in question
          if (windowPos >= divPos && windowPos < (divPos + divHeight)) {
            $('a[href="' + theID + '"]').addClass('active');
          } else {
            $('a[href="' + theID + '"]').removeClass('active');
          }
        } catch (err) {}
      }
      if (windowPos + windowHeight == docHeight) {
        if (!$('ul.sub_nav li:last-child a').hasClass('active')) {
          var navActiveCurrent = $('.nav-active').attr('href');
          $('ul.sub_nav li:last-child a').addClass('active');
        }
      }
      $('.buyticket').removeClass('active');
    });

    try {
      $(window).resize(App.resize).trigger('resize');
    } catch(e) {}

    $('.wrap_nav .link_nav')
      .each(function (index){$(this).data('id',index)})
      .on('click', function(e) {
        // Hide link menu
        $('#linkMenu').hide();
        // Scroll to top only if page is scrolled already
        if(window.pageYOffset == 0) {
          App.showNav(e);
          return;
        }
        $('html, body').animate({
              scrollTop: 0
            }, 'slow',function() {
          if(_controller === "home"){
            $(window).unbind("scroll",App.bindScrollMenuToggle);
            if( $(window).width() <= 640){
              App.showNav(e);
            } else {
              App.showNav(e);
            }
          }
         });
      });


    $('.trigger_app_nav').on('click', function(e)  {
        window.location = "https://www.bitsandpretzels.com/tickets";
//        App.showNav(e);
    }); 
    $('.trigger_app_nl').on('click', function(e)  {
        App.showNav(e);
    });

    $('#closenav').on('click', App.closeNav);
    $('.linkMenu').on('click',App.toggleMenu).css('zIndex', 1002);

    var completeScrollCalled = false;
    $('#link_to_top').click(function(e) {
      "use strict";
      $("html, body").stop(true, true).animate({
        scrollTop: "0px"
      }, {
        complete: function() {
          if (completeScrollCalled) {
            completeScrollCalled = false;
            return;
          }
          completeScrollCalled = true;
          var timeout_ = 100;

          if (jQBrowser.mozilla) {
            timeout_ = 500;
          }

          // Timout hack for Firefox see https://bugs.jquery.com/ticket/14820
          setTimeout(function() {
            if (_controller === "home" && !App.is_mobile) {
              // Hide link menu
              $('#linkMenu').hide();

              $(window).unbind("scroll", App.bindScrollMenuToggle);
              if ($(window).width() <= 640) {
                $('.link_nav')[0].click();
              } else {
                //$('.link_nav')[0].click();
                App.showNav(e);
                //return;
              }
            }
          }, timeout_);
        }
      });
    });

    $('.topic').on('click',App.toggleAccordionMenu);
    $('#loadMore').on('click', App.loadMore);
    $('.close_popup').on('click', App.closePopup);
    $('#likes-popup').on('click', App.openPopup);
    $('#promotional_link').on('click', App.openPromotionalBox);
    $('.sub-nav-link').on('click', App.openSubMenuNav);
    $('.show-fullscreen-nav').on('click', App.showCatSubNav);

    if(_buy_section === 'Buy' || _buy_section === 'buy') {
      App.setEqualHeightOfElements();

      // Link menu white
//      $('.linkMenu').find('span').css('background', '#fff');

      var popup_desktop = $('.popup_desktop');

    setTimeout(function(){
        TweenMax.fromTo(popup_desktop, 2,{autoAlpha:0, y:-20},{autoAlpha:1, y:0});
      }, 6000);

      // Fadout panic box when scrolled
//      $(window).on('scroll', function(e) {
//        var scroll_top = $(window).scrollTop();
//        if(scroll_top > 200) {
//          popup_desktop.fadeOut('slow');
//          popup_desktop.addClass('inactive');
//        }
//      });

      // Fadeout panic box after 5 seconds
      setTimeout(function(){
        popup_desktop.fadeOut('slow');
        popup_desktop.addClass('inactive');
      }, 10000)
    }

    if(!($('html').hasClass('mobile'))) {
      ParallaxUtil.fromTo('.paralax_section', {top: "top+1", bottom:'bottom+1'}, {y:50, autoAlpha:0}, {y:0, autoAlpha:1}, {time:1.2, line:0.9});
    }

    KeyboardUtil.add({keys: KeyCode.ESC, down: function() {
      App.closeMenu();
      App.closeNav();
    }});

    // Table Captain filter
    $('.selection-button').on('click', function() {
      $('.selection-button').removeClass('active');
      var _this = $(this);
      _this.addClass('active');

      $('.tc_box').show().css('visibility', 'hidden');
      if(_this.data('type') === 'all') {
        $('.tc_box').show().css('visibility', 'visible');
        return;
      }

      $('.tc_box').fadeOut('slow', function() {
        $('.tc_box').each(function( index ) {
          if ($(this).data('type').indexOf(_this.data('type')) >= 0) {
            $(this).css('visibility', 'visible').fadeIn('slow');
          }
        });
      });

      $('.tc_box').each(function(index) {
        if ($(this).data('type').indexOf(_this.data('type')) < 0) {
          $(this).fadeOut('slow');
        }
      }); 
    });

    // Equal Height Exhibiton Testimonials
    App.setEqualHeightOfElements('.info_box .main_title');
  },

  openSubMenuNav: function(e) {
    //console.log('openSubMenuNav');
    if(!$(e.target).hasClass('inner')) {e.preventDefault();}
  },

  setEqualHeightOfElements: function(obj) {
    try {
      var heights = $(obj).map(function() {
        return $(this).innerHeight();
      }).get(),
      maxHeight = Math.max.apply(null, heights);
      $(obj).map(function () {
        return $(this).innerHeight(maxHeight);
      });
    } catch(e) {
      // NO ELEMENT
    }
  },

  openPromotionalBox: function(e) {
    e.preventDefault();
    var el =  $("#promotional_code");
    el.removeClass('dsp-none');
    TweenMax.fromTo(el, 0.5, {y:50, autoAlpha:0}, {y:0, autoAlpha:1});

  },

  openPopup: function() {
    console.log('openPopup');
    var popup = $('#like-popup');
    var data = $(this).find('.data').attr('data-value');
    $('#like-popup').find('.data').text(data);
    popup.removeClass('dsp-none');
    TweenMax.fromTo(popup, 0.4, {autoAlpha:0, y:-20},{autoAlpha:1, y:0});
  },

  closePopup: function() {
    console.log('closePopUp');
    var el = $(this).parent();
    TweenMax.to(el,0.6,{autoAlpha:0, onComplete: function() {
      el.addClass('dsp-none');
    }});
  },

  loadMore: function() {
      if (x > 10*252) {
            x = 252;
      }
      $.ajax({
            'type': 'GET',
            'url': 'ajax/ajax_attendees.php?x=' + x,
            'success': function(data) {
                var attendees = $.parseJSON(data);
                for(var i = 0; i < 252; i++) {
                    $('#myList').append( '<div class="user_box"> \
                                                <div class="user-offset"> \
                                                    <div class=""> \
                                                        <div class="img_user img_user-wrapper" style="width:100%;height:130px; overflow:hidden;"> \
                                                            <img style="border:0; border-radius:0" class="lazyload-scroll" src="' + attendees[i].image + '" data-src="https://www.bitsandpretzels.com/src/images/icons/placeholder.svg" alt=""> \
                                                            <noscript> \
                                                                &lt;img style="border:0; border-radius:0" src="' + attendees[i].image + '" style="width:100%; height:auto; border-radius:0" alt=""&gt; \
                                                            </noscript> \
                                                        </div> \
                                                    </div> \
                                                    <p class="name_user">' + attendees[i].fname + ' '+ attendees[i].lname + '</p> \
                                                    <p>' + attendees[i].title + '</p> \
                                                    <p>' + attendees[i].company + '</p> \
                                                </div> \
                                            </div>');
                }
            }
        })
      
        x = x + 252;
  },

  toggleAccordionMenu: function(e) {
    var container = $(this);
    if (!container.hasClass('active')) {
      container.addClass('active').find('.description_info').slideDown('fast', 'linear');
    } else {
      container.removeClass('active').find('.description_info').slideUp('fast', 'linear');
    }
  },

  resize: function() {
    var viewport = getViewportSize();
    App.w = viewport.width;
    App.h = viewport.height;

    App.mobile_ver = $(window).width() <= 1006;
    App.mobile_ver_tb = $(window).width() <= 1024;
    App.is_mobile = App.w <= 640;

    if($('.svg_item').length != 0) {
      $('.svg_item').scale({
        scale:'fill',
        image:{width: 1920,height: 1080},
        viewport:{width: App.w, height: App.h},
        alignY:0
      })
    }

    if (App.mobile_ver_tb) {
      $('html').addClass('mobile');
      $(window).unbind("scroll wheel", App.Home.handler);
      if (_controller === "home") {
        $("#video .poster_img").scale({
          image: {
            width: 1280,
            height: 855
          }
        });
      }
    } else {
      $('html').removeClass('mobile');
      if (_controller === "home") {
        App.Home.handler();
      }
    }

    if(_controller != "home") {
      $('.linkMenu').addClass('link_menu_inner');
      var default_size = {
        width:getViewportSize().width,
        height:getViewportSize().height
      }
      backgroundResize($('#video-tag'),{viewport: default_size, image:{width:2560, height:1709}});
    }
    else{
//      App.backgroundVideoCover();
      $('.linkMenu').removeClass('link_menu_inner');
    }

    if(App.w > 768) {
      var nav_detail = $('.nav_detail');
      App.mathHeight( nav_detail.find('.descr') );
      if($('.section_buy .sub_heading').length > 1) {
        App.setEqualHeightOfElements('.section_buy .sub_links_buy');
        App.setEqualHeightOfElements('.section_buy .sub_heading');
        App.setEqualHeightOfElements('.section_buy .st_heading');
      }
      App.setEqualHeightOfElements('#apply .form_h');
    }

    // Choose category text homepage
    if(_controller == "home") {
      App.positionHomeSVGDescription();

      setTimeout(function() {
        return;  // Other solution

        // this is no more active as the previous statement is "return". No idea why this was done but I will just comment the following code in case we have to reuse it. it won't show up in production.
        // var main_nav = $('#main_nav');
        // var choose_cat_tag = $('#chooseCatTag');
        // var sub_title = $('h2.sub_title');
        //
        // // Positioning
        // choose_cat_tag.css({position:'absolute', top: '5px'});
        //
        // var _h_app = (App.w/2)*1.000;
        // var _h_cct = (choose_cat_tag.width()/2)*1.000;
        // var left_pos = _h_app - _h_cct;
        //
        // if(App.is_mobile) {
        //   left_pos = left_pos - 10;
        // }
        //
        // var midpoint = (main_nav.offset().top - (sub_title.offset().top+sub_title.height())) / 2 - (choose_cat_tag.height() / 2); // for relative positioning
        // midpoint = ((main_nav.offset().top + (sub_title.offset().top+sub_title.height())) / 2) - (choose_cat_tag.height() / 2); // for absolute positionin
        //
        // choose_cat_tag.css({position:'absolute', top: (midpoint-8)+'px', left: left_pos + 'px'});
      }, 300);
    }

    // interact with the "choose category" link in the header
    $('#section_0').on('click', '.fake_link', function(e) {
      App.showNav(e);
      return;
    });

//    App.sublineSVGText();

    // Resize Exhibiton Testimonials
    App.setEqualHeightOfElements('.info_box .main_title');

    //
    App.positionArrowDown();    
  },

//  backgroundVideoCover: function() {
//
//    var min_w = 300; // minimum video width allowed
//    var vid_w_orig;  // original video dimensions
//    var vid_h_orig;
//
//    // Video Background Cover
//    vid_w_orig = parseInt($('video').attr('width'));
//    vid_h_orig = parseInt($('video').attr('height'));
//
//    // set the video viewport to the window size
//    $('#video_home').width($(window).width());
//    $('#video_home').height($(window).height()+App.vid_h_addon);
//
//    // use largest scale factor of horizontal/vertical
//    var scale_h = $(window).width() / vid_w_orig;
//    var scale_v = $(window).height()+App.vid_h_addon / vid_h_orig;
//    var scale = scale_h > scale_v ? scale_h : scale_v;
//
//    // don't allow scaled width < minimum video width
//    if (scale * vid_w_orig < min_w) {scale = min_w / vid_w_orig;};
//
//    // now scale the video
//    $('video').width(scale * vid_w_orig);
//    $('video').height(scale * vid_h_orig);
//
//
//    // and center it by scrolling the video viewport
//    $('#video_home').scrollLeft(($('video').width() - $(window).width()) / 2);
//    $('#video_home').scrollTop(($('video').height() - $(window).height()+App.vid_h_addon) / 2);
//
//    var vpWidth = $('#video_home').width();
//    var vpHeight = $('#video_home').height();
//
//    var page = $('#video').data('page');
//
//    if(vpWidth >= 1026) {
//      if(!App.videoFlag) {
//        App.handleVideo('add', page);
//        App.videoFlag = true;
//      }
//    } else {
//      App.handleVideo('remove', page);
//      App.videoFlag = false;
//    }
//  },

  // what happens here??? only vars set but no action?
  resizeSVG: function(svg, size, viewport) {
    console.log('resizeSVG');
    var master_ratio = Math.max(viewport.width / size.width, viewport.height / size.height);
    var new_width = Math.ceil(master_ratio * size.width);
    var new_height = Math.ceil(master_ratio * size.height);
  },

  // Subline text
//  sublineSVGText: function() {
//    var timeout_ = 100;
//    var el = $('.section_top .descr');
//
//    // Buggy Firefox OSX hack
//    if(jQBrowser.mozilla) {
//      timeout_ = 300;
//    }
//
//    el.css('visibility', 'hidden');
//
//    setTimeout(function() {
//      if(_controller != 'home' && App.w > 1025) {
//        var svg_section_height = $('div.content_svg_section').outerHeight();
//        var descr_height = $('.section_top .descr').height();
//        el.css({
//          position: 'relative',
//          top: -(0.16*svg_section_height - (0.5*descr_height)) + 'px'
//        });
//      } else {
//        el.css({
//          position: 'inherit',
//          top: 'auto'
//        });
//      }
//      el.css('visibility', 'visible');
//    }, timeout_);
//  },

  // still neccessary?
  positionHomeSVGDescription: function() {
    //console.log('positionHomeSVGDescription');
    try {
      var viewport = getViewportSize();
      var el = document.getElementById('white-rect'); // or other selector like querySelector()
      var rect = el.getBoundingClientRect(); // get the bounding rectangle
      var svg_info_box = $('#svgInfoBox');
      var pos_cache = $('#svgInfoBox').scrollTop();

      App.w = viewport.width;
      App.h = viewport.height;

      if(viewport.width > 1024) {
        svg_info_box.css({
          position: 'relative',
          top: ((rect.height/2) - (svg_info_box.height()/2)-35) + 'px'
        });
      } else {
        svg_info_box.css({
          position: 'relative',
          top: pos_cache + 'px'
        });
      }
    }
    catch(e) {}
  },

  loadEventbriteLastPurchase: function() {
    var el = $('#lastEventbriteBooking');
    if(!el.length) {
      return;
    }

    var request = $.ajax({
      url: '/ajax/get_last_purchase',
      method: 'get',
      dataType: 'json'
    });

    request.done(function(data) {
      el.text(data);
    });

    request.fail(function(jqXHR, textStatus) {
      // Failed :/
    });
  },

  showNav: function(e) {
    //console.log('showNav');
    var nav_detail = $('.nav_detail');
    var cols = $('.nav_detail .col');


    if(!App.is_mobile) {
      // pause video
      try{
        App.videoAction('pause');
      } catch(e) {}
    }

    TweenMax.fromTo(nav_detail.show(), .6, {y:App.h-167}, {y:0});
    if(App.is_mobile){
      // Fixed $(this) -> $(e.target)
      cols.css('display','block').eq( $(e.target).data('id') ).css('display', 'block');
    } else {
      TweenMax.staggerFromTo(cols.css('display', 'block'), .5, {y:150, autoAlpha:0}, {y:0, autoAlpha:1, ease: Back.easeOut}, .1);
      $('#linkMenu').css('zIndex', 100);
    }
    App.mathHeight(nav_detail.find('.descr'));
    return false;
  },
    
  bindScrollMenuToggle: function() {
    if(getScrollTop() > 100 && $('.nav_detail').css('display') === 'block' && !App.mobile_ver) {
      var nav_detail = $('.nav_detail');
      //App.closeNav();
      $(window).unbind('scroll', App.bindScrollMenuToggle);
      TweenMax.to(nav_detail, .6, {y:-App.h, onComplete: function() {
        nav_detail.hide();
      }});
      $('.linkMenu').show();
    }
  },

  closeNav: function () {
    $(window).unbind('scroll', App.bindScrollMenuToggle);
    var nav_detail = $('.nav_detail');
    TweenMax.to(nav_detail, .6, {y:App.h, onComplete: function() {
      nav_detail.hide();
    }});

    $('.linkMenu').show();
    $('#linkMenu').show();
    $('#linkMenu').css('zIndex', 1002);

    if(!App.is_mobile) {
      // continue video
      App.videoAction('play');
    }
  },

  toggleMenu: function(e) {
    //console.log('toggleMenu');
    $('.linkMenu').hasClass('active') ? App.closeMenu(e) : App.openMenu(e);
  },

  openMenu: function(e) {
    //console.log('openMenu');
    var nav_w =  $('#header_nav').outerWidth();
    var nav = $('#header_nav').css('zIndex',101);

    e.stopPropagation();
    $('.linkMenu').addClass('active');
    nav.show();
    if(!App.is_mobile){
      TweenMax.fromTo( nav , .6, {x:0}, {x:nav_w});
    }
    $('.left_nav').css({opacity:0});
    $('body').bind('click', App.bodyHandler);

    if(!App.is_mobile) {
      // pause video
      try{
        App.videoAction('pause');
      } catch(e) {}
    }
  },

  bodyHandler: function(e) {
    e.stopPropagation();
    if(!(e.target.nodeName == 'LI') && !$(e.target).hasClass('link') && !$(e.target).hasClass('icon-arrow_right') && $('.linkMenu').hasClass('active') && !$(e.target).hasClass('sub-nav-link') && !$(e.target).hasClass('header_nav')){
      App.closeMenu();
    }
  },

  closeMenu: function(e) {
    var nav = $('#header_nav');
    $('body').unbind('click', App.bodyHandler);
    $('.linkMenu').removeClass('active');
    if(!App.is_mobile) {
      TweenMax.to(nav , .6,{x:0,onComplete:function (){nav.hide();}});
    }
    else {
      nav.hide();
    }
    $('.left_nav').css({opacity:1});

    if(!App.is_mobile) {
      // continue video
      try {
        App.videoAction('play');
      } catch(e) {}
    }
  },

  mathHeight: function (targets) {
    var max = 0;
    var descr = targets;
    descr.each( function (){
      var self = $(this);
      max = Math.max(self.removeAttr('style').height(), max);
      //console.log('max: ', max);
    }).height(max);
  },

  videoAction: function(action) {
    // only call if not in mobile viewport
    var winWidth = $(window).width();
    if((winWidth >= 1025)) {
      var myVideo = document.getElementById('bgVideo');
      if (action === 'pause' && typeof myVideo !== 'undefined') {
        myVideo.pause();
      } else {
        myVideo.play();
      }
    }
  },

  // check if newsletter form has a valid email address.
  validateEmail: function() {
    $('#newsletterForm').validate({
      rules: {
        inputEmail: {
          required: true,
          email: true
        }
      }
    });

    $('#newsletterForm input').on('keyup blur', function() {
      if ($('#newsletterForm').valid()) {
        if(!$('#emailInput').val()) {
          $('button.button').prop('disabled', 'disabled').addClass('disabled');
          $('.text_field').addClass('error');
        } else {
          $('button.button').prop('disabled', false).removeClass('disabled');
          $('.text_field').removeClass('error');
        }
      } else {
        $('button.button').prop('disabled', 'disabled').addClass('disabled');
        $('.text_field').addClass('error');
      }
    });
  },

  handleVideo: function(option, page) {
    page = typeof page !== 'undefined' ? page : 'home';

    if(page==='home') {
      console.log()
      video_data = {
        mp4: '/src/media/video_b_and_p.mp4',
        webm: '/src/media/video_b_and_p.mp4',
        poster: '/src/images/poster.jpg'
      }
    } else if(page==='success') {
       video_data = {
        mp4: '/src/media/success.mp4',
        webm: '/src/media/success.mp4',
        poster: '/src/images/poster.jpg'
      }     
    }

    // show video
    if(option == 'add') {
      $('#video_home').videoBG({
        mp4: video_data.mp4,
        webm: video_data.webm,
        poster: video_data.poster,
        videoId:'bgVideo',
        autoplay:true,
        preload:'auto',
        loop:true,
        scale: true,
        zIndex: 0,
        opacity: 0.8,
        fullscreen: true,
        imgFallback: true
      });;
    } else {
      // how only a poster
      $('#video_home').removeAttr('style');
      $('.videoBG_wrapper').remove();
      $('.videoBG').remove();
    }
  },
    initFirstRevolutionSlider: function() {
        var tpj = jQuery;

                var revapi202;
                tpj(document).ready(function() {
                    if (tpj("#rev_slider_202_1").revolution == undefined) {
                        revslider_showDoubleJqueryError("#rev_slider_202_1");
                    } else {
                        revapi202 = tpj("#rev_slider_202_1").show().revolution({
                            sliderType: "standard",
                            jsFileLocation: "revolution/js/",
                            sliderLayout: "fullscreen",
                            dottedOverlay: "none",
                            
                            stopAfterLoops:0, 
                            stopAtSlide:6,
                            
                            delay: 7000,
                            navigation: {
                                keyboardNavigation: "off",
                                keyboard_direction: "horizontal",
                                mouseScrollNavigation: "off",
                                onHoverStop: "off",
                                touch: {
                                    touchenabled: "on",
                                    swipe_threshold: 75,
                                    swipe_min_touches: 50,
                                    swipe_direction: "horizontal",
                                    drag_block_vertical: false
                                },
                                bullets: {
                                    enable: false,
                                    hide_onmobile: true,
                                    hide_under: 600,
                                    style: "zeus",
                                    hide_onleave: false,
                                    direction: "horizontal",
                                    h_align: "center",
                                    v_align: "bottom",
                                    h_offset: 0,
                                    v_offset: 30,
                                    space: 5,
                                    tmp: '<span class="tp-bullet-image"></span><span class="tp-bullet-imageoverlay"></span><span class="tp-bullet-title">{{title}}</span>'
                                }
                            },
                            responsiveLevels: [1240, 1024, 778, 480],
                            visibilityLevels: [1240, 1024, 778, 480],
                            gridwidth: [1240, 1024, 778, 480],
                            gridheight: [868, 768, 960, 720],
                            viewPort:{
                               enable:true,
                               outof:'wait',
                               visible_area:'60%'
                            },
                            lazyType: "none",
                            shadow: 0,
                            spinner: "spinner2",
                            shuffle: "off",
                            autoHeight: "off",
                            fullScreenAutoWidth: "off",
                            fullScreenAlignForce: "off",
                            fullScreenOffsetContainer: "",
                            fullScreenOffset: "0",
                            disableProgressBar: "off",
                            hideThumbsOnMobile: "off",
                            hideSliderAtLimit: 0,
                            hideCaptionAtLimit: 0,
                            hideAllCaptionAtLilmit: 0,
                            debugMode: false,
                            fallbacks: {
                                simplifyAll: "off",
                                nextSlideOnWindowFocus: "off",
                                disableFocusListener: true,
                            }
                        });
                    }
                }); /*ready*/
        
                // No autoplay Revolution Slider
                tpj(document).ready(function() {
                    if (tpj("#rev_slider_202_2").revolution == undefined) {
                        revslider_showDoubleJqueryError("#rev_slider_202_2");
                    } else {
                        revapi202 = tpj("#rev_slider_202_2").show().revolution({
                            sliderType: "standard",
                            jsFileLocation: "revolution/js/",
                            sliderLayout: "fullscreen",
                            dottedOverlay: "none",
                            stopAfterLoops:0, 
                            stopAtSlide:1,
                             
                            delay: 7000,
                            navigation: {
                                keyboardNavigation: "off",
                                keyboard_direction: "horizontal",
                                mouseScrollNavigation: "off",
                                onHoverStop: "off",
                                touch: {
                                    touchenabled: "on",
                                    swipe_threshold: 75,
                                    swipe_min_touches: 50,
                                    swipe_direction: "horizontal",
                                    drag_block_vertical: false
                                },
                            },
                            responsiveLevels: [1240, 1024, 778, 480],
                            visibilityLevels: [1240, 1024, 778, 480],
                            gridwidth: [1240, 1024, 778, 480],
                            gridheight: [868, 768, 960, 720],
                            lazyType: "none",
                            
                            
                            /*Drückt zu sehr auf die Perfomance: */
                            shadow: 0,
                            spinner: "spinner2",
                            shuffle: "off",
                            autoHeight: "off",
                            fullScreenAutoWidth: "off",
                            fullScreenAlignForce: "off",
                            fullScreenOffsetContainer: "",
                            fullScreenOffset: "0",
                            disableProgressBar: "off",
                            hideThumbsOnMobile: "off",
                            hideSliderAtLimit: 0,
                            hideCaptionAtLimit: 0,
                            hideAllCaptionAtLilmit: 0,
                            debugMode: false,
                            fallbacks: {
                                simplifyAll: "off",
                                nextSlideOnWindowFocus: "off",
                                disableFocusListener: true,
                            }
                        });
                    }
                }); /*ready*/
    },
    initSecondRevolutionSlider: function(){
        
            var tpj = jQuery;
                var revapi202;
        
                    // Second Revolution Slider on Home-Page
                    tpj(document).ready(function() {
                    if (tpj("#rev_slider_202_3").revolution == undefined) {
                        revslider_showDoubleJqueryError("#rev_slider_202_3");
                    } else {
                        revapi202 = tpj("#rev_slider_202_3").show().revolution({
                            sliderType: "standard",
                            jsFileLocation: "revolution/js/",
                            sliderLayout: "fullscreen",
                            dottedOverlay: "none",
                            
//                            stopAfterLoops:0, 
//                            stopAtSlide:6,
                            
                            delay: 7000,
                            navigation: {
                                keyboardNavigation: "off",
                                keyboard_direction: "horizontal",
                                mouseScrollNavigation: "off",
                                onHoverStop: "off",
                                touch: {
                                    touchenabled: "on",
                                    swipe_threshold: 75,
                                    swipe_min_touches: 50,
                                    swipe_direction: "horizontal",
                                    drag_block_vertical: false
                                },
                                bullets: {
                                    enable: true,
                                    hide_onmobile: true,
                                    hide_under: 600,
                                    style: "zeus",
                                    hide_onleave: false,
                                    direction: "horizontal",
                                    h_align: "center",
                                    v_align: "bottom",
                                    h_offset: 0,
                                    v_offset: 30,
                                    space: 5,
                                    tmp: '<span class="tp-bullet-image"></span><span class="tp-bullet-imageoverlay"></span><span class="tp-bullet-title">{{title}}</span>'
                                }
                            },
                            responsiveLevels: [1240, 1024, 778, 480],
                            visibilityLevels: [1240, 1024, 778, 480],
                            gridwidth: [1240, 1024, 778, 480],
                            gridheight: [868, 768, 960, 720],
                            viewPort:{
                               enable:true,
                               outof:'wait',
                               visible_area:'10%'
                            },
                            lazyType: "none",
                            shadow: 0,
                            spinner: "spinner2",
                            shuffle: "off",
                            autoHeight: "off",
                            fullScreenAutoWidth: "off",
                            fullScreenAlignForce: "off",
                            fullScreenOffsetContainer: "",
                            fullScreenOffset: "0",
                            disableProgressBar: "off",
                            hideThumbsOnMobile: "off",
                            hideSliderAtLimit: 0,
                            hideCaptionAtLimit: 0,
                            hideAllCaptionAtLilmit: 0,
                            debugMode: false,
                            fallbacks: {
                                simplifyAll: "off",
                                nextSlideOnWindowFocus: "off",
                                disableFocusListener: true,
                            }
                        });
                    }
                }); /*ready*/
    },
    
    
    
    
    
//  preloaderVideo: function(v) {
//    //removes the video backdrop after video is loaded
//    var pWrapper = $('#preload_wrapper');
//    var vpWidth = $('#video_home').width();
//    var vpHeight = $('#video_home').height();
//    var winWidth = $(window).width();
//    pWrapper.css({'width':''+vpWidth+'', 'height':''+vpHeight+''});
//
//    function Preloader(v) {
//      window.addEventListener('load', function() {
//        var video = document.querySelector(v);
//        
//        function checkLoad() {
//
//          if(typeof video === 'undefined' || video == null || typeof video.readyState === 'undefined') {
//            return;
//          }
//
//          if (video.readyState === 4) {
//            $('#video').find('#preload_wrapper').remove();
//          } else {
//            setTimeout(checkLoad, 100);
//          }
//        }
//        checkLoad();
//      });
//    }
//
//    if((App.is_mobile) || (winWidth <= 1025)) {
//      // remove preloader on init if mobile view!
//      $('#video').find('#preload_wrapper').remove();
//    } else {
//      // fire preloader function
//      Preloader(v);
//    }
//  },

  positionArrowDown: function() {

    // Initial positioning
    var windowHeight = $(window).height();
    var windowWidth = $(window).width();
    var secionTop = $('#section_top').height();
    var arrow = $('.arrow-wrap');
    $('.arrow-wrap').css({
      top: Math.round(secionTop-(100)) + 'px',
    });

    if(windowWidth <= 640) {
      $('.arrow-wrap').css({
        top: Math.round(secionTop-(70)) + 'px',
      });     
    }

    // Recompute updates
    $(window).scroll(function() {
        
      var topWindow = $(window).scrollTop();
      var topWindow = topWindow * 1.5;
      windowHeight = $(window).height();
      var position = topWindow / windowHeight;
      position = 1 - position;
      $('.arrow-wrap').css('opacity', position);
      try {
        $('.on-video-content .arrow-bottom-wrap').css('opacity', position);
      } catch(e) {}
    });
  },

  unityForm: function() {  
  
    $(".unity_form_submit").on("click", function(e){      
      var _this = $(this);
      var form_action = _this.data('action');
      var form = _this.closest("form");
      e.preventDefault();

      //console.log(form[0].checkValidity());

	  if(form[0].checkValidity()) {
	  	form.attr('action', form_action).submit();
	  	$('.error_message').hide();
      } else {
      	$('.error_message').show();
      	return false;
      }
      //form.attr('action', form_action).submit();
    });
    
  },

  showCatSubNav: function(e) {
    var viewport = getViewportSize();
    App.w = viewport.width;
    if(App.w > 768) {
      App.showNav(e);
      $('.header_nav').hide();
      $('#linkMenu').removeClass('active');
    }
  }

    // EOF
}